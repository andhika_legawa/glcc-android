package com.goldgainer.admin

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import com.goldgainer.glcclib.data.FirebaseDataSource
import kotlinx.coroutines.launch

class AdminActivity : BaseAdminActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_dashboard,
                R.id.nav_manageprofile,
                R.id.nav_managewithdraw,
                R.id.nav_menustockist,
                R.id.nav_voucher_find,
                R.id.nav_report,
                R.id.nav_setting
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        menu = navView.menu
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_main_drawer, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        checkMenu();
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun checkMenu() {
        lifecycleScope.launch {
            currentUser?.email?.let {
                val permission = FirebaseDataSource().getAdmin(email = it)?:return@launch
                menu.findItem(R.id.nav_manageprofile).isVisible = permission.profile || permission.zuper
                menu.findItem(R.id.nav_managewithdraw).isVisible = permission.withdraw || permission.zuper
                menu.findItem(R.id.nav_menustockist).isVisible = permission.stockist || permission.zuper
                menu.findItem(R.id.nav_voucher_find).isVisible = permission.voucher || permission.zuper
                menu.findItem(R.id.nav_rpc_manage).isVisible = permission.rewardpair || permission.zuper
                menu.findItem(R.id.nav_report).isVisible = permission.zuper
                menu.findItem(R.id.nav_setting).isVisible = permission.zuper
                menu.findItem(R.id.nav_admin_log).isVisible = permission.zuper
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }
}