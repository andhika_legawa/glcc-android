package com.goldgainer.admin

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.launch

open class BaseAdminActivity : AppCompatActivity()  {

    private lateinit var firebaseAuth : FirebaseAuth
    protected var currentUser : FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        currentUser = firebaseAuth.currentUser
    }

    override fun onResume() {
        super.onResume()
        checkLogin()
        checkVersion()
    }

    private fun checkLogin(){
        if (currentUser == null || !currentUser!!.isEmailVerified) {
            doLogin()
        }
        checkAdmin()
    }

    private fun checkVersion(){
        lifecycleScope.launch {
            val helper = Helper()
            val latestVersion = helper.getGlobalSettings("admin_version", 1) as Int
            if (BuildConfig.VERSION_CODE < latestVersion) {
                doLogin()
            }
        }
    }

    private fun checkAdmin(){
        lifecycleScope.launch {
            currentUser?.email?.let {
                val permission = FirebaseDataSource().getAdmin(email = it)
                if (permission == null) {
                    doLogin()
                }
            }
        }
    }

    private fun doLogin(){
        startActivity(Intent(this, AdminLoginActivity::class.java))
        finish()
    }

}