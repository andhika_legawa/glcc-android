package com.goldgainer.admin.ui.profile

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ManageProfileFragment : Fragment() {

    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var profileList: RecyclerView
    private lateinit var profileAdapter : ProfilePagingAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_profilemanage, container, false)
        profileAdapter = ProfilePagingAdapter() { profile ->
            val params = bundleOf("owner" to profile.owner)
            findNavController().navigate(R.id.action_nav_manageprofile_to_nav_profiledetail, params)
        }
        val pullToRefresh = root.findViewById<SwipeRefreshLayout>(R.id.swiperefresh)
        val searchFab = root.findViewById<FloatingActionButton>(R.id.fab)

        searchFab.setOnClickListener {
            val email = EditText(requireContext())
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Find")
                    .setMessage("Find profile by email.")
                    .setView(FrameLayout(requireContext()).apply {
                        addView(email)
                        setPadding(32, 0, 32, 0)
                    })
                    .setPositiveButton(android.R.string.search_go) { _: DialogInterface, _: Int ->
                        profileViewModel.currentOwner = email.text.toString()
                        profileViewModel.getProfile()
                    }
                    .show()
        }

        profileList = root.findViewById(R.id.profilelist)
        val verticalLayoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        val divider = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.list_divider)!!)
        profileList.addItemDecoration(divider)
        profileList.layoutManager = verticalLayoutManager
        profileList.adapter = profileAdapter

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            profileViewModel.flow.collect {
                profileAdapter.submitData(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            profileAdapter.loadStateFlow.collectLatest { loadStates ->
                pullToRefresh.isRefreshing = loadStates.refresh is LoadState.Loading
            }
        }

        profileViewModel.currentProfile.observe(viewLifecycleOwner, Observer {
            val profile = it?:return@Observer

            profileViewModel.resetProfile()
            val params = bundleOf("owner" to profile.owner.toString())
            findNavController().navigate(R.id.action_nav_manageprofile_to_nav_profiledetail, params)
        })

        profileViewModel.profileError.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })

        pullToRefresh.setOnRefreshListener {
            profileAdapter.refresh()
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        profileAdapter.refresh()
    }
}