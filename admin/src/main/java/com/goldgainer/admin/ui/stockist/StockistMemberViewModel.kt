package com.goldgainer.admin.ui.stockist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.admin.Helper
import com.goldgainer.admin.R
import com.goldgainer.admin.toDigitalCurrency
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.Stockist
import com.goldgainer.glcclib.data.model.StockistTransaction
import com.goldgainer.glcclib.data.model.UserData
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class StockistMemberViewModel : ViewModel() {

    private val _localAdmin = Helper.getLocalAdminUser()

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _database = FirebaseDataSource()

    private val _stockistList = MutableLiveData<List<Stockist>>()
    val stockistList: LiveData<List<Stockist>> = _stockistList

    private val _totalVoucher = MutableLiveData<TotalVoucher>()
    val totalVoucher: LiveData<TotalVoucher> = _totalVoucher

    private val _currentStockist = MutableLiveData<StockistInfo?>()
    val currentStockist: LiveData<StockistInfo?> = _currentStockist

    private val _filteredTransactions = MutableLiveData<List<StockistTransaction>>()
    val filteredTransactions: LiveData<List<StockistTransaction>> = _filteredTransactions

    private var _transactions : List<StockistTransaction> = listOf()

    var transactionType : Int = 0

    fun reset() {
        _error.value = null
    }

    fun loadStockistList(email: String?) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val stockistList = _database.getStockists(email = email)
                _stockistList.value = stockistList
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadStockistInfo(email: String) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val stockistData = _database.getStockist(email)?:throw Exception("No stockist data")
                val userData = _database.getUserDetail(email)?:throw Exception("No user data")
                _currentStockist.value = StockistInfo(user = userData, stockist = stockistData)
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadStockistTransaction(email: String, start: Date, end: Date) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                if (_transactions.isEmpty()) {
                    val trxList = _database.getStockistTransaction(email, null, null)
                    _transactions = trxList

                    val topup = _transactions.filter { it.type == 0 && it.status == "APPROVED" }.sumBy { it.amount }
                    val sold = _transactions.filter { it.type == 1 }.sumBy { it.amount }
                    _totalVoucher.value = TotalVoucher(
                        topup = topup - sold,
                        sold = sold
                    )
                }
                val filteredByType = _transactions.filter { it.type == transactionType }
                    .filter {
                        val date = it.date
                        if(date != null) {
                            date in start..end
                        } else {
                            false
                        }
                    }
                if (transactionType == 0) {
                    _filteredTransactions.value = filteredByType.filter { it.status == "APPROVED" }
                } else {
                    _filteredTransactions.value = filteredByType
                }
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }
}

data class StockistInfo (
    var user: UserData,
    var stockist: Stockist)

data class TotalVoucher (
    val topup: Int,
    val sold : Int
        )

class StockistAdapter(theContext: Context, stockists: List<Stockist>)
    : ArrayAdapter<Stockist>(theContext,0, stockists) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val stockist = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_stockist_item, parent, false)
        val emailTxt = view.findViewById<TextView>(R.id.email_textview)
        val joinTxt = view.findViewById<TextView>(R.id.join_textview)

        val sdf = SimpleDateFormat("d MMM yyyy")
        var joinDate:Date? = stockist.joinedDate
        if (joinDate == null) {
            joinDate = stockist.registerDate
        }
        emailTxt.text = stockist.owner
        joinTxt.text = sdf.format(joinDate)

        return view
    }
}

class StockistTrxAdapter(theContext: Context, transactions: List<StockistTransaction>)
    : ArrayAdapter<StockistTransaction>(theContext,0, transactions) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val transaction = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_sell_item, parent, false)
        val dateTxt = view.findViewById<TextView>(R.id.date_textview)
        val emailTxt = view.findViewById<TextView>(R.id.email_textview)
        val sellTxt = view.findViewById<TextView>(R.id.sell_textview)

        val sdf = SimpleDateFormat("d MMM")
        dateTxt.text = sdf.format(transaction.date)
        if (transaction.type == 0) {
            emailTxt.text =  "${transaction.price.toDigitalCurrency(transaction.currency)}"
            sellTxt.text = "${transaction.amount} - ${transaction.nodeType} Node"
        } else {
            emailTxt.text = transaction.soldTo
            sellTxt.text = "${transaction.amount} - ${transaction.nodeType} Node"
        }



        return view
    }
}