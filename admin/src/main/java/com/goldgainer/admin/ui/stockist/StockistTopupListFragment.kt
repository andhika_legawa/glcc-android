package com.goldgainer.admin.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class StockistTopupListFragment : Fragment() {

    private lateinit var viewModel: StockistTransactionViewModel

    private lateinit var transactionAdapter: StockistTransactionAdapter

    private var tab = "PENDING"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(StockistTransactionViewModel::class.java)
        viewModel.reset()

        val root = inflater.inflate(R.layout.fragment_stockist_topup_list, container, false)

        val pendingBtn = root.findViewById<MaterialButton>(R.id.pending_button)
        val approvedBtn = root.findViewById<MaterialButton>(R.id.approved_button)
        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)

        val emailEdit = root.findViewById<EditText>(R.id.email_edit)
        val searchBtn = root.findViewById<Button>(R.id.search_btn)
        val topupList = root.findViewById<ListView>(R.id.topup_list)

        transactionAdapter = StockistTransactionAdapter(requireContext(), arrayListOf())
        topupList.adapter = transactionAdapter
        topupList.emptyView = root.findViewById(R.id.empty_text)

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        viewModel.filteredTransactions.observe(viewLifecycleOwner, Observer { transactions ->
            transactionAdapter.clear()
            if (transactions.isNotEmpty()) {
                transactions.forEach {
                    transactionAdapter.add(it)
                }
            }
            transactionAdapter.notifyDataSetChanged()
        })

        topupList.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapterView: AdapterView<*>?, view: View?, position: Int, longpos: Long) {
                val transaction = viewModel.filteredTransactions.value?:return
                val selectedTransaction = transaction[position]
                val params = bundleOf("transactionId" to selectedTransaction.id)
                findNavController().navigate(R.id.action_nav_topuplist_to_nav_topupdetail, params)
            }
        }

        searchBtn.setOnClickListener {
            var email:String? = null
            if (emailEdit.text.isNotEmpty()) {
                email = emailEdit.text.toString()
            }
            if (tab == "PENDING") {
                viewModel.loadPendingTransaction(email)
            } else {
                viewModel.loadApprovedTransaction(email)
            }
        }

        pendingBtn.setOnClickListener {
            pendingBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            pendingBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_gray))
            approvedBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_white))
            approvedBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
            tab = "PENDING"

            var email:String? = null
            if (emailEdit.text.isNotEmpty()) {
                email = emailEdit.text.toString()
            }
            viewModel.loadPendingTransaction(email)
        }

        approvedBtn.setOnClickListener {
            approvedBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            approvedBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_gray))
            pendingBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_white))
            pendingBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
            tab = "APPROVED"

            var email:String? = null
            if (emailEdit.text.isNotEmpty()) {
                email = emailEdit.text.toString()
            }
            viewModel.loadApprovedTransaction(email)
        }

        var email:String? = null
        if (emailEdit.text.isNotEmpty()) {
            email = emailEdit.text.toString()
        }
        if (tab == "PENDING") {
            viewModel.loadPendingTransaction(email)
        } else {
            viewModel.loadApprovedTransaction(email)
        }

        return root
    }
}