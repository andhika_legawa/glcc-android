package com.goldgainer.admin.ui.rewardpair

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.goldgainer.admin.R
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class RPCHistoryFragment : Fragment() {

    private lateinit var viewModel: RPCViewModel
    private lateinit var redeemList: RecyclerView
    private lateinit var redeemAdapter: RPCPagingAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(RPCViewModel::class.java)
        viewModel.reset()
        val root = inflater.inflate(R.layout.fragment_rpchistory, container, false)

        val requestBtn = root.findViewById<Button>(R.id.request_button)
        requestBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        redeemAdapter = RPCPagingAdapter() { redeem ->
            val bundle = bundleOf("id" to redeem.id)
            findNavController().navigate(R.id.action_global_nav_rpc_detail, bundle)
        }

        redeemList = root.findViewById(R.id.request_list)
        val verticalLayoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        redeemList.layoutManager = verticalLayoutManager
        redeemList.adapter = redeemAdapter

        val pullToRefresh = root.findViewById<SwipeRefreshLayout>(R.id.swiperefresh)
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewModel.searchFlow.collect {
                redeemAdapter.submitData(it)
            }
        }

        val emptyView = root.findViewById<TextView>(R.id.empty_text)
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            redeemAdapter.loadStateFlow.collectLatest { loadStates ->
                pullToRefresh.isRefreshing = loadStates.refresh is LoadState.Loading
                if(loadStates.refresh is LoadState.NotLoading && redeemAdapter.itemCount == 0){
                    emptyView.visibility = View.VISIBLE
                } else {
                    emptyView.visibility = View.GONE
                }
            }
        }

        pullToRefresh.setOnRefreshListener {
            redeemAdapter.refresh()
        }

        val notReceivedRadio = root.findViewById<RadioButton>(R.id.not_received_radio)
        val receivedRadio = root.findViewById<RadioButton>(R.id.received_radio)

        notReceivedRadio.setOnClickListener {
            viewModel.searchStatus = "APPROVED"
        }

        receivedRadio.setOnClickListener {
            viewModel.searchStatus = "RECEIVED"
        }

        val emailText = root.findViewById<EditText>(R.id.email_edit)
        val searchBtn = root.findViewById<Button>(R.id.search_btn)
        searchBtn.setOnClickListener {
            val searchEmail = emailText.text.toString()
            if (searchEmail.isNullOrBlank()){
                viewModel.searchEmail = null
            } else {
                viewModel.searchEmail = searchEmail
            }
            redeemAdapter.refresh()
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        redeemAdapter.refresh()
    }
}