package com.goldgainer.admin.ui.stockist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.admin.Helper
import com.goldgainer.admin.R
import com.goldgainer.admin.toDigitalCurrency
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.NetworkCategory
import com.goldgainer.glcclib.data.model.StockistTransaction
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.text.SimpleDateFormat
import java.util.*

class StockistTransactionViewModel : ViewModel() {

    private val _localAdmin = Helper.getLocalAdminUser()

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _networkCategorySet = MutableLiveData<List<NetworkCategory>>()
    val networkCategories: LiveData<List<NetworkCategory>> = _networkCategorySet

    private val _filteredTransactions = MutableLiveData<List<StockistTransaction>>()
    val filteredTransactions: LiveData<List<StockistTransaction>> = _filteredTransactions

    private val _currentTransaction = MutableLiveData<StockistTransaction?>()
    val currentTransaction: LiveData<StockistTransaction?> = _currentTransaction

    var selectedNetworkIndex : Int = 0

    private val _database = FirebaseDataSource()

    fun reset() {
        _error.value = null
        _currentTransaction.value = null
    }

    fun getActiveCategories() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val iCategories = _database.getNetworkCategories("I")
                val gCategories = _database.getNetworkCategories("G")
                var activeCategories = iCategories.filter { it.active && !it.locked } + gCategories.filter {  it.active && !it.locked }
                _networkCategorySet.value = activeCategories
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }


    fun loadPendingTransaction(email: String?) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val trxList = _database.getStockistTopupPaged(500, email, "PENDING")
                _filteredTransactions.value = trxList
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadApprovedTransaction(email: String?) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val trxList = _database.getStockistTopupPaged(500, email, "APPROVED")
                _filteredTransactions.value = trxList
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadSoldTransaction(start: Date, end: Date, network: NetworkCategory, email: String?) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val trxList = _database.getStockistSold(start, end, network.type, network.number, email)
                _filteredTransactions.value = trxList
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadTransaction(trxId: String) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val trx = _database.getStockistTransaction(trxId)
                _currentTransaction.value = trx
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    suspend fun processTransaction(approved: Boolean, reason: String?) : Boolean {
        _isBusy.value = true
        try {
            var trx = _currentTransaction.value?:throw Exception("No Stockist topup selected")
            val previousStatus = trx.status
            if (previousStatus != "PENDING") { throw Exception("Transaction already processed") }
            if (approved) {
                trx.status = "APPROVED"
            } else {
                trx.status = "REJECTED"
                trx.reason = reason
            }
            trx.processedDate = Date()
            _database.saveAdminLog(_localAdmin.email, "STOCKIST", "${trx.status} Topup Transaction", trx.owner)
            _database.saveStockistTransaction(trx)
            if (approved) {
                val functions = Firebase.functions(regionOrCustomDomain = "asia-southeast2")
                functions.getHttpsCallable("generateVoucher").call(hashMapOf("transactionId" to trx.id)).await().data as String
            }
            _isBusy.value = false
            return true
        } catch (e: Exception) {
            e.message.also { _error.value = it }
            _isBusy.value = false
            return false
        }
    }
}

class StockistTransactionAdapter(theContext: Context, transactions: List<StockistTransaction>)
    : ArrayAdapter<StockistTransaction>(theContext,0, transactions) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val transaction = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_topup_item, parent, false)
        val emailTxt = view.findViewById<TextView>(R.id.email_text)
        val packageTxt = view.findViewById<TextView>(R.id.package_text)
        val nodeTypeTxt = view.findViewById<TextView>(R.id.nodetype_text)

        emailTxt.text = transaction.owner
        packageTxt.text = "${transaction.amount} - ${transaction.price.toDigitalCurrency(transaction.currency)}"
        nodeTypeTxt.text = "${transaction.nodeType} Node"

        return view
    }
}

class StockistSoldAdapter(theContext: Context, transactions: List<StockistTransaction>)
    : ArrayAdapter<StockistTransaction>(theContext,0, transactions) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val transaction = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_sell_item, parent, false)
        val dateTxt = view.findViewById<TextView>(R.id.date_textview)
        val emailTxt = view.findViewById<TextView>(R.id.email_textview)
        val sellTxt = view.findViewById<TextView>(R.id.sell_textview)

        val sdf = SimpleDateFormat("d MMM")
        dateTxt.text = sdf.format(transaction.date)
        emailTxt.text = transaction.owner
        sellTxt.text = "${transaction.amount} - ${transaction.nodeType} Node"

        return view
    }
}