package com.goldgainer.admin.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R

class StockistMenuFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_stockist_menu, container, false)

        val topupBtn = root.findViewById<Button>(R.id.topup_button)
        val trxBtn = root.findViewById<Button>(R.id.trx_button)
        val memberBtn = root.findViewById<Button>(R.id.member_button)

        topupBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_menustockist_to_nav_topuplist)
        }

        trxBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_menustockist_to_nav_vouchersell)
        }

        memberBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_menustockist_to_nav_stockistlist)
        }

        return root
    }
}