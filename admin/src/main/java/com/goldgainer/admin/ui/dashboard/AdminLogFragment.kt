package com.goldgainer.admin.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.text.SimpleDateFormat
import java.util.*

class AdminLogFragment : Fragment() {

    private lateinit var viewModel : AdminLogViewModel

    private lateinit var logAdapter : AdminLogAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(AdminLogViewModel::class.java)
        viewModel.reset()

        val root = inflater.inflate(R.layout.fragment_log, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val emailEdit = root.findViewById<EditText>(R.id.admin_edit)
        val yearSpinner = root.findViewById<Spinner>(R.id.year_spinner)
        val monthSpinner = root.findViewById<Spinner>(R.id.month_spinner)
        val logList = root.findViewById<ListView>(R.id.log_list)
        val searchBtn = root.findViewById<Button>(R.id.log_search)

        logAdapter = AdminLogAdapter(requireContext(), arrayListOf())
        logList.adapter = logAdapter
        logList.emptyView = root.findViewById(R.id.empty_text)

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        viewModel.filteredLogs.observe(viewLifecycleOwner, Observer { logs ->
            logAdapter.clear()
            if (logs.isNotEmpty()) {
                logs.forEach {
                    logAdapter.add(it)
                }
            }
            logAdapter.notifyDataSetChanged()
        })

        val yearAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        yearAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        val start = 2021
        var currentYear = Calendar.getInstance().get(Calendar.YEAR)
        while ( currentYear >= start ) {
            yearAdapter.add(currentYear.toString())
            currentYear -= 1
        }
        yearSpinner.adapter = yearAdapter

        val monthAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        monthAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        var currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        var january = 0
        val sdf = SimpleDateFormat("MMM")
        while ( january <= 11) {
            val cal = Calendar.getInstance()
            cal.set(currentYear, january, 1)
            monthAdapter.add(sdf.format(cal.time))
            january += 1
        }
        monthSpinner.adapter = monthAdapter
        monthSpinner.setSelection(currentMonth)

        searchBtn.setOnClickListener {
            val yearStr = yearSpinner.selectedItem as String
            val year = yearStr.toInt()
            val month = monthSpinner.selectedItemPosition
            val cal = Calendar.getInstance()
            cal.set(year, month, 1, 0, 0 ,0)
            val start = cal.time
            cal.set(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59)
            val end = cal.time
            var email:String? = null
            if (emailEdit.text.isNotEmpty()) {
                email = emailEdit.text.toString()
            }
            viewModel.loadActivityLog(start, end, email)
        }

        searchBtn.performClick()

        return root
    }
}