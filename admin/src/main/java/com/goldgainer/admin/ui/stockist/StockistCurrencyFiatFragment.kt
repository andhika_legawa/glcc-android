package com.goldgainer.admin.ui.stockist

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch

class StockistCurrencyFiatFragment : Fragment() {

    private lateinit var viewModel: StockistSettingViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StockistSettingViewModel::class.java)
        viewModel.reset()

        val root = inflater.inflate(R.layout.fragment_stockist_currency_fiat, container, false)

        val digitalBtn = root.findViewById<MaterialButton>(R.id.digital_button)
        val accountEdit = root.findViewById<EditText>(R.id.bank_account_edit)
        val numberEdit = root.findViewById<EditText>(R.id.bank_number_edit)
        val nameEdit = root.findViewById<EditText>(R.id.bank_name_edit)

        val updateBtn = root.findViewById<Button>(R.id.update_button)

        viewModel.fiatCurrency.observe(viewLifecycleOwner, Observer {
            accountEdit.setText(it.account)
            numberEdit.setText(it.number)
            nameEdit.setText(it.name)
        })

        digitalBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_setting_fiat_to_nav_setting_digital)
        }

        updateBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                .setTitle("Update Fiat Currency")
                .setMessage("Update fiat account details?")
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    viewLifecycleOwner.lifecycleScope.launch {
                        val success = viewModel.saveFiatCurrency(
                            accountEdit.text.toString(),
                            numberEdit.text.toString(),
                            nameEdit.text.toString())
                        if (success) {
                            Toast.makeText(context, "Account Updated", Toast.LENGTH_LONG).show()
                            viewModel.loadSettings()
                        }
                    }
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        viewModel.loadSettings()

        return root
    }
}