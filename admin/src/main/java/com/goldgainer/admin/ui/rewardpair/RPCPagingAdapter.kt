package com.goldgainer.admin.ui.rewardpair

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.admin.R
import com.goldgainer.glcclib.data.model.RewardPairRedeem

class RPCPagingAdapter(private val listener:(RewardPairRedeem) -> Unit): PagingDataAdapter<RewardPairRedeem, RPCPagingAdapter.RedeemViewHolder>(Companion) {

    companion object : DiffUtil.ItemCallback<RewardPairRedeem>() {
        override fun areItemsTheSame(oldItem: RewardPairRedeem, newItem: RewardPairRedeem): Boolean {
            return oldItem.email == newItem.email
        }
        override fun areContentsTheSame(oldItem: RewardPairRedeem, newItem: RewardPairRedeem): Boolean {
            return oldItem == newItem
        }
    }

    class RedeemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onBindViewHolder(holder: RedeemViewHolder, position: Int) {
        val redeem = getItem(position) as RewardPairRedeem
        val name = holder.itemView.findViewById<TextView>(R.id.name_text)
        val viewBtn = holder.itemView.findViewById<Button>(R.id.view_btn)
        name.text = redeem.name + " - " + redeem.pairString
        viewBtn.setOnClickListener { listener(redeem) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RedeemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.partials_rpc_redeem_item, parent, false) as View
        return RedeemViewHolder(view)
    }
}