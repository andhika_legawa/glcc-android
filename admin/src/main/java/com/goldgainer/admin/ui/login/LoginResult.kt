package com.goldgainer.admin.ui.login

import com.goldgainer.glcclib.data.model.Admin

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
        val success: Admin? = null,
        val error: Int? = null,
        val errorMessage: String? = null
)