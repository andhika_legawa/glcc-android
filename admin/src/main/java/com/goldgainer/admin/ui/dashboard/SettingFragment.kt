package com.goldgainer.admin.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R

class SettingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)

        val withdrawBtn = root.findViewById<Button>(R.id.withdraw_button)
        val stockistBtn = root.findViewById<Button>(R.id.stockist_button)

        withdrawBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_setting_to_nav_setting_withdraw)
        }

        stockistBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_setting_to_nav_setting_stockist)
        }

        return root
    }
}