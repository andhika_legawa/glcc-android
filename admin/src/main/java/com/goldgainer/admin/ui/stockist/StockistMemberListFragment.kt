package com.goldgainer.admin.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class StockistMemberListFragment : Fragment() {

    private lateinit var viewModel : StockistMemberViewModel

    private lateinit var stockistAdapter : StockistAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(StockistMemberViewModel::class.java)
        viewModel.reset()

        val root = inflater.inflate(R.layout.fragment_stockist_member_list, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val totalText = root.findViewById<TextView>(R.id.total_text)
        val emailEdit = root.findViewById<EditText>(R.id.stockist_email_edit)
        val searchBtn = root.findViewById<Button>(R.id.stockist_search_btn)

        val stockistList = root.findViewById<ListView>(R.id.stockist_list)

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        stockistAdapter = StockistAdapter(requireContext(), arrayListOf())
        stockistList.adapter = stockistAdapter
        stockistList.emptyView = root.findViewById(R.id.empty_text)

        stockistList.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapterView: AdapterView<*>?, view: View?, position: Int, longpos: Long) {
                val stockists = viewModel.stockistList.value?:return
                val stockist = stockists[position]
                val params = bundleOf("email" to stockist.owner)
                findNavController().navigate(R.id.action_nav_stockistlist_to_nav_stockistdetail, params)
            }
        }

        viewModel.stockistList.observe(viewLifecycleOwner, Observer { stockists ->
            totalText.text = "TOTAL STOCKIST : 0"
            stockistAdapter.clear()
            if (stockists.isNotEmpty()) {
                stockists.forEach {
                    stockistAdapter.add(it)
                }
                totalText.text= "TOTAL STOCKIST : ${stockists.size}"
            }
            stockistAdapter.notifyDataSetChanged()
        })

        searchBtn.setOnClickListener {
            var email:String? = null
            if (emailEdit.text.isNotEmpty()) {
                email = emailEdit.text.toString()
            }
            viewModel.loadStockistList(email)
        }

        var email:String? = null
        if (emailEdit.text.isNotEmpty()) {
            email = emailEdit.text.toString()
        }
        viewModel.loadStockistList(email)

        return root
    }
}