package com.goldgainer.admin.ui.stockist

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import com.goldgainer.admin.toDigitalCurrency
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch
import java.io.File
import java.text.SimpleDateFormat

class StockistTopupDetailFragment : Fragment() {

    private lateinit var viewModel: StockistTransactionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(StockistTransactionViewModel::class.java)
        viewModel.reset()

        val root = inflater.inflate(R.layout.fragment_stockist_topup_detail, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val nodeText = root.findViewById<TextView>(R.id.topup_node_type_text)
        val packageText = root.findViewById<TextView>(R.id.topup_node_package_text)

        val ownerText = root.findViewById<TextView>(R.id.owner_text)
        val subText = root.findViewById<TextView>(R.id.topup_subtotal_text)
        val taxText = root.findViewById<TextView>(R.id.topup_tax_text)
        val totalText = root.findViewById<TextView>(R.id.topup_total_text)

        val paymentBtn = root.findViewById<Button>(R.id.topup_view_payment_btn)
        val paymentView = root.findViewById<ImageView>(R.id.payment_view)

        val topupStatus = root.findViewById<LinearLayout>(R.id.topup_status)
        val paymentStatusText = root.findViewById<TextView>(R.id.topup_payment_status_text)
        val paymentReasonText = root.findViewById<TextView>(R.id.topup_payment_reason_text)

        val topupApproval = root.findViewById<ConstraintLayout>(R.id.topup_approval_panel)
        val notApprovedRadio = root.findViewById<RadioButton>(R.id.topup_notapproved_radio)
        val approvedRadio = root.findViewById<RadioButton>(R.id.topup_approved_radio)
        val reasonEdit = root.findViewById<EditText>(R.id.topup_reason_edit)

        val backBtn = root.findViewById<Button>(R.id.button_back)
        val proceedBtn = root.findViewById<Button>(R.id.button_proceed)

        arguments?.let {
            val packageId = it.getString("transactionId", "")
            viewModel.loadTransaction(packageId)
        }

        backBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        topupStatus.visibility = View.GONE
        topupApproval.visibility = View.GONE

        val outputDirectory = getOutputDirectory()

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        reasonEdit.visibility = View.INVISIBLE

        notApprovedRadio.setOnClickListener {
            notApprovedRadio.isChecked = true
            approvedRadio.isChecked = false
            reasonEdit.visibility = View.VISIBLE
        }

        approvedRadio.setOnClickListener {
            notApprovedRadio.isChecked = false
            approvedRadio.isChecked = true
            reasonEdit.visibility = View.INVISIBLE
        }

        viewModel.currentTransaction.observe(viewLifecycleOwner, Observer {
            val transaction = it?:return@Observer
            ownerText.text = transaction.owner
            nodeText.text = "${transaction.nodeType} Node"
            packageText.text = "${transaction.price.toDigitalCurrency(transaction.currency)} : Qty = ${transaction.amount} Vouchers"
            subText.text = "${transaction.price.toDigitalCurrency(transaction.currency)}"
            val tax = transaction.total - transaction.price
            taxText.text = "${tax.toDigitalCurrency(transaction.currency)}"
            totalText.text = "${transaction.total.toDigitalCurrency(transaction.currency)}"

            val storageRef = Firebase.storage("gs://glcc-app-users/").reference
            transaction.paymentPhoto?.let { paymentPhoto ->
                paymentBtn.setOnClickListener {
                    val photoFile = File(outputDirectory, "selfie.jpg")
                    storageRef.child(paymentPhoto).getFile(photoFile)
                    .addOnSuccessListener {
                        Picasso.get().load(photoFile).memoryPolicy(MemoryPolicy.NO_CACHE).resize(
                            WIDTH,
                            HEIGHT
                        ).centerCrop().into(paymentView)
                    }
                    .addOnFailureListener{ e ->
                        Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                    }
                }
            }

            if (transaction.status == "PENDING") {
                topupStatus.visibility = View.GONE
                topupApproval.visibility = View.VISIBLE
            } else {
                topupStatus.visibility = View.VISIBLE
                topupApproval.visibility = View.GONE

                val sdf = SimpleDateFormat("d MMM yyyy - HH:mm:ss")
                paymentStatusText.text = "${transaction.status} ON ${sdf.format(transaction.processedDate)}"
                paymentReasonText.text = transaction.reason?:""
            }
        })

        proceedBtn.setOnClickListener {
            if (!notApprovedRadio.isChecked && !approvedRadio.isChecked) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Error")
                    .setMessage("Must select decision")
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
                return@setOnClickListener
            }
            if (notApprovedRadio.isChecked && reasonEdit.text.isEmpty()) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Error")
                    .setMessage("Reason cannot be empty")
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
                return@setOnClickListener
            }

            viewLifecycleOwner.lifecycleScope.launch {
                val success = viewModel.processTransaction(approvedRadio.isChecked, reasonEdit.text.toString())
                if (success) {
                    MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                        .setTitle("Info")
                        .setMessage("Transaction processed")
                        .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                            findNavController().popBackStack()
                        }
                        .setCancelable(false)
                        .show()
                }
            }

        }

        return root
    }

    private fun getOutputDirectory(): File {
        val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name)).apply { mkdirs() } }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else requireActivity().filesDir
    }

    companion object {
        private const val WIDTH = 320
        private const val HEIGHT = 200
    }
}