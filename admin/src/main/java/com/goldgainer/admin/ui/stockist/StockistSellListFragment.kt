package com.goldgainer.admin.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import com.goldgainer.admin.R
import com.goldgainer.glcclib.data.model.NetworkCategory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.util.*

class StockistSellListFragment : Fragment() {

    private lateinit var viewModel: StockistTransactionViewModel

    private lateinit var networkAdapter : ArrayAdapter<String>

    private lateinit var transactionAdapter : StockistSoldAdapter

    private lateinit var selectedNetwork : NetworkCategory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(StockistTransactionViewModel::class.java)
        viewModel.reset()

        val root = inflater.inflate(R.layout.fragment_stockist_sell_list, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val networkSpinner = root.findViewById<Spinner>(R.id.network_spinner)
        val emailEdit = root.findViewById<EditText>(R.id.stockist_email_edit)
        val yearSpinner = root.findViewById<Spinner>(R.id.stockist_trx_year)
        val monthSpinner = root.findViewById<Spinner>(R.id.stockist_trx_month)

        val searchBtn = root.findViewById<Button>(R.id.stockist_sell_search)

        val sellList = root.findViewById<ListView>(R.id.sell_list)

        networkAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        networkAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        networkSpinner.adapter = networkAdapter

        transactionAdapter = StockistSoldAdapter(requireContext(), arrayListOf())
        sellList.adapter = transactionAdapter
        sellList.emptyView = root.findViewById(R.id.empty_text)

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        val yearAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        yearAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        val start = 2021
        var currentYear = Calendar.getInstance().get(Calendar.YEAR)
        while ( currentYear >= start ) {
            yearAdapter.add(currentYear.toString())
            currentYear -= 1
        }
        yearSpinner.adapter = yearAdapter

        val monthAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        monthAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        var currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        var january = 0
        val sdf = SimpleDateFormat("MMM")
        while ( january <= 11) {
            val cal = Calendar.getInstance()
            cal.set(currentYear, january, 1)
            monthAdapter.add(sdf.format(cal.time))
            january += 1
        }
        monthSpinner.adapter = monthAdapter
        monthSpinner.setSelection(currentMonth)

        viewModel.networkCategories.observe(viewLifecycleOwner, Observer { categories ->
            if (categories.isNotEmpty()) {
                networkAdapter.clear()
                categories.map { it.description }.forEach { category ->
                    networkAdapter.add(category)
                }
                networkAdapter.notifyDataSetChanged()
                networkSpinner.setSelection(viewModel.selectedNetworkIndex)
            }
        })

        networkSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                val categories = viewModel.networkCategories.value?:return
                selectedNetwork = categories[position]

                val yearStr = yearSpinner.selectedItem as String
                val year = yearStr.toInt()
                val month = monthSpinner.selectedItemPosition
                val cal = Calendar.getInstance()
                cal.set(year, month, 1, 0, 0 ,0)
                val start = cal.time
                cal.set(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59)
                val end = cal.time
                var email:String? = null
                if (emailEdit.text.isNotEmpty()) {
                    email = emailEdit.text.toString()
                }
                viewModel.loadSoldTransaction(start, end, selectedNetwork, email)
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        viewModel.filteredTransactions.observe(viewLifecycleOwner, Observer { transactions ->
            transactionAdapter.clear()
            if (transactions.isNotEmpty()) {
                transactions.forEach {
                    transactionAdapter.add(it)
                }
            }
            transactionAdapter.notifyDataSetChanged()
        })

        searchBtn.setOnClickListener {
            val yearStr = yearSpinner.selectedItem as String
            val year = yearStr.toInt()
            val month = monthSpinner.selectedItemPosition
            val cal = Calendar.getInstance()
            cal.set(year, month, 1, 0, 0 ,0)
            val start = cal.time
            cal.set(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59)
            val end = cal.time
            var email:String? = null
            if (emailEdit.text.isNotEmpty()) {
                email = emailEdit.text.toString()
            }
            viewModel.loadSoldTransaction(start, end, selectedNetwork, email)
        }

        viewModel.getActiveCategories()

        return root
    }

    override fun onResume() {
        super.onResume()
    }
}