package com.goldgainer.admin.ui.profile

import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.goldgainer.admin.Helper
import com.goldgainer.admin.R
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.Profile
import com.goldgainer.glcclib.data.model.UserData
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch

class ProfileViewModel : ViewModel() {

	private val _localAdmin = Helper.getLocalAdminUser()

	var currentOwner = ""
	var currentUser: UserData? = null
	var dBankArray = arrayOf<String>()
	var dRelationshipArray = arrayOf<String>()

	private val _profileList = MutableLiveData<Array<Profile>>()
	val profileList: LiveData<Array<Profile>> = _profileList

	private val _currentProfile = MutableLiveData<Profile?>()
	val currentProfile: LiveData<Profile?> = _currentProfile

	private val _profileError = MutableLiveData<String>()
	val profileError: LiveData<String> = _profileError

	private val _profileSaved = MutableLiveData<Boolean>()
	val profileSaved : LiveData<Boolean> = _profileSaved

	private val database = FirebaseDataSource()

	val flow = Pager(PagingConfig(15)) {
		ProfilePagingSource(FirebaseFirestore.getInstance())
	}.flow.cachedIn(viewModelScope)

	fun resetProfile() {
		_currentProfile.value = null
	}

	fun getProfile() {
		viewModelScope.launch {
			try {
				currentUser = database.getUserDetail(currentOwner)
				if (currentUser == null) throw Exception("User not exist!")

				val helper = Helper()
				dBankArray = helper.getGlobalSettingsArr("bank_array")
				dRelationshipArray = helper.getGlobalSettingsArr("relationship_array")

				_currentProfile.value = database.getProfile(currentOwner)
			}catch(e: Exception){
				_profileError.value = e.message
			}
		}
	}

	fun saveProfile(fullname: String, address: String, phone: String, idnumber: String, taxnumber: String,
					bankname: String, bankaccname: String, bankaccnumber: String, glcwallet: String, usdtwallet: String,
					sucname: String, sucrel: String, sucphone: String, sucmail: String, vanumber: String, approved: Boolean, rejected: Boolean, reason: String) {
		viewModelScope.launch {
			try {
				val profile = _currentProfile.value
				profile?.let{
					it.fullname = fullname
					it.address = address
					it.phone = phone
					it.nationalId = idnumber
					it.taxId = taxnumber
					it.bankName = bankname
					it.bankAccount = bankaccname
					it.bankNumber = bankaccnumber
					it.glcWallet = glcwallet
					it.usdtWallet = usdtwallet
					it.successorFullname = sucname
					it.successorRelation = sucrel
					it.successorPhone = sucphone
					it.successorEmail = sucmail
					it.virtualAccount = vanumber
					it.statusNote = reason
					if (approved) {
						it.status = "AV"
					}
					if (rejected) {
						it.status = "RA"
						if (it.statusNote.isBlank()) {
							throw Exception("Please input rejection reason!")
						}
					}
					if (approved == false && rejected == false) {
						it.status = "PA"
					}

					database.saveAdminLog(admin = _localAdmin.email, module = "PROFILE", action = "Save Profile ${it.status}", target = profile.owner)
					database.saveProfile(currentOwner, it)
					_profileSaved.value = true
				}
			}catch(e: Exception) {
				_profileError.value = e.message
			}
		}
	}

}
