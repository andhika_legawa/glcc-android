package com.goldgainer.admin.ui.profile

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.goldgainer.glcclib.data.model.Profile
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.tasks.await

class ProfilePagingSource(private val db: FirebaseFirestore) : PagingSource<QuerySnapshot, Profile>() {

    private val query = db.collection("profiles").whereEqualTo("status", "PA").orderBy("lastUpdatedDate", Query.Direction.ASCENDING)

    private val limit = 15L

    override suspend fun load(params: LoadParams<QuerySnapshot>): LoadResult<QuerySnapshot, Profile> {
        return try {
            val currentPage = params.key ?: query.limit(limit).get().await()

            val lastDoc = currentPage.documents[currentPage.size() - 1]
            val nextPage = query.limit(limit).startAfter(lastDoc).get().await()

            LoadResult.Page(
                data = currentPage.toObjects(Profile::class.java),
                prevKey = null,
                nextKey = nextPage
            )
        } catch(e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<QuerySnapshot, Profile>): QuerySnapshot? {
        return null
    }

}