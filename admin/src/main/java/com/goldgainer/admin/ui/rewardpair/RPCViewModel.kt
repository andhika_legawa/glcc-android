package com.goldgainer.admin.ui.rewardpair

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.goldgainer.admin.Helper
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.RewardPairRedeem
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.launch
import java.util.*

class RPCViewModel : ViewModel() {
    val storageRef = Firebase.storage("gs://glcc-app-users/").reference
    private val _localAdmin = Helper.getLocalAdminUser()

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _errorMessage = MutableLiveData<String?>()
    val error:LiveData<String?> = _errorMessage

    var docImageUri: Uri? = null
    var selfImageUri: Uri? = null
    private val _database = FirebaseDataSource()

    var searchEmail: String? = null
    var searchStatus: String? = null

    private val _currentRedeem = MutableLiveData<RewardPairRedeem?>()
    val currentRedeem: LiveData<RewardPairRedeem?> = _currentRedeem

    fun reset(){
        _isBusy.value = false
        _errorMessage.value = null
    }

    fun loadRedeem(redeemId: String?, email: String?) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                if (redeemId != null) {
                    _currentRedeem.value = _database.getRPCRedeemById(redeemId)
                } else if (email != null) {
                    _currentRedeem.value = _database.getRPCRedeemRequest(email)
                }
                prepareImageUri()
            } catch (e: Exception) {
                _errorMessage.value = e.message
            }
            _isBusy.value = false
        }
    }

    fun rejectRedeem() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val redeem = _currentRedeem.value?:throw Exception("No current redeem")
                redeem.status = "REJECTED"
                val documentId = _database.processRPCRedeem(redeem)
                if (documentId != null) {
                    redeem.id = documentId
                    _database.saveAdminLog(admin = _localAdmin.email, module = "RPC", action = "Rejected Redeem", target = redeem.email)
                }
                _currentRedeem.value = redeem
            } catch (e: Exception) {
                _errorMessage.value = e.message
            }
            _isBusy.value = false
        }
    }

    fun approveRedeem() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val redeem = _currentRedeem.value?:throw Exception("No current redeem")
                redeem.status = "APPROVED"
                redeem.approveDate = Date()
                val documentId = _database.processRPCRedeem(redeem)
                if (documentId != null) {
                    redeem.id = documentId
                    _database.saveAdminLog(admin = _localAdmin.email, module = "RPC", action = "Approved Redeem", target = redeem.email)
                }
                _currentRedeem.value = redeem
            } catch (e: Exception) {
                _errorMessage.value = e.message
            }
            _isBusy.value = false
        }
    }

    fun receiveRedeem(){
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val docUri = docImageUri?:throw Exception("Handover document picture cannot be empty!")
                val selfUri = selfImageUri?:throw Exception("Selfie picture cannot be empty!")
                var redeem = _currentRedeem.value?:throw Exception("No current redeem")
                redeem.status = "RECEIVED"
                redeem.isRedeemed = true
                redeem.redeemDate = Date()
                redeem.handoverPhoto = "${redeem.email}/redeem/doc_${redeem.id}.jpg"
                redeem.selfiePhoto = "${redeem.email}/redeem/self_${redeem.id}.jpg"
                redeem.handoverPhoto?.let {
                    storageRef.child(it).putFile(docUri)
                }
                redeem.selfiePhoto?.let {
                    storageRef.child(it).putFile(selfUri)
                }

                val success = _database.saveRPCRedeem(redeem)
                if (success) {
                    _database.saveAdminLog(admin = _localAdmin.email, module = "RPC", action = "Give Redeem", target = redeem.id)
                }
                _currentRedeem.value = redeem
                prepareImageUri()
            } catch (e: Exception) {
                _errorMessage.value = e.message
            }
            _isBusy.value = false
        }
    }

    private fun prepareImageUri(){
        _currentRedeem.value?.let {
            it.handoverPhoto?.let { path ->
                storageRef.child(path).downloadUrl.addOnSuccessListener { imageUri ->
                    docImageUri = imageUri
                }
            }
            it.selfiePhoto?.let { path ->
                storageRef.child(path).downloadUrl.addOnSuccessListener { imageUri ->
                    selfImageUri = imageUri
                }
            }
        }
    }

    val pendingFlow = Pager(PagingConfig(15)) {
        RPCPagingSource(FirebaseFirestore.getInstance())
    }.flow.cachedIn(viewModelScope)

    val searchFlow = Pager(PagingConfig(15)) {
        RPCSearchPagingSource(FirebaseFirestore.getInstance(), searchEmail, searchStatus)
    }.flow.cachedIn(viewModelScope)
}