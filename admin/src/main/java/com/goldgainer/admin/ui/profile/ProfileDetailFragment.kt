package com.goldgainer.admin.ui.profile

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.hbb20.CCPCountry
import com.hbb20.CountryCodePicker
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import java.io.File

class ProfileDetailFragment : Fragment() {

    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_profiledetail, container, false)

        val email = root.findViewById<TextView>(R.id.edit_email)
        val country = root.findViewById<TextView>(R.id.edit_country)
        val fullname = root.findViewById<EditText>(R.id.edit_fullname)
        val fulladdress = root.findViewById<EditText>(R.id.edit_address)
        val mobilephone = root.findViewById<EditText>(R.id.edit_phone)
        val idtext = root.findViewById<TextView>(R.id.textview_nationalid)
        val idnumber = root.findViewById<EditText>(R.id.edit_national_id)
        val taxtext = root.findViewById<TextView>(R.id.textview_tax)
        val taxnumber = root.findViewById<EditText>(R.id.edit_tax_id)
        val imageidtext = root.findViewById<TextView>(R.id.textview_id)
        val imageid = root.findViewById<ImageView>(R.id.imageview_id)
        val imageselfie = root.findViewById<ImageView>(R.id.imageview_selfie)
        val banknametext = root.findViewById<TextView>(R.id.textview_bankname)
        val bankname = root.findViewById<Spinner>(R.id.edit_bank_name)
        val bankacctext = root.findViewById<TextView>(R.id.textview_bankaccount)
        val bankaccname = root.findViewById<EditText>(R.id.edit_bank_account_name)
        val bankaccnumbertext = root.findViewById<TextView>(R.id.textview_bankaccountnumber)
        val bankaccnumber = root.findViewById<EditText>(R.id.edit_bank_account_number)
        val glcwallet = root.findViewById<EditText>(R.id.edit_glc_wallet)
        val usdtwallet = root.findViewById<EditText>(R.id.edit_usdt_wallet)
        val sucname = root.findViewById<EditText>(R.id.edit_successor_fullname)
        val sucrel = root.findViewById<Spinner>(R.id.edit_successor_relationship)
        val sucphone = root.findViewById<EditText>(R.id.edit_successor_phone)
        val sucmail = root.findViewById<EditText>(R.id.edit_successor_email)
        val vaaccount = root.findViewById<EditText>(R.id.edit_vanumber)

        val approved = root.findViewById<CheckBox>(R.id.checkbox_approved)
        val rejected = root.findViewById<CheckBox>(R.id.checkbox_rejected)
        val reason = root.findViewById<EditText>(R.id.edit_reason)
        val btnback = root.findViewById<Button>(R.id.button_back)
        val btnproceed = root.findViewById<Button>(R.id.button_proceed)

        val bundle = arguments
        if (bundle != null) {
            profileViewModel.currentOwner = bundle.getString("owner", "")
            email.text = profileViewModel.currentOwner
        }

        val outputDirectory = getOutputDirectory()

        reason.visibility = View.GONE
        approved.setOnClickListener {
            rejected.isChecked = false
            reason.visibility = View.GONE
        }

        rejected.setOnClickListener {
            approved.isChecked = false
            if (rejected.isChecked) {
                reason.visibility = View.VISIBLE
            }else {
                reason.visibility = View.GONE
            }
        }

        ArrayAdapter.createFromResource(requireContext(), R.array.bank_array, android.R.layout.simple_spinner_item).also {
            it.setDropDownViewResource(R.layout.partials_dropdown_item)
            bankname.adapter = it
        }

        ArrayAdapter.createFromResource(requireContext(), R.array.relationship_array, android.R.layout.simple_spinner_item).also {
            it.setDropDownViewResource(R.layout.partials_dropdown_item)
            sucrel.adapter = it
        }

        profileViewModel.currentProfile.observe(viewLifecycleOwner, Observer {
            val profile = it ?:return@Observer

            if (profileViewModel.currentUser?.country != "ID") {
                taxtext.visibility = View.GONE
                taxnumber.visibility = View.GONE
                banknametext.visibility = View.GONE
                bankname.visibility = View.GONE
                bankacctext.visibility = View.GONE
                bankaccname.visibility = View.GONE
                bankaccnumbertext.visibility = View.GONE
                bankaccnumber.visibility = View.GONE
                idtext.text = "Passport Number:"
                imageidtext.text = "Passport Photo:"
            }

            profileViewModel.currentUser?.country?.let {
                val ccpCountry = CCPCountry.getCountryForNameCodeFromLibraryMasterList(requireContext(), CountryCodePicker.Language.ENGLISH, it)
                country.text = ccpCountry.englishName
            }


            val storageRef = Firebase.storage("gs://glcc-app-users/").reference

            profile.fullname?.let {
                fullname.setText(it)
            }
            profile.address?.let {
                fulladdress.setText(it)
            }
            profile.phone?.let {
                mobilephone.setText(it)
            }
            profile.nationalId?.let {
                idnumber.setText(it)
            }
            profile.taxId?.let {
                taxnumber.setText(it)
            }
            profile.nationalIdPhoto?.let {
                val photoFile = File(outputDirectory, "nationalid.jpg")
                storageRef.child(it).getFile(photoFile)
                .addOnSuccessListener {
                    Picasso.get().load(photoFile).memoryPolicy(MemoryPolicy.NO_CACHE).resize(WIDTH, HEIGHT).centerCrop().into(imageid)
                }
                .addOnFailureListener{ e ->
                    Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                }
            }
            profile.selfiePhoto?.let {
                val photoFile = File(outputDirectory, "selfie.jpg")
                storageRef.child(it).getFile(photoFile)
                .addOnSuccessListener {
                    Picasso.get().load(photoFile).memoryPolicy(MemoryPolicy.NO_CACHE).resize(WIDTH, HEIGHT).centerCrop().into(imageselfie)
                }
                .addOnFailureListener{ e ->
                    Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                }
            }
            profile.bankName?.let {
                val bankArr = context?.resources?.getStringArray(R.array.bank_array)
                bankArr?.let { arr ->
                    bankname.setSelection(arr.indexOf(it))
                }
            }
            if (profileViewModel.dBankArray.isNotEmpty()) {
                ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, profileViewModel.dBankArray).also {
                    it.setDropDownViewResource(R.layout.partials_dropdown_item)
                    bankname.adapter = it
                }
                profile.bankName?.let {
                    bankname.setSelection(profileViewModel.dBankArray.indexOf(it))
                }
            }
            profile.bankAccount?.let {
                bankaccname.setText(it)
            }
            profile.bankNumber?.let {
                bankaccnumber.setText(it)
            }
            profile.glcWallet?.let {
                glcwallet.setText(it)
            }
            profile.usdtWallet?.let {
                usdtwallet.setText(it)
            }
            profile.successorFullname?.let {
                sucname.setText(it)
            }
            profile.successorRelation?.let {
                val relArr = context?.resources?.getStringArray(R.array.relationship_array)
                relArr?.let { arr ->
                    sucrel.setSelection(arr.indexOf(it))
                }
            }
            if (profileViewModel.dRelationshipArray.isNotEmpty()) {
                ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, profileViewModel.dRelationshipArray).also {
                    it.setDropDownViewResource(R.layout.partials_dropdown_item)
                    sucrel.adapter = it
                }
                profile.successorRelation?.let {
                    sucrel.setSelection(profileViewModel.dRelationshipArray.indexOf(it))
                }
            }
            profile.successorPhone?.let {
                sucphone.setText(it)
            }
            profile.successorEmail?.let {
                sucmail.setText(it)
            }
            profile.virtualAccount?.let {
                vaaccount.setText(it)
            }
            profile.status?.let {
                if (it == "AV") {
                    approved.isChecked = true
                    reason.visibility = View.GONE
                }
                if (it == "RA") {
                    rejected.isChecked = true
                    reason.visibility = View.VISIBLE
                }
            }
            profile.statusNote?.let {
                reason.setText(it)
            }
        })

        btnback.setOnClickListener {
            findNavController().popBackStack()
        }

        btnproceed.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Update")
                    .setMessage("Proceed updating this personal information?")
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                        profileViewModel.saveProfile(
                                fullname = fullname.text.toString(),
                                address = fulladdress.text.toString(),
                                phone = mobilephone.text.toString(),
                                idnumber = idnumber.text.toString(),
                                taxnumber = taxnumber.text.toString(),
                                bankname = bankname.selectedItem.toString(),
                                bankaccname = bankaccname.text.toString(),
                                bankaccnumber = bankaccnumber.text.toString(),
                                glcwallet = glcwallet.text.toString(),
                                usdtwallet = usdtwallet.text.toString(),
                                sucname = sucname.text.toString(),
                                sucrel = sucrel.selectedItem.toString(),
                                sucphone = sucphone.text.toString(),
                                sucmail = sucmail.text.toString(),
                                vanumber = vaaccount.text.toString(),
                                approved = approved.isChecked,
                                rejected = rejected.isChecked,
                                reason = reason.text.toString()
                        )
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .setCancelable(false)
                    .show()
        }

        profileViewModel.profileSaved.observe(viewLifecycleOwner, Observer { success ->
            if (success) {
                profileViewModel.getProfile()
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                        .setTitle("Personal info saved")
                        .setMessage("Personal info has been updated successfully.")
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
            }
        })

        profileViewModel.profileError.observe(viewLifecycleOwner, Observer {
            val errorMessage = it ?:return@Observer
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
        })

        return root
    }

    private fun getOutputDirectory(): File {
        val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name)).apply { mkdirs() } }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else requireActivity().filesDir
    }

    override fun onStart() {
        super.onStart()
        profileViewModel.getProfile()
    }

    companion object {
        private const val WIDTH = 320
        private const val HEIGHT = 200
    }
}