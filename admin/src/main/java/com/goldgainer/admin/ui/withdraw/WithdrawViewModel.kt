package com.goldgainer.admin.ui.withdraw

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.goldgainer.admin.Helper
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.Profile
import com.goldgainer.glcclib.data.model.Withdraw
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch

class WithdrawViewModel : ViewModel() {
    private val _localAdmin = Helper.getLocalAdminUser()

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _withdrawList = MutableLiveData<Array<Withdraw>>()
    val withdrawList: LiveData<Array<Withdraw>> = _withdrawList

    private val _detailLoaded = MutableLiveData<Boolean>()
    val detailLoaded: LiveData<Boolean> = _detailLoaded

    private val _withdrawProcessed = MutableLiveData<Boolean>()
    val withdrawProcessed: LiveData<Boolean> = _withdrawProcessed

    private val _errorMessage = MutableLiveData<String>()
    val error:LiveData<String> = _errorMessage

    private val database = FirebaseDataSource()

    val flow = Pager(PagingConfig(15)) {
        WithdrawPagingSource(FirebaseFirestore.getInstance())
    }.flow.cachedIn(viewModelScope)

    var owner: String = ""

    var currentWithdraw: Withdraw? = null
    var currentProfile: Profile? = null

    suspend fun checkWithdraw(email:String): Boolean{
        currentWithdraw = database.getWithdrawRequest(email)
        return currentWithdraw != null
    }

    fun loadDetail() {
        viewModelScope.launch {
            try{
                currentWithdraw = database.getWithdrawRequest(owner)
                currentProfile = database.getProfile(owner)
                _detailLoaded.value = true
            }catch (e:Exception){
                _errorMessage.value = e.message
            }
        }
    }

    fun reject() {
        viewModelScope.launch {
            try{
                currentWithdraw?.let {
                    if (it.status != "SUBMITTED") throw Exception("Withdraw already processed : " + it.status)
                    it.status = "REJECTED"
                    database.saveAdminLog(admin = _localAdmin.email, module = "WITHDRAW", action = "Rejected Withdraw", target = owner)
                    if (!database.addWithdraws(it)) throw Exception("Failed to update")
                    if (!database.deleteWithdrawRequest(owner)) throw Exception("Failed to update")
                    _withdrawProcessed.value = true
                }
            }catch (e:Exception){
                _errorMessage.value = e.message
            }

        }
    }

    fun manual() {
        _isBusy.value?.let {
            if (it) {
                Log.d("MANUAL", "returned")
                return
            }
        }
        viewModelScope.launch {
            _isBusy.value = true
            try {
                currentWithdraw?.let {
                    if (it.status != "SUBMITTED") throw Exception("Withdraw already processed : " + it.status)
                    it.status = "SUCCESS"
                    it.transferMethod = "MANUAL"
                    database.saveAdminLog(admin = _localAdmin.email, module = "WITHDRAW", action = "MT Withdraw", target = owner)
                    if (!database.addWithdraws(it)) throw Exception("Failed to update")
                    if (!database.deleteWithdrawRequest(owner)) throw Exception("Failed to update")
                    _withdrawProcessed.value = true
                    _isBusy.value = false
                }
            }catch (e:Exception){
                _errorMessage.value = e.message
                _isBusy.value = false
            }
        }
    }

    fun auto() {
        viewModelScope.launch {
            try {
                currentWithdraw?.let {
                    if (it.status != "SUBMITTED") throw Exception("Withdraw already processed : " + it.status)
                    it.status = "APPROVED"
                    it.transferMethod = "AUTO"
                    database.saveAdminLog(admin = _localAdmin.email, module = "WITHDRAW", action = "AT Withdraw", target = owner)
                    if (!database.addWithdraws(it)) throw Exception("Failed to update")
                    if (!database.deleteWithdrawRequest(owner)) throw Exception("Failed to update")
                    _withdrawProcessed.value = true
                }
            }catch (e:Exception){
                _errorMessage.value = e.message
            }
        }
    }
}