package com.goldgainer.admin.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class StockistRankFragment : Fragment()  {

    private lateinit var viewModel: StockistReportViewModel

    private lateinit var rankAdapter : StockistRankAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StockistReportViewModel::class.java)
        viewModel.reset()
        val root = inflater.inflate(R.layout.fragment_stockist_report_rank, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val rankList = root.findViewById<ListView>(R.id.trx_list)

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        rankAdapter = StockistRankAdapter(requireContext(), arrayListOf())
        rankList.adapter = rankAdapter
        rankList.emptyView = root.findViewById(R.id.empty_text)

        viewModel.rankList.observe(viewLifecycleOwner, Observer { ranks ->
            rankAdapter.clear()
            if (ranks.isNotEmpty()) {
                ranks.forEach {
                    rankAdapter.add(it)
                }
            }
            rankAdapter.notifyDataSetChanged()
        })

        viewModel.loadRank()

        return root
    }
}