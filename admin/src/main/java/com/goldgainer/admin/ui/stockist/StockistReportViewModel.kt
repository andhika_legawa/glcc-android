package com.goldgainer.admin.ui.stockist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.admin.Helper
import com.goldgainer.admin.R
import com.goldgainer.glcclib.data.FirebaseDataSource
import kotlinx.coroutines.launch

class StockistReportViewModel : ViewModel() {
    private val _localAdmin = Helper.getLocalAdminUser()

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _totalSold = MutableLiveData<Int>()
    val totalSold: LiveData<Int> = _totalSold

    private val _reportList = MutableLiveData<List<NetworkReport>>()
    val reportList: LiveData<List<NetworkReport>> = _reportList

    private val _rankList = MutableLiveData<List<RankReport>>()
    val rankList: LiveData<List<RankReport>> = _rankList

    private val _database = FirebaseDataSource()

    fun reset() {
        _error.value = null
    }

    fun loadReport() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val transactions = _database.getSoldTransactions()
                val iCategories = _database.getNetworkCategories("I")
                val gCategories = _database.getNetworkCategories("G")
                var activeCategories = iCategories.filter { it.active && !it.locked } + gCategories.filter {  it.active && !it.locked }

                val reports = arrayListOf<NetworkReport>()
                activeCategories.forEach { category ->
                    val report = NetworkReport()
                    report.description = category.description
                    report.node1 = transactions.filter { it.networkType == category.type && it.networkNumber == category.number }
                        .filter { it.nodeType == 1 }
                        .sumBy { it.amount }
                    report.node3 = transactions.filter { it.networkType == category.type && it.networkNumber == category.number }
                        .filter { it.nodeType == 3 }
                        .sumBy { it.amount }
                    report.node7 = transactions.filter { it.networkType == category.type && it.networkNumber == category.number }
                        .filter { it.nodeType == 7 }
                        .sumBy { it.amount }
                    report.node15 = transactions.filter { it.networkType == category.type && it.networkNumber == category.number }
                        .filter { it.nodeType == 15 }
                        .sumBy { it.amount }
                    report.total = report.node1 + report.node3 + report.node7 + report.node15
                    reports.add(report)
                }
                val totalSold = reports.sumBy { it.total }
                _reportList.value = reports.toList()
                _totalSold.value = totalSold
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadRank() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val transactions = _database.getSoldTransactions()
                val soldMap = transactions.groupBy { it.owner }
                val reports = arrayListOf<RankReport>()
                soldMap.entries.forEach { (email, trx) ->
                    var rankReport = RankReport()
                    rankReport.email = email
                    rankReport.sold = trx.sumBy { it.amount }
                    val topupTrx = _database.getTopupTransactions(email)
                    val voucher = topupTrx.sumBy { it.amount }
                    rankReport.vouchers = voucher - rankReport.sold
                    reports.add(rankReport)
                }
                val sortedRank = reports.sortedByDescending { it.sold }.toMutableList()
                var rank = 0
                val rankedReport = arrayListOf<RankReport>()
                while (sortedRank.isNotEmpty() && rank < 100) {
                    val report = sortedRank.removeAt(0)
                    report.rank = rank + 1
                    rankedReport.add(report)
                    rank++
                }
                _rankList.value = rankedReport
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }
}


data class NetworkReport(
    var description: String = "",
    var total: Int = 0,
    var node1: Int = 0,
    var node3: Int = 0,
    var node7: Int = 0,
    var node15: Int = 0
)

data class RankReport(
    var rank: Int = 0,
    var email: String = "",
    var vouchers: Int = 0,
    var sold: Int = 0
)

class NetworkReportAdapter(theContext: Context, reports: List<NetworkReport>)
    : ArrayAdapter<NetworkReport>(theContext,0, reports) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val report = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_stockist_report_total_item, parent, false)
        val descriptionText = view.findViewById<TextView>(R.id.network_description_text)
        val totalText = view.findViewById<TextView>(R.id.report_total_sold)
        val node1Text = view.findViewById<TextView>(R.id.stockist_node_1)
        val node3Text = view.findViewById<TextView>(R.id.stockist_node_3)
        val node7Text = view.findViewById<TextView>(R.id.stockist_node_7)
        val node15Text = view.findViewById<TextView>(R.id.stockist_node_15)

        descriptionText.text = report.description
        totalText.text = report.total.toString()
        node1Text.text = report.node1.toString()
        node3Text.text = report.node3.toString()
        node7Text.text = report.node7.toString()
        node15Text.text = report.node15.toString()

        return view
    }
}

class StockistRankAdapter(theContext: Context, reports: List<RankReport>)
    : ArrayAdapter<RankReport>(theContext,0, reports) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val report = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_rank_item, parent, false)
        val rank = view.findViewById<TextView>(R.id.date_textview)
        val vouchers = view.findViewById<TextView>(R.id.email_textview)
        val sold = view.findViewById<TextView>(R.id.sell_textview)

        rank.text = "(${report.rank}) ${report.email}"
        vouchers.text = report.vouchers.toString()
        sold.text = report.sold.toString()

        return view
    }
}

