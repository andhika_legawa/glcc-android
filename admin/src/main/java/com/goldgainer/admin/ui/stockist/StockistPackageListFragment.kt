package com.goldgainer.admin.ui.stockist

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch

class StockistPackageListFragment : Fragment() {

    private lateinit var viewModel: StockistSettingViewModel

    private lateinit var networkAdapter : ArrayAdapter<String>

    private lateinit var nodeTypeAdapter : ArrayAdapter<String>

    private lateinit var currencyAdapter : ArrayAdapter<String>

    private lateinit var packageAdapter : VoucherPackageAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StockistSettingViewModel::class.java)
        viewModel.reset()
        val root = inflater.inflate(R.layout.fragment_stockist_package_list, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val createView = root.findViewById<LinearLayout>(R.id.create_tab)
        val packageView = root.findViewById<LinearLayout>(R.id.package_content)
        val networkSpinner = root.findViewById<Spinner>(R.id.network_spinner)
        val nodeSpinner = root.findViewById<Spinner>(R.id.nodetype_spinner)
        val currencySpinner = root.findViewById<Spinner>(R.id.currency_spinner)
        val priceEdit = root.findViewById<EditText>(R.id.price_edit)
        val amountEdit = root.findViewById<EditText>(R.id.amount_edit)
        val packageList = root.findViewById<ListView>(R.id.package_list)

        val saveBtn = root.findViewById<Button>(R.id.save_button)

        progressCircular.visibility = View.GONE
        createView.visibility = View.VISIBLE
        packageView.visibility = View.VISIBLE

        networkAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        networkAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        networkSpinner.adapter = networkAdapter

        currencyAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        currencyAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        currencySpinner.adapter = currencyAdapter

        nodeTypeAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, arrayOf("1 Node", "3 Node", "7 Node", "15 Node"))
        nodeTypeAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        nodeSpinner.adapter = nodeTypeAdapter
        nodeSpinner.setSelection(0)

        packageAdapter = VoucherPackageAdapter(requireContext(), arrayListOf())
        packageList.adapter = packageAdapter

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        viewModel.voucherPackages.observe(viewLifecycleOwner, Observer { packages ->
            if (packages.isNotEmpty()) {
                packageAdapter.clear()
                packages.forEach {
                    packageAdapter.add(it)
                }
                packageAdapter.notifyDataSetChanged()
            }
        })

        viewModel.networkCategories.observe(viewLifecycleOwner, Observer { categories ->
            if (categories.isNotEmpty()) {
                networkAdapter.clear()
                categories.map { it.description }.forEach { category ->
                    networkAdapter.add(category)
                }
                networkAdapter.notifyDataSetChanged()
                networkSpinner.setSelection(viewModel.selectedNetworkIndex)
            }
        })

        viewModel.digitalCurrencies.observe(viewLifecycleOwner, Observer { currencies ->
            if (currencies.isNotEmpty()) {
                currencyAdapter.clear()
                currencies.forEach {
                    currencyAdapter.add(it.name)
                }
                currencyAdapter.notifyDataSetChanged()
                currencySpinner.setSelection(0)
            }
        })

        networkSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                val categories = viewModel.networkCategories.value?:return
                viewModel.selectedNetworkIndex = position
                val selectedNetwork = categories[viewModel.selectedNetworkIndex]
                if (selectedNetwork.type == "G") {
                    viewModel.loadDigitalCurrencies()
                } else {
                    currencyAdapter.clear()
                    currencyAdapter.add("IDR")
                    currencyAdapter.notifyDataSetChanged()
                    currencySpinner.setSelection(0)
                }
                viewModel.getVoucherPackages()
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        saveBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                .setTitle("Create Voucher Package")
                .setMessage("Create new package in this network?")
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    val selectedNode = nodeSpinner.selectedItem as String
                    var nodeType = 1
                    when (selectedNode) {
                        "1 Node" -> nodeType = 1
                        "3 Node" -> nodeType = 3
                        "7 Node" -> nodeType = 7
                        "15 Node" -> nodeType = 15
                    }
                    try {
                        val price = priceEdit.text.toString().toDouble()
                        val currency = currencySpinner.selectedItem as String
                        val amount = amountEdit.text.toString().toInt()
                        viewLifecycleOwner.lifecycleScope.launch {
                            val categories = viewModel.networkCategories.value?:return@launch
                            val selectedNetwork = categories[viewModel.selectedNetworkIndex]
                            val success = viewModel.saveVoucherPackage(selectedNetwork.type, selectedNetwork.number, nodeType, price, currency, amount, null)
                            if (success) {
                                Toast.makeText(context, "Package Created", Toast.LENGTH_LONG).show()
                                viewModel.getVoucherPackages()
                            }
                        }
                    } catch (e: Exception) {
                        e.message.also {
                            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
                        }
                    }
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        packageList.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapterView: AdapterView<*>?, view: View?, position: Int, longpos: Long) {
                val packages = viewModel.voucherPackages.value?:return
                val selectedPackage = packages[position]
                val params = bundleOf("packageId" to selectedPackage.id)
                findNavController().navigate(R.id.action_nav_setting_package_to_nav_setting_package_edit, params)
            }
        }

        viewModel.getActiveCategories()

        return root
    }

}