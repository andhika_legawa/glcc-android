package com.goldgainer.admin.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import kotlinx.coroutines.launch

class StockistSettingFragment : Fragment() {

    lateinit var viewModel: StockistSettingViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StockistSettingViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_stockist_settings, container, false)

        val stockistSwitch = root.findViewById<SwitchCompat>(R.id.stockist_switch)
        val packageBtn = root.findViewById<Button>(R.id.package_button)
        val addressBtn = root.findViewById<Button>(R.id.address_button)

        stockistSwitch.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                val checked = stockistSwitch.isChecked
                val success = viewModel.saveStockistOn(checked)
                if (success) {
                    if (checked) {
                        Toast.makeText(context, "Stockist feature ON", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(context, "Stockist feature OFF", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

        viewModel.stockistOn.observe(viewLifecycleOwner, Observer {
            stockistSwitch.isChecked = it
        })

        packageBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_setting_stockist_to_nav_setting_package)
        }

        addressBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_setting_stockist_to_nav_setting_fiat)
        }

        viewModel.loadSettings()

        return root
    }
}