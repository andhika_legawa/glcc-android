package com.goldgainer.admin.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class StockistReportFragment : Fragment()  {

    private lateinit var viewModel: StockistReportViewModel

    private lateinit var reportAdapter : NetworkReportAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StockistReportViewModel::class.java)
        viewModel.reset()
        val root = inflater.inflate(R.layout.fragment_stockist_report, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val sellList = root.findViewById<ListView>(R.id.sell_list)
        val totalSold = root.findViewById<TextView>(R.id.report_total_sold)
        val rankBtn = root.findViewById<Button>(R.id.stockist_rank_button)

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        rankBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_stockist_report_to_nav_stockist_rank)
        }

        reportAdapter = NetworkReportAdapter(requireContext(), arrayListOf())
        sellList.adapter = reportAdapter
        sellList.emptyView = root.findViewById(R.id.empty_text)

        viewModel.reportList.observe(viewLifecycleOwner, Observer { reports ->
            reportAdapter.clear()
            if (reports.isNotEmpty()) {
                reports.forEach {
                    reportAdapter.add(it)
                }
            }
            reportAdapter.notifyDataSetChanged()
        })

        viewModel.totalSold.observe(viewLifecycleOwner, Observer {
            totalSold.text = it.toString()
        })

        viewModel.loadReport()

        return root
    }
}
