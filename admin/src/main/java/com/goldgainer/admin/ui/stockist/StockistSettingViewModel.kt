package com.goldgainer.admin.ui.stockist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.admin.Helper
import com.goldgainer.admin.R
import com.goldgainer.admin.toDigitalCurrency
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.DigitalCurrency
import com.goldgainer.glcclib.data.model.NetworkCategory
import com.goldgainer.glcclib.data.model.VoucherPackage
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

class StockistSettingViewModel : ViewModel() {

    private val _localAdmin = Helper.getLocalAdminUser()

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _stockistOn = MutableLiveData<Boolean>()
    val stockistOn: LiveData<Boolean> = _stockistOn

    private val _fiatCurrency = MutableLiveData<FiatCurrency>()
    val fiatCurrency: LiveData<FiatCurrency> = _fiatCurrency

    private val _digitalCurrencies = MutableLiveData<List<DigitalCurrency>>()
    val digitalCurrencies: LiveData<List<DigitalCurrency>> = _digitalCurrencies

    private val _networkCategorySet = MutableLiveData<List<NetworkCategory>>()
    val networkCategories: LiveData<List<NetworkCategory>> = _networkCategorySet

    private val _voucherPackageSet = MutableLiveData<List<VoucherPackage>>()
    val voucherPackages: LiveData<List<VoucherPackage>> = _voucherPackageSet

    var selectedNetworkIndex : Int = 0

    private val _selectedVoucherPackage = MutableLiveData<VoucherPackage?>()
    var selectedVoucherPackage : LiveData<VoucherPackage?> = _selectedVoucherPackage

    private val _database = FirebaseDataSource()

    fun reset() {
        _error.value = null
    }

    fun getActiveCategories() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val iCategories = _database.getNetworkCategories("I")
                val gCategories = _database.getNetworkCategories("G")
                var activeCategories = iCategories.filter { it.active && !it.locked } + gCategories.filter {  it.active && !it.locked }
                _networkCategorySet.value = activeCategories
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadSettings(){
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val helper = Helper()
                val stockistOn = helper.getGlobalSettings("stockist_on", 0) as Int
                _stockistOn.value = stockistOn == 1

                val bankAccount = helper.getGlobalSettings("bank_account", "Goldkoin") as String
                val bankNumber = helper.getGlobalSettings("bank_number", "000000 000000 000000") as String
                val bankName = helper.getGlobalSettings("bank_name", "BNI") as String

                val fiatCurrency = FiatCurrency(
                    account = bankAccount,
                    number = bankNumber,
                    name = bankName
                )
                _fiatCurrency.value = fiatCurrency
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadDigitalCurrencies() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val currencies = _database.getDigitalCurrencies()
                _digitalCurrencies.value = currencies
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    suspend fun saveStockistOn(checked: Boolean) : Boolean {
        _isBusy.value = true
        try {
            var stockistOn = "0"
            var action = "Turn Off Stockist Features"
            if (checked) {
                stockistOn = "1"
                action = "Turn On Stockist Features"
            }
            _database.saveAdminLog(admin = _localAdmin.email, module = "SETTING", action = action, target = null)
            _database.saveGlobalSettings("stockist_on", stockistOn)
            return true
        } catch (e: Exception) {
            e.message.also { _error.value = it }
        }
        _isBusy.value = false
        return false
    }

    suspend fun saveFiatCurrency(account: String, number: String, name: String) : Boolean {
        _isBusy.value = true
        try {
            _database.saveAdminLog(admin = _localAdmin.email, module = "SETTING", action = "Save Fiat Currency", target = null)
            _database.saveGlobalSettings("bank_account", account)
            _database.saveGlobalSettings("bank_number", number)
            _database.saveGlobalSettings("bank_name", name)
            return true
        } catch (e: Exception) {
            e.message.also { _error.value = it }
        }
        _isBusy.value = false
        return false
    }

    suspend fun saveDigitalCurrency(ticker: String, address: String, id: String?) : Boolean {
        _isBusy.value = true
        try {
            val digitalCurrency = DigitalCurrency(
                id = id?:"",
                name = ticker,
                address = address
            )
            _database.saveAdminLog(admin = _localAdmin.email, module = "SETTING", action = "Save Digital Currency", target = id)
            _database.saveDigitalCurrency(digitalCurrency)
            return true
        } catch (e: Exception) {
            e.message.also { _error.value = it }
        }
        _isBusy.value = false
        return false
    }

    suspend fun deleteDigitalCurrency(id: String) : Boolean {
        _isBusy.value = true
        try {
            _database.saveAdminLog(admin = _localAdmin.email, module = "SETTING", action = "Delete Currency", target = id)
            _database.deleteDigitalCurrency(id)
            return true
        } catch (e: Exception) {
            e.message.also { _error.value = it }
        }
        _isBusy.value = false
        return false
    }

    fun getVoucherPackages() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val packages = _database.getVoucherPackages()
                val categories = _networkCategorySet.value
                categories?.let {
                    val networkCategory = categories[selectedNetworkIndex]
                    val filteredPackages = packages.filter { it.networkType == networkCategory.type && it.networkNumber == networkCategory.number }
                    _voucherPackageSet.value = filteredPackages
                }
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    fun loadVoucherPackage(packageId: String) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val voucherPackage = _database.getVoucherPackage(packageId)
                _selectedVoucherPackage.value = voucherPackage
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    suspend fun saveVoucherPackage(networkType: String, networkNumber: Int, nodeType: Int, nodePrice: Double, currency: String, amount: Int, id: String?) : Boolean {
        _isBusy.value = true
        try {
            if (nodePrice <= 0) { throw Exception("Price must be greater than zero") }
            if (amount <= 0) { throw Exception("Amount must be greater than zero") }
            val voucherPackage = VoucherPackage(
                id = id?:"",
                networkType = networkType,
                networkNumber = networkNumber,
                nodeType = nodeType,
                price = nodePrice,
                currency = currency,
                amount = amount
            )
            _database.saveAdminLog(admin = _localAdmin.email, module = "SETTING", action = "Save Voucher", target = id)
            _database.saveVoucherPackage(voucherPackage)
            _isBusy.value = false
            return true
        } catch (e: Exception) {
            e.message.also { _error.value = it }
            _isBusy.value = false
            return false
        }
    }

    suspend fun deleteVoucherPackage(id: String) : Boolean {
        _isBusy.value = true
        try {
            _database.saveAdminLog(admin = _localAdmin.email, module = "SETTING", action = "Delete Voucher", target = id)
            _database.deleteVoucherPackage(id)
            return true
        } catch (e: Exception) {
            e.message.also { _error.value = it }
        }
        _isBusy.value = false
        return false
    }

}

data class FiatCurrency (
    val account: String,
    val number: String,
    val name: String
)

class VoucherPackageAdapter(theContext: Context, packages: List<VoucherPackage>)
    : ArrayAdapter<VoucherPackage>(theContext,0, packages) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val voucherPackage = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_voucherpackage_item, parent, false)
        val nodeTypeTxt = view.findViewById<TextView>(R.id.voucher_node_type)
        val amountTxt = view.findViewById<TextView>(R.id.voucher_amount)
        val priceTxt = view.findViewById<TextView>(R.id.voucher_price)

        nodeTypeTxt.text = "${voucherPackage.nodeType} Node"
        amountTxt.text = voucherPackage.amount.toString()
        priceTxt.text = voucherPackage.price.toDigitalCurrency(voucherPackage.currency)

        return view
    }
}

class DigitalCurrencyAdapter(theContext: Context, currencies: List<DigitalCurrency>, private val deleteClicked:(String) -> Unit)
    : ArrayAdapter<DigitalCurrency>(theContext,0, currencies) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val digitalCurrency = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_digital_item, parent, false)
        val tickerText = view.findViewById<TextView>(R.id.ticker_text)
        val deleteText = view.findViewById<TextView>(R.id.delete_text)
        val addressText = view.findViewById<TextView>(R.id.address_text)

        tickerText.text = digitalCurrency.name
        addressText.text = digitalCurrency.address
        deleteText.setOnClickListener {
            deleteClicked(digitalCurrency.id)
        }

        return view
    }
}