package com.goldgainer.admin.ui.withdraw

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.admin.Helper
import com.goldgainer.glcclib.data.FirebaseDataSource
import kotlinx.coroutines.launch

class WithdrawSettingViewModel : ViewModel() {

    private val _localAdmin = Helper.getLocalAdminUser()

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _database = FirebaseDataSource()

    private val _settings = MutableLiveData<WithdrawSetting>()
    val settings: LiveData<WithdrawSetting> = _settings

    fun reset() {
        _error.value = null
    }

    fun loadSettings() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val helper = Helper()
                val withdrawInt = helper.getGlobalSettings("withdraw_enabled", 0) as Int
                val goldcoinInt = helper.getGlobalSettings("goldcoin_enabled", 0) as Int
                val pgoldInt = helper.getGlobalSettings("pgold_enabled", 0) as Int
                _settings.value = WithdrawSetting(
                    withdrawEnabled = withdrawInt == 1,
                    goldCoinEnabled = goldcoinInt == 1,
                    pgoldEnabled = pgoldInt == 1,
                    minWithdraw = helper.getGlobalSettings("min_withdraw", "50000") as String,
                    maxWithdraw = helper.getGlobalSettings("min_withdraw", "10000000") as String,
                    transferFee = helper.getGlobalSettings("transfer_fee", "5000") as String,
                    adminFee = helper.getGlobalSettings("admin_fee", "1000") as String,
                    taxOne = helper.getGlobalSettings("tax_one", "5") as String,
                    taxTwo = helper.getGlobalSettings("tax_two", "15") as String,
                    taxThree = helper.getGlobalSettings("tax_three", "25") as String,
                    taxFour = helper.getGlobalSettings("tax_four", "30") as String,
                    glcUsdt = helper.getGlobalSettings("glc_usdt", "0.0") as String,
                    usdIdr = helper.getGlobalSettings("usd_idr", "0.0") as String,
                    slippage = helper.getGlobalSettings("slippage", "0.05") as String,
                    goldIdr = helper.getGlobalSettings("gold_idr", "0") as String,
                    goldTransferFee = helper.getGlobalSettings("gold_transfer_fee", "5000") as String,
                    goldPackingFee = helper.getGlobalSettings("gold_packing_fee", "5000") as String,
                    goldAdminFee = helper.getGlobalSettings("gold_admin_fee", "1000") as String
                )
            } catch (e: Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }

    suspend fun saveSettings(setting:WithdrawSetting) : Boolean {
        _isBusy.value = true
        try {
            _database.saveAdminLog(admin = _localAdmin.email, module = "SETTING", action = "Save withdraw settings", target = null)
            _database.saveGlobalSettings("withdraw_enabled", if (setting.withdrawEnabled) "1" else "0")
            _database.saveGlobalSettings("goldcoin_enabled", if (setting.goldCoinEnabled) "1" else "0")
            _database.saveGlobalSettings("pgold_enabled", if (setting.pgoldEnabled) "1" else "0")
            _database.saveGlobalSettings("min_withdraw", setting.minWithdraw)
            _database.saveGlobalSettings("max_withdraw", setting.maxWithdraw)
            _database.saveGlobalSettings("transfer_fee", setting.transferFee)
            _database.saveGlobalSettings("admin_fee", setting.adminFee)
            _database.saveGlobalSettings("tax_one", setting.taxOne)
            _database.saveGlobalSettings("tax_two", setting.taxTwo)
            _database.saveGlobalSettings("tax_three", setting.taxThree)
            _database.saveGlobalSettings("tax_four", setting.taxFour)
            _database.saveGlobalSettings("glc_usdt", setting.glcUsdt)
            _database.saveGlobalSettings("usd_idr", setting.usdIdr)
            _database.saveGlobalSettings("slippage", setting.slippage)
            _database.saveGlobalSettings("gold_idr", setting.goldIdr)
            _database.saveGlobalSettings("gold_transfer_fee", setting.goldTransferFee)
            _database.saveGlobalSettings("gold_packing_fee", setting.goldPackingFee)
            _database.saveGlobalSettings("gold_admin_fee", setting.goldAdminFee)
        } catch (e: Exception) {
            e.message.also { _error.value = it }
            return false
        }
        _isBusy.value = false
        return true
    }

}

data class WithdrawSetting(
    val withdrawEnabled:Boolean,
    val goldCoinEnabled:Boolean,
    val pgoldEnabled:Boolean,
    val minWithdraw:String,
    val maxWithdraw:String,
    val transferFee:String,
    val adminFee:String,
    val taxOne:String,
    val taxTwo:String,
    val taxThree:String,
    val taxFour:String,
    val glcUsdt:String,
    val usdIdr:String,
    val slippage:String,
    val goldIdr:String,
    val goldTransferFee:String,
    val goldPackingFee:String,
    val goldAdminFee:String
)