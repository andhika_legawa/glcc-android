package com.goldgainer.admin.ui.withdraw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ManageWithdrawFragment : Fragment() {

    private lateinit var withdrawViewModel: WithdrawViewModel
    private lateinit var withdrawList: RecyclerView
    private lateinit var withdrawAdapter : WithdrawPagingAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        withdrawViewModel = ViewModelProvider(this).get(WithdrawViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_withdrawmanage, container, false)
        withdrawAdapter = WithdrawPagingAdapter() { withdraw ->
            viewLifecycleOwner.lifecycleScope.launch {
                val withdrawChecked = withdrawViewModel.checkWithdraw(withdraw.owner)
                if (withdrawChecked) {
                    val params = bundleOf("owner" to withdrawViewModel.currentWithdraw?.owner)
                    findNavController().navigate(R.id.action_nav_managewithdraw_to_nav_withdrawdetail, params)
                } else {
                    MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                        .setTitle("Info")
                        .setMessage("Withdraw Transaction for this user already processed.")
                        .setPositiveButton(android.R.string.ok, null)
                        .setCancelable(false)
                        .show()
                }
            }
        }

//        val spinner = root.findViewById<Spinner>(R.id.filter_spinner)
//        ArrayAdapter.createFromResource(
//                requireContext(),
//                R.array.withdraw_filter_array,
//                android.R.layout.simple_spinner_item
//        ).also { adapter ->
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//            spinner.adapter = adapter
//        }

        withdrawList = root.findViewById(R.id.withdrawlist)
        val verticalLayoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        val divider = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.list_divider)!!)
        withdrawList.addItemDecoration(divider)
        withdrawList.layoutManager = verticalLayoutManager
        withdrawList.adapter = withdrawAdapter

        val pullToRefresh = root.findViewById<SwipeRefreshLayout>(R.id.swiperefresh)
        val searchFab = root.findViewById<FloatingActionButton>(R.id.fab)

        searchFab.setOnClickListener {
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            withdrawViewModel.flow.collect {
                withdrawAdapter.submitData(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            withdrawAdapter.loadStateFlow.collectLatest { loadStates ->
                pullToRefresh.isRefreshing = loadStates.refresh is LoadState.Loading
            }
        }

        pullToRefresh.setOnRefreshListener {
            withdrawAdapter.refresh()
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        withdrawAdapter.refresh()
    }
}