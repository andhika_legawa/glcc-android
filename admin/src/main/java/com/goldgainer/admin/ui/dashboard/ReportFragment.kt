package com.goldgainer.admin.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R

class ReportFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_reports, container, false)

        val stockistBtn = root.findViewById<Button>(R.id.stockist_report_button)

        stockistBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_report_to_nav_stockist_report)
        }

        return root
    }
}