package com.goldgainer.admin.ui.rewardpair

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.goldgainer.glcclib.data.model.RewardPairRedeem
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.tasks.await

class RPCPagingSource(db: FirebaseFirestore) : PagingSource<QuerySnapshot, RewardPairRedeem>() {

    private val query = db.collection("rpc_redeem_req").orderBy("requestDate", Query.Direction.ASCENDING)

    private val limit = 15L

    override suspend fun load(params: LoadParams<QuerySnapshot>): LoadResult<QuerySnapshot, RewardPairRedeem> {
        return try {
            val currentPage = params.key ?: query.limit(limit).get().await()

            var nextPage:QuerySnapshot? =  null
            if (currentPage.documents.isNotEmpty()) {
                val lastDoc = currentPage.documents[currentPage.size() - 1]
                nextPage = query.limit(limit).startAfter(lastDoc).get().await()
            }

            LoadResult.Page(
                data = currentPage.toObjects(RewardPairRedeem::class.java),
                prevKey = null,
                nextKey = nextPage
            )
        } catch(e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<QuerySnapshot, RewardPairRedeem>): QuerySnapshot? {
        return null
    }
}