package com.goldgainer.admin.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.AdminActivity
import com.goldgainer.admin.BuildConfig
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class AdminLoginFragment : Fragment() {

    private lateinit var loginViewModel: AdminLoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_adminlogin, container, false)

        loginViewModel = ViewModelProvider(this, LoginViewModelFactory()).get(AdminLoginViewModel::class.java)

        val username = root.findViewById<EditText>(R.id.login_username)
        val password = root.findViewById<EditText>(R.id.login_password)
        val login = root.findViewById<Button>(R.id.login_button)
        val loading = root.findViewById<ProgressBar>(R.id.login_loading)

        loginViewModel.loginResult.observe(viewLifecycleOwner, Observer {
            login.isEnabled = true

            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                if (loginResult.errorMessage != null) {
                    MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                        .setTitle("Login Failed")
                        .setMessage(loginResult.errorMessage)
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                } else {
                    Toast.makeText(activity, loginResult.error, Toast.LENGTH_LONG).show()
                }
            }
            if (loginResult.success != null) {
                startActivity(Intent(activity, AdminActivity::class.java))
            }
        })

        login.setOnClickListener {
            login.isEnabled = false
            loading.visibility = View.VISIBLE
            loginViewModel.login(username.text.toString(), password.text.toString())
        }

        loginViewModel.latestVersion.observe(viewLifecycleOwner, Observer {
            if (BuildConfig.VERSION_CODE < it) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Application Need Update")
                    .setMessage("There is a new update available, please update to continue using GLCC admin.")
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        loginViewModel.checkVersion()

        return root
    }

    override fun onStart() {
        super.onStart()
        loginViewModel.logout()
    }

    override fun onResume() {
        super.onResume()
        loginViewModel.logout()
    }
}