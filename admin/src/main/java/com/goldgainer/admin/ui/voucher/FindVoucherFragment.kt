package com.goldgainer.admin.ui.voucher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

class FindVoucherFragment : Fragment() {

    private lateinit var viewModel: FindVoucherViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(FindVoucherViewModel::class.java)
        viewModel.reset()

        val root = inflater.inflate(R.layout.fragment_voucher_find, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val emailEdit = root.findViewById<EditText>(R.id.stockist_email_edit)
        val searchBtn = root.findViewById<Button>(R.id.voucher_search_btn)
        val detailPanel = root.findViewById<LinearLayout>(R.id.voucher_detail_panel)
        val codeTxt = root.findViewById<TextView>(R.id.code_text)
        val redByTxt = root.findViewById<TextView>(R.id.redeemed_by_text)
        val redDateTxt = root.findViewById<TextView>(R.id.redeemed_date_text)

        detailPanel.visibility = View.GONE

        progressCircular.visibility = View.GONE
        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        searchBtn.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                if (emailEdit.text.isNullOrEmpty()) {
                    MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                        .setTitle("Info")
                        .setMessage("Voucher code cannot be empty")
                        .setPositiveButton(android.R.string.ok, null)
                        .setCancelable(false)
                        .show()
                    return@launch
                }
                val email = emailEdit.text.toString()
                val voucher = viewModel.getVoucher(email)
                if (voucher != null) {
                    detailPanel.visibility = View.VISIBLE
                    codeTxt.text = voucher.code
                    if (voucher.redeemedBy != null) {
                        redByTxt.text = voucher.redeemedBy
                    } else {
                        redByTxt.text = "(empty)"
                    }
                    if (voucher.redeemedDate != null) {
                        val sdf = SimpleDateFormat("d MMM yyyy HH:mm:ss Z")
                        redDateTxt.text = sdf.format(voucher.redeemedDate)
                    } else {
                        redDateTxt.text = "(empty)"
                    }
                } else {
                    detailPanel.visibility = View.GONE
                    MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                        .setTitle("Info")
                        .setMessage("Voucher not found!")
                        .setPositiveButton(android.R.string.ok, null)
                        .setCancelable(false)
                        .show()
                }
            }
        }

        return root
    }
}