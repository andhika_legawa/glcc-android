package com.goldgainer.admin.ui.withdraw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.launch

class WithdrawSettingFragment : Fragment() {

    lateinit var viewModel: WithdrawSettingViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(WithdrawSettingViewModel::class.java)
        viewModel.reset()
        val root = inflater.inflate(R.layout.fragment_withdraw_settings, container, false)

        val withdrawSwitch = root.findViewById<SwitchCompat>(R.id.withdraw_enabled)
        val pgoldSwitch = root.findViewById<SwitchCompat>(R.id.pgold_enabled)
        val goldcoinSwitch = root.findViewById<SwitchCompat>(R.id.goldcoin_enabled)
        val minWithdraw = root.findViewById<TextInputEditText>(R.id.min_withdraw)
        val maxWithdraw = root.findViewById<TextInputEditText>(R.id.max_withdraw)
        val transferFee = root.findViewById<TextInputEditText>(R.id.transfer_fee)
        val adminFee = root.findViewById<TextInputEditText>(R.id.admin_fee)
        val taxOne = root.findViewById<TextInputEditText>(R.id.tax_one)
        val taxTwo = root.findViewById<TextInputEditText>(R.id.tax_two)
        val taxThree = root.findViewById<TextInputEditText>(R.id.tax_three)
        val taxFour = root.findViewById<TextInputEditText>(R.id.tax_four)
        val glcUsdt = root.findViewById<TextInputEditText>(R.id.glc_usdt)
        val usdIdr = root.findViewById<TextInputEditText>(R.id.usd_idr)
        val slippage = root.findViewById<TextInputEditText>(R.id.slippage)
        val goldIdr = root.findViewById<TextInputEditText>(R.id.gold_idr)
        val goldTransferFee = root.findViewById<TextInputEditText>(R.id.gold_transfer_fee)
        val goldPackingFee = root.findViewById<TextInputEditText>(R.id.gold_packing_fee)
        val goldAdminFee = root.findViewById<TextInputEditText>(R.id.gold_admin_fee)
        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val saveBtn = root.findViewById<Button>(R.id.save_button)

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        viewModel.settings.observe(viewLifecycleOwner, Observer {
            withdrawSwitch.isChecked = it.withdrawEnabled
            pgoldSwitch.isChecked = it.pgoldEnabled
            goldcoinSwitch.isChecked = it.goldCoinEnabled
            minWithdraw.setText(it.minWithdraw)
            maxWithdraw.setText(it.maxWithdraw)
            transferFee.setText(it.transferFee)
            adminFee.setText(it.adminFee)
            taxOne.setText(it.taxOne)
            taxTwo.setText(it.taxTwo)
            taxThree.setText(it.taxThree)
            taxFour.setText(it.taxFour)
            glcUsdt.setText(it.glcUsdt)
            usdIdr.setText(it.usdIdr)
            slippage.setText(it.slippage)
            goldIdr.setText(it.goldIdr)
            goldTransferFee.setText(it.goldTransferFee)
            goldPackingFee.setText(it.goldPackingFee)
            goldAdminFee.setText(it.goldAdminFee)
        })

        saveBtn.setOnClickListener {
            val setting = WithdrawSetting(
                withdrawEnabled = withdrawSwitch.isChecked,
                goldCoinEnabled = goldcoinSwitch.isChecked,
                pgoldEnabled = pgoldSwitch.isChecked,
                minWithdraw = minWithdraw.text.toString(),
                maxWithdraw = maxWithdraw.text.toString(),
                transferFee = transferFee.text.toString(),
                adminFee = adminFee.text.toString(),
                taxOne = taxOne.text.toString(),
                taxTwo = taxTwo.text.toString(),
                taxThree = taxThree.text.toString(),
                taxFour = taxFour.text.toString(),
                glcUsdt = glcUsdt.text.toString(),
                usdIdr = usdIdr.text.toString(),
                slippage = slippage.text.toString(),
                goldIdr = goldIdr.text.toString(),
                goldTransferFee = goldTransferFee.text.toString(),
                goldPackingFee = goldPackingFee.text.toString(),
                goldAdminFee = goldAdminFee.text.toString()
            )
            viewLifecycleOwner.lifecycleScope.launch {
                val success = viewModel.saveSettings(setting)
                if (success) {
                    Toast.makeText(context, "Settings Saved", Toast.LENGTH_LONG).show()
                }
            }
        }

        viewModel.loadSettings()

        return root
    }
}