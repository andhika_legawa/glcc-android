package com.goldgainer.admin.ui.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.admin.R
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.AdminLog
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class AdminLogViewModel : ViewModel() {

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _filteredLogs = MutableLiveData<List<AdminLog>>()
    val filteredLogs: LiveData<List<AdminLog>> = _filteredLogs

    private val _database = FirebaseDataSource()

    fun reset() {
        _error.value = null
    }

    fun loadActivityLog(start: Date, end: Date, email:String?) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val logs = _database.getAdminLogs(start, end, email)
                _filteredLogs.value = logs
            } catch (e:Exception) {
                e.message.also { _error.value = it }
            }
            _isBusy.value = false
        }
    }
}

class AdminLogAdapter(theContext: Context, stockists: List<AdminLog>)
    : ArrayAdapter<AdminLog>(theContext,0, stockists) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val log = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_adminactivity_item, parent, false)
        val dateText = view.findViewById<TextView>(R.id.date_text)
        val emailText = view.findViewById<TextView>(R.id.email_text)
        val actionText = view.findViewById<TextView>(R.id.action_text)
        val targetText = view.findViewById<TextView>(R.id.target_text)

        val sdf = SimpleDateFormat("d MMM yyyy - HH:mm:ss")
        dateText.text = sdf.format(log.createdDate)
        emailText.text = log.admin
        actionText.text = "${log.action}"

        targetText.visibility = View.GONE
        if (log.target != null) {
            targetText.visibility = View.VISIBLE
            targetText.text = log.target
        }

        return view
    }
}