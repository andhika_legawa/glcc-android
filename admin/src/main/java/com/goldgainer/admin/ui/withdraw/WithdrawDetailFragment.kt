package com.goldgainer.admin.ui.withdraw

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.AdminActivity
import com.goldgainer.admin.R
import com.goldgainer.admin.toCurrency
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class WithdrawDetailFragment : Fragment() {

    private lateinit var withdrawViewModel: WithdrawViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        withdrawViewModel = ViewModelProvider(this).get(WithdrawViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_withdrawdetail, container, false)

        arguments?.let {
            withdrawViewModel.owner = it.getString("owner", "")
        }

        val bonusType = root.findViewById<TextView>(R.id.text_bonus)
        val trxId = root.findViewById<TextView>(R.id.text_trxid)
        val name = root.findViewById<TextView>(R.id.text_name)
        val email = root.findViewById<TextView>(R.id.text_email)
        val phone = root.findViewById<TextView>(R.id.text_phone)
        val bankName = root.findViewById<TextView>(R.id.text_bankname)
        val bankAcc = root.findViewById<TextView>(R.id.text_accname)
        val bankNumber = root.findViewById<TextView>(R.id.text_accnumber)
        val amount = root.findViewById<TextView>(R.id.text_amount)
        val vanumber = root.findViewById<TextView>(R.id.text_vanumber)

        val bankPanel = root.findViewById<ConstraintLayout>(R.id.bank_panel)
        val walletPanel = root.findViewById<ConstraintLayout>(R.id.coin_panel)
        val wallet = root.findViewById<TextView>(R.id.wallet_text)
        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)

        val backBtn = root.findViewById<Button>(R.id.btn_back)
        val rejectBtn = root.findViewById<Button>(R.id.btn_reject)
        val manualBtn = root.findViewById<Button>(R.id.btn_manual)
        val autoBtn = root.findViewById<Button>(R.id.btn_auto)
        val walletBtn = root.findViewById<Button>(R.id.wallet_btn)

        bankPanel.visibility = View.GONE
        walletPanel.visibility = View.GONE

        backBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        withdrawViewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        rejectBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Reject")
                    .setMessage("Reject this withdraw request?")
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                        withdrawViewModel.reject()
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .setCancelable(false)
                    .show()
        }

        manualBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Manual Transfer")
                    .setMessage("Are you sure to transfer with manual transfer?")
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                        withdrawViewModel.manual()
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .setCancelable(false)
                    .show()
        }

        autoBtn.setOnClickListener {
            var prefix = ""
            if (withdrawViewModel.currentProfile?.virtualAccount.isNullOrEmpty()) {
                prefix = "This member doesn't have virtual account number registered. "
            }
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Auto Transfer")
                    .setMessage(prefix + "Are you sure to transfer with auto transfer?")
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                        withdrawViewModel.auto()
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .setCancelable(false)
                    .show()
        }

        withdrawViewModel.detailLoaded.observe(viewLifecycleOwner, Observer {
            if(!it) return@Observer
            if( withdrawViewModel.currentWithdraw == null ) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage("Withdraw Transaction for this user already processed.")
                    .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
                        findNavController().popBackStack()
                    }
                    .setCancelable(false)
                    .show()
                return@Observer
            }
            withdrawViewModel.currentWithdraw?.let { request ->
                if (request.subtype == "BS") {
                    bonusType.text = "R.R"
                }

                if (request.subtype == "BP") {
                    bonusType.text = "C.R"
                }

                if (request.subtype == "BR") {
                    bonusType.text = "A.R"
                }

                if (request.subtype == "GC") {
                    bankPanel.visibility = View.GONE
                    walletPanel.visibility = View.VISIBLE
                    wallet.text = request.glcWallet
                    bonusType.text = "(GOLDCOIN)"
                    autoBtn.alpha = 0.5f
                    autoBtn.isEnabled = false
                    amount.text = String.format("%.8f GLC", request.savingGoldcoin)
                    walletBtn.setOnClickListener {
                        Toast.makeText(context, "Address copied to clipboard!", Toast.LENGTH_LONG).show()
                        val mainActivity = activity as AdminActivity
                        val clipboard = mainActivity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        val clip = ClipData.newPlainText("Wallet Address", request.glcWallet)
                        clipboard.setPrimaryClip(clip)
                    }
                } else if (request.subtype == "PG") {
                    bankPanel.visibility = View.GONE
                    bonusType.text = "(PHYSICAL GOLD)"
                    autoBtn.alpha = 0.5f
                    autoBtn.isEnabled = false
                    amount.text = String.format("%.1f Grams", request.savingGold) + " - " + request.amountReceived.toCurrency("IDR")
                } else {
                    bankPanel.visibility = View.VISIBLE
                    walletPanel.visibility = View.GONE
                    amount.text = request.amountReceived.toCurrency("IDR")
                }

                trxId.text = "TRANSACTION ID: " + request.trxId
                email.text = request.owner

            }

            withdrawViewModel.currentProfile?.let { profile ->
                name.text = profile.fullname
                phone.text = profile.phone
                bankName.text = profile.bankName
                bankAcc.text = profile.bankAccount
                bankNumber.text = profile.bankNumber
                vanumber.text = profile.virtualAccount
            }
        })

        withdrawViewModel.withdrawProcessed.observe(viewLifecycleOwner, Observer {
            if(!it) return@Observer
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage("Withdraw process done.")
                    .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
                        findNavController().popBackStack()
                    }
                    .setCancelable(false)
                    .show()
        })

        withdrawViewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })

        withdrawViewModel.loadDetail()

        return root
    }
}