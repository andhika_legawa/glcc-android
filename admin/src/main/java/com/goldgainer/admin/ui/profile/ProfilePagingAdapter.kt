package com.goldgainer.admin.ui.profile

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.admin.R
import com.goldgainer.glcclib.data.model.Profile
import java.text.SimpleDateFormat

class ProfilePagingAdapter(private val listener:(Profile) -> Unit): PagingDataAdapter<Profile, ProfilePagingAdapter.ProfileViewHolder>(Companion) {

    companion object : DiffUtil.ItemCallback<Profile>() {
        override fun areItemsTheSame(oldItem: Profile, newItem: Profile): Boolean {
            return oldItem.owner == newItem.owner
        }

        override fun areContentsTheSame(oldItem: Profile, newItem: Profile): Boolean {
            return oldItem == newItem
        }
    }

    class ProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        val profile = getItem(position) as Profile
        val email = holder.itemView.findViewById<TextView>(R.id.email_textview)
        val fullname = holder.itemView.findViewById<TextView>(R.id.name_textview)
        val date = holder.itemView.findViewById<TextView>(R.id.date_textview)
        val status = holder.itemView.findViewById<TextView>(R.id.status_textview)
        email.text = profile.owner
        email.setTypeface(null, Typeface.ITALIC)
        fullname.text = profile.fullname
        status.text = profile.status
        val sdf = SimpleDateFormat("MMM dd, YYYY")
        date.text = sdf.format(profile.lastUpdatedDate).toString()
        date.setTypeface(null, Typeface.ITALIC)

        holder.itemView.setOnClickListener { listener(profile) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.partials_profile_item, parent, false) as View
        return ProfileViewHolder(view)
    }
}