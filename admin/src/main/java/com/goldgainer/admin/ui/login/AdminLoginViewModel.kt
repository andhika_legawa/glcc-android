package com.goldgainer.admin.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.admin.Helper
import com.goldgainer.admin.R
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.LoginRepository
import com.goldgainer.glcclib.data.Result
import kotlinx.coroutines.launch

class AdminLoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

	private val _loginResult = MutableLiveData<LoginResult>()
	val loginResult: LiveData<LoginResult> = _loginResult

	private val _latestVersion = MutableLiveData<Int>()
	val latestVersion: LiveData<Int> = _latestVersion

	fun login(username: String, password: String) {
		viewModelScope.launch {
			val result = loginRepository.login(username, password)
			if (result is Result.Success) {
				result.data?.let{
					if (!it.emailVerified) {
						_loginResult.value = LoginResult(error = R.string.verify_email)
						return@launch
					}

					//get admin permission
					val adminUser = FirebaseDataSource().getAdmin(it.email!!)
					if (adminUser!=null) {
						Helper.setLocalAdminUser(adminUser)
						_loginResult.value = LoginResult(success = adminUser)
					}else{
						_loginResult.value = LoginResult(error = R.string.userdata_failed)
					}
				}
			}
			if (result is Result.Error) {
				result.exception?.let {
					_loginResult.value = LoginResult(error = R.string.login_failed, errorMessage = it.message)
				}
			}
		}
	}

	fun checkVersion(){
		viewModelScope.launch {
			val helper = Helper()
			_latestVersion.value = helper.getGlobalSettings("admin_version", 1) as Int
		}
	}

	fun logout(){
		loginRepository.logout()
	}
}