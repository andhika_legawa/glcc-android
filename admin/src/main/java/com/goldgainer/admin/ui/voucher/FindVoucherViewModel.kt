package com.goldgainer.admin.ui.voucher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.goldgainer.admin.Helper
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.Voucher

class FindVoucherViewModel : ViewModel() {
    private val _localAdmin = Helper.getLocalAdminUser()

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _database = FirebaseDataSource()

    fun reset() {
        _error.value = null
    }

    suspend fun getVoucher(code: String) : Voucher? {
        _isBusy.value = true
        try {
            val voucher = _database.getVoucherByCode(code)
            _isBusy.value = false
            return voucher
        } catch (e: Exception) {
            e.message.also { _error.value = it }
        }
        _isBusy.value = false
        return null
    }
}