package com.goldgainer.admin.ui.stockist

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.goldgainer.admin.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.text.SimpleDateFormat
import java.util.*

class StockistMemberDetailFragment : Fragment() {

    private lateinit var viewModel : StockistMemberViewModel

    private lateinit var transactionAdapter: StockistTrxAdapter

    private lateinit var selectedEmail : String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StockistMemberViewModel::class.java)
        viewModel.reset()

        val root = inflater.inflate(R.layout.fragment_stockist_member_detail, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val nameText = root.findViewById<TextView>(R.id.stockist_name_text)
        val whatsappBtn = root.findViewById<ImageButton>(R.id.stockist_item_whatsapp)
        val telegramBtn = root.findViewById<ImageButton>(R.id.stockist_item_telegram)
        val provinceText = root.findViewById<TextView>(R.id.stockist_province)

        val totalTopupText = root.findViewById<TextView>(R.id.stockist_total_topup_text)
        val totalSellText = root.findViewById<TextView>(R.id.stockist_total_sold_text)

        val yearSpinner = root.findViewById<Spinner>(R.id.stockist_trx_year)
        val monthSpinner = root.findViewById<Spinner>(R.id.stockist_trx_month)

        val topupTab = root.findViewById<MaterialButton>(R.id.topup_button)
        val sellTab = root.findViewById<MaterialButton>(R.id.sell_button)

        val packageText = root.findViewById<TextView>(R.id.package_text)

        val searchBtn = root.findViewById<Button>(R.id.stockist_sell_search)

        val trxList = root.findViewById<ListView>(R.id.trx_list)

        arguments?.let {
            selectedEmail = it.getString("email", "")
            viewModel.loadStockistInfo(selectedEmail)
        }

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })


        val yearAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        yearAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        val start = 2021
        var currentYear = Calendar.getInstance().get(Calendar.YEAR)
        while ( currentYear >= start ) {
            yearAdapter.add(currentYear.toString())
            currentYear -= 1
        }
        yearSpinner.adapter = yearAdapter

        val monthAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        monthAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        var currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        var january = 0
        val sdf = SimpleDateFormat("MMM")
        while ( january <= 11) {
            val cal = Calendar.getInstance()
            cal.set(currentYear, january, 1)
            monthAdapter.add(sdf.format(cal.time))
            january += 1
        }
        monthSpinner.adapter = monthAdapter
        monthSpinner.setSelection(currentMonth)

        transactionAdapter = StockistTrxAdapter(requireContext(), arrayListOf())
        trxList.adapter = transactionAdapter
        trxList.emptyView = root.findViewById(R.id.empty_text)

        viewModel.currentStockist.observe(viewLifecycleOwner, Observer {
            val info = it?:return@Observer
            nameText.text = info.user.name
            provinceText.text = "${info.stockist.province}, ${info.stockist.city}"

            whatsappBtn.setOnClickListener {
                val packageManager = requireActivity().packageManager
                val i = Intent(Intent.ACTION_VIEW)
                val url = "https://api.whatsapp.com/send?phone=${info.stockist.whatsapp}"
                i.setPackage("com.whatsapp")
                i.data = Uri.parse(url)
                if (i.resolveActivity(packageManager) != null) {
                    startActivity(i)
                } else {
                    MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage("No whatsapp detected. Stockist Whatsapp number : ${info.stockist.whatsapp}")
                    .setPositiveButton(android.R.string.ok, null)
                    .show()
                }
            }

            telegramBtn.setOnClickListener {
                val telegramHandle = info.stockist.telegram.substringAfter("@")
                val packageManager = requireActivity().packageManager
                val i = Intent(Intent.ACTION_VIEW)
                val url = "http://telegram.me/$telegramHandle"
                i.setPackage("org.telegram.messenger")
                i.data = Uri.parse(url)
                if (i.resolveActivity(packageManager) != null) {
                    startActivity(i)
                } else {
                    MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage("No telegram detected. Stockist Telegram User : $telegramHandle")
                    .setPositiveButton(android.R.string.ok, null)
                    .show()
                }
            }
        })

        viewModel.totalVoucher.observe(viewLifecycleOwner, Observer {
            totalTopupText.text = it.topup.toString() + " Vouchers"
            totalSellText.text = it.sold.toString() + " Vouchers"
        })

        viewModel.filteredTransactions.observe(viewLifecycleOwner, Observer { transactions ->
            transactionAdapter.clear()
            if (transactions.isNotEmpty()) {
                transactions.forEach {
                    transactionAdapter.add(it)
                }
            }
            transactionAdapter.notifyDataSetChanged()
        })

        topupTab.setOnClickListener {
            topupTab.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            topupTab.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_gray))
            sellTab.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_white))
            sellTab.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
            viewModel.transactionType = 0
            packageText.text = "PACKAGE"

            val yearStr = yearSpinner.selectedItem as String
            val year = yearStr.toInt()
            val month = monthSpinner.selectedItemPosition
            val cal = Calendar.getInstance()
            cal.set(year, month, 1, 0, 0 ,0)
            val start = cal.time
            cal.set(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59)
            val end = cal.time

            viewModel.loadStockistTransaction(selectedEmail, start, end)
        }

        sellTab.setOnClickListener {
            sellTab.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            sellTab.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_gray))
            topupTab.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_white))
            topupTab.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
            viewModel.transactionType = 1
            packageText.text = "SOLD TO"

            val yearStr = yearSpinner.selectedItem as String
            val year = yearStr.toInt()
            val month = monthSpinner.selectedItemPosition
            val cal = Calendar.getInstance()
            cal.set(year, month, 1, 0, 0 ,0)
            val start = cal.time
            cal.set(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59)
            val end = cal.time

            viewModel.loadStockistTransaction(selectedEmail, start, end)
        }

        searchBtn.setOnClickListener {
            val yearStr = yearSpinner.selectedItem as String
            val year = yearStr.toInt()
            val month = monthSpinner.selectedItemPosition
            val cal = Calendar.getInstance()
            cal.set(year, month, 1, 0, 0 ,0)
            val start = cal.time
            cal.set(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59)
            val end = cal.time

            viewModel.loadStockistTransaction(selectedEmail, start, end)
        }

        topupTab.performClick()

        return root
    }
}