package com.goldgainer.admin.ui.stockist

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch

class StockistCurrencyDigitalFragment : Fragment() {

    private lateinit var viewModel: StockistSettingViewModel

    private lateinit var currencyAdapter: DigitalCurrencyAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StockistSettingViewModel::class.java)
        viewModel.reset()
        val root = inflater.inflate(R.layout.fragment_stockist_currency_digital, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val fiatBtn = root.findViewById<MaterialButton>(R.id.fiat_button)
        val tickerEdit = root.findViewById<EditText>(R.id.ticker_edit)
        val addressEdit = root.findViewById<EditText>(R.id.address_edit)

        val saveBtn = root.findViewById<Button>(R.id.save_button)
        val digitalList = root.findViewById<ListView>(R.id.digital_list)

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        currencyAdapter = DigitalCurrencyAdapter(requireContext(), arrayListOf(), deleteClicked = { currencyId ->
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                .setTitle("Digital Currency")
                .setMessage("Delete this currency?")
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    viewLifecycleOwner.lifecycleScope.launch {
                        val success = viewModel.deleteDigitalCurrency(currencyId)
                        if (success) {
                            Toast.makeText(context, "Currency Deleted", Toast.LENGTH_LONG).show()
                            viewModel.loadDigitalCurrencies()
                        }
                    }
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        })

        digitalList.adapter = currencyAdapter

        viewModel.digitalCurrencies.observe(viewLifecycleOwner, Observer { currencies ->
            currencyAdapter.clear()
            currencies.forEach {
                currencyAdapter.add(it)
            }
            currencyAdapter.notifyDataSetChanged()
        })

        fiatBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        saveBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                .setTitle("Digital Currency")
                .setMessage("Create new digital currency address?")
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    viewLifecycleOwner.lifecycleScope.launch {
                        val success = viewModel.saveDigitalCurrency(
                            tickerEdit.text.toString(),
                            addressEdit.text.toString(),
                            null)
                        if (success) {
                            Toast.makeText(context, "Currency Created", Toast.LENGTH_LONG).show()
                            viewModel.loadDigitalCurrencies()
                        }
                    }
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        viewModel.loadDigitalCurrencies()

        return root
    }
}