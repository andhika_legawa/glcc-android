package com.goldgainer.admin.ui.stockist

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.admin.R
import com.goldgainer.admin.toDigitalCurrency
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch

class StockistPackageDetailFragment : Fragment() {

    private lateinit var viewModel: StockistSettingViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StockistSettingViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_stockist_package_detail, container, false)

        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)
        val nodeTypeEdit = root.findViewById<EditText>(R.id.nodetype_edit)
        val priceEdit = root.findViewById<EditText>(R.id.price_edit)
        val currencyEdit = root.findViewById<EditText>(R.id.currency_edit)
        val amountEdit = root.findViewById<EditText>(R.id.amount_edit)

        val cancelBtn = root.findViewById<Button>(R.id.cancel_button)
        val deleteBtn = root.findViewById<Button>(R.id.delete_button)
        val updateBtn = root.findViewById<Button>(R.id.update_button)

        arguments?.let {
            val packageId = it.getString("packageId", "")
            viewModel.loadVoucherPackage(packageId)
        }

        viewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        viewModel.selectedVoucherPackage.observe(viewLifecycleOwner, Observer { voucherPackage ->
            val vpackage = voucherPackage?:return@Observer
            nodeTypeEdit.setText("${vpackage.nodeType} Node")
            priceEdit.setText(vpackage.price.toBigDecimal().toPlainString())
            currencyEdit.setText(vpackage.currency)
            amountEdit.setText(vpackage.amount.toString())
        })

        cancelBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        deleteBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                .setTitle("Delete Voucher Package")
                .setMessage("Delete voucher package?")
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    viewLifecycleOwner.lifecycleScope.launch {
                        val voucherPackage = viewModel.selectedVoucherPackage.value?:return@launch
                        val success = viewModel.deleteVoucherPackage(voucherPackage.id)
                        if (success) {
                            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                                .setTitle("Info")
                                .setMessage("Voucher deleted")
                                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                                    findNavController().popBackStack()
                                }
                                .setCancelable(false)
                                .show()
                        }
                    }
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        updateBtn.setOnClickListener {
            val price = priceEdit.text.toString().toDouble()
            val amount = amountEdit.text.toString().toInt()
            viewLifecycleOwner.lifecycleScope.launch {
                val voucherPackage = viewModel.selectedVoucherPackage.value?:return@launch
                val success = viewModel.saveVoucherPackage(
                    voucherPackage.networkType, voucherPackage.networkNumber,
                    voucherPackage.nodeType, price, voucherPackage.currency, amount, voucherPackage.id)
                if (success) {
                    Toast.makeText(context, "Package Updated", Toast.LENGTH_LONG).show()
                    viewModel.getVoucherPackages()
                }
            }
        }

        return root
    }
}