package com.goldgainer.admin.ui.withdraw

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.admin.R
import com.goldgainer.admin.toCurrency
import com.goldgainer.admin.toDigitalCurrency
import com.goldgainer.glcclib.data.model.Withdraw
import java.text.SimpleDateFormat

class WithdrawPagingAdapter(private val listener:(Withdraw) -> Unit): PagingDataAdapter<Withdraw, WithdrawPagingAdapter.WithdrawViewHolder>(Companion) {

    companion object : DiffUtil.ItemCallback<Withdraw>() {
        override fun areItemsTheSame(oldItem: Withdraw, newItem: Withdraw): Boolean {
            return oldItem.owner == newItem.owner
        }

        override fun areContentsTheSame(oldItem: Withdraw, newItem: Withdraw): Boolean {
            return oldItem == newItem
        }
    }

    class WithdrawViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onBindViewHolder(holder: WithdrawViewHolder, position: Int) {
        val withdraw = getItem(position) as Withdraw
        val email = holder.itemView.findViewById<TextView>(R.id.email_textview)
        val fullname = holder.itemView.findViewById<TextView>(R.id.name_textview)
        val date = holder.itemView.findViewById<TextView>(R.id.date_textview)
        val status = holder.itemView.findViewById<TextView>(R.id.status_textview)
        email.text = ""
        email.setTypeface(null, Typeface.ITALIC)
        fullname.text = withdraw.owner
        if (withdraw.subtype == "GC") {
            status.text = String.format("%.8f GLC", withdraw.savingGoldcoin)
        } else if (withdraw.subtype == "PG") {
            status.text = String.format("%.1f Grams", withdraw.savingGold)
        } else {
            status.text = withdraw.amountReceived.toCurrency("IDR")
        }
        val sdf = SimpleDateFormat("MMM dd, yyy")
        date.text = sdf.format(withdraw.requestDate).toString()
        date.setTypeface(null, Typeface.ITALIC)

        holder.itemView.setOnClickListener { listener(withdraw) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WithdrawViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.partials_withdraw_item, parent, false) as View
        return WithdrawViewHolder(view)
    }
}