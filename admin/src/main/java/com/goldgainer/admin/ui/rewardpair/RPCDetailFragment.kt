package com.goldgainer.admin.ui.rewardpair

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.github.dhaval2404.imagepicker.ImagePicker
import com.goldgainer.admin.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.squareup.picasso.Picasso

class RPCDetailFragment : Fragment() {

    private lateinit var viewModel: RPCViewModel
    private lateinit var documentImage: ImageView
    private lateinit var selfieImage: ImageView
    var pickingImage:String = "NONE"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(RPCViewModel::class.java)
        viewModel.reset()
        val root = inflater.inflate(R.layout.fragment_rpcdetail, container, false)

        val pairStringText = root.findViewById<TextView>(R.id.pair_text)
        val statusText = root.findViewById<TextView>(R.id.status_text)
        val nameText = root.findViewById<TextView>(R.id.name_text)
        val nationalIdText = root.findViewById<TextView>(R.id.nationalid_text)
        val emailText = root.findViewById<TextView>(R.id.email_text)
        val mobilePhoneText = root.findViewById<TextView>(R.id.phone_text)
        val pictureLayout = root.findViewById<LinearLayout>(R.id.picture_layout)
        val instructionText = root.findViewById<TextView>(R.id.instruction_text)
        val pendingLayout = root.findViewById<ConstraintLayout>(R.id.pending_layout)
        val approveLayout = root.findViewById<LinearLayout>(R.id.approve_layout)
        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)

        val backBtn = root.findViewById<Button>(R.id.back_btn)
        val rejectBtn = root.findViewById<Button>(R.id.reject_btn)
        val approveBtn = root.findViewById<Button>(R.id.approve_btn)

        val documentBtn = root.findViewById<Button>(R.id.document_picture_btn)
        val selfieBtn = root.findViewById<Button>(R.id.selfie_picture_btn)
        val proceedBtn = root.findViewById<Button>(R.id.proceed_btn)
        val returnBtn = root.findViewById<Button>(R.id.return_btn)

        viewModel.isBusy.observe(viewLifecycleOwner, {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, {
            val msg = it?:return@observe
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })

        arguments?.let {
            val redeemId = it.getString("id")
            val email = it.getString("email")
            viewModel.loadRedeem(redeemId, email)
        }

        viewModel.currentRedeem.observe(viewLifecycleOwner, {
            val redeem = it?:return@observe
            pairStringText.text = redeem.pairString
            statusText.text = redeem.status
            nameText.text = redeem.name
            nationalIdText.text = redeem.nationalId
            emailText.text = redeem.email
            mobilePhoneText.text = redeem.mobilePhone

            if (redeem.status == "PENDING") {
                pendingLayout.visibility = View.VISIBLE
                returnBtn.visibility = View.GONE
                instructionText.text = "Press Approve Button if reward is available for this member."
            } else {
                pendingLayout.visibility = View.GONE
                returnBtn.visibility = View.VISIBLE
            }

            if (redeem.status == "APPROVED") {
                approveLayout.visibility = View.VISIBLE
                pictureLayout.visibility = View.VISIBLE
                documentBtn.visibility = View.GONE
                selfieBtn.visibility = View.GONE
                instructionText.text = "Capture below information for proof of reward handover for above member."
            } else {
                pictureLayout.visibility = View.GONE
            }

            if (redeem.status == "RECEIVED") {
                approveLayout.visibility = View.VISIBLE
                proceedBtn.visibility = View.GONE
                documentBtn.visibility = View.VISIBLE
                selfieBtn.visibility = View.VISIBLE
            }
        })

        rejectBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                .setTitle("Reject Request")
                .setMessage("Are you sure you want to reject?")
                .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
                    viewModel.rejectRedeem()
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        approveBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                .setTitle("Approve Request")
                .setMessage("Are you sure you want to approve?")
                .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
                    viewModel.approveRedeem()
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        proceedBtn.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCCAdmin_Dialog_Alert)
                .setTitle("Proceed Request")
                .setMessage("Are you sure all the captured are valid?")
                .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
                    viewModel.receiveRedeem()
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        backBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        returnBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        documentBtn.setOnClickListener {
            viewModel.docImageUri?.let {
                val promotionFragment = BigImageDialog(it)
                promotionFragment.show(parentFragmentManager, "fullimage")
            }
        }

        selfieBtn.setOnClickListener {
            viewModel.selfImageUri?.let {
                val promotionFragment = BigImageDialog(it)
                promotionFragment.show(parentFragmentManager, "fullimage")
            }
        }

        documentImage = root.findViewById(R.id.document_image)
        val documentCaptureBtn = root.findViewById<ImageButton>(R.id.document_capture_btn)
        documentCaptureBtn.setOnClickListener {
            pickingImage = "DOC"
            ImagePicker.with(this)
                .crop()
                .compress(512)
                .start()
        }

        selfieImage = root.findViewById(R.id.selfie_image)
        val selfieCaptureBtn = root.findViewById<ImageButton>(R.id.selfie_capture_btn)
        selfieCaptureBtn.setOnClickListener {
            pickingImage = "SELF"
            ImagePicker.with(this)
                .crop()
                .compress(512)
                .start()
        }
        return root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val uri: Uri = data?.data!!
            if (pickingImage == "DOC") {
                documentImage.visibility = View.VISIBLE
                documentImage.setImageURI(uri)
                viewModel.docImageUri = uri
            } else if (pickingImage == "SELF") {
                selfieImage.visibility = View.VISIBLE
                selfieImage.setImageURI(uri)
                viewModel.selfImageUri = uri
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(requireContext(), ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Picker Canceled", Toast.LENGTH_SHORT).show()
        }
        pickingImage = "NONE"
    }

    class BigImageDialog(private val imageUri: Uri) : DialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val root = inflater.inflate(R.layout.partials_image_dialog, container, false)
            val bigImage = root.findViewById<ImageView>(R.id.full_image)
            if (imageUri.scheme != null && imageUri.scheme!!.startsWith("http")) {
                Picasso.get().load(imageUri).into(bigImage)
            } else {
                bigImage.setImageURI(imageUri)
            }

            return root
        }
    }
}