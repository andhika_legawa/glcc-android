package com.goldgainer.admin.ui.withdraw

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.goldgainer.glcclib.data.model.Withdraw
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.tasks.await

class WithdrawPagingSource(private val db: FirebaseFirestore) : PagingSource<QuerySnapshot, Withdraw>() {

    private val query = db.collection("withdraws_req").whereEqualTo("status", "SUBMITTED").orderBy("requestDate", Query.Direction.ASCENDING)

    private val limit = 15L

    override suspend fun load(params: LoadParams<QuerySnapshot>): LoadResult<QuerySnapshot, Withdraw> {
        return try {
            val currentPage = params.key ?: query.limit(limit).get().await()

            val lastDoc = currentPage.documents[currentPage.size() - 1]
            val nextPage = query.limit(limit).startAfter(lastDoc).get().await()

            LoadResult.Page(
                data = currentPage.toObjects(Withdraw::class.java),
                prevKey = null,
                nextKey = nextPage
            )
        } catch(e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<QuerySnapshot, Withdraw>): QuerySnapshot? {
        return null
    }

}