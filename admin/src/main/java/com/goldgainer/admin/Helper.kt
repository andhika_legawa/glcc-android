package com.goldgainer.admin

import android.util.Log
import androidx.preference.PreferenceManager
import com.goldgainer.glcclib.data.FirebaseDataSource
import com.goldgainer.glcclib.data.model.Admin
import com.goldgainer.glcclib.data.model.GlobalSetting
import com.google.firebase.auth.FirebaseAuth

class Helper {
    private var _currentGlobalSettings : List<GlobalSetting> = listOf()

    suspend fun getGlobalSettings(name:String, default: Any) : Any? {
        val settings = getGlobalSettings()
        if (settings.isNotEmpty()) {
            return try {
                val setting = settings.first { it -> it.name == name }
                if ( default is Int){
                    setting.value.toString().toInt()
                }else{
                    setting.value.toString()
                }
            }catch (e: Exception){
                Log.d("HELPER", e.message.toString())
                default
            }
        }
        return default
    }

    suspend fun getGlobalSettingsArr(name:String) : Array<String> {
        val settings = getGlobalSettings()
        if (settings.isNotEmpty()) {
            return try {
                val setting = settings.first { it -> it.name == name }
                setting.valueArr.toTypedArray()
            }catch (e: Exception){
                Log.d("HELPER", e.message.toString())
                arrayOf()
            }
        }
        return arrayOf()
    }

    private suspend fun getGlobalSettings(): List<GlobalSetting> {
        if ( _currentGlobalSettings.isEmpty() ){
            val firebaseDataSource = FirebaseDataSource()
            _currentGlobalSettings = firebaseDataSource.getGlobalSettings()
        }
        return _currentGlobalSettings
    }

    companion object {
        fun getLocalAdminUser() : Admin {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(GLCCAdminApplication.getAppContext())
            val firebaseAuth = FirebaseAuth.getInstance()
            val currentUser = firebaseAuth.currentUser
            if (currentUser != null) {
                return Admin(
                    email = currentUser.email?:""
                )
            }
            return Admin(
                email = sharedPref.getString("email", "")!!
            )
        }
        fun setLocalAdminUser(admin: Admin) {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(GLCCAdminApplication.getAppContext())
            with (sharedPref.edit()) {
                putString("email", admin.email)
                apply()
            }
        }
    }
}