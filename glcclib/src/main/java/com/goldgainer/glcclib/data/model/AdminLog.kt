package com.goldgainer.glcclib.data.model

import java.util.*

data class AdminLog (
    var createdDate: Date? = null,
    var admin: String = "",
    var module: String = "",
    var action: String = "",
    var target: String? = null
)