package com.goldgainer.glcclib.data.model

import java.util.*

data class Withdraw (
    var trxId:String = "",
    var owner:String = "",
	var networkId:String = "",
	var networkNumber:Int = 0,
	var networkType:String = "",
	var networkCategory:String = "",
    var requestDate:Date = Date(),
    var type:String = "",
    var subtype:String = "",
	var available:Double = 0.0,
    var amount:Double = 0.0,
	var savingGold:Double = 0.0,
    var savingGoldValue:Double = 0.0,
	var savingGoldcoin:Double = 0.0,
    var savingGoldcoinValue:Double = 0.0,
    var transferFee:Double = 0.0,
    var adminFee:Double = 0.0,
    var tax:Double = 0.0,
	var taxPercent:Double = 0.0,
    var amountReceived:Double = 0.0,
    var pin:String = "",
	var pinExpired:Date? = null,
    var status:String = "",
	var bankName:String? = "",
	var bankAccount:String? = "",
	var bankNumber:String? = "",
	var processedDate:Date? = null,
	var processedBy:String? = null,
	var transferMethod:String? = null,
	var transferId:String? = null,
	var vanumber:String? = null,
	var glcWallet:String? = "",
	var usdtWallet:String? = ""
)
