package com.goldgainer.glcclib.data.model

import com.google.firebase.firestore.DocumentId

data class GlobalSetting (
    @DocumentId
    var id: String = "",
    var name: String = "",
    var value: Any? = null,
	var valueArr: List<String> = listOf()
)