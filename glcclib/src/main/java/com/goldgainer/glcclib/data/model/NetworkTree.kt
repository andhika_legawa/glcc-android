package com.goldgainer.glcclib.data.model

data class NetworkTree(
    val root: Network,
    val platinumTrainers: List<Network>,
    val goldTrainers: List<Network>
)