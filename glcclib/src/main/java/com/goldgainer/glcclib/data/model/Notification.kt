package com.goldgainer.glcclib.data.model

import java.util.*

data class Notification (
    var date: Date = Date(),
    var email: String = "",
    var title: String = "",
    var message: String = "",
    var type: String = "",
    var status: String = "",
    var networkId: String = "",
    var networkonhold: Network? = null
)