package com.goldgainer.glcclib.data.model

import com.google.firebase.firestore.DocumentId
import java.util.*

data class RewardPairRedeem (
    @DocumentId
    var id: String = "",
    var pairString: String = "0:0",
    var networkType: String = "I",
    var networkNumber: Int = 1,
    var networkId: String = "0",
    var isRedeemed: Boolean = false,
    var pairLeft: Long = 0,
    var pairRight: Long = 0,
    var name: String? = "",
    var email: String = "",
    var nationalId: String? = "",
    var mobilePhone: String? = "",
    var status: String = "",
    var handoverPhoto: String? = null,
    var selfiePhoto: String? = null,
    var requestDate: Date? = null,
    var approveDate: Date? = null,
    var redeemDate: Date? = null,
) {
    fun getIsRedeemed(): Boolean? {
        return isRedeemed
    }
}