package com.goldgainer.glcclib.data.model

import com.google.firebase.firestore.DocumentId

data class NetworkCategory (
        @DocumentId
        var id : String = "",
        var name: String = "",
        var description: String = "",
        var type: String = "",
        var currency: String = "",
        var number: Int = 0,
        var nodevalue: Int = 0,
        var locked: Boolean = true,
        var active: Boolean = true,
        var filled: Network? = null,
        var onhold: Network? = null
        )