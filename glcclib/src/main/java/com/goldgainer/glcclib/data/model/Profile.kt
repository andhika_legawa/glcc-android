package com.goldgainer.glcclib.data.model

import java.util.*

data class Profile (
	var owner: String? = null,
	var status: String = "EDIT",
	var statusNote: String = "",
	var adminNote: String = "",
	var lastUpdatedDate: Date? = null,
	var verifiedDate: Date? = null,
	var fullname: String? = null,
	var address: String? = null,
	var phone: String? = null,
	var nationalId: String? = null,
	var taxId: String? = null,
	var bankName: String? = null,
	var bankAccount: String? = null,
	var bankNumber: String? = null,
	var glcWallet: String? = null,
	var usdtWallet: String? = null,
	var successorFullname: String? = null,
	var successorRelation: String? = null,
	var successorPhone: String? = null,
	var successorEmail: String? = null,
	var nationalIdPhoto: String? = null,
	var selfiePhoto: String? = null,
	var virtualAccount: String? = null
)
