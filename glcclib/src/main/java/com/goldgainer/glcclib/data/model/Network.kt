package com.goldgainer.glcclib.data.model

import java.util.Date

data class Network (
        var category: String = "",
        var owner: String = "",
        var ownerName: String = "",
        var referralId: String = "",
        var type: String = "I",
        var number: Int = 1,
        var sponsorId: String? = "",
        var trainerId: String? = "",
        var binaryPosition: Int = 0,
        var binaryState: Int = 0,
        var createdDate: Date = Date(),
        var lastUpdatedDate: Date? = null,
        var activated: Boolean = false
)