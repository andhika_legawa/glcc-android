package com.goldgainer.glcclib.data.model

import com.google.firebase.firestore.DocumentId
import java.util.*

data class Node (
        @DocumentId
        var id: String = "",
        var owner:String = "",
        var referralId: String = "",
        var type: String = "I",
        var value: Int = 0,
        var acquisitionDate: Date? = null
)