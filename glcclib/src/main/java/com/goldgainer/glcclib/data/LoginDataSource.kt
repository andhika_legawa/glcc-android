package com.goldgainer.glcclib.data

import com.goldgainer.glcclib.data.model.LoggedInUser
import com.goldgainer.glcclib.data.model.UserData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import kotlinx.coroutines.tasks.await

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    private val firebaseAuth = FirebaseAuth.getInstance()

    suspend fun login(email: String, password: String): Result<LoggedInUser> {
        return try {
            val data = firebaseAuth.signInWithEmailAndPassword(email, password).await()
            Result.Success(LoggedInUser(userId = data?.user?.uid, displayName = data?.user?.displayName, email = data?.user?.email, emailVerified = data?.user?.isEmailVerified?:false))
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    fun logout(): Result<String> {
        return try {
            firebaseAuth.signOut()
            Result.Success("Logout Success")
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    suspend fun register(email: String, password: String, username: String): Result<LoggedInUser>{
        return try {
            val data = firebaseAuth.createUserWithEmailAndPassword(email, password).await()
            data.user?.updateProfile(userProfileChangeRequest{
                displayName = username
            })
            data.user?.sendEmailVerification()?.await()
            Result.Success(LoggedInUser(userId = data?.user?.uid, displayName = data?.user?.displayName, email = data?.user?.email, emailVerified = data?.user?.isEmailVerified?:false))
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    fun getInfo(): Result<UserData>{
        if (firebaseAuth.currentUser == null){
            return Result.Error(Exception("No current user"))
        }

        val user = UserData()
        firebaseAuth.currentUser?.let {
            user.userId = it.uid
            user.email = it.email
            user.name = it.displayName
        }

        return Result.Success(user)
    }

    suspend fun resetPassword(email: String): Result<String> {
        return try {
            firebaseAuth.sendPasswordResetEmail(email).await()
            Result.Success("Password Reset Sent")
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    suspend fun resendVerification(): Result<String> {
        try {
            if (firebaseAuth.currentUser == null){
                return Result.Error(Exception("No current user"))
            }
            firebaseAuth.currentUser?.sendEmailVerification()?.await()
            return Result.Success("Verification email sent, check your inbox")
        }catch (e: Exception) {
            return Result.Error(e)
        }
    }
}