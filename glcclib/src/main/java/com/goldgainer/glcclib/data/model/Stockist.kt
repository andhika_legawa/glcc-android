package com.goldgainer.glcclib.data.model

import java.util.*

data class Stockist (
    var owner: String ="",
    var username: String ="",
    var province: String ="",
    var city: String ="",
    var whatsapp: String ="",
    var telegram: String ="",
    var type: Int = 0,
    var status: Int = 0,
    var registerDate: Date? = null,
    var updatedDate: Date? = null,
    var joinedDate: Date? = null
)