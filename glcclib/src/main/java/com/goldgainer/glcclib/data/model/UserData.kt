package com.goldgainer.glcclib.data.model

data class UserData (
    var userId: String? = "",
    var name: String? = "",
    var email: String? = "",
    var country: String? = "",
    var password: String? = "",
    var verified: Boolean = false
)