package com.goldgainer.glcclib.data.model

import java.util.*

data class Voucher(
    val code : String = "",
    val description: String = "",
    val type: String = "",
    val value: Int = 0,
    val fee: Int = 0,
    val currency: String = "",
    val quantity:Int = 0,
    val redeemedDate: Date? = null,
    val redeemedBy: String? = null
)