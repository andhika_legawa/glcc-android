package com.goldgainer.glcclib.data.model

import com.google.firebase.firestore.DocumentId

data class DigitalCurrency (
    @DocumentId
    var id: String = "",
    val name: String = "",
    val address: String = "",
    val isDefault: Boolean = false
)