package com.goldgainer.glcclib.data.model

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class LoggedInUser (
        val userId: String? = null,
        val displayName: String? = null,
        val email: String? = null,
        val emailVerified: Boolean = false
)