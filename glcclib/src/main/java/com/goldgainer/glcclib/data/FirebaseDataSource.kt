package com.goldgainer.glcclib.data

import android.util.Log
import com.goldgainer.glcclib.data.model.*
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.util.*

class FirebaseDataSource {

    private val db = Firebase.firestore

    suspend fun getGlobalSettings(): List<GlobalSetting>{
        return try{
            val data = db.collection("settings").get().await()
            data.toObjects(GlobalSetting::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun saveGlobalSettings(name: String, value: String) : Boolean{
        return try {
            val data = db.collection("settings").whereEqualTo("name", name).get().await()
            val settings = data.toObjects(GlobalSetting::class.java)
            settings.forEach {
                if (it.name == name) {
                    it.value = value
                    db.collection("settings").document(it.id).set(it).await()
                    return true
                }
            }
            if (settings.isEmpty()) {
                val newSetting = GlobalSetting(
                    name = name,
                    value = value
                )
                db.collection("settings").add(newSetting).await()
                return true
            }
            return false
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            return false
        }
    }

    suspend fun saveUserDetail(user: UserData) : Boolean {
        return try {
            val data = db.collection("users")
                .document(user.email!!)
                .set(user)
                .await()
            true
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getUserDetail(email: String) : UserData? {
        return try {
            val data = db.collection("users")
                .document(email)
                .get()
                .await()
            data.toObject(UserData::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun checkUsernameExist(username: String): Boolean{
        return try{
            val data = db.collection("users").whereEqualTo("name", username).get().await()
            data.size() > 0
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            true
        }
    }

    suspend fun checkEmailExist(email: String) : Boolean{
        return try{
            val data = db.collection("users").whereEqualTo("email", email).get().await()
            data.size() > 0
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            true
        }
    }

    suspend fun createNetwork(network: Network) : Boolean {
        return try {
            network.lastUpdatedDate = Date()
            db.collection("creatednetworks").document(network.referralId).set(network).await()
            true
        }catch(e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getNetwork(referralId: String) : Network? {
        return try {
            val data = db.collection("networks").document(referralId).get().await()
            data.toObject(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun getNetworkFirstLevel(referralId: String) : List<Network> {
        return try {
            val data = db.collection("networks").whereEqualTo("trainerId", referralId).get().await()
            data.toObjects(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getOnHoldNetwork(referralId: String) : Network? {
        return try {
            val data = db.collection("creatednetworks").document(referralId).get().await()
            data.toObject(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun getOnHoldNetworks(owner: String) : List<Network> {
        return try {
            val data = db.collection("creatednetworks").whereEqualTo("owner", owner).whereEqualTo("activated", false).get().await()
            data.toObjects(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getUserNetworks(owner: String, type: String? = null) : List<Network> {
        return try {
            var query = db.collection("networks").whereEqualTo("owner", owner)
            if(type != null){
                query = query.whereEqualTo("type", type)
            }
            val data = query.get().await()
            data.toObjects(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getNetworkCategories(type: String) : List<NetworkCategory> {
        return try {
            val data = db.collection("networkcategories").whereEqualTo("type", type).get().await()
            data.toObjects(NetworkCategory::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getNodes(referralId: String) : List<Node> {
        return try{
            var data = db.collection("nodes").whereEqualTo("referralId", referralId).get().await()
            data.toObjects(Node::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getAllNodes(email: String) : List<Node> {
        return try{
            var data = db.collection("nodes").whereEqualTo("owner", email).get().await()
            data.toObjects(Node::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getNetworkTree(referralId: String) : NetworkTree? {
        return try{
            val network = getNetwork(referralId) ?: throw Exception("invalid referralId network")
            val leftNetwork = getTrainee(referralId, 0)
            val rightNetwork = getTrainee(referralId, 1)
            NetworkTree(network, leftNetwork, rightNetwork)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    private suspend fun getTrainee(trainerId: String, position: Int?) : List<Network> {
        try{
            var query = db.collection("networks").whereEqualTo("trainerId", trainerId)
            if (position != null){
                query = query.whereEqualTo("binaryPosition", position)
            }
            val data = query.get().await()
            val traineeList = data.toObjects(Network::class.java)
            if(traineeList.isNotEmpty()){
                val traineeNetworkList = mutableListOf<Network>()
                traineeNetworkList.addAll(traineeList)
                val traineeIterator = traineeList.iterator()
                while(traineeIterator.hasNext()){
                    val traineeNetwork = traineeIterator.next()
                    traineeNetworkList.addAll(getTrainee(traineeNetwork.referralId, null))
                }
                return traineeNetworkList.toList()
            }
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
        }
        return listOf()
    }

    suspend fun checkReferralExist(referralId: String, typeLock: String? = null, numberLock: Int? = null): Boolean {
        return try {
            var query = db.collection("networks")
                    .whereEqualTo("referralId", referralId)
            if (typeLock != null) {
                query = query.whereEqualTo("type", typeLock)
            }
            if (numberLock != null) {
                query = query.whereEqualTo("number", numberLock)
            }
            val data =query
                    .get()
                    .await()
            !data.isEmpty
        }catch (e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getVoucherByCode(code: String): Voucher? {
        return try {
            val data = db.collection("vouchers").whereEqualTo("code", code).get().await()
            data.first().toObject(Voucher::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun getTransaction(trxId: String): Transaction? {
        return try {
            val data = db.collection("transactions").document(trxId).get().await()
            data.toObject(Transaction::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun getCreatedTransaction(trxId: String): Transaction? {
        return try {
            val data = db.collection("createdtransactions").document(trxId).get().await()
            data.toObject(Transaction::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun saveCreatedTransaction(transaction: Transaction): Boolean {
        return try {
            transaction.lastUpdatedDate = Date()
            db.collection("createdtransactions").document(transaction.id).set(transaction).await()
            true
        }catch(e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getLatestSnapshot(referralId: String): Snapshot? {
        return try {
            val data = db.collection("snapshots").whereEqualTo("referralId", referralId).orderBy("date", Query.Direction.DESCENDING).limit(1).get().await()
            data.first().toObject(Snapshot::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun getXenditCharge(id: String): XenditCharge? {
        val data = db.collection("xenditcharges").document(id).get().await()
        return data.toObject(XenditCharge::class.java)
    }

    suspend fun getLatestNotifications(email: String): List<Notification> {
        return try {
            val data = db.collection("notifications").whereEqualTo("email", email).orderBy("date", Query.Direction.DESCENDING).limit(10).get().await()
            data.toObjects(Notification::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun isBlacklisted(email: String): Boolean {
        return db.collection("blacklist").document(email).get().await().exists()
    }

    suspend fun getProfile(email: String): Profile? {
        return try {
            val data = db.collection("profiles").document(email).get().await()
            if(!data.exists()) {
                return Profile(owner = email)
            }
            data.toObject(Profile::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun saveProfile(email: String, profile: Profile): Boolean {
        return try {
            profile.lastUpdatedDate = Date()
            db.collection("profiles").document(email).set(profile).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getAdmin(email: String): Admin? {
        try {
            val data = db.collection("adminusers").document(email).get().await()
            if (data.exists()) {
                return data.toObject(Admin::class.java)
            }
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
        }
        return null
    }

    suspend fun getProfiles(last: Profile?, limit: Long): List<Profile> {
        try {
            return if (last != null) {
                val lastDoc = db.collection("profiles").document(last.owner!!).get().await()
                val data = db.collection("profiles").orderBy("lastUpdatedDate").startAt(lastDoc).limit(limit).get().await()
                data.toObjects(Profile::class.java)
            } else {
                val data = db.collection("profiles").orderBy("lastUpdatedDate").limit(limit).get().await()
                data.toObjects(Profile::class.java)
            }
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
        }
        return listOf()
    }

    suspend fun getWithdrawRequest(email: String): Withdraw? {
        return try {
            val data = db.collection("withdraws_req").document(email).get().await()
            data.toObject(Withdraw::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun saveWithdrawRequest(email:String, withdraw: Withdraw): Boolean {
        return try {
            withdraw.requestDate = Date()
            db.collection("withdraws_req").document(email).set(withdraw).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun deleteWithdrawRequest(email:String): Boolean {
        return try {
            db.collection("withdraws_req").document(email).delete().await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getWithdraws(email: String, referralId: String): List<Withdraw> {
        return try {
            val data = db.collection("withdraws").whereEqualTo("owner", email).whereEqualTo("networkId", referralId).get().await()
            data.toObjects(Withdraw::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun addWithdraws(withdraw: Withdraw) : Boolean {
        return try {
            val data = db.collection("withdraws").add(withdraw).await()
            true
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getStockist(email: String) : Stockist? {
        return try {
            val data = db.collection("stockists").document(email).get().await()
            data.toObject(Stockist::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun saveStockist(email: String, stockist: Stockist) : Boolean {
        return try {
            stockist.updatedDate = Date()
            db.collection("stockists").document(email).set(stockist).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getStockists(email: String?) : List<Stockist> {
        return try {
            var query = db.collection("stockists").orderBy("registerDate", Query.Direction.DESCENDING)
            if (email != null) {
                query = query.whereEqualTo("owner", email)
            }
            val data = query.get().await()
            data.toObjects(Stockist::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getVoucherPackages() : List<VoucherPackage> {
        return try {
            val data = db.collection("voucher_packages").get().await()
            data.toObjects(VoucherPackage::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getVoucherPackage(packageId: String) : VoucherPackage? {
        return try {
            val data = db.collection("voucher_packages").document(packageId).get().await()
            data.toObject(VoucherPackage::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun saveVoucherPackage(voucher: VoucherPackage) : Boolean {
        return try {
            if(voucher.id.isNotEmpty()) {
                db.collection("voucher_packages").document(voucher.id).set(voucher).await()
            } else {
                db.collection("voucher_packages").add(voucher).await()
            }
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun deleteVoucherPackage(id: String) : Boolean {
        return try {
            db.collection("voucher_packages").document(id).delete().await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getDigitalCurrencies() : List<DigitalCurrency> {
        return try {
            val data = db.collection("digital_currencies").get().await()
            data.toObjects(DigitalCurrency::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun saveDigitalCurrency(currency: DigitalCurrency) : Boolean {
        return try {
            if(currency.id.isNotEmpty()) {
                db.collection("digital_currencies").document(currency.id).set(currency).await()
            } else {
                db.collection("digital_currencies").add(currency).await()
            }
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun deleteDigitalCurrency(id: String) : Boolean {
        return try {
            db.collection("digital_currencies").document(id).delete().await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun saveStockistTransaction(transaction: StockistTransaction) : Boolean {
        return try {
            if (transaction.id.isNotEmpty()) {
                db.collection("stockist_transactions").document(transaction.id).set(transaction).await()
            } else {
                db.collection("stockist_transactions").add(transaction).await()
            }
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getStockistTransaction(email: String, start: Date?, end: Date?) : List<StockistTransaction> {
        return try {
            var query = db.collection("stockist_transactions")
                .whereEqualTo("owner", email)
            if (start != null && end != null) {
                query = query.whereGreaterThanOrEqualTo("date", start)
                    .whereLessThanOrEqualTo("date", end)
            }
            val data = query.orderBy("date", Query.Direction.DESCENDING).get().await()
            data.toObjects(StockistTransaction::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getStockistTopupPaged(limit: Long, email: String?, status: String?) : List<StockistTransaction> {
        return try {
            var query = db.collection("stockist_transactions").whereEqualTo("type", 0)
            if (email != null) {
                query = query.whereEqualTo("owner", email)
            }
            if (status != null) {
                query = query.whereEqualTo("status", status)
            }
            val data = query.orderBy("date", Query.Direction.DESCENDING).limit(limit).get().await()
            data.toObjects(StockistTransaction::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getStockistSold(start: Date, end: Date, networkType: String, networkNumber: Int, email: String?) : List<StockistTransaction> {
        return try {
            var query = db.collection("stockist_transactions")
                .whereEqualTo("type", 1)
                .whereGreaterThanOrEqualTo("date", start)
                .whereLessThanOrEqualTo("date", end)
                .whereEqualTo("networkType", networkType)
                .whereEqualTo("networkNumber", networkNumber)
            if (email != null) {
                query = query.whereEqualTo("owner", email)
            }
            val data = query.orderBy("date", Query.Direction.DESCENDING).get().await()
            data.toObjects(StockistTransaction::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getTopupTransactions(email: String) : List<StockistTransaction> {
        return try {
            var query = db.collection("stockist_transactions")
                .whereEqualTo("type",0)
                .whereEqualTo("status", "APPROVED")
                .whereEqualTo("owner", email)
            val data = query.get().await()
            data.toObjects(StockistTransaction::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getSoldTransactions() : List<StockistTransaction> {
        return try {
            var query = db.collection("stockist_transactions")
                .whereEqualTo("type",1)
            val data = query.get().await()
            data.toObjects(StockistTransaction::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getStockistTransaction(id: String) : StockistTransaction? {
        return try {
            val data = db.collection("stockist_transactions").document(id).get().await()
            data.toObject(StockistTransaction::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun getStockistByLocation(province: String, city: String) : List<Stockist> {
        return try {
            val query = db.collection("stockists")
                .whereEqualTo("province", province)
                .whereEqualTo("city", city)
            val data = query.get().await()
            data.toObjects(Stockist::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getVouchersByStockist(email: String) : List<Voucher> {
        return try {
            val query = db.collection("vouchers")
                .whereEqualTo("generatedFor", email)
            val data = query.get().await()
            data.toObjects(Voucher::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun saveAdminLog(admin: String, module: String, action: String, target: String?) : Boolean {
        return try {
            val log = AdminLog(
                createdDate = Date(),
                admin = admin,
                module = module,
                action = action,
                target = target
            )
            db.collection("admin_log").add(log).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }

    suspend fun getAdminLogs(start: Date, end: Date, email: String?) : List<AdminLog> {
        return try {
            var query = db.collection("admin_log")
                .whereGreaterThanOrEqualTo("createdDate", start)
                .whereLessThanOrEqualTo("createdDate", end)
            if (email != null) {
                query = query.whereEqualTo("admin", email)
            }
            val data = query.orderBy("createdDate", Query.Direction.DESCENDING).get().await()
            data.toObjects(AdminLog::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            listOf()
        }
    }

    suspend fun getRPCRedeemById(redeemId: String) : RewardPairRedeem? {
        return try {
            val data = db.collection("rpc_redeem").document(redeemId).get().await()
            data.toObject(RewardPairRedeem::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun getRPCRedeemRequest(email: String) : RewardPairRedeem? {
        return try {
            val data = db.collection("rpc_redeem_req").document(email).get().await()
            data.toObject(RewardPairRedeem::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun processRPCRedeem(request: RewardPairRedeem) : String? {
        return try {
            val doc = db.collection("rpc_redeem").add(request).await()
            db.collection("rpc_redeem_req").document(request.email).delete().await()
            doc.id
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            null
        }
    }

    suspend fun saveRPCRedeem(request: RewardPairRedeem) : Boolean {
        return try {
            db.collection("rpc_redeem").document(request.id).set(request).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE",  e.message.toString())
            false
        }
    }
}