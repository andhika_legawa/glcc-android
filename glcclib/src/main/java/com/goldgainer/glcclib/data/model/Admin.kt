package com.goldgainer.glcclib.data.model

import com.google.firebase.firestore.DocumentId

data class Admin (
    @DocumentId
    var email: String = "",
    var profile : Boolean = false,
    var withdraw : Boolean = false,
    var stockist: Boolean = false,
    var voucher: Boolean = false,
    var zuper: Boolean = false,
    var rewardpair: Boolean = false,
)