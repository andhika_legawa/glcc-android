package com.goldgainer.glcclib.data.model

import com.google.firebase.firestore.DocumentId
import java.util.*

data class StockistTransaction (
    @DocumentId
    var id: String = "",
    var owner: String = "",
    var type: Int = 0,
    var status: String = "",
    var networkType: String = "",
    var networkNumber: Int = 0,
    var nodeType: Int = 0,
    var price: Double = 0.0,
    var total: Double = 0.0,
    var amount: Int = 0,
    var currency: String = "",
    var paymentPhoto: String? = null,
    var date: Date? = null,
    var soldTo: String = "",
    var reason: String? = null,
    var processedDate: Date? = null
)