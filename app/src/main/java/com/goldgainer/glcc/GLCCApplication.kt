package com.goldgainer.glcc

import android.app.Application
import android.content.Context

class GLCCApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        GLCCApplication.context = applicationContext
    }

    companion object {
        private var context: Context? = null

        fun getAppContext() : Context? {
            return GLCCApplication.context
        }
    }
}