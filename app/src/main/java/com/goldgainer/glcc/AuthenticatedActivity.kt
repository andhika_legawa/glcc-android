package com.goldgainer.glcc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.LoggedInUser
import com.goldgainer.glcc.ui.dashboard.PromotionFragment
import com.goldgainer.glcc.ui.login.LoginActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

open class AuthenticatedActivity : AppCompatActivity() {

    private lateinit var firebaseAuth : FirebaseAuth
    private var currentUser : FirebaseUser? = null

    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            firebaseAuth = FirebaseAuth.getInstance()
            currentUser = firebaseAuth.currentUser
        }catch (e:Exception) {
            Log.d("FIRESTORE", e.message.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        checkLogin()
    }

    private fun checkLogin(){
        if (currentUser == null || !currentUser!!.isEmailVerified) {
            doLogin()
        }else{
            checkVersion()
        }
    }

    private fun doLogin(){
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun checkVersion(){
        var versionIsSafe = true
        scope.launch {
            val helper = Helper()
            val latestVersion = helper.getGlobalSettings("latest_version", 1) as Int
            if (BuildConfig.VERSION_CODE < latestVersion) {
                versionIsSafe = false
                doLogin()
            }
            currentUser?.let {
                if (helper.isBlacklisted(it.email!!)) {
                    versionIsSafe = false
                    doLogin()
                }
            }
            if (versionIsSafe) {
                checkPromotion()
            }
        }
    }

    fun getCurrentUser(): LoggedInUser?{
        val user = currentUser
        if (user != null) {
            return LoggedInUser(userId = user.uid, displayName = user.displayName, email = user.email, emailVerified = user.isEmailVerified)
        }
        return null
    }

    private fun checkPromotion() {
        if (Helper.shouldShowPromotion()) {
            try {
                val fragmentManager = supportFragmentManager
                val promotionFragment = PromotionFragment()
                promotionFragment.show(fragmentManager, "promotion")
            }catch (e: Exception) {

            }
        }
    }

}