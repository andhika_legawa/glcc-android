package com.goldgainer.glcc.data.model

import com.google.firebase.auth.FirebaseUser

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class LoggedInUser (
        val userId: String? = null,
        val displayName: String? = null,
        val email: String? = null,
        val emailVerified: Boolean = false
)