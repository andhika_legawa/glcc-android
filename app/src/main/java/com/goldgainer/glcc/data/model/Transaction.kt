package com.goldgainer.glcc.data.model

import com.google.firebase.firestore.DocumentId
import java.util.*

data class Transaction (
        @DocumentId
        var id: String = "",
        var owner: String = "",
        var type: String = "",
        var data: Map<String, String> = mapOf(),
        var value: Long = 0,
        var status: String = "",
        var createdDate: Date = Date(),
        var lastUpdatedDate: Date? = null
)
