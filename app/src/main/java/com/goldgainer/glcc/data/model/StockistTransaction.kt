package com.goldgainer.glcc.data.model

import java.util.*

data class StockistTransaction (
    var owner: String = "",
    var type: Int = 0,
    var status: String = "",
    var networkType: String = "",
    var networkNumber: Int = 0,
    var nodeType: Int = 0,
    var price: Double = 0.0,
    var total: Double = 0.0,
    var amount: Int = 0,
    var currency: String = "",
    var paymentPhoto: String? = null,
    var date: Date? = null,
    var soldTo: String = ""
)