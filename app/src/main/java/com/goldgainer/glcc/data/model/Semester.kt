package com.goldgainer.glcc.data.model

data class Semester (
    var id:String = "",
    var order:Int = 0,
    var name:String = "",
    var courseTotal:Int = 0,
    var sksTotal:Int = 0,
    var courseList: List<Course> = arrayListOf()
)