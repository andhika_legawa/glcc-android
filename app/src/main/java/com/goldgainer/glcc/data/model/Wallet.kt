package com.goldgainer.glcc.data.model

data class Wallet (
	var referralId:String = "",
	var bonusSponsor:Double = 0.0,
	var tBonusSponsor:Double = 0.0,
	var bonusPairing:Double = 0.0,
	var tBonusPairing:Double = 0.0,
	var bonusRollup:Double = 0.0,
	var tBonusRollup:Double = 0.0,
	var savingGold:Double = 0.0,
	var savingGoldcoin:Double = 0.0
)
