package com.goldgainer.glcc.data.model

import java.util.*

data class RewardPairConnection (
    var pairString: String = "0:0",
    var networkType: String = "I",
    var networkNumber: Int = 1,
    var isAvailable: Boolean = true,
    var isRedeemed: Boolean = false,
    var isEligible: Boolean = false,
    var pairLeft: Int = 0,
    var pairRight: Int = 0,
    var redeemDate: Date? = null,
) {
    fun getIsAvailable(): Boolean? {
        return isAvailable
    }
}