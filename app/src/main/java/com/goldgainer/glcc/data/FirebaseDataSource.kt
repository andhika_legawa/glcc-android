package com.goldgainer.glcc.data

import android.util.Log
import com.goldgainer.glcc.data.model.*
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.util.*

class FirebaseDataSource {

    private val db = Firebase.firestore

    suspend fun getGlobalSettings(): List<GlobalSetting>{
        return try{
            val data = db.collection("settings").get().await()
            data.toObjects(GlobalSetting::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun saveUserDetail(user: UserData) : Boolean {
        return try {
            val data = db.collection("users")
                .document(user.email!!)
                .set(user)
                .await()
            true
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun getUserDetail(email: String) : UserData? {
        return try {
            val data = db.collection("users")
                .document(email)
                .get()
                .await()
            data.toObject(UserData::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun checkUsernameExist(username: String): Boolean{
        return try{
            val data = db.collection("users").whereEqualTo("name", username).get().await()
            data.size() > 0
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            true
        }
    }

    suspend fun checkEmailExist(email: String) : Boolean{
        return try{
            val data = db.collection("users").whereEqualTo("email", email).get().await()
            data.size() > 0
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            true
        }
    }

    suspend fun createNetwork(network: Network) : Boolean {
        return try {
            network.lastUpdatedDate = Date()
            db.collection("creatednetworks").document(network.referralId).set(network).await()
            true
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun getNetwork(referralId: String) : Network? {
        return try {
            val data = db.collection("networks").document(referralId).get().await()
            data.toObject(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun getNetworkFirstLevel(referralId: String) : List<Network> {
        return try {
            val data = db.collection("networks").whereEqualTo("trainerId", referralId).get().await()
            data.toObjects(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getOnHoldNetwork(referralId: String) : Network? {
        return try {
            val data = db.collection("creatednetworks").document(referralId).get().await()
            data.toObject(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun getOnHoldNetworks(owner: String) : List<Network> {
        return try {
            val data = db.collection("creatednetworks").whereEqualTo("owner", owner).whereEqualTo("activated", false).get().await()
            data.toObjects(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getUserNetworks(owner: String, type: String? = null) : List<Network> {
        return try {
            var query = db.collection("networks").whereEqualTo("owner", owner)
            if(type != null){
                query = query.whereEqualTo("type", type)
            }
            val data = query.get().await()
            data.toObjects(Network::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getNetworkCategories(type: String) : List<NetworkCategory> {
        return try {
            val data = db.collection("networkcategories").whereEqualTo("type", type).get().await()
            data.toObjects(NetworkCategory::class.java)
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getNodes(referralId: String) : List<Node> {
        return try{
            var data = db.collection("nodes").whereEqualTo("referralId", referralId).get().await()
            data.toObjects(Node::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getAllNodes(email: String) : List<Node> {
        return try{
            var data = db.collection("nodes").whereEqualTo("owner", email).get().await()
            data.toObjects(Node::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getNetworkTree(referralId: String) : NetworkTree? {
        return try{
            val network = getNetwork(referralId) ?: throw Exception("invalid referralId network")
            val leftNetwork = getTrainee(referralId, 0)
            val rightNetwork = getTrainee(referralId, 1)
            NetworkTree(network, leftNetwork, rightNetwork)
        }catch (e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    private suspend fun getTrainee(trainerId: String, position: Int?) : List<Network> {
        try{
            var query = db.collection("networks").whereEqualTo("trainerId", trainerId)
            if (position != null){
                query = query.whereEqualTo("binaryPosition", position)
            }
            val data = query.get().await()
            val traineeList = data.toObjects(Network::class.java)
            if(traineeList.isNotEmpty()){
                val traineeNetworkList = mutableListOf<Network>()
                traineeNetworkList.addAll(traineeList)
                val traineeIterator = traineeList.iterator()
                while(traineeIterator.hasNext()){
                    val traineeNetwork = traineeIterator.next()
                    traineeNetworkList.addAll(getTrainee(traineeNetwork.referralId, null))
                }
                return traineeNetworkList.toList()
            }
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
        }
        return listOf()
    }

    suspend fun checkReferralExist(referralId: String, typeLock: String? = null, numberLock: Int? = null): Boolean {
        return try {
            var query = db.collection("networks")
                    .whereEqualTo("referralId", referralId)
            if (typeLock != null) {
                query = query.whereEqualTo("type", typeLock)
            }
            if (numberLock != null) {
                query = query.whereEqualTo("number", numberLock)
            }
            val data =query
                    .get()
                    .await()
            !data.isEmpty
        }catch (e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun getVoucherByCode(code: String): Voucher? {
        return try {
            val data = db.collection("vouchers").whereEqualTo("code", code).get().await()
            data.first().toObject(Voucher::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun getTransaction(trxId: String): Transaction? {
        return try {
            val data = db.collection("transactions").document(trxId).get().await()
            data.toObject(Transaction::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun getCreatedTransaction(trxId: String): Transaction? {
        return try {
            val data = db.collection("createdtransactions").document(trxId).get().await()
            data.toObject(Transaction::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun saveCreatedTransaction(transaction: Transaction): Boolean {
        return try {
            transaction.lastUpdatedDate = Date()
            db.collection("createdtransactions").document(transaction.id).set(transaction).await()
            true
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun getLatestSnapshot(referralId: String): Snapshot? {
        return try {
            val data = db.collection("snapshots").whereEqualTo("referralId", referralId).orderBy("date", Query.Direction.DESCENDING).limit(1).get().await()
            data.first().toObject(Snapshot::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun getXenditCharge(id: String): XenditCharge? {
        val data = db.collection("xenditcharges").document(id).get().await()
        return data.toObject(XenditCharge::class.java)
    }

    suspend fun getLatestNotifications(email: String): List<Notification> {
        return try {
            val data = db.collection("notifications").whereEqualTo("email", email).orderBy("date", Query.Direction.DESCENDING).limit(10).get().await()
            data.toObjects(Notification::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun isBlacklisted(email: String): Boolean {
        return db.collection("blacklist").document(email).get().await().exists()
    }

    suspend fun getProfile(email: String): Profile? {
        return try {
            val data = db.collection("profiles").document(email).get().await()
            if(!data.exists()) {
                return Profile(owner = email)
            }
            data.toObject(Profile::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun saveProfile(email: String, profile: Profile): Boolean {
        return try {
            profile.lastUpdatedDate = Date()
            db.collection("profiles").document(email).set(profile).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun getWithdrawRequest(email: String): Withdraw? {
        return try {
            val data = db.collection("withdraws_req").document(email).get().await()
            data.toObject(Withdraw::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun saveWithdrawRequest(email:String, withdraw: Withdraw): Boolean {
        return try {
            withdraw.requestDate = Date()
            db.collection("withdraws_req").document(email).set(withdraw).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun deleteWithdrawRequest(email:String): Boolean {
        return try {
            db.collection("withdraws_req").document(email).delete().await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun getWithdraws(email: String, referralId: String): List<Withdraw> {
        return try {
            val data = db.collection("withdraws").whereEqualTo("owner", email).whereEqualTo("networkId", referralId).get().await()
            data.toObjects(Withdraw::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getWithdrawsPendingTax(email: String, referralId: String): List<Withdraw> {
        return try {
            val data = db.collection("withdraws_pendingtax").whereEqualTo("owner", email).whereEqualTo("networkId", referralId).get().await()
            data.toObjects(Withdraw::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getStockist(email: String) : Stockist? {
        return try {
            val data = db.collection("stockists").document(email).get().await()
            data.toObject(Stockist::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun saveStockist(email: String, stockist: Stockist) : Boolean {
        return try {
            stockist.updatedDate = Date()
            db.collection("stockists").document(email).set(stockist).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun getVoucherPackages() : List<VoucherPackage> {
        return try {
            val data = db.collection("voucher_packages").get().await()
            data.toObjects(VoucherPackage::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getDigitalCurrencies() : List<DigitalCurrency> {
        return try {
            val data = db.collection("digital_currencies").get().await()
            data.toObjects(DigitalCurrency::class.java)
        }catch (e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun saveStockistTransaction(transaction: StockistTransaction) : Boolean {
        return try {
            db.collection("stockist_transactions").add(transaction).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

    suspend fun getStockistTransaction(email: String, start: Date, end: Date) : List<StockistTransaction> {
        return try {
            val query = db.collection("stockist_transactions")
                .whereEqualTo("owner", email)
                .whereGreaterThanOrEqualTo("date", start)
                .whereLessThanOrEqualTo("date", end)
            val data = query.get().await()
            data.toObjects(StockistTransaction::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getStockistByLocation(province: String, city: String) : List<Stockist> {
        return try {
            val query = db.collection("stockists")
                .whereEqualTo("province", province)
                .whereEqualTo("city", city)
            val data = query.get().await()
            data.toObjects(Stockist::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getVouchersByStockist(email: String) : List<Voucher> {
        return try {
            val query = db.collection("vouchers")
                .whereEqualTo("generatedFor", email)
            val data = query.get().await()
            data.toObjects(Voucher::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getTransactionList(email: String, start: Date, end: Date): List<Transaction> {
        return try {
            val data = db.collection("transactions")
                .whereEqualTo("owner", email)
                .whereGreaterThanOrEqualTo("createdDate", start)
                .whereLessThanOrEqualTo("createdDate", end).get().await()
            data.toObjects(Transaction::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getCreatedTransactionList(email: String, start: Date, end: Date): List<Transaction> {
        return try {
            val data = db.collection("createdtransactions")
                .whereEqualTo("owner", email)
                .whereGreaterThanOrEqualTo("createdDate", start)
                .whereLessThanOrEqualTo("createdDate", end)
                .get().await()
            data.toObjects(Transaction::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getWithdrawList(email: String, start: Date, end: Date): List<Withdraw> {
        return try {
            val data = db.collection("withdraws")
                .whereEqualTo("owner", email)
                .whereGreaterThanOrEqualTo("requestDate", start)
                .whereLessThanOrEqualTo("requestDate", end)
                .get().await()
            data.toObjects(Withdraw::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getRewardPairIndonesia(): List<RewardPairConnection> {
        return try {
            val data = db.collection("rpc_setup_indonesia")
                .get().await()
            data.toObjects(RewardPairConnection::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getRPCRedeemList(email: String): List<RewardPairRedeem> {
        return try {
            val data = db.collection("rpc_redeem")
                .whereEqualTo("email", email)
                .get().await()
            data.toObjects(RewardPairRedeem::class.java)
        }catch(e: Exception){
            Log.d("FIRESTORE", e.message.toString())
            listOf()
        }
    }

    suspend fun getRPCRedeemRequest(email: String): RewardPairRedeem? {
        return try {
            val data = db.collection("rpc_redeem_req").document(email).get().await()
            data.toObject(RewardPairRedeem::class.java)
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            null
        }
    }

    suspend fun saveRPCRedeemRequest(redeem: RewardPairRedeem): Boolean {
        return try {
            redeem.status = "PENDING"
            redeem.requestDate = Date()
            db.collection("rpc_redeem_req").document(redeem.email).set(redeem).await()
            true
        }catch(e: Exception) {
            Log.d("FIRESTORE", e.message.toString())
            false
        }
    }

}