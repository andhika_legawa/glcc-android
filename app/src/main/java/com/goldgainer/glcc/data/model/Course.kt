package com.goldgainer.glcc.data.model

data class Course (
    var id:String = "",
    var order:Int = 0,
    var name:String = "",
    var sks:Int = 0,
)