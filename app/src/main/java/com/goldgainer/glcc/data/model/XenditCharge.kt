package com.goldgainer.glcc.data.model

import com.google.firebase.firestore.DocumentId

data class XenditCharge (
        @DocumentId
        var id: String = "",
        val transactionId: String = "",
        val status: String = "",
        val request: Map<String, Any> = mapOf(),
        val response: Map<String, Any> = mapOf()
)