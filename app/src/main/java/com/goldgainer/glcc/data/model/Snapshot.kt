package com.goldgainer.glcc.data.model

import com.google.firebase.firestore.DocumentId
import java.util.*

data class Snapshot (
        @DocumentId
        var id:String = "",
        var date: Date? = null,
        var owner: String = "",
        var referralId: String = "",
        var type: String = "",
        var number: Int = 0,
        var nodes: Int = 0,
        var totalAsset: Long = 0,

        // total platinum growth
        var platinumNetworks: Long = 0,
        var platinumNodes: Long = 0,
        var platinumBSponsor : Long = 0,
        var platinumBPairing : Long = 0,
        var platinumBRollup : Long = 0,

        // to date platinum node growth
        var toDatePlatinumNodeCarry : Long = 0,
        var toDatePlatinumNodeAvg : Long = 0,
        var toDatePlatinumNodes : Long = 0,
        var toDatePlatinumBSponsor : Long = 0,
        var toDatePlatinumBPairing : Long = 0,
        var toDatePlatinumBRollup : Long = 0,

        // total gold growth
        var goldNetworks: Long = 0,
        var goldNodes: Long = 0,
        var goldBSponsor : Long = 0,
        var goldBPairing : Long = 0,
        var goldBRollup : Long = 0,
        var goldNodeCary : Long = 0,

        // to date gold node growth
        var toDateGoldNodeCarry : Long = 0,
        var toDateGoldNodeAvg : Long = 0,
        var toDateGoldNodes : Long = 0,
        var toDateGoldBSponsor : Long = 0,
        var toDateGoldBPairing : Long = 0,
        var toDateGoldBRollup : Long = 0
)