package com.goldgainer.glcc.data.model

data class DigitalCurrency (
    val name: String = "",
    val address: String = "",
    val isDefault: Boolean = false
)