package com.goldgainer.glcc.data.model

import java.util.*

data class Voucher(
    val code : String = "",
    val description: String = "",
    val type: String = "",
    val number: Int = 0,
    val value: Int = 0,
    val fee: Int = 0,
    val currency: String = "",
    val quantity: Int = 0,
    val soldTo: String = "",
    val soldDate: Date? = null,
    val redeemedDate: Date? = null,
    val redeemedBy: String? = null,
    val generatedDate: Date? = null,
    val generatedBy: String = "",
    val generatedFor: String = "",
    val generatedTrx: String = ""
)