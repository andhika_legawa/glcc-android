package com.goldgainer.glcc.data.model

data class GlobalSetting (
    var name: String = "",
    var value: Any? = null,
	var valueArr: List<String> = listOf()
)