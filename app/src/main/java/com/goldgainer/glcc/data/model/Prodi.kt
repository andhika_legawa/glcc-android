package com.goldgainer.glcc.data.model

data class Prodi (
    var id:String = "",
    var order: Int = 0,
    var name:String = "",
    var semesterTotal:Int = 0,
    var courseTotal:Int = 0,
    var sksTotal:Int = 0,
    var semesterList: List<Semester> = arrayListOf()
)

class ProdiStatic () {
    companion object {
        fun getProdi1() : Prodi {
            var totalSKS = 0;
            val semesterList = arrayListOf<Semester>()

            val semester1Courses = arrayListOf<Course>()
            semester1Courses.add(Course(order = 1, name = "Bahasa Indonesia", sks = 2))
            semester1Courses.add(Course(order = 2, name = "Pancasila", sks = 2))
            semester1Courses.add(Course(order = 3, name = "Pengenalan Goldcoin", sks = 3))
            semester1Courses.add(Course(order = 4, name = "Pengenalan Aset Crypto", sks = 3))
            semester1Courses.add(Course(order = 5, name = "Pengenalan Bitcoin", sks = 3))
            semester1Courses.add(Course(order = 6, name = "Blockchain", sks = 3))
            semester1Courses.add(Course(order = 7, name = "Smart Contract", sks = 2))
            semester1Courses.add(Course(order = 8, name = "Sistem Digital", sks = 2))
            totalSKS += semester1Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 1,
                    name = "Semester 1",
                    courseTotal = semester1Courses.size,
                    courseList = semester1Courses.toList(),
                    sksTotal = semester1Courses.sumOf { it.sks }
                )
            )

            val semester2Courses = arrayListOf<Course>()
            semester2Courses.add(Course(order = 1, name = "Bahasa Inggris", sks = 2))
            semester2Courses.add(Course(order = 2, name = "PKN", sks = 2))
            semester2Courses.add(Course(order = 3, name = "Agama", sks = 2))
            semester2Courses.add(Course(order = 4, name = "Pemrograman Komputer", sks = 3))
            semester2Courses.add(Course(order = 5, name = "Fundamental cryptocurrency", sks = 3))
            semester2Courses.add(Course(order = 6, name = "Pengenalan Koin dan Token", sks = 3))
            semester2Courses.add(Course(order = 7, name = "Industri Digital", sks = 3))
            semester2Courses.add(Course(order = 8, name = "Praktikum Sistem Digital", sks = 2))
            totalSKS += semester2Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 2,
                    name = "Semester 2",
                    courseTotal = semester2Courses.size,
                    courseList = semester2Courses.toList(),
                    sksTotal = semester2Courses.sumOf { it.sks }
                )
            )

            val semester3Courses = arrayListOf<Course>()
            semester3Courses.add(Course(order = 1, name = "Bahasa Inggris Teknik", sks = 3))
            semester3Courses.add(Course(order = 2, name = "Matematika Teknik", sks = 3))
            semester3Courses.add(Course(order = 3, name = "Algoritma dan Pemrograman", sks = 2))
            semester3Courses.add(Course(order = 4, name = "Pengantar Teknologi Informasi", sks = 2))
            semester3Courses.add(Course(order = 5, name = "Praktikum Algoritma dan Pemrograman", sks = 3))
            semester3Courses.add(Course(order = 6, name = "Kewirausahaan", sks = 2))
            semester3Courses.add(Course(order = 7, name = "Struktur data", sks = 3))
            totalSKS += semester3Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 3,
                    name = "Semester 3",
                    courseTotal = semester3Courses.size,
                    courseList = semester3Courses.toList(),
                    sksTotal = semester3Courses.sumOf { it.sks }
                )
            )

            val semester4Courses = arrayListOf<Course>()
            semester4Courses.add(Course(order = 1, name = "Manajemen", sks = 2))
            semester4Courses.add(Course(order = 2, name = "Interpersonal Skill", sks = 2))
            semester4Courses.add(Course(order = 3, name = "Basis Data", sks = 2))
            semester4Courses.add(Course(order = 4, name = "Pemrograman Berorientasi Obyek", sks = 3))
            semester4Courses.add(Course(order = 5, name = "Sistem Terdistribusi", sks = 2))
            semester4Courses.add(Course(order = 6, name = "Kecerdasan Buatan", sks = 3))
            semester4Courses.add(Course(order = 7, name = "Analisis dan Desain Berorientasi Obyek", sks = 3))
            semester4Courses.add(Course(order = 8, name = "Pemodelan dan Simulasi", sks = 3))
            totalSKS += semester4Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 4,
                    name = "Semester 4",
                    courseTotal = semester4Courses.size,
                    courseList = semester4Courses.toList(),
                    sksTotal = semester4Courses.sumOf { it.sks }
                )
            )

            val semester5Courses = arrayListOf<Course>()
            semester5Courses.add(Course(order = 1, name = "Pengembangan Koin Crypto", sks = 3))
            semester5Courses.add(Course(order = 2, name = "Pengembangan Token Crypto", sks = 3))
            semester5Courses.add(Course(order = 3, name = "Executing Minimal Viable Token", sks = 2))
            semester5Courses.add(Course(order = 4, name = "Adding Parameters", sks = 3))
            semester5Courses.add(Course(order = 5, name = "Executing MyToken Stage 2", sks = 3))
            semester5Courses.add(Course(order = 6, name = "Adding Admin", sks = 3))
            semester5Courses.add(Course(order = 6, name = "Adding Automatic Buy & Sell feature", sks = 3))
            totalSKS += semester5Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 5,
                    name = "Semester 5",
                    courseTotal = semester5Courses.size,
                    courseList = semester5Courses.toList(),
                    sksTotal = semester5Courses.sumOf { it.sks }
                )
            )

            val semester6Courses = arrayListOf<Course>()
            semester6Courses.add(Course(order = 1, name = "Supply Chain", sks = 3))
            semester6Courses.add(Course(order = 2, name = "Land Owenership", sks = 3))
            semester6Courses.add(Course(order = 3, name = "Finance Industry", sks = 3))
            semester6Courses.add(Course(order = 4, name = "Multichain", sks = 3))
            semester6Courses.add(Course(order = 5, name = "Testing Full & Final Cryptocurrency", sks = 3))
            semester6Courses.add(Course(order = 6, name = "ICO", sks = 3))
            semester6Courses.add(Course(order = 7, name = "DAO (Decentralized Autonomous Organization) ", sks = 3))
            totalSKS += semester6Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 6,
                    name = "Semester 6",
                    courseTotal = semester6Courses.size,
                    courseList = semester6Courses.toList(),
                    sksTotal = semester6Courses.sumOf { it.sks }
                )
            )

            val semester7Courses = arrayListOf<Course>()
            semester7Courses.add(Course(order = 1, name = "Teknologi Antar Jaringan", sks = 2))
            semester7Courses.add(Course(order = 2, name = "Desain Jaringan Enterprise", sks = 2))
            semester7Courses.add(Course(order = 3, name = "Keamanan Jaringan", sks = 2))
            semester7Courses.add(Course(order = 4, name = "Metode Penelitian", sks = 3))
            semester7Courses.add(Course(order = 5, name = "Assesment", sks = 3))
            semester7Courses.add(Course(order = 6, name = "Setting up Metamask and Testing fund transfer", sks = 3))
            semester7Courses.add(Course(order = 7, name = "Digital Wallet", sks = 3))
            totalSKS += semester7Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 7,
                    name = "Semester 7",
                    courseTotal = semester7Courses.size,
                    courseList = semester7Courses.toList(),
                    sksTotal = semester7Courses.sumOf { it.sks }
                )
            )

            val semester8Courses = arrayListOf<Course>()
            semester8Courses.add(Course(order = 1, name = "Magang", sks = 3))
            semester8Courses.add(Course(order = 2, name = "Tugas Akhir", sks = 6))
            totalSKS += semester8Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 8,
                    name = "Semester 8",
                    courseTotal = semester8Courses.size,
                    courseList = semester8Courses.toList(),
                    sksTotal = semester8Courses.sumOf { it.sks }
                )
            )

            return Prodi(
                order = 1,
                name = "Blockchain and Security System",
                semesterList = semesterList.toList(),
                semesterTotal = semesterList.size,
                courseTotal = semesterList.sumOf { it.courseTotal },
                sksTotal = totalSKS
            )
        }

        fun getProdi2() : Prodi {
            var totalSKS = 0;
            val semesterList = arrayListOf<Semester>()

            val semester1Courses = arrayListOf<Course>()
            semester1Courses.add(Course(order = 1, name = "Bahasa Indonesia", sks = 2))
            semester1Courses.add(Course(order = 2, name = "Pancasila", sks = 2))
            semester1Courses.add(Course(order = 3, name = "Pengenalan Goldcoin", sks = 3))
            semester1Courses.add(Course(order = 4, name = "Pengenalan Aset Crypto", sks = 3))
            semester1Courses.add(Course(order = 5, name = "Pengenalan Bitcoin", sks = 3))
            semester1Courses.add(Course(order = 6, name = "Blockchain", sks = 3))
            semester1Courses.add(Course(order = 7, name = "Smart Contract", sks = 2))
            semester1Courses.add(Course(order = 8, name = "Sistem Digital", sks = 2))
            totalSKS += semester1Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 1,
                    name = "Semester 1",
                    courseTotal = semester1Courses.size,
                    courseList = semester1Courses.toList(),
                    sksTotal = semester1Courses.sumOf { it.sks }
                )
            )

            val semester2Courses = arrayListOf<Course>()
            semester2Courses.add(Course(order = 1, name = "Bahasa Inggris", sks = 2))
            semester2Courses.add(Course(order = 2, name = "PKN", sks = 2))
            semester2Courses.add(Course(order = 3, name = "Agama", sks = 2))
            semester2Courses.add(Course(order = 4, name = "Pemrograman Komputer", sks = 3))
            semester2Courses.add(Course(order = 5, name = "Fundamental cryptocurrency", sks = 3))
            semester2Courses.add(Course(order = 6, name = "Pengenalan Koin dan Token", sks = 3))
            semester2Courses.add(Course(order = 7, name = "Industri Digital", sks = 3))
            semester2Courses.add(Course(order = 8, name = "Praktikum Sistem Digital", sks = 2))
            totalSKS += semester2Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 2,
                    name = "Semester 2",
                    courseTotal = semester2Courses.size,
                    courseList = semester2Courses.toList(),
                    sksTotal = semester2Courses.sumOf { it.sks }
                )
            )

            val semester3Courses = arrayListOf<Course>()
            semester3Courses.add(Course(order = 1, name = "Bahasa Inggris Teknik", sks = 2))
            semester3Courses.add(Course(order = 2, name = "Matematika Teknik", sks = 3))
            semester3Courses.add(Course(order = 3, name = "Algoritma dan Pemrograman", sks = 3))
            semester3Courses.add(Course(order = 4, name = "Pengantar Teknologi Informasi", sks = 3))
            semester3Courses.add(Course(order = 5, name = "Praktikum Algoritma dan Pemrograman", sks = 3))
            semester3Courses.add(Course(order = 6, name = "Kewirausahaan", sks = 3))
            semester3Courses.add(Course(order = 7, name = "Struktur data", sks = 3))
            totalSKS += semester3Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 3,
                    name = "Semester 3",
                    courseTotal = semester3Courses.size,
                    courseList = semester3Courses.toList(),
                    sksTotal = semester3Courses.sumOf { it.sks }
                )
            )

            val semester4Courses = arrayListOf<Course>()
            semester4Courses.add(Course(order = 1, name = "Manajemen", sks = 2))
            semester4Courses.add(Course(order = 2, name = "Interpersonal Skill", sks = 2))
            semester4Courses.add(Course(order = 3, name = "Basis Data", sks = 2))
            semester4Courses.add(Course(order = 4, name = "Pemrograman Berorientasi Obyek", sks = 2))
            semester4Courses.add(Course(order = 5, name = "Sistem Terdistribusi", sks = 3))
            semester4Courses.add(Course(order = 6, name = "Kecerdasan Buatan", sks = 3))
            semester4Courses.add(Course(order = 7, name = "Analisis dan Desain Berorientasi Obyek", sks = 3))
            semester4Courses.add(Course(order = 8, name = "Pemodelan dan Simulasi", sks = 3))
            totalSKS += semester4Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 4,
                    name = "Semester 4",
                    courseTotal = semester4Courses.size,
                    courseList = semester4Courses.toList(),
                    sksTotal = semester4Courses.sumOf { it.sks }
                )
            )

            val semester5Courses = arrayListOf<Course>()
            semester5Courses.add(Course(order = 1, name = "Blockchain", sks = 3))
            semester5Courses.add(Course(order = 2, name = "Proof of Work", sks = 2))
            semester5Courses.add(Course(order = 3, name = "Proof of Stake", sks = 2))
            semester5Courses.add(Course(order = 4, name = "Proof of Importance", sks = 3))
            semester5Courses.add(Course(order = 5, name = "Proof of Burn", sks = 3))
            semester5Courses.add(Course(order = 6, name = "Blocks, transactions, private key & addresses", sks = 3))
            totalSKS += semester5Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 5,
                    name = "Semester 5",
                    courseTotal = semester5Courses.size,
                    courseList = semester5Courses.toList(),
                    sksTotal = semester5Courses.sumOf { it.sks }
                )
            )

            val semester6Courses = arrayListOf<Course>()
            semester6Courses.add(Course(order = 1, name = "Supply Chain", sks = 3))
            semester6Courses.add(Course(order = 2, name = "Land Owenership", sks = 3))
            semester6Courses.add(Course(order = 3, name = "Finance Industry", sks = 3))
            semester6Courses.add(Course(order = 4, name = "Multichain", sks = 3))
            semester6Courses.add(Course(order = 5, name = "Transaction How transaction gets executed & distributed", sks = 3))
            semester6Courses.add(Course(order = 6, name = "Consensus How conflicts are being resolved", sks = 3))
            semester6Courses.add(Course(order = 7, name = "Cyber Security", sks = 3))
            totalSKS += semester6Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 6,
                    name = "Semester 6",
                    courseTotal = semester6Courses.size,
                    courseList = semester6Courses.toList(),
                    sksTotal = semester6Courses.sumOf { it.sks }
                )
            )

            val semester7Courses = arrayListOf<Course>()
            semester7Courses.add(Course(order = 1, name = "Setting up Security groups & Ubuntu Server", sks = 3))
            semester7Courses.add(Course(order = 2, name = "Desain Jaringan Enterprise", sks = 3))
            semester7Courses.add(Course(order = 3, name = "Keamanan Jaringan", sks = 3))
            semester7Courses.add(Course(order = 4, name = "Metode Penelitian", sks = 3))
            semester7Courses.add(Course(order = 5, name = "Assesmen", sks = 3))
            semester7Courses.add(Course(order = 6, name = "Digital Wallet", sks = 3))
            totalSKS += semester7Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 7,
                    name = "Semester 7",
                    courseTotal = semester7Courses.size,
                    courseList = semester7Courses.toList(),
                    sksTotal = semester7Courses.sumOf { it.sks }
                )
            )

            val semester8Courses = arrayListOf<Course>()
            semester8Courses.add(Course(order = 1, name = "Magang", sks = 3))
            semester8Courses.add(Course(order = 2, name = "Tugas Akhir", sks = 6))
            totalSKS += semester8Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 8,
                    name = "Semester 8",
                    courseTotal = semester8Courses.size,
                    courseList = semester8Courses.toList(),
                    sksTotal = semester8Courses.sumOf { it.sks }
                )
            )

            return Prodi(
                order = 2,
                name = "Coin and Token",
                semesterList = semesterList.toList(),
                semesterTotal = semesterList.size,
                courseTotal = semesterList.sumOf { it.courseTotal },
                sksTotal = totalSKS
            )
        }

        fun getProdi3() : Prodi {
            var totalSKS = 0;
            val semesterList = arrayListOf<Semester>()

            val semester1Courses = arrayListOf<Course>()
            semester1Courses.add(Course(order = 1, name = "Bahasa Indonesia", sks = 2))
            semester1Courses.add(Course(order = 2, name = "Pancasila", sks = 2))
            semester1Courses.add(Course(order = 3, name = "Pengenalan Goldcoin", sks = 3))
            semester1Courses.add(Course(order = 4, name = "Pengenalan Aset Crypto", sks = 3))
            semester1Courses.add(Course(order = 5, name = "Pengenalan Bitcoin", sks = 3))
            semester1Courses.add(Course(order = 6, name = "Blockchain", sks = 3))
            semester1Courses.add(Course(order = 7, name = "Smart Contract", sks = 2))
            semester1Courses.add(Course(order = 8, name = "Sistem Digital", sks = 2))
            totalSKS += semester1Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 1,
                    name = "Semester 1",
                    courseTotal = semester1Courses.size,
                    courseList = semester1Courses.toList(),
                    sksTotal = semester1Courses.sumOf { it.sks }
                )
            )

            val semester2Courses = arrayListOf<Course>()
            semester2Courses.add(Course(order = 1, name = "Bahasa Inggris", sks = 2))
            semester2Courses.add(Course(order = 2, name = "PKN", sks = 2))
            semester2Courses.add(Course(order = 3, name = "Agama", sks = 2))
            semester2Courses.add(Course(order = 4, name = "Pengantar Ekonomi", sks = 3))
            semester2Courses.add(Course(order = 5, name = "Matematika Ekonomi", sks = 3))
            semester2Courses.add(Course(order = 6, name = "Pengantar Akuntansi", sks = 2))
            semester2Courses.add(Course(order = 7, name = "Pengantar Bisnis", sks = 2))
            semester2Courses.add(Course(order = 8, name = "Akuntansi Sektor Publik", sks = 2))
            totalSKS += semester2Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 2,
                    name = "Semester 2",
                    courseTotal = semester2Courses.size,
                    courseList = semester2Courses.toList(),
                    sksTotal = semester2Courses.sumOf { it.sks }
                )
            )

            val semester3Courses = arrayListOf<Course>()
            semester3Courses.add(Course(order = 1, name = "Bahasa Inggris", sks = 2))
            semester3Courses.add(Course(order = 2, name = "Matematika", sks = 3))
            semester3Courses.add(Course(order = 3, name = "Akuntansi Perhotelan", sks = 3))
            semester3Courses.add(Course(order = 4, name = "Akuntansi  Keuangan  1", sks = 3))
            semester3Courses.add(Course(order = 5, name = "Akuntansi Biaya", sks = 3))
            semester3Courses.add(Course(order = 6, name = "Praktikum Akuntansi Sektor Publik", sks = 3))
            semester3Courses.add(Course(order = 7, name = "Pengantar Manajemen", sks = 3))
            totalSKS += semester3Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 3,
                    name = "Semester 3",
                    courseTotal = semester3Courses.size,
                    courseList = semester3Courses.toList(),
                    sksTotal = semester3Courses.sumOf { it.sks }
                )
            )

            val semester4Courses = arrayListOf<Course>()
            semester4Courses.add(Course(order = 1, name = "Manajemen", sks = 2))
            semester4Courses.add(Course(order = 2, name = "Interpersonal Skill", sks = 2))
            semester4Courses.add(Course(order = 3, name = "Akuntansi Keuangan  2", sks = 2))
            semester4Courses.add(Course(order = 4, name = "Aplikasi Komputer Akuntansi 1", sks = 2))
            semester4Courses.add(Course(order = 5, name = "Perpajakan", sks = 3))
            semester4Courses.add(Course(order = 6, name = "Akuntansi Manajemen", sks = 3))
            semester4Courses.add(Course(order = 7, name = "English For Bussiness", sks = 3))
            semester4Courses.add(Course(order = 8, name = "Praktikum Perpajakan", sks = 3))
            totalSKS += semester4Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 4,
                    name = "Semester 4",
                    courseTotal = semester4Courses.size,
                    courseList = semester4Courses.toList(),
                    sksTotal = semester4Courses.sumOf { it.sks }
                )
            )

            val semester5Courses = arrayListOf<Course>()
            semester5Courses.add(Course(order = 1, name = "Praktikum Akuntansi Biaya", sks = 3))
            semester5Courses.add(Course(order = 2, name = "Akuntansi Manajemen Sektor Publik", sks = 2))
            semester5Courses.add(Course(order = 3, name = "Kewirausahaan", sks = 2))
            semester5Courses.add(Course(order = 4, name = "Etika Bisnis dan Profesi", sks = 2))
            semester5Courses.add(Course(order = 5, name = "Teori Ekonomi Mikro", sks = 2))
            semester5Courses.add(Course(order = 6, name = "Perkoperasian dan UMKM", sks = 3))
            semester5Courses.add(Course(order = 7, name = "Hyperledger", sks = 3))
            semester5Courses.add(Course(order = 8, name = "Building Your First Network", sks = 3))
            totalSKS += semester5Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 5,
                    name = "Semester 5",
                    courseTotal = semester5Courses.size,
                    courseList = semester5Courses.toList(),
                    sksTotal = semester5Courses.sumOf { it.sks }
                )
            )

            val semester6Courses = arrayListOf<Course>()
            semester6Courses.add(Course(order = 1, name = "Installing Binaries and Docker Images", sks = 3))
            semester6Courses.add(Course(order = 2, name = "Launching the FabCar Network", sks = 3))
            semester6Courses.add(Course(order = 3, name = "Installing JavaScript Dependencies", sks = 3))
            semester6Courses.add(Course(order = 4, name = "Ledger", sks = 3))
            semester6Courses.add(Course(order = 5, name = "Look Inside Chaincode Files", sks = 3))
            semester6Courses.add(Course(order = 6, name = "Stellar", sks = 3))
            semester6Courses.add(Course(order = 7, name = "Passive Income on Digital Asset", sks = 3))
            totalSKS += semester6Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 6,
                    name = "Semester 6",
                    courseTotal = semester6Courses.size,
                    courseList = semester6Courses.toList(),
                    sksTotal = semester6Courses.sumOf { it.sks }
                )
            )

            val semester7Courses = arrayListOf<Course>()
            semester7Courses.add(Course(order = 1, name = "Teknologi Pumping", sks = 3))
            semester7Courses.add(Course(order = 2, name = "Pumping Mining", sks = 3))
            semester7Courses.add(Course(order = 3, name = "Pumping Carity", sks = 3))
            semester7Courses.add(Course(order = 4, name = "Metode Penelitian", sks = 3))
            semester7Courses.add(Course(order = 5, name = "Assesment", sks = 3))
            semester7Courses.add(Course(order = 6, name = "Digital Wallet", sks = 3))
            totalSKS += semester7Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 7,
                    name = "Semester 7",
                    courseTotal = semester7Courses.size,
                    courseList = semester7Courses.toList(),
                    sksTotal = semester7Courses.sumOf { it.sks }
                )
            )

            val semester8Courses = arrayListOf<Course>()
            semester8Courses.add(Course(order = 1, name = "Magang", sks = 3))
            semester8Courses.add(Course(order = 2, name = "Tugas Akhir", sks = 6))
            totalSKS += semester8Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 8,
                    name = "Semester 8",
                    courseTotal = semester8Courses.size,
                    courseList = semester8Courses.toList(),
                    sksTotal = semester8Courses.sumOf { it.sks }
                )
            )

            return Prodi(
                order = 3,
                name = "Digital Fundraising",
                semesterList = semesterList.toList(),
                semesterTotal = semesterList.size,
                courseTotal = semesterList.sumOf { it.courseTotal },
                sksTotal = totalSKS
            )
        }

        fun getProdi4() : Prodi {
            var totalSKS = 0;
            val semesterList = arrayListOf<Semester>()

            val semester1Courses = arrayListOf<Course>()
            semester1Courses.add(Course(order = 1, name = "Bahasa Indonesia", sks = 2))
            semester1Courses.add(Course(order = 2, name = "Pancasila", sks = 2))
            semester1Courses.add(Course(order = 3, name = "Pengenalan Goldcoin", sks = 3))
            semester1Courses.add(Course(order = 4, name = "Pengenalan Aset Crypto", sks = 3))
            semester1Courses.add(Course(order = 5, name = "Pengenalan Bitcoin", sks = 3))
            semester1Courses.add(Course(order = 6, name = "Blockchain", sks = 3))
            semester1Courses.add(Course(order = 7, name = "Smart Contract", sks = 2))
            semester1Courses.add(Course(order = 8, name = "Sistem Digital", sks = 2))
            totalSKS += semester1Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 1,
                    name = "Semester 1",
                    courseTotal = semester1Courses.size,
                    courseList = semester1Courses.toList(),
                    sksTotal = semester1Courses.sumOf { it.sks }
                )
            )

            val semester2Courses = arrayListOf<Course>()
            semester2Courses.add(Course(order = 1, name = "Bahasa Inggris", sks = 2))
            semester2Courses.add(Course(order = 2, name = "PKN", sks = 2))
            semester2Courses.add(Course(order = 3, name = "Agama", sks = 2))
            semester2Courses.add(Course(order = 4, name = "Pengantar Ekonomi", sks = 3))
            semester2Courses.add(Course(order = 5, name = "Matematika Ekonomi", sks = 3))
            semester2Courses.add(Course(order = 6, name = "Pengantar Akuntansi", sks = 2))
            semester2Courses.add(Course(order = 7, name = "Pengantar Bisnis", sks = 2))
            semester2Courses.add(Course(order = 8, name = "Akuntansi Sektor Publik", sks = 2))
            totalSKS += semester2Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 2,
                    name = "Semester 2",
                    courseTotal = semester2Courses.size,
                    courseList = semester2Courses.toList(),
                    sksTotal = semester2Courses.sumOf { it.sks }
                )
            )

            val semester3Courses = arrayListOf<Course>()
            semester3Courses.add(Course(order = 1, name = "Bahasa Inggris", sks = 2))
            semester3Courses.add(Course(order = 2, name = "Matematika", sks = 3))
            semester3Courses.add(Course(order = 3, name = "Akuntansi Perhotelan", sks = 3))
            semester3Courses.add(Course(order = 4, name = "Akuntansi  Keuangan  1", sks = 3))
            semester3Courses.add(Course(order = 5, name = "Akuntansi Biaya", sks = 3))
            semester3Courses.add(Course(order = 6, name = "Praktikum Akuntansi Sektor Publik", sks = 3))
            semester3Courses.add(Course(order = 7, name = "Pengantar Manajemen", sks = 3))
            totalSKS += semester3Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 3,
                    name = "Semester 3",
                    courseTotal = semester3Courses.size,
                    courseList = semester3Courses.toList(),
                    sksTotal = semester3Courses.sumOf { it.sks }
                )
            )

            val semester4Courses = arrayListOf<Course>()
            semester4Courses.add(Course(order = 1, name = "Manajemen", sks = 2))
            semester4Courses.add(Course(order = 2, name = "Interpersonal Skill", sks = 2))
            semester4Courses.add(Course(order = 3, name = "Akuntansi Keuangan  2", sks = 2))
            semester4Courses.add(Course(order = 4, name = "Aplikasi Komputer Akuntansi 1", sks = 2))
            semester4Courses.add(Course(order = 5, name = "Perpajakan", sks = 3))
            semester4Courses.add(Course(order = 6, name = "Akuntansi Manajemen", sks = 3))
            semester4Courses.add(Course(order = 7, name = "English For Bussiness", sks = 3))
            semester4Courses.add(Course(order = 8, name = "Praktikum Perpajakan", sks = 3))
            totalSKS += semester4Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 4,
                    name = "Semester 4",
                    courseTotal = semester4Courses.size,
                    courseList = semester4Courses.toList(),
                    sksTotal = semester4Courses.sumOf { it.sks }
                )
            )

            val semester5Courses = arrayListOf<Course>()
            semester5Courses.add(Course(order = 1, name = "Praktikum Akuntansi Biaya", sks = 3))
            semester5Courses.add(Course(order = 2, name = "Akuntansi Manajemen Sektor Publik", sks = 3))
            semester5Courses.add(Course(order = 3, name = "Kewirausahaan", sks = 2))
            semester5Courses.add(Course(order = 4, name = "Etika Bisnis dan Profesi", sks = 3))
            semester5Courses.add(Course(order = 5, name = "Teori Ekonomi Mikro", sks = 3))
            semester5Courses.add(Course(order = 6, name = "Perkoperasian dan UMKM", sks = 3))
            semester5Courses.add(Course(order = 7, name = "Ekonomi Sumber Daya", sks = 3))
            totalSKS += semester5Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 5,
                    name = "Semester 5",
                    courseTotal = semester5Courses.size,
                    courseList = semester5Courses.toList(),
                    sksTotal = semester5Courses.sumOf { it.sks }
                )
            )

            val semester6Courses = arrayListOf<Course>()
            semester6Courses.add(Course(order = 1, name = "Supply Chain", sks = 3))
            semester6Courses.add(Course(order = 2, name = "Land Ownership", sks = 3))
            semester6Courses.add(Course(order = 3, name = "Finance Industry", sks = 3))
            semester6Courses.add(Course(order = 4, name = "Multichain", sks = 3))
            semester6Courses.add(Course(order = 5, name = "P2P Network", sks = 3))
            semester6Courses.add(Course(order = 6, name = "Digital Marketing", sks = 3))
            semester6Courses.add(Course(order = 7, name = "Multichain Blockchain", sks = 3))
            totalSKS += semester6Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 6,
                    name = "Semester 6",
                    courseTotal = semester6Courses.size,
                    courseList = semester6Courses.toList(),
                    sksTotal = semester6Courses.sumOf { it.sks }
                )
            )

            val semester7Courses = arrayListOf<Course>()
            semester7Courses.add(Course(order = 1, name = "Teknologi Pumping", sks = 3))
            semester7Courses.add(Course(order = 2, name = "Jual dan Beli Aset Crypto", sks = 3))
            semester7Courses.add(Course(order = 3, name = "Teknologi Swap dan Stacking", sks = 3))
            semester7Courses.add(Course(order = 4, name = "Metode Penelitian", sks = 3))
            semester7Courses.add(Course(order = 5, name = "Assesment", sks = 3))
            semester7Courses.add(Course(order = 6, name = "Digital Wallet", sks = 3))
            totalSKS += semester7Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 7,
                    name = "Semester 7",
                    courseTotal = semester7Courses.size,
                    courseList = semester7Courses.toList(),
                    sksTotal = semester7Courses.sumOf { it.sks }
                )
            )

            val semester8Courses = arrayListOf<Course>()
            semester8Courses.add(Course(order = 1, name = "Magang", sks = 3))
            semester8Courses.add(Course(order = 2, name = "Tugas Akhir", sks = 6))
            totalSKS += semester8Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 8,
                    name = "Semester 8",
                    courseTotal = semester8Courses.size,
                    courseList = semester8Courses.toList(),
                    sksTotal = semester8Courses.sumOf { it.sks }
                )
            )

            return Prodi(
                order = 4,
                name = "Digital Marketing",
                semesterList = semesterList.toList(),
                semesterTotal = semesterList.size,
                courseTotal = semesterList.sumOf { it.courseTotal },
                sksTotal = totalSKS
            )
        }

        fun getProdi5() : Prodi {
            var totalSKS = 0;
            val semesterList = arrayListOf<Semester>()

            val semester1Courses = arrayListOf<Course>()
            semester1Courses.add(Course(order = 1, name = "Bahasa Indonesia", sks = 2))
            semester1Courses.add(Course(order = 2, name = "Pancasila", sks = 2))
            semester1Courses.add(Course(order = 3, name = "Pengenalan Goldcoin", sks = 3))
            semester1Courses.add(Course(order = 4, name = "Aset Crypto", sks = 3))
            semester1Courses.add(Course(order = 5, name = "Teknik Analisa Aset Crypto", sks = 3))
            semester1Courses.add(Course(order = 6, name = "Research dan Analisa terhadap pasar Cryptocurrency", sks = 2))
            semester1Courses.add(Course(order = 7, name = "Trading tool: hardware dan software", sks = 3))
            semester1Courses.add(Course(order = 8, name = "Konsep Diri yang Positif", sks = 2))
            totalSKS += semester1Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 1,
                    name = "Semester 1",
                    courseTotal = semester1Courses.size,
                    courseList = semester1Courses.toList(),
                    sksTotal = semester1Courses.sumOf { it.sks }
                )
            )

            val semester2Courses = arrayListOf<Course>()
            semester2Courses.add(Course(order = 1, name = "Bahasa Inggris", sks = 2))
            semester2Courses.add(Course(order = 2, name = "PKN", sks = 2))
            semester2Courses.add(Course(order = 3, name = "Agama", sks = 2))
            semester2Courses.add(Course(order = 4, name = "Training Goldcoin", sks = 3))
            semester2Courses.add(Course(order = 5, name = "Fundamental Cryptocurrency", sks = 3))
            semester2Courses.add(Course(order = 6, name = "Strategi Investasi Cryptocurrency", sks = 2))
            semester2Courses.add(Course(order = 7, name = "Analisa Teknikal pola jual beli di market", sks = 2))
            semester2Courses.add(Course(order = 8, name = "Kepatuhan terhadap SOP", sks = 2))
            totalSKS += semester2Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 2,
                    name = "Semester 2",
                    courseTotal = semester2Courses.size,
                    courseList = semester2Courses.toList(),
                    sksTotal = semester2Courses.sumOf { it.sks }
                )
            )

            val semester3Courses = arrayListOf<Course>()
            semester3Courses.add(Course(order = 1, name = "Bahasa Inggris Teknik", sks = 2))
            semester3Courses.add(Course(order = 2, name = "Matematika Teknik", sks = 2))
            semester3Courses.add(Course(order = 3, name = "Short Selling", sks = 2))
            semester3Courses.add(Course(order = 4, name = "Praktek Short Selling", sks = 2))
            semester3Courses.add(Course(order = 5, name = "Candle Stick", sks = 2))
            semester3Courses.add(Course(order = 6, name = "Psikologi Market", sks = 2))
            semester3Courses.add(Course(order = 7, name = "Digital Marketing", sks = 2))
            semester3Courses.add(Course(order = 8, name = "Digital Wallet", sks = 2))
            semester3Courses.add(Course(order = 9, name = "Digital Fundraising", sks = 2))
            semester3Courses.add(Course(order = 10, name = "Digital Mining", sks = 2))
            totalSKS += semester3Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 3,
                    name = "Semester 3",
                    courseTotal = semester3Courses.size,
                    courseList = semester3Courses.toList(),
                    sksTotal = semester3Courses.sumOf { it.sks }
                )
            )

            val semester4Courses = arrayListOf<Course>()
            semester4Courses.add(Course(order = 1, name = "Manajemen", sks = 2))
            semester4Courses.add(Course(order = 2, name = "Kestabilan emosi", sks = 2))
            semester4Courses.add(Course(order = 3, name = "Portofolio: catatan history, data base, dan report", sks = 2))
            semester4Courses.add(Course(order = 4, name = "Resiko Investasi Aset Digital", sks = 2))
            semester4Courses.add(Course(order = 5, name = "Trading otomatis dengan Robot", sks = 3))
            semester4Courses.add(Course(order = 6, name = "Praktek Trading otomatis dengann Robot", sks = 3))
            semester4Courses.add(Course(order = 7, name = "Tutorial Trading Lokal", sks = 3))
            semester4Courses.add(Course(order = 8, name = "Praktek Trading Lokal", sks = 3))
            totalSKS += semester4Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 4,
                    name = "Semester 4",
                    courseTotal = semester4Courses.size,
                    courseList = semester4Courses.toList(),
                    sksTotal = semester4Courses.sumOf { it.sks }
                )
            )

            val semester5Courses = arrayListOf<Course>()
            semester5Courses.add(Course(order = 1, name = "Tutorial Trading Global 1", sks = 3))
            semester5Courses.add(Course(order = 2, name = "Tutorial Trading Global 1", sks = 3))
            semester5Courses.add(Course(order = 3, name = "Tutorial Trading Global 2", sks = 3))
            semester5Courses.add(Course(order = 4, name = "Tutorial Trading Global 2", sks = 3))
            semester5Courses.add(Course(order = 5, name = "Tutorial Trading Global 3", sks = 3))
            semester5Courses.add(Course(order = 6, name = "Tutorial Trading Global 3", sks = 3))
            semester5Courses.add(Course(order = 7, name = "Analisis Pasar", sks = 2))
            totalSKS += semester5Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 5,
                    name = "Semester 5",
                    courseTotal = semester5Courses.size,
                    courseList = semester5Courses.toList(),
                    sksTotal = semester5Courses.sumOf { it.sks }
                )
            )

            val semester6Courses = arrayListOf<Course>()
            semester6Courses.add(Course(order = 1, name = "Trading at BlockDX", sks = 3))
            semester6Courses.add(Course(order = 2, name = "Praktek Trading at BlockDX", sks = 3))
            semester6Courses.add(Course(order = 3, name = "Trading at Serum DX", sks = 3))
            semester6Courses.add(Course(order = 4, name = "Praktek Trading at Serum DX", sks = 3))
            semester6Courses.add(Course(order = 5, name = "Trading at Binance Dex", sks = 3))
            semester6Courses.add(Course(order = 6, name = "Praktek at Binance Dex", sks = 3))
            semester6Courses.add(Course(order = 7, name = "Trading at MDEX", sks = 3))
            totalSKS += semester6Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 6,
                    name = "Semester 6",
                    courseTotal = semester6Courses.size,
                    courseList = semester6Courses.toList(),
                    sksTotal = semester6Courses.sumOf { it.sks }
                )
            )

            val semester7Courses = arrayListOf<Course>()
            semester7Courses.add(Course(order = 1, name = "Trading at Waves Exchanges", sks = 3))
            semester7Courses.add(Course(order = 2, name = "Praktek at Waves Exchange", sks = 3))
            semester7Courses.add(Course(order = 3, name = "Trading at Vitex", sks = 3))
            semester7Courses.add(Course(order = 4, name = "Praktek at Vitex", sks = 3))
            semester7Courses.add(Course(order = 5, name = "Trading at Bambo Relay", sks = 3))
            semester7Courses.add(Course(order = 6, name = "Praktek at Bambo Relay", sks = 3))
            totalSKS += semester7Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 7,
                    name = "Semester 7",
                    courseTotal = semester7Courses.size,
                    courseList = semester7Courses.toList(),
                    sksTotal = semester7Courses.sumOf { it.sks }
                )
            )

            val semester8Courses = arrayListOf<Course>()
            semester8Courses.add(Course(order = 1, name = "Magang", sks = 3))
            semester8Courses.add(Course(order = 2, name = "Tugas Akhir", sks = 6))
            totalSKS += semester8Courses.sumOf { it.sks }
            semesterList.add(
                Semester(
                    order = 8,
                    name = "Semester 8",
                    courseTotal = semester8Courses.size,
                    courseList = semester8Courses.toList(),
                    sksTotal = semester8Courses.sumOf { it.sks }
                )
            )

            return Prodi(
                order = 5,
                name = "Trader",
                semesterList = semesterList.toList(),
                semesterTotal = semesterList.size,
                courseTotal = semesterList.sumOf { it.courseTotal },
                sksTotal = totalSKS
            )
        }
    }
}
