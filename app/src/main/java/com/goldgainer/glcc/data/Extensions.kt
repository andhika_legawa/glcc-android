package com.goldgainer.glcc.data

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import br.com.sapereaude.maskedEditText.MaskedEditText
import java.text.NumberFormat
import java.util.*

fun String.pluralize(count: Int) : String? {
    return if (count > 1) {
        this + 's'
    } else {
        this
    }
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

fun MaskedEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(rawText)
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

fun Double.toCurrency(currencyCode: String) : String {
    val formatter = NumberFormat.getCurrencyInstance()
    formatter.maximumFractionDigits = 0
    formatter.currency = Currency.getInstance(currencyCode)
    return formatter.format(this)
}

fun Double.toDigitalCurrency(currencyCode: String) : String {
    val formatter = NumberFormat.getInstance()
    formatter.maximumFractionDigits = 8
    return formatter.format(this) + " " + currencyCode
}

fun Int.toCurrency(currencyCode: String) : String {
    val formatter = NumberFormat.getCurrencyInstance()
    formatter.maximumFractionDigits = 0
    formatter.currency = Currency.getInstance(currencyCode)
    return formatter.format(this)
}
