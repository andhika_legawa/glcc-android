package com.goldgainer.glcc.data.model

import com.google.firebase.firestore.DocumentId

data class VoucherPackage (
    @DocumentId
    var id : String = "",
    val networkType: String = "",
    val networkNumber: Int = 0,
    val nodeType: Int = 0,
    val price: Double = 0.0,
    val amount: Int = 0,
    val currency: String = ""
    )