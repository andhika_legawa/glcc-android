package com.goldgainer.glcc.data

import android.util.Log
import androidx.preference.PreferenceManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.goldgainer.glcc.GLCCApplication
import com.goldgainer.glcc.data.model.GlobalSetting
import com.goldgainer.glcc.data.model.UserData
import org.json.JSONObject
import retrofit2.http.GET
import java.net.URL
import java.util.*

class Helper {
    private var _currentGlobalSettings : List<GlobalSetting> = listOf()

    suspend fun getGlobalSettings(name:String, default: Any) : Any? {
        val settings = getGlobalSettings()
        if (settings.isNotEmpty()) {
            return try {
                val setting = settings.first { it -> it.name == name }
                if ( default is Int){
                    setting.value.toString().toInt()
                }else{
                    setting.value.toString()
                }
            }catch (e: Exception){
                Log.d("HELPER", e.message.toString())
                default
            }
        }
        return default
    }

    suspend fun getGlobalSettingsArr(name:String) : Array<String> {
        val settings = getGlobalSettings()
        if (settings.isNotEmpty()) {
            return try {
                val setting = settings.first { it -> it.name == name }
                setting.valueArr.toTypedArray()
            }catch (e: Exception){
                Log.d("HELPER", e.message.toString())
                arrayOf()
            }
        }
        return arrayOf()
    }

    suspend fun isBlacklisted(email: String): Boolean {
        return FirebaseDataSource().isBlacklisted(email)
    }

    private suspend fun getGlobalSettings(): List<GlobalSetting> {
        if ( _currentGlobalSettings.isEmpty() ){
            val firebaseDataSource = FirebaseDataSource()
            _currentGlobalSettings = firebaseDataSource.getGlobalSettings()
        }
        return _currentGlobalSettings
    }

    fun nodeIDGenerator(prefix:String?) : String {
        var allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        val randomized = (1..34).map { allowedChars.random() }.joinToString("")
        if (prefix != null) {
            return "$prefix-$randomized"
        }
        return randomized
    }

    companion object {
        fun getLocalUser() : UserData {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(GLCCApplication.getAppContext())
            return UserData(
                userId = sharedPref.getString("userId", null),
                email = sharedPref.getString("userEmail", null),
                country = sharedPref.getString("userCountry", null),
                name = sharedPref.getString("userName", null),
                verified = sharedPref.getBoolean("userVerified", false)
            )
        }
        fun setLocalUser(email: String, userData: UserData) {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(GLCCApplication.getAppContext())
            with (sharedPref.edit()) {
                putString("userId", userData.userId)
                putString("userEmail", email)
                putString("userCountry", userData.country)
                putString("userName", userData.name)
                putBoolean("userVerified", userData.verified)
                apply()
            }
        }
        fun shouldShowPromotion() : Boolean {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(GLCCApplication.getAppContext())
            val country = sharedPref.getString("userCountry", null)
            if (country != "ID") {
                return false
            }
            val lastShown = sharedPref.getLong("lastShownPromotion", 0)
            val currentTime = Date().time
            var showPromotion = true
            if (lastShown > 0 ) {
                val diff = currentTime - lastShown
                val seconds =  diff / 3600
                val minutes = seconds / 60
                val hours = minutes / 60
                val days = hours / 24
                if (days < 1) {
                    showPromotion = false
                }
            }
            if (showPromotion) {
                sharedPref.edit().putLong("lastShownPromotion", currentTime).apply()
            }
            return showPromotion
        }
    }
}