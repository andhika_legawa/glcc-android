package com.goldgainer.glcc.data

import com.goldgainer.glcc.data.model.LoggedInUser
import com.goldgainer.glcc.data.model.UserData

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(val dataSource: LoginDataSource) {

    // in-memory cache of the loggedInUser object
    private var user: LoggedInUser? = null

    val isLoggedIn: Boolean
        get() = user != null

    suspend fun login(username: String, password: String): Result<LoggedInUser> {
        return dataSource.login(username, password)
    }

    fun logout() {
        user = null
        dataSource.logout()
    }

    suspend fun register(email: String, password: String, username: String): Result<LoggedInUser> {
        return dataSource.register(email, password, username)
    }

    fun getInfo(): Result<UserData>{
        return dataSource.getInfo()
    }

    suspend fun resetPassword(email: String): Result<String>{
        return dataSource.resetPassword(email)
    }

    suspend fun resendVerification(): Result<String>{
        return dataSource.resendVerification()
    }

}