package com.goldgainer.glcc.data

data class Province(val provinsi: String, val kota: Array<String>)