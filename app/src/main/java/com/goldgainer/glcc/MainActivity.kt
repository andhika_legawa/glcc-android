package com.goldgainer.glcc

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ActivityNavigator
import com.goldgainer.glcc.data.Helper
import kotlinx.coroutines.launch

class MainActivity : AuthenticatedActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var menu:Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.main_nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.nav_dashboard, R.id.nav_stockist_info, R.id.nav_profile, R.id.nav_wallet, R.id.nav_login_activity)
            , drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        this.menu = navView.menu
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_main_drawer, menu)
        return true
    }

    private fun getMenuOptions() {
        lifecycleScope.launch {
            val helper = Helper()
            val isON = helper.getGlobalSettings("stockist_on", 0) as Int
            menu.findItem(R.id.nav_stockist_info).isVisible = isON == 1
            val isID = Helper.getLocalUser().country == "ID"
            menu.findItem(R.id.nav_academy).isVisible = isID
            menu.findItem(R.id.nav_reward_journey).isVisible = isID
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        getMenuOptions()
        val navController = findNavController(R.id.main_nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }
}