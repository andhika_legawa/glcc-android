package com.goldgainer.glcc.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.model.Notification
import java.text.SimpleDateFormat

class NotificationViewAdapter(private val notificationSet : List<Notification>, private val listener:(Notification) -> Unit) : RecyclerView.Adapter<NotificationViewAdapter.NodeViewHolder>(){

    class NodeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NodeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.partials_notification_item, parent, false) as View
        return NodeViewHolder(view)
    }

    override fun onBindViewHolder(holder: NodeViewHolder, position: Int) {
        val item = notificationSet[position]
        val dateText = holder.itemView.findViewById<TextView>(R.id.notification_date)
        val msgText = holder.itemView.findViewById<TextView>(R.id.notification_message)

        val sdf = SimpleDateFormat("MMM dd, YYYY - HH:mm:ss")
        dateText.text = sdf.format(item.date).toString()
        msgText.text = item.message

        holder.itemView.setOnClickListener { listener(item) }
    }

    override fun getItemCount() = notificationSet.size
}