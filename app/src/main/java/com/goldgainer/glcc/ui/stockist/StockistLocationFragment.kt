package com.goldgainer.glcc.ui.stockist

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import kotlinx.coroutines.launch
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class StockistLocationFragment : AuthenticatedFragment() {

    private lateinit var viewModel: StockistViewModel

    private lateinit var provinceAdapter : ArrayAdapter<String>
    private lateinit var cityAdapter : ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_stockist_location, container, false)
        setUserHeader(root.findViewById(R.id.stockist_user_header))
        viewModel = ViewModelProvider(this).get(StockistViewModel::class.java)

        val editProvince = root.findViewById<Spinner>(R.id.edit_province)
        val editCity = root.findViewById<Spinner>(R.id.edit_city)
        val whatsapp = root.findViewById<EditText>(R.id.edit_whatsapp)
        val telegram = root.findViewById<EditText>(R.id.edit_telegram)

        val saveBtn = root.findViewById<Button>(R.id.stockist_location_save)

        provinceAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        provinceAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        editProvince.adapter = provinceAdapter

        cityAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        cityAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        editCity.adapter = cityAdapter

        editProvince.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                cityAdapter.clear()
                val regions = viewModel.regions.value
                regions?.let {
                    val province = it.getJSONObject(position)
                    val cities = province.getJSONArray("kota")
                    var cityIndex = 0
                    for (i in 0 until cities.length() step 1) {
                        val cityName = cities.getString(i)
                        cityAdapter.add(cityName)
                        val stockist = viewModel.currentStockist.value
                        stockist?.let {
                            if (cityName == it.city) {
                                cityIndex = i
                            }
                        }
                    }
                    cityAdapter.notifyDataSetChanged()
                    editCity.setSelection(cityIndex)
                }
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        var isStockist = false

        viewModel.regions.observe(viewLifecycleOwner, Observer { provinces ->
            provinceAdapter.clear()
            var provinceIndex = 0
            for (i in 0 until provinces.length() step 1) {
                val jsonObject = provinces.getJSONObject(i)
                val provinceName = jsonObject.getString("provinsi")
                provinceAdapter.add(provinceName)
                val stockist = viewModel.currentStockist.value
                stockist?.let {
                    if (provinceName == it.province) {
                        provinceIndex = i
                    }
                }
            }
            provinceAdapter.notifyDataSetChanged()
            editProvince.setSelection(provinceIndex)
        })

        viewLifecycleOwner.lifecycleScope.launch {
            isStockist = viewModel.isStockist()
            viewModel.getProvinces(requireContext())
            val stockist = viewModel.currentStockist.value
            stockist?.let {
                whatsapp.setText(it.whatsapp)
                telegram.setText(it.telegram)
            }
        }

        saveBtn.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                val success = viewModel.saveLocation(
                    editProvince.selectedItem.toString(),
                    editCity.selectedItem.toString(),
                    whatsapp.text.toString(),
                    telegram.text.toString()
                )
                if (success) {
                    var message =  "Your Stockist Location and Contact have been updated."
                    if (!isStockist) {
                        message = "$message Top Up The Voucher to start selling your vouchers."
                    }

                    MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                        .setTitle("Success")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                            findNavController().popBackStack()
                        }
                        .show()
                } else {
                    MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                        .setTitle("Error")
                        .setMessage("Error saving Location & Contact.")
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                }
            }
        }

        return root
    }

}