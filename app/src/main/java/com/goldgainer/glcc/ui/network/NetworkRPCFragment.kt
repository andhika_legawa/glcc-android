package com.goldgainer.glcc.ui.network

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment

class NetworkRPCFragment : AuthenticatedFragment() {

    lateinit var viewModel: NetworkRPCViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_network_rpc, container, false)
        setUserHeader(root.findViewById(R.id.user_header))

        viewModel = ViewModelProvider(this).get(NetworkRPCViewModel::class.java)

        var selectedNumber = 0
        arguments?.let {
            selectedNumber = it.getInt("number", 0)
        }

        val loading = root.findViewById<ProgressBar>(R.id.progress_circular)
        viewModel.isBusy.observe(viewLifecycleOwner, {
            if (it) {
                loading.visibility = View.VISIBLE
            } else {
                loading.visibility = View.GONE
            }
        })
        viewModel.reset()

        viewModel.error.observe(viewLifecycleOwner, {
            if (it != null) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })

        val networkSpinner = root.findViewById<Spinner>(R.id.network_spinner)
        val networkAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, arrayListOf())
        networkAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        networkSpinner.adapter = networkAdapter
        networkSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                val categories = viewModel.networkCategories.value
                val category = categories?.get(position)
                category?.let {
                    viewModel.loadRPC(it.type, it.number, null)
                }
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }
        viewModel.networkCategories.observe(viewLifecycleOwner, { categories ->
            if (categories.isNotEmpty()) {
                networkAdapter.clear()
                categories.forEach {
                    networkAdapter.add(it.description)
                }
                networkAdapter.notifyDataSetChanged()
                if (selectedNumber == 0) {
                    networkSpinner.setSelection(0)
                } else {
                    val index = categories.indexOfFirst { it.number == selectedNumber }
                    if (index < 0) {
                        networkSpinner.setSelection(0)
                    } else {
                        networkSpinner.setSelection(index)
                    }
                }
            }
        })
        val pairString = root.findViewById<TextView>(R.id.pair_string)
        viewModel.networkRPC.observe(viewLifecycleOwner, {
            pairString.text = "${it.pairLeft}:${it.pairRight}"
            viewModel.loadJourney()
        })

        viewModel.loadNetworks()

        val journeyRecyclerView = root.findViewById<RecyclerView>(R.id.journey_list)
        val journeyAdapter = NetworkRPCAdapter { pairString ->
            viewModel.networkRPC.value?.let {
                val network = viewModel.selectedNetworkCategory.value?:return@let
                val bundle = bundleOf("pairString" to pairString , "networkType" to network.type, "networkNumber" to network.number)
                findNavController().navigate(R.id.action_nav_reward_journey_to_nav_reward_redeem, bundle)
            }
//            Toast.makeText(requireContext(), pairString, Toast.LENGTH_LONG).show()
        }
        journeyRecyclerView.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        journeyRecyclerView.adapter = journeyAdapter

        viewModel.networkJourney.observe(viewLifecycleOwner, {
            journeyAdapter.updateList(it)
        })

        return root
    }
}