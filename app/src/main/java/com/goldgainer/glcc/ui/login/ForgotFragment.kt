package com.goldgainer.glcc.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.LoginDataSource
import com.goldgainer.glcc.data.LoginRepository
import com.goldgainer.glcc.data.afterTextChanged

class ForgotFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_forgot, container, false)

        val email = root.findViewById<EditText>(R.id.forgot_email)
        val cancel = root.findViewById<Button>(R.id.forgot_cancel_button)
        val reset = root.findViewById<Button>(R.id.forgot_reset_button)

        loginViewModel = ViewModelProvider(this, LoginViewModelFactory()).get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(viewLifecycleOwner, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            reset.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                email.error = getString(loginState.usernameError)
            }
        })

        loginViewModel.sendResult.observe(viewLifecycleOwner, Observer {
            val sendResult = it ?: return@Observer

            if (sendResult.success != null){
                Toast.makeText(activity, sendResult.success, Toast.LENGTH_SHORT).show()
            }
            if (sendResult.error != null){
                Toast.makeText(activity, sendResult.error, Toast.LENGTH_SHORT).show()
            }
        })

        email.afterTextChanged {
            loginViewModel.forgotDataChanged(email = email.text.toString())
        }

        cancel.setOnClickListener {
            findNavController().popBackStack()
        }

        reset.setOnClickListener {
            loginViewModel.resetPassword(email.text.toString())
        }

        return root
    }
}