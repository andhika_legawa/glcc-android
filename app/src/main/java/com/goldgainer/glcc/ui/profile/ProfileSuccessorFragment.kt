package com.goldgainer.glcc.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment

class ProfileSuccessorFragment : AuthenticatedFragment() {

	private lateinit var profileViewModel: ProfileViewModel

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_profile_successor, container, false)
		setUserHeader(root.findViewById(R.id.profile_user_header))

		loggedInUser?.let {
			profileViewModel.currentOwner = it.email!!
		}

		val fullname = root.findViewById<EditText>(R.id.edit_successor_fullname)
		val relationship = root.findViewById<Spinner>(R.id.edit_successor_relationship)
		val phone = root.findViewById<EditText>(R.id.edit_successor_phone)
		val email = root.findViewById<EditText>(R.id.edit_successor_email)

		val confirm = root.findViewById<TextView>(R.id.successor_text_confirm)
		val save = root.findViewById<Button>(R.id.btn_save_successor)

		ArrayAdapter.createFromResource(requireContext(), R.array.relationship_array, android.R.layout.simple_spinner_item).also {
			it.setDropDownViewResource(R.layout.partials_dropdown_item)
			relationship.adapter = it
		}

		profileViewModel.currentProfile.observe(viewLifecycleOwner, Observer { profile ->
			if (profileViewModel.dRelationshipArray.isNotEmpty()) {
				ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, profileViewModel.dRelationshipArray).also {
					it.setDropDownViewResource(R.layout.partials_dropdown_item)
					relationship.adapter = it
				}
			}
			profile.successorFullname?.let{
				fullname.setText(it)
			}
			profile.successorRelation?.let{
				val relationArr = context?.resources?.getStringArray(R.array.relationship_array)
				relationArr?.let { arr ->
					relationship.setSelection(arr.indexOf(it))
				}
			}
			profile.successorPhone?.let{
				phone.setText(it)
			}
			profile.successorEmail?.let{
				email.setText(it)
			}
			save.isEnabled = true
			if (profile.status == "PA" || profile.status == "AV") {
				fullname.isEnabled = false
				relationship.isEnabled = false
				phone.isEnabled = false
				email.isEnabled = false
				save.isEnabled = false
				confirm.visibility = View.INVISIBLE
				save.visibility = View.INVISIBLE
			}
		})

		profileViewModel.profileError.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		profileViewModel.getProfile()

		profileViewModel.profileSaved.observe(viewLifecycleOwner, Observer { success ->
			if (success) {
				findNavController().popBackStack()
			}
		})

		save.setOnClickListener {
			profileViewModel.saveSuccessor(
					fullname = fullname.text.toString(),
					relationship = relationship.selectedItem.toString(),
					phone = phone.text.toString(),
					email = email.text.toString())
		}

		return root
	}

}