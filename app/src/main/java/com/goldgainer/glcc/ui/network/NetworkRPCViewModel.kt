package com.goldgainer.glcc.ui.network

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.github.vipulasri.timelineview.TimelineView
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.*
import kotlinx.coroutines.launch
import java.util.*
import kotlin.math.min

class NetworkRPCViewModel : ViewModel() {

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String?>()
    val error: LiveData<String?> = _error

    private val _networkRPC = MutableLiveData<NetworkRPC>()
    val networkRPC: LiveData<NetworkRPC> = _networkRPC

    private var _userNetworks:List<Network> = listOf()
    private val _networkCategory = MutableLiveData<List<NetworkCategory>>()
    val networkCategories: LiveData<List<NetworkCategory>> = _networkCategory

    private val _networkJourney = MutableLiveData<List<RewardPairConnection>>()
    val networkJourney: LiveData<List<RewardPairConnection>> = _networkJourney

    private val _selectedNetworkCategory = MutableLiveData<NetworkCategory>()
    val selectedNetworkCategory: LiveData<NetworkCategory> = _selectedNetworkCategory

    private val _currentRedeem = MutableLiveData<RewardPairRedeem>()
    val currentRedeem: LiveData<RewardPairRedeem> = _currentRedeem

    var pairString: String = "000:000"

    private val _database = FirebaseDataSource()
    private val localUser = Helper.getLocalUser()

    fun reset() {
        _error.value = null
        _isBusy.value = false
        _networkRPC.value = NetworkRPC( "",0,0)
    }

    fun loadNetworks() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                fetchCategories()
            }catch(e:Exception) {
                _error.value = e.message
            }
            _isBusy.value = false
        }
    }

    private suspend fun fetchCategories() {
        var iCategories = _database.getNetworkCategories("I")
        localUser.country?.let { country ->
            if (country != "ID") {
                iCategories = listOf()
            }
        }
        localUser.email?.let {
            _userNetworks = _database.getUserNetworks(it)
        }
        val activeCategories = iCategories.filter { it.active && !it.locked }
        activeCategories.forEach {
            it.filled = _userNetworks.find { network ->
                it.type == network.type && it.number == network.number
            }
        }
        val filledCategories = activeCategories.filter { it.filled != null }
        _networkCategory.value = filledCategories
    }

    private fun countNetworkRPC(snapshot: Snapshot) : NetworkRPC{
        val div = min(snapshot.platinumNodes, snapshot.goldNodes)
        return NetworkRPC(snapshot.referralId, div, div)
    }

    fun loadJourney() {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val rpc = _database.getRewardPairIndonesia()
                val userRpc = _networkRPC.value
                userRpc?.let {

                    rpc.forEach {
                        if(it.pairLeft <= userRpc.pairLeft && it.pairRight <= userRpc.pairRight) {
                            it.isEligible = true
                            val redeem = getUserRedeem(it.pairString, "I", 1)
                            if(redeem != null && redeem.isRedeemed) {
                                it.isEligible = false
                                it.isRedeemed = true
                            }
                        }
                        if (!localUser.verified ) {
                            it.isEligible = false
                        }
                    }
                }
                _networkJourney.value = rpc.sortedBy { it.pairLeft }
            }catch (e: Exception) {
                _error.value = e.message
            }
            _isBusy.value = false
        }
    }

    private var redeemList: List<RewardPairRedeem>? = null
    private suspend fun getUserRedeem(pairString: String, networkType: String, networkNumber: Int) : RewardPairRedeem? {
        localUser.email?.let { email ->
            if (redeemList == null) redeemList = _database.getRPCRedeemList(email)
            return redeemList!!.firstOrNull{
                it.pairString == pairString && it.networkType == networkType && it.networkNumber == networkNumber
            }
        }
        return null
    }

    private suspend fun doLoadRPC(networkType: String, networkNumber: Int) {
        fetchCategories()
        val categories = _networkCategory.value
        val selectedCategory =
            categories?.firstOrNull { it.type == networkType && it.number == networkNumber }
                ?:throw Exception("Invalid network category")
        _selectedNetworkCategory.value = selectedCategory

        val userNetwork = _userNetworks.first{ it.type == networkType && it.number == networkNumber }
        val snapshot = _database.getLatestSnapshot(userNetwork.referralId)?:throw Exception("No user network snapshot")
        _networkRPC.value = countNetworkRPC(snapshot)
    }

    fun loadRPC (networkType: String, networkNumber: Int, pairString: String?) {
        pairString?.let { this.pairString = it }
        viewModelScope.launch {
            _isBusy.value = true
            try {
                doLoadRPC(networkType, networkNumber)
            } catch (e: Exception) {
                _error.value = e.message
            }
            _isBusy.value = false
        }
    }

    fun checkRedeem () {
        _currentRedeem.value?.let {
            checkRedeem(it.networkType, it.networkNumber, it.pairString)
        }
    }

    fun checkRedeem (networkType: String, networkNumber: Int, pairString: String) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                doLoadRPC(networkType, networkNumber)
                var userRedeem = getUserRedeem(pairString, networkType, networkNumber)
                if (userRedeem != null) {
                    _currentRedeem.value = userRedeem
                } else {
                    localUser.email?.let { email ->
                        // check if there is a pending redeem
                        userRedeem = _database.getRPCRedeemRequest(email)
                        if (userRedeem == null) {
                            val networkRPC = _networkRPC.value?:throw Exception("Reward Pair not available")
                            val profile = _database.getProfile(email)?:throw Exception("Profile not available")
                            userRedeem = RewardPairRedeem(
                                pairString = pairString,
                                networkType = networkType,
                                networkNumber = networkNumber,
                                networkId = networkRPC.networkReferral,
                                pairLeft = networkRPC.pairLeft,
                                pairRight = networkRPC.pairRight,
                                name = profile.fullname,
                                email = email,
                                nationalId = profile.nationalId,
                                mobilePhone = profile.phone,
                            )
                        }
                        _currentRedeem.value = userRedeem
                    }
                }
            } catch (e: Exception) {
                _error.value = e.message
            }
            _isBusy.value = false
        }
    }

    suspend fun redeemReward() : Boolean {
        _isBusy.value = true
        try {
            val currentRedeem = _currentRedeem.value?:throw Exception("No current redeem data")
            _database.saveRPCRedeemRequest(currentRedeem)
            return true
        } catch (e: Exception) {
            _error.value = e.message
        }
        _isBusy.value = false
        return false
    }
}

class NetworkRPCAdapter(private val redeemClicked:(String) -> Unit) : RecyclerView.Adapter<NetworkRPCAdapter.TimeLineViewHolder>() {

    private var mFeedList: List<RewardPairConnection> = listOf()
    private lateinit var mLayoutInflater: LayoutInflater

    @SuppressLint("NotifyDataSetChanged")
    fun updateList(newList: List<RewardPairConnection>) {
        mFeedList = newList
        this.notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return TimelineView.getTimeLineViewType(position, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeLineViewHolder {

        if(!::mLayoutInflater.isInitialized) {
            mLayoutInflater = LayoutInflater.from(parent.context)
        }

        return TimeLineViewHolder(mLayoutInflater.inflate(R.layout.partials_item_timeline, parent, false), viewType)
    }

    override fun onBindViewHolder(holder: TimeLineViewHolder, position: Int) {

        val pairConnection = mFeedList[position]

        holder.redeemTxt.setTypeface(holder.redeemTxt.typeface, Typeface.NORMAL)
        holder.redeemBtn.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.glcc_gray))
        holder.redeemBtn.isEnabled = false
        holder.redeemBtn.alpha = 0.7f

        when {
            pairConnection.isRedeemed -> {
                setMarker(holder, R.drawable.ic_marker_redeemed, R.color.glcc_orange)
                holder.redeemTxt.text = "Redeemed"
                holder.redeemTxt.setTextColor( ContextCompat.getColor(holder.itemView.context, R.color.glcc_green))
                holder.redeemBtn.isEnabled = true
                holder.redeemBtn.alpha = 1.0f
            }
            !pairConnection.isAvailable -> {
                setMarker(holder, R.drawable.ic_marker_available, R.color.glcc_red)
                holder.redeemTxt.text = "Missed"
                holder.redeemTxt.setTypeface(holder.redeemTxt.typeface, Typeface.ITALIC)
                holder.redeemTxt.setTextColor( ContextCompat.getColor(holder.itemView.context, R.color.glcc_red))
            }
            pairConnection.isAvailable && pairConnection.isEligible -> {
                setMarker(holder, R.drawable.ic_marker_available, R.color.glcc_green)
                holder.redeemTxt.text = "Redeem Now"
                holder.redeemTxt.setTextColor( ContextCompat.getColor(holder.itemView.context, R.color.glcc_green))
                holder.redeemBtn.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.glcc_orange))
                holder.redeemBtn.isEnabled = true
                holder.redeemBtn.alpha = 1.0f
            }
            else -> {
                setMarker(holder, R.drawable.ic_marker_available, R.color.glcc_orange)
                holder.redeemTxt.text = "Waiting"
                holder.redeemTxt.setTypeface(holder.redeemTxt.typeface, Typeface.ITALIC)
                holder.redeemTxt.setTextColor( ContextCompat.getColor(holder.itemView.context, R.color.glcc_green))
            }
        }

        holder.pairTxt.text = pairConnection.pairString
        holder.redeemBtn.setOnClickListener {
            redeemClicked(pairConnection.pairString)
        }
    }

    private fun setMarker(holder: TimeLineViewHolder, drawableResId: Int, colorFilter: Int) {
        holder.timeline.marker = VectorDrawableUtils.getDrawable(holder.itemView.context, drawableResId, ContextCompat.getColor(holder.itemView.context, colorFilter))
    }

    override fun getItemCount() = mFeedList.size

    inner class TimeLineViewHolder(itemView:View, viewType: Int) : RecyclerView.ViewHolder(itemView) {

        val pairTxt: TextView = itemView.findViewById(R.id.pair_string)
        val redeemTxt: TextView = itemView.findViewById(R.id.redeem_text)
        val redeemBtn: Button = itemView.findViewById(R.id.redeem_btn)
        val timeline: TimelineView = itemView.findViewById(R.id.timeline)

        init {
            timeline.initLine(viewType)
        }
    }
}

object VectorDrawableUtils {

    private fun getDrawable(context: Context, drawableResId: Int): Drawable? {
        return VectorDrawableCompat.create(context.resources, drawableResId, context.theme)
    }

    fun getDrawable(context: Context, drawableResId: Int, colorFilter: Int): Drawable? {
        val drawable = getDrawable(context, drawableResId)
        drawable?.setTint(colorFilter)
        return drawable
    }
}

data class NetworkRPC (
    val networkReferral: String,
    val pairLeft: Long,
    val pairRight: Long
)