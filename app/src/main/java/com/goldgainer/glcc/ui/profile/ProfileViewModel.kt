package com.goldgainer.glcc.ui.profile

import android.content.Context
import android.graphics.Bitmap
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.Profile
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.io.File

class ProfileViewModel : ViewModel() {
	var currentOwner : String = ""
	var currentCountry : String = ""

	var detailComplete = false
	var bankComplete = false
	var walletComplete = false
	var successorComplete = false
	var photoComplete = false

	var dBankArray = arrayOf<String>()
	var dRelationshipArray = arrayOf<String>()

	private val _currentProfile = MutableLiveData<Profile>()
	val currentProfile : LiveData<Profile> = _currentProfile

	private val _profileError = MutableLiveData<String>()
	val profileError : LiveData<String> = _profileError

	private val _profileSaved = MutableLiveData<Boolean>()
	val profileSaved : LiveData<Boolean> = _profileSaved

	private val _coachTrainers = MutableLiveData<List<CoachTrainer>>()
	val coachTrainers : LiveData<List<CoachTrainer>> = _coachTrainers

	private val _database = FirebaseDataSource()

	private var _isBusy = false

	fun getProfile() {
		_profileSaved.value = false
		viewModelScope.launch {
			try {
				val helper = Helper()
				dBankArray = helper.getGlobalSettingsArr("bank_array")
				dRelationshipArray = helper.getGlobalSettingsArr("relationship_array")
				val profile = _database.getProfile(currentOwner)?:throw Exception("failed to get profile")

				if( !profile.fullname.isNullOrBlank() && !profile.address.isNullOrBlank() && !profile.phone.isNullOrBlank() && !profile.nationalId.isNullOrBlank()) {
					if(currentCountry == "ID") {
						if (!profile.taxId.isNullOrBlank()) {
							detailComplete = true
						}
					}else{
						detailComplete = true
					}
				}

				if( !profile.bankName.isNullOrBlank() && !profile.bankAccount.isNullOrBlank() && !profile.bankNumber.isNullOrBlank()) {
					bankComplete = true
				}

				if(currentCountry != "ID") {
					bankComplete = true
				}

				if( !profile.glcWallet.isNullOrBlank() && !profile.usdtWallet.isNullOrBlank()) {
					walletComplete = true
				}

				if( !profile.successorFullname.isNullOrBlank() && !profile.successorRelation.isNullOrBlank()  && !profile.successorPhone.isNullOrBlank() && !profile.successorEmail.isNullOrBlank()) {
					successorComplete = true
				}

				if( !profile.nationalIdPhoto.isNullOrBlank() && !profile.selfiePhoto.isNullOrBlank()) {
					photoComplete = true
				}

				_currentProfile.value = profile
			}catch (e: Exception) {
				_profileError.value = e.message
			}
		}
	}

	fun requestVerification() {
		viewModelScope.launch {
			try {
				_currentProfile.value?.let {
					if (it.fullname.isNullOrBlank()) {
						throw Exception("Full name cannot be empty!")
					}
					if (it.address.isNullOrBlank()) {
						throw Exception("Address cannot be empty!")
					}
					if (it.phone.isNullOrBlank()) {
						throw Exception("Phone cannot be empty!")
					}
					if (it.nationalId.isNullOrBlank()) {
						if(currentCountry == "ID") {
							throw Exception("National ID cannot be empty!")
						} else {
							throw Exception("Passport cannot be empty!")
						}
					}
					if(currentCountry == "ID") {
						if (it.taxId.isNullOrBlank()) {
							throw Exception("Tax ID cannot be empty!")
						}
					}
					if(currentCountry == "ID") {
						if (it.bankName.isNullOrBlank()) {
							throw Exception("Bank name cannot be empty!")
						}
						if (it.bankAccount.isNullOrBlank()) {
							throw Exception("Bank account name cannot be empty!")
						}
						if (it.bankNumber.isNullOrBlank()) {
							throw Exception("Bank account number cannot be empty!")
						}
					}
					if (it.glcWallet.isNullOrBlank()) {
						throw Exception("GLC wallet cannot be empty!")
					}
					if (it.usdtWallet.isNullOrBlank()) {
						throw Exception("USDT wallet cannot be empty!")
					}
					if (it.successorFullname.isNullOrBlank()) {
						throw Exception("Successor full name cannot be empty!")
					}
					if (it.successorRelation.isNullOrBlank()) {
						throw Exception("Successor relationship cannot be empty!")
					}
					if (it.successorPhone.isNullOrBlank()) {
						throw Exception("Successor phone cannot be empty!")
					}
					if (it.successorEmail.isNullOrBlank()) {
						throw Exception("Successor email cannot be empty!")
					}
					it.successorEmail?.let { email ->
						if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
							throw Exception("Successor email format is not valid!")
						}
					}

					if (it.nationalIdPhoto.isNullOrBlank()) {
						throw Exception("ID photo cannot be empty!")
					}
					if (it.selfiePhoto.isNullOrBlank()) {
						throw Exception("Selfie photo cannot be empty!")
					}
					it.status = "PA"
					val success = _database.saveProfile(currentOwner, it)
					if (!success) { throw Exception("failed to save profile") }
					_profileSaved.value = true
				}
			}catch(e: Exception) {
				_profileError.value = e.message
			}
		}
	}

	fun saveDetail(fullname: String, address: String, phone: String, nationalid: String, taxid: String) {
		viewModelScope.launch {
			try {
				if (fullname.isNullOrBlank()) {
					throw Exception("Full name cannot be empty!")
				}
				if (address.isNullOrBlank()) {
					throw Exception("Address cannot be empty!")
				}
				if (phone.isNullOrBlank()) {
					throw Exception("Phone cannot be empty!")
				}
				if (nationalid.isNullOrBlank()) {
					if(currentCountry == "ID") {
						throw Exception("National ID cannot be empty!")
					} else {
						throw Exception("Passport cannot be empty!")
					}
				}
				if(currentCountry == "ID") {
					if (taxid.isNullOrBlank()) {
						throw Exception("Tax ID cannot be empty!")
					}
				}
				_currentProfile.value?.let {
					it.fullname = fullname
					it.address = address
					it.phone = phone
					it.nationalId = nationalid
					it.taxId = taxid
					val success = _database.saveProfile(currentOwner, it)
					if (!success) { throw Exception("failed to save profile") }
					_profileSaved.value = true
				}
			} catch (e: Exception) {
				_profileError.value = e.message
			}
		}
	}

	fun saveBank(bankname: String, bankaccount: String, banknumber: String) {
		viewModelScope.launch {
			try {
				if (bankname.isNullOrBlank()) {
					throw Exception("Bank name cannot be empty!")
				}
				if (bankaccount.isNullOrBlank()) {
					throw Exception("Bank account name cannot be empty!")
				}
				if (banknumber.isNullOrBlank()) {
					throw Exception("Bank account number cannot be empty!")
				}
				_currentProfile.value?.let {
					it.bankName = bankname
					it.bankAccount = bankaccount
					it.bankNumber = banknumber
					val success = _database.saveProfile(currentOwner, it)
					if (!success) { throw Exception("failed to save profile") }
					_profileSaved.value = true
				}
			} catch (e: Exception) {
				_profileError.value = e.message
			}
		}
	}

	fun saveWallet(glcwallet: String, usdtwallet: String) {
		viewModelScope.launch {
			try {
				if (glcwallet.isNullOrBlank()) {
					throw Exception("GLC wallet cannot be empty!")
				}
				if (usdtwallet.isNullOrBlank()) {
					throw Exception("USDT wallet cannot be empty!")
				}
				_currentProfile.value?.let {
					it.glcWallet = glcwallet
					it.usdtWallet = usdtwallet
					val success = _database.saveProfile(currentOwner, it)
					if (!success) { throw Exception("failed to save profile") }
					_profileSaved.value = true
				}
			} catch (e: Exception) {
				_profileError.value = e.message
			}
		}
	}

	fun saveSuccessor(fullname: String, relationship: String, phone: String, email: String) {
		viewModelScope.launch {
			try {
				if (fullname.isNullOrBlank()) {
					throw Exception("Successor full name cannot be empty!")
				}
				if (relationship.isNullOrBlank()) {
					throw Exception("Successor relationship cannot be empty!")
				}
				if (phone.isNullOrBlank()) {
					throw Exception("Successor phone cannot be empty!")
				}
				if (email.isNullOrBlank()) {
					throw Exception("Successor email cannot be empty!")
				}
				email?.let {
					if (!Patterns.EMAIL_ADDRESS.matcher(it).matches()) {
						throw Exception("Successor email format is not valid!")
					}
				}
				_currentProfile.value?.let {
					it.successorFullname = fullname
					it.successorRelation = relationship
					it.successorPhone = phone
					it.successorEmail = email
					val success = _database.saveProfile(currentOwner, it)
					if (!success) { throw Exception("failed to save profile") }
					_profileSaved.value = true
				}
			} catch (e: Exception) {
				_profileError.value = e.message
			}
		}
	}

	fun savePhoto(idphoto: File?, selfiephoto: File?) {
		viewModelScope.launch {
			try {
				if (_isBusy) {
					throw Exception("Processing image, please try again.")
				}

				_currentProfile.value?.let {
					if (it.nationalIdPhoto == null && idphoto == null) {
						if(currentCountry == "ID") {
							throw Exception("National ID Card photo cannot be empty!")
						}else{
							throw Exception("Passport photo cannot be empty!")
						}
					}
					if (it.selfiePhoto == null && selfiephoto == null) {
						throw Exception("Selfie photo cannot be empty!")
					}

					//upload foto
					val storage = Firebase.storage("gs://glcc-app-users/")
					var storageRef = storage.reference

					if (idphoto != null) {
						var nationalIdRef = storageRef.child("$currentOwner/nationalid.jpg")
						nationalIdRef.putFile(idphoto.toUri()).await()
						it.nationalIdPhoto = nationalIdRef.path
					}
					if (selfiephoto != null) {
						var selfieRef = storageRef.child("$currentOwner/selfie.jpg")
						selfieRef.putFile(selfiephoto.toUri()).await()
						it.selfiePhoto = selfieRef.path
					}

					val success = _database.saveProfile(currentOwner, it)
					if (!success) { throw Exception("failed to save profile") }
					_profileSaved.value = true
				}
			} catch (e: Exception) {
				_profileError.value = e.message
			}
		}
	}

	fun compressPhoto(photoFile: File, context: Context) {
		_isBusy = true
		viewModelScope.launch {
			Compressor.compress(context, photoFile) {
				quality(80)
				format(Bitmap.CompressFormat.JPEG)
				size(200000)
				destination(photoFile)
			}
			_isBusy = false
		}
	}

	fun loadCoachTrainer() {
		viewModelScope.launch {
			try {
				val userNetworks = _database.getUserNetworks(currentOwner)
				val coachTrainers = userNetworks.map {
					val coachNetwork = _database.getNetwork(it.sponsorId!!)?:throw Exception("no coach")
					val trainerNetwork = _database.getNetwork(it.trainerId!!)?:throw Exception("no trainer")
					CoachTrainer(
						category = it.category,
						coach = _database.getProfile(coachNetwork.owner),
						trainer = _database.getProfile(trainerNetwork.owner)
					)
				}
				_coachTrainers.value = coachTrainers
			} catch (e: Exception) {
				_profileError.value = e.message
			}
		}
	}

	suspend fun submitVirtualAccount(account: String): String? {
		if (account.isNullOrEmpty()) {
			return "Account number cannot be empty."
		}
		_isBusy = true
		val profile = _currentProfile.value
		if (profile != null) {
			profile.virtualAccount = account
			_database.saveProfile(currentOwner, profile)
			return null
		}
		return "Error saving virtual account."
		_isBusy = false
	}

}

data class CoachTrainer (
	val category: String,
	val coach: Profile?,
	val trainer: Profile?
)

class CoachTrainerAdapter(theContext: Context, coachTrainers: List<CoachTrainer>)
	: ArrayAdapter<CoachTrainer>(theContext,0, coachTrainers) {

	override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
		val coachTrainer = getItem(position)?:return super.getView(position, convertView, parent)

		var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_coachtrainer_item, parent, false)

		val networkCategory = view.findViewById<TextView>(R.id.networkcategory_text)
		val coachName = view.findViewById<TextView>(R.id.coach_name)
		val coachEmail = view.findViewById<TextView>(R.id.coach_email)
		val coachPhone = view.findViewById<TextView>(R.id.coach_phone)

		val trainerName = view.findViewById<TextView>(R.id.trainer_name)
		val trainerEmail = view.findViewById<TextView>(R.id.trainer_email)
		val trainerPhone = view.findViewById<TextView>(R.id.trainer_phone)

		networkCategory.text = coachTrainer.category

		if (coachTrainer.coach != null) {
			coachName.text = "Name : ${coachTrainer.coach.fullname?:"-n/a-"}"
			coachEmail.text = "Email : ${coachTrainer.coach.owner?:"-n/a-"}"
			coachPhone.text = "Phone : ${coachTrainer.coach.phone?:"-n/a-"}"
		}

		if (coachTrainer.trainer != null) {
			trainerName.text = "Name : ${coachTrainer.trainer.fullname?:"-n/a-"}"
			trainerEmail.text = "Email : ${coachTrainer.trainer.owner?:"-n/a-"}"
			trainerPhone.text = "Phone : ${coachTrainer.trainer.phone?:"-n/a-"}"
		}

		return view
	}
}