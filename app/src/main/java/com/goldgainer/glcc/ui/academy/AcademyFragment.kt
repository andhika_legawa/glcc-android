package com.goldgainer.glcc.ui.academy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.skydoves.expandablelayout.ExpandableLayout

class AcademyFragment : AuthenticatedFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_academy, container, false)
        setUserHeader(root.findViewById(R.id.user_header))

        val prodi1 = root.findViewById<ExpandableLayout>(R.id.prodi1)
        val prodi1Btn = root.findViewById<Button>(R.id.prodi1_btn)

        val prodi2 = root.findViewById<ExpandableLayout>(R.id.prodi2)
        val prodi2Btn = root.findViewById<Button>(R.id.prodi2_btn)

        val prodi3 = root.findViewById<ExpandableLayout>(R.id.prodi3)
        val prodi3Btn = root.findViewById<Button>(R.id.prodi3_btn)

        val prodi4 = root.findViewById<ExpandableLayout>(R.id.prodi4)
        val prodi4Btn = root.findViewById<Button>(R.id.prodi4_btn)

        val prodi5 = root.findViewById<ExpandableLayout>(R.id.prodi5)
        val prodi5Btn = root.findViewById<Button>(R.id.prodi5_btn)

        prodi1.setOnClickListener {
            if (prodi1.isExpanded) {
                prodi1.collapse()
            } else {
                prodi1.expand()
            }
        }
        prodi1Btn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_academy_to_nav_academy_course, bundleOf("prodi" to 1))
        }

        prodi2.setOnClickListener {
            if (prodi2.isExpanded) {
                prodi2.collapse()
            } else {
                prodi2.expand()
            }
        }
        prodi2Btn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_academy_to_nav_academy_course, bundleOf("prodi" to 2))
        }

        prodi3.setOnClickListener {
            if (prodi3.isExpanded) {
                prodi3.collapse()
            } else {
                prodi3.expand()
            }
        }
        prodi3Btn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_academy_to_nav_academy_course, bundleOf("prodi" to 3))
        }

        prodi4.setOnClickListener {
            if (prodi4.isExpanded) {
                prodi4.collapse()
            } else {
                prodi4.expand()
            }
        }
        prodi4Btn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_academy_to_nav_academy_course, bundleOf("prodi" to 4))
        }

        prodi5.setOnClickListener {
            if (prodi5.isExpanded) {
                prodi5.collapse()
            } else {
                prodi5.expand()
            }
        }
        prodi5Btn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_academy_to_nav_academy_course, bundleOf("prodi" to 5))
        }

        return root
    }
}