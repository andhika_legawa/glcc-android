package com.goldgainer.glcc.ui.dashboard

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.glcc.MainActivity
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.Network
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.*


class DashboardFragment : AuthenticatedFragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        dashboardViewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        setUserHeader(root.findViewById(R.id.dashboard_user_header))
        dashboardViewModel.userData.observe(viewLifecycleOwner, Observer {
            setUserHeader(root.findViewById(R.id.dashboard_user_header))
        })

        val feedTitle = root.findViewById<TextView>(R.id.dashboard_feed_text)
        val feedSpacer = root.findViewById<View>(R.id.dashboard_feed_spacer)
        val notifications = root.findViewById<RecyclerView>(R.id.dashboard_notifications)
        val nodeGrid = root.findViewById<RecyclerView>(R.id.dashboard_recyclerview)

        feedTitle.visibility = View.GONE
        feedSpacer.visibility = View.GONE
        notifications.visibility = View.GONE

        val feedManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        notifications.layoutManager = feedManager

        val gridManager = GridLayoutManager(activity, 5, GridLayoutManager.VERTICAL, false)
        nodeGrid.layoutManager = gridManager

        val regionSwitcher = root.findViewById<LinearLayout>(R.id.dashboard_region_switch)
        val indonesiaRadio = root.findViewById<RadioButton>(R.id.indonesia_radio)
        val globalRadio = root.findViewById<RadioButton>(R.id.global_radio)

        if (Helper.getLocalUser().country == "ID") {
            indonesiaRadio.setOnClickListener {
                loggedInUser?.let {
                    dashboardViewModel.selectNetworkType(it.email!!, "I")
                }
            }
            globalRadio.setOnClickListener {
                loggedInUser?.let {
                    dashboardViewModel.selectNetworkType(it.email!!, "G")
                }
            }
        } else {
            regionSwitcher.visibility = View.GONE
        }

        dashboardViewModel.selectedType.observe(viewLifecycleOwner, Observer {
            val selectedType = it ?: return@Observer

            if (selectedType == "I") {
                indonesiaRadio.isChecked = true
                globalRadio.isChecked = false
            }

            if (selectedType == "G") {
                indonesiaRadio.isChecked = false
                globalRadio.isChecked = true
            }
        })

        dashboardViewModel.notifications.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) {
                feedTitle.visibility = View.GONE
                feedSpacer.visibility = View.GONE
                notifications.visibility = View.GONE
            } else {
                feedTitle.visibility = View.VISIBLE
                feedSpacer.visibility = View.VISIBLE
                notifications.visibility = View.VISIBLE

                notifications.adapter = NotificationViewAdapter(it) { notification ->
                    when (notification.type) {
                        "ONHOLD" -> {
                            val networkOnHold = notification.networkonhold
                            if (networkOnHold != null) {
                                activateOnHoldNetwork(networkOnHold, true)
                            }
                        }
                        "TOPUP" -> {
                            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                                .setTitle(notification.title)
                                .setMessage(notification.message)
                                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                                    val bundle = bundleOf(
                                        "selectedNetwork" to notification.networkId
                                    )
                                    findNavController().navigate(
                                        R.id.action_nav_dashboard_to_nav_nodes_create,
                                        bundle
                                    )
                                }
                                .setNegativeButton(android.R.string.cancel, null)
                                .show()
                        }
                        else -> {
                            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                                .setTitle(notification.title)
                                .setMessage(notification.message)
                                .setPositiveButton(android.R.string.ok, null)
                                .show()
                        }
                    }
                }
            }

        })

        val categoryRange = root.findViewById<TextView>(R.id.dashboard_networkcategory_range)
        dashboardViewModel.networkCategories.observe(viewLifecycleOwner, Observer { it ->
            if (it.isNullOrEmpty()) { return@Observer }
            categoryRange.text = "( ${it.first().name} - ${it.last().name} )"
            nodeGrid.adapter = NetworkGridAdapter(it) { network ->
                //what is clicked
                when {
                    network.locked -> {
                        Toast.makeText(context, "Network Locked!", Toast.LENGTH_LONG).show()
                    }
                    !network.active -> {
                        Toast.makeText(context, "Network Closed!", Toast.LENGTH_LONG).show()
                    }
                    network.filled != null -> {
                        val bundle = bundleOf("selectedNetwork" to network.filled?.referralId, "selectedType" to network.filled?.type)
                        findNavController().navigate(R.id.action_nav_dashboard_to_nav_snapshot, bundle)
                    }
                    network.onhold != null -> {
                        val networkOnHold = network.onhold
                        if (networkOnHold!=null) {
                            activateOnHoldNetwork(networkOnHold, true)
                        }
                    }
                    else -> {
                        val bundle = bundleOf("selectedType" to network.type, "selectedDesc" to network.description, "selectedNumber" to network.number)
                        findNavController().navigate(R.id.action_nav_dashboard_to_nav_network_create, bundle)
                    }
                }
            }
        })

        dashboardViewModel.networkOnHold.observe(viewLifecycleOwner, Observer { networkOnHold ->
            activateOnHoldNetwork(networkOnHold)
        })

        val networkString = root.findViewById<TextView>(R.id.dashboard_network_string)
        val totalAsset = root.findViewById<TextView>(R.id.dashboard_totalbonus)
        val totalNodes = root.findViewById<TextView>(R.id.dashboard_totalnodes)
        val totalBSponsor = root.findViewById<TextView>(R.id.dashboard_totalbsponsor)
        val totalBPairing = root.findViewById<TextView>(R.id.dashboard_totalbpairing)
        val totalBRollup = root.findViewById<TextView>(R.id.dashboard_totalbrollup)

        dashboardViewModel.totalAsset.observe(viewLifecycleOwner, Observer {
            networkString.text = it.assetNetwork
            totalNodes.text = it.totalNodes
            totalAsset.text = it.totalBonus
            totalBSponsor.text = it.totalBSponsor
            totalBPairing.text = it.totalBPairing
            totalBRollup.text = it.totalBRollup
        })

        val pairList = root.findViewById<RecyclerView>(R.id.pair_recycler)
        val pairManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        pairList.layoutManager = pairManager
        dashboardViewModel.pairConnection.observe(viewLifecycleOwner) {
            pairList.adapter = PairConnectionAdapter(it) { pairConnection ->
                val bundle = bundleOf("type" to pairConnection.networkType, "number" to pairConnection.networkNumber )
                findNavController().navigate( R.id.action_global_nav_reward_journey, bundle)
            }
        }

        return root
    }

    private fun activateOnHoldNetwork(networkOnHold: Network, cancelable:Boolean = true) {
        var type = "Indonesia"
        if (networkOnHold.type == "G") {
            type = "Global"
        }
        MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
            .setTitle("Network on-hold")
            .setMessage("To activate $type Network N${networkOnHold.number}, you need to create Node(s). You will be redirected to node creation page.")
            .setPositiveButton(android.R.string.ok) { dialog: DialogInterface, _: Int ->
                val bundle = bundleOf(
                    "selectedNetwork" to networkOnHold.referralId,
                    "selectedType" to networkOnHold.type
                )
                findNavController().navigate(
                    R.id.action_nav_dashboard_to_nav_nodes_create,
                    bundle
                )
            }
            .setCancelable(cancelable)
            .show()
    }

    override fun onStart() {
        super.onStart()

        val mainActivity = activity as MainActivity
        val loggedInUser = mainActivity.getCurrentUser()
        loggedInUser?.let {
            var defaultType = "I"
            if (Helper.getLocalUser().country != "ID") {
                defaultType = "G"
            }
            dashboardViewModel.selectNetworkType(it.email!!, defaultType)
            dashboardViewModel.checkNetworkOnHold(it.email!!)
            dashboardViewModel.updateNotification(it.email!!)
        }
    }

}