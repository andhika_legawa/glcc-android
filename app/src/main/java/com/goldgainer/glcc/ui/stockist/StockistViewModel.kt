package com.goldgainer.glcc.ui.stockist

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.NetworkCategory
import com.goldgainer.glcc.data.model.Stockist
import com.goldgainer.glcc.data.model.StockistTransaction
import com.goldgainer.glcc.data.model.VoucherPackage
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.destination
import id.zelory.compressor.constraint.format
import id.zelory.compressor.constraint.quality
import id.zelory.compressor.constraint.size
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import org.json.JSONArray
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class StockistViewModel : ViewModel() {

    private val _localUser = Helper.getLocalUser()
    private val _database = FirebaseDataSource()

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _networkCategorySet = MutableLiveData<List<NetworkCategory>>()
    val networkCategories: LiveData<List<NetworkCategory>> = _networkCategorySet

    private val _voucherPackageSet = MutableLiveData<List<VoucherPackage>>()
    val voucherPackages: LiveData<List<VoucherPackage>> = _voucherPackageSet

    private val _paymentInfo = MutableLiveData<PaymentInfo>()
    val paymentInfo: LiveData<PaymentInfo> = _paymentInfo

    var selectedNetworkIndex : Int = 0

    var selectedVoucherPackage : VoucherPackage? = null

    private val _currentStockist = MutableLiveData<Stockist>()
    val currentStockist: LiveData<Stockist> = _currentStockist

    private val _stock = MutableLiveData<VoucherStock>()
    val voucherStock: LiveData<VoucherStock> = _stock

    private val _regions = MutableLiveData<JSONArray>()
    val regions: LiveData<JSONArray> = _regions

    private var sendPINDate: Date? = null

    private var currentPIN: String? = null

    fun getActiveCategories() {
        viewModelScope.launch {
            val iCategories = _database.getNetworkCategories("I")
            val gCategories = _database.getNetworkCategories("G")
            var activeCategories = iCategories.filter { it.active && !it.locked } + gCategories.filter {  it.active && !it.locked }
            _networkCategorySet.value = activeCategories
        }
    }

    fun getVoucherPackages() {
        viewModelScope.launch {
            val packages = _database.getVoucherPackages()
            val categories = _networkCategorySet.value
            categories?.let {
                val networkCategory = categories[selectedNetworkIndex]
                val filteredPackages = packages.filter { it.networkType == networkCategory.type && it.networkNumber == networkCategory.number }
                _voucherPackageSet.value = filteredPackages.sortedBy { it.amount }
            }
        }
    }

    fun getPaymentInfo(currency: String) {
        viewModelScope.launch {
            val helper = Helper()
            if (currency == "IDR") {
                val bankAccount = helper.getGlobalSettings("bank_account", "NA") as String
                val bankNumber = helper.getGlobalSettings("bank_number", "NA") as String
                val bankName = helper.getGlobalSettings("bank_name", "NA") as String
                val bankInfo = PaymentInfo(type = 0, account = bankAccount, number = bankNumber, name = bankName)
                _paymentInfo.value = bankInfo
                return@launch
            }

            val currencies = _database.getDigitalCurrencies()
            val digitalCurrency = currencies.find { it.name == currency }

            var paymentInfo = PaymentInfo(type = 1, account = "NA", number = "NA", name = "NA")
            if (digitalCurrency != null) {
                paymentInfo = PaymentInfo(type = 1, account = digitalCurrency.name, number = digitalCurrency.address, name =  digitalCurrency.name)
            }
            _paymentInfo.value = paymentInfo
        }
    }

    private suspend fun getVoucherStock() : VoucherStock {
        val emptyStock = VoucherStock( "",0,0, 0, 0, 0)
        _localUser.email?.let { email ->
            val vouchers = _database.getVouchersByStockist(email)
            if (vouchers.isEmpty()) {
                return emptyStock
            }
            val networks = _networkCategorySet.value ?: return emptyStock
            val network = networks[selectedNetworkIndex]
            val filtered = vouchers.filter { it.type == network.type && it.number == network.number && it.soldDate == null }
            val node1 = filtered.filter { it.quantity == 1 }.size
            val node3 = filtered.filter { it.quantity == 3 }.size
            val node7 = filtered.filter { it.quantity == 7 }.size
            val node15 = filtered.filter { it.quantity == 15 }.size
            return VoucherStock(network.type, network.number, node1, node3, node7, node15)
        }
        return emptyStock
    }

    fun loadVoucherStock() {
        viewModelScope.launch {
            _stock.value = getVoucherStock()
        }
    }

    suspend fun networkActivated() : Boolean {
        _localUser.email?.let {
            val nodes = _database.getAllNodes(it)
            if (nodes.isEmpty()) {
                return false
            }
        }
        return true
    }

    suspend fun isStockist() : Boolean {
        _localUser.email?.let {
            val stockist = _database.getStockist(it)
            if (stockist != null && stockist.status == 1) {
                _currentStockist.value = stockist
                return true;
            }
        }
        return false
    }

    fun getProvinces(context : Context) {
        val jsonText = context.assets.open("regions.json").bufferedReader().use { it.readText() }
        val jsonArray = JSONArray(jsonText)
        _regions.value = jsonArray
    }

    suspend fun saveLocation(province: String, city: String, whatsapp: String, telegram: String) : Boolean {
        try {
            val email = _localUser.email?:throw Error("No Email")
            val username = _localUser.name?:throw Error("No Username")
            var stockist = _database.getStockist(email)
            if (stockist == null) {
                stockist = Stockist(
                    owner = email,
                    registerDate = Date(),
                    status = 1
                )
            }
            stockist.username = username
            stockist.province = province
            stockist.city = city
            stockist.whatsapp = whatsapp
            stockist.telegram = telegram
            return _database.saveStockist(email, stockist)
        } catch (e: Exception) {
            return false
        }
    }

    fun compressPhoto(photoFile: File, context: Context) {
        viewModelScope.launch {
            Compressor.compress(context, photoFile) {
                quality(80)
                format(Bitmap.CompressFormat.JPEG)
                size(200000)
                destination(photoFile)
            }
        }
    }

    suspend fun requestTopup(paymentFile: File?) : String? {
        val voucherPackage = selectedVoucherPackage ?: return "No package selected"
        if (paymentFile == null) {
            return "No payment confirmation uploaded"
        }
        try {
            _localUser.email?.let { email ->
                var totalPayment = voucherPackage.price
                if (voucherPackage.currency == "IDR") {
                    val calTax = voucherPackage.price * 0.1
                    totalPayment += calTax
                }

                val storage = Firebase.storage("gs://glcc-app-users/")
                var storageRef = storage.reference

                var paymentPath: String? = null
                if (paymentFile != null) {
                    var paymentRef = storageRef.child("$email/payment/" + paymentFile.name)
                    paymentRef.putFile(paymentFile.toUri()).await()
                    paymentPath = paymentRef.path
                }

                val topupTransaction = StockistTransaction(
                    owner = email,
                    type = 0,
                    status = "PENDING",
                    networkType = voucherPackage.networkType,
                    networkNumber = voucherPackage.networkNumber,
                    nodeType = voucherPackage.nodeType,
                    amount = voucherPackage.amount,
                    price = voucherPackage.price,
                    total = totalPayment,
                    currency = voucherPackage.currency,
                    paymentPhoto = paymentPath,
                    date = Date()
                )

                val success = _database.saveStockistTransaction(topupTransaction)
                if (!success) {
                    return "Failed saving request"
                }
            }
        } catch (e: Exception) {
            return e.toString()
        }
        return null
    }

    suspend fun findTransaction(start: Date, end: Date, isTopup: Boolean, isSell: Boolean) : List<StockistTransaction> {
        _localUser.email?.let { email ->
            val result = _database.getStockistTransaction(email, start, end)
            if (isTopup && !isSell) {
                return result.filter { it.type == 0 }.sortedByDescending { it.date }
            }
            if (!isTopup && isSell) {
                return result.filter { it.type == 1 }.sortedByDescending { it.date }
            }
            return result.sortedByDescending { it.date }
        }
        return listOf()
    }

    suspend fun findStockist(province: String, city: String): List<Stockist> {
        val stockistList = _database.getStockistByLocation(province, city)
        return stockistList.filter { stockist ->
            val vouchers = _database.getVouchersByStockist(stockist.owner)
            val filteredVouchers = vouchers.filter { it.soldDate == null }
            filteredVouchers.isNotEmpty()
        }
    }

    fun sendPIN() {
        viewModelScope.launch {
            try {
                val lastSend = sendPINDate
                if (lastSend != null ) {
                    val diff = Date().time - lastSend.time
                    val seconds =  diff / 3600
                    val minutes = seconds / 60
                    if (minutes < 1) throw Exception("Please try again in a minute.")
                }

                val owner = _localUser.email?:throw Exception("Null email")
                val functions = Firebase.functions(regionOrCustomDomain = "asia-southeast2")
                currentPIN = functions.getHttpsCallable("sendPIN2").call(hashMapOf("email" to owner)).await().data as String
                sendPINDate = Date()
                _error.value = "PIN sent to $owner"
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    suspend fun sellVoucher(type: Int, amountInt: Int, email: String, pinInput: String) : String? {
        try {
            val lastSend = sendPINDate
            if (lastSend != null ) {
                val diff = Date().time - lastSend.time
                val seconds =  diff / 3600
                val minutes = seconds / 60
                if (minutes > 1) currentPIN = null
            }

            if (pinInput != currentPIN) {
                return "Wrong PIN!"
            }

            if (amountInt <= 0) {
                return "Amount must be greater than zero"
            }

            val currentStock = getVoucherStock()
            if (type == 1 && currentStock.node1 < amountInt) {
                return "You don't have enough 1 Node Voucher"
            }
            if (type == 3 && currentStock.node3 < amountInt) {
                return "You don't have enough 3 Node Voucher"
            }
            if (type == 7 && currentStock.node7 < amountInt) {
                return "You don't have enough 7 Node Voucher"
            }
            if (type == 15 && currentStock.node15 < amountInt) {
                return "You don't have enough 15 Node Voucher"
            }

            val sellTransaction = StockistTransaction(
                owner = _localUser.email?:"",
                type = 1,
                status = "PENDING",
                networkType = currentStock.networkType,
                networkNumber = currentStock.networkNumber,
                nodeType = type,
                amount = amountInt,
                price = 0.0,
                total = 0.0,
                currency = "",
                paymentPhoto = "",
                date = Date(),
                soldTo = email
            )

            val success = _database.saveStockistTransaction(sellTransaction)
            if (!success) {
                return "Failed saving request"
            }

        }catch(e:Exception) {
            return e.message
        }
        return null
    }
}

data class VoucherStock (
    val networkType: String = "",
    val networkNumber: Int = 0,
    val node1: Int = 0,
    val node3: Int = 0,
    val node7: Int = 0,
    val node15: Int = 0
)

data class PaymentInfo (
    val type: Int = 0,
    val account: String = "",
    val number: String = "",
    val name: String = ""
)

class StockistAdapter(theContext: Context, stockists: List<Stockist>, private val whatsappClicked:(String) -> Unit, private val telegramClicked:(String) -> Unit)
    : ArrayAdapter<Stockist>(theContext,0, stockists) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val stockist = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_stockist_item, parent, false)
        val nameText = view.findViewById<TextView>(R.id.stockist_item_username)
        val waBtn = view.findViewById<ImageButton>(R.id.stockist_item_whatsapp)
        val teBtn = view.findViewById<ImageButton>(R.id.stockist_item_telegram)

        nameText.text = stockist.username

        if (stockist.whatsapp.isNotEmpty()) {
            waBtn.alpha = 1.0f
            waBtn.setOnClickListener {
                whatsappClicked(stockist.whatsapp)
            }
        } else {
            waBtn.alpha = 0.5f
            waBtn.setOnClickListener {  }
        }

        if (stockist.telegram.isNotEmpty()) {
            teBtn.alpha = 1.0f
            teBtn.setOnClickListener {
                telegramClicked(stockist.telegram)
            }
        } else {
            teBtn.alpha = 0.5f
            teBtn.setOnClickListener {  }
        }

        return view
    }
}

class StockistTrxAdapter(theContext: Context, transactions: List<StockistTransaction>)
    : ArrayAdapter<StockistTransaction>(theContext,0, transactions) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val trx = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_voucher_item, parent, false)
        val dateTxt = view.findViewById<TextView>(R.id.voucher_item_date)
        val infoTxt = view.findViewById<TextView>(R.id.voucher_item_info)
        val amountTxt = view.findViewById<TextView>(R.id.voucher_item_amount)

        val sdf = SimpleDateFormat("MMM dd")
        dateTxt.text = sdf.format(trx.date)
        var info = trx.soldTo
        var color = ContextCompat.getColor(view.context, R.color.glcc_green)
        if (trx.type == 0) {
            info = "TP : ${trx.nodeType} Node"
            color = ContextCompat.getColor(view.context, R.color.white)
        }
        infoTxt.text = info
        infoTxt.setTextColor(color)
        amountTxt.text = trx.amount.toString()
        amountTxt.setTextColor(color)

        return view
    }
}