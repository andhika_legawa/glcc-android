package com.goldgainer.glcc.ui.profile

import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.*


class ProfileFragment: AuthenticatedFragment() {

	private lateinit var profileViewModel: ProfileViewModel

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_profile, container, false)
		setUserHeader(root.findViewById(R.id.profile_user_header))

		loggedInUser?.let {
			profileViewModel.currentOwner = it.email!!
		}

		val job = Job()
		val uiScope = CoroutineScope(Dispatchers.Main + job)

		val userData = Helper.getLocalUser()

		val profileBtn = root.findViewById<Button>(R.id.profile_profile_btn)
		val bankBtn = root.findViewById<Button>(R.id.profile_bankaccount_btn)
		val walletBtn = root.findViewById<Button>(R.id.profile_digitalwallet_btn)
		val successorBtn = root.findViewById<Button>(R.id.profile_successor_btn)
		val photoBtn = root.findViewById<Button>(R.id.profile_photo_btn)

		val profileStatus = root.findViewById<TextView>(R.id.text_profile_status)
		val verifyBtn = root.findViewById<Button>(R.id.profile_verify_btn)
		val virtualBtn = root.findViewById<Button>(R.id.profile_virtual_account)
		val virtualLabel = root.findViewById<TextView>(R.id.text_virtual_account)

		val coachTab = root.findViewById<MaterialButton>(R.id.coach_tab)

		userData.country?.let{
			profileViewModel.currentCountry = it
			if (it == "ID") {
				bankBtn.visibility = View.VISIBLE
			}
		}

		coachTab.setOnClickListener {
			findNavController().navigate(R.id.action_nav_profile_to_nav_coach)
		}

		profileBtn.setOnClickListener {
			findNavController().navigate(R.id.action_nav_profile_to_nav_profile_detail)
		}

		bankBtn.setOnClickListener {
			findNavController().navigate(R.id.action_nav_profile_to_nav_profile_bank)
		}

		walletBtn.setOnClickListener {
			findNavController().navigate(R.id.action_nav_profile_to_nav_profile_wallet)
		}

		successorBtn.setOnClickListener {
			findNavController().navigate(R.id.action_nav_profile_to_nav_profile_successor)
		}

		photoBtn.setOnClickListener {
			findNavController().navigate(R.id.action_nav_profile_to_nav_profile_photo)
		}

		profileViewModel.currentProfile.observe(viewLifecycleOwner, Observer { profile ->
			if (profileViewModel.detailComplete) {
				profileBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_orange))
				profileBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
			}
			if (profileViewModel.bankComplete) {
				bankBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_orange))
				bankBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
			}
			if (profileViewModel.walletComplete) {
				walletBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_orange))
				walletBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
			}
			if (profileViewModel.successorComplete) {
				successorBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_orange))
				successorBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
			}
			if (profileViewModel.photoComplete) {
				photoBtn.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.glcc_orange))
				photoBtn.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_black))
			}

			if (profile.status == "EDIT") {
				if (profileViewModel.detailComplete && profileViewModel.walletComplete && profileViewModel.successorComplete && profileViewModel.photoComplete) {
					if (profileViewModel.currentCountry == "ID") {
						if(profileViewModel.bankComplete){
							verifyBtn.visibility = View.VISIBLE
							profileStatus.text = ""
						}
					}else{
						verifyBtn.visibility = View.VISIBLE
						profileStatus.text = ""
					}
				}
			}

			if (profile.status == "PA") {
				profileStatus.text = "Your personal information is being verified"
				verifyBtn.visibility = View.GONE
			}

			if (profile.status == "RA") {
				if (profile.statusNote.isNotBlank()){
					profileStatus.text = profile.statusNote
				}else{
					profileStatus.text = "Your personal information is rejected. You can edit and request verification again."
				}
				verifyBtn.visibility = View.VISIBLE
			}

			if (profile.status == "AV") {
				profileStatus.text = "Your personal information has been approved."
				val greenColor = ContextCompat.getColor(requireContext(), R.color.glcc_green)
				profileStatus.setTextColor(greenColor)
				verifyBtn.visibility = View.GONE
				if (profile.virtualAccount.isNullOrEmpty()) {
					virtualBtn.visibility = View.VISIBLE
				} else {
					virtualLabel.visibility = View.VISIBLE
					virtualLabel.text = "Your VA Number : " + profile.virtualAccount
				}
			}
		})

		profileViewModel.profileError.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		profileViewModel.profileSaved.observe(viewLifecycleOwner, Observer { success ->
			if (success) {
				profileViewModel.getProfile()
				MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
					.setTitle("Request Verification Sent")
					.setMessage("Your Personal Information has been submitted successfully. Please wait for Admin Approval")
					.setPositiveButton(android.R.string.ok, null)
					.show()
			}
		})

		verifyBtn.setOnClickListener {
			MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
				.setTitle("Request Verification")
				.setMessage("Submit request for verification? All information will be verified by Admin.")
				.setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
					profileViewModel.requestVerification()
				}
				.setNegativeButton(android.R.string.cancel, null)
				.setCancelable(false)
				.show()
		}

		virtualBtn.setOnClickListener {
			val container = LinearLayout(context)
			container.orientation = LinearLayout.VERTICAL
			val virtualEditText = EditText(context)
			val layout = LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT
			)
			virtualEditText.setTextColor(Color.BLACK)
			container.addView(virtualEditText)
			layout.setMargins(60, 0, 60, 0)
			virtualEditText.layoutParams = layout
			MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
				.setTitle("Submit Virtual Account")
				.setMessage("Please make sure it's the valid one.")
				.setPositiveButton("SUBMIT") { _: DialogInterface, _: Int ->
					uiScope.launch {
						val error =  profileViewModel.submitVirtualAccount(virtualEditText.text.toString())
						if (error == null) {
							MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
								.setTitle("Virtual Account Submitted")
								.setMessage("Your virtual account has been submitted successfully. Contact admin if you need to change it.")
								.setPositiveButton(android.R.string.ok, null)
								.show()
							virtualBtn.visibility = View.GONE
						} else {
							MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
								.setTitle("Submit Failed")
								.setMessage(error)
								.setPositiveButton(android.R.string.ok, null)
								.show()
						}
					}
				}
				.setView(container)
				.setNegativeButton(android.R.string.cancel, null)
				.setCancelable(false)
				.show()
		}

		return root
	}

	override fun onResume() {
		super.onResume()
		profileViewModel.getProfile()
	}

}