package com.goldgainer.glcc.ui.network

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.afterTextChanged
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.goldgainer.glcc.ui.login.Trainer
import com.goldgainer.glcc.ui.login.TrainerArrayAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class CreateNetworkFragment : AuthenticatedFragment() {

    private lateinit var networkViewModel: NetworkViewModel

    private lateinit var platinumTrainerAdapter : TrainerArrayAdapter
    private lateinit var goldTrainerAdapter : TrainerArrayAdapter
    private lateinit var trainer : AutoCompleteTextView
    private lateinit var trainerId: TextView
    private var selectedPosition : Int = 0
    private var selectedTrainerPosition : Int = 0

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        networkViewModel = ViewModelProvider(this).get(NetworkViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_networks_create, container, false)
        setUserHeader(root.findViewById(R.id.ncreate_user_header))

        val sponsor = root.findViewById<EditText>(R.id.ncreate_sponsor)
        trainer = root.findViewById(R.id.ncreate_trainer)
        trainerId = root.findViewById(R.id.ncreate_trainer_id)
        val platinumRadio = root.findViewById<RadioButton>(R.id.platinum_radio)
        val goldRadio = root.findViewById<RadioButton>(R.id.gold_radio)
        val trainerPlatinumRadio = root.findViewById<RadioButton>(R.id.platinum_radio3)
        val trainerGoldRadio = root.findViewById<RadioButton>(R.id.gold_radio3)
        val nextButton = root.findViewById<Button>(R.id.ncreate_next_button)
        nextButton.isEnabled = true

        val networkText = root.findViewById<TextView>(R.id.ncreate_description_textview)
        arguments?.let {
            networkViewModel.selectedType = it.getString( "selectedType")
            networkViewModel.selectedNumber = it.getInt( "selectedNumber")
            networkText.text = it.getString("selectedDesc")
        }

        sponsor.afterTextChanged {
            val userData = Helper.getLocalUser()
            networkViewModel.referralDataChanged(sponsor.text.toString(), userData.country)
        }

        platinumTrainerAdapter = TrainerArrayAdapter(requireContext(), R.layout.partials_dropdown_item, android.R.id.text1, listOf())
        goldTrainerAdapter = TrainerArrayAdapter(requireContext(), R.layout.partials_dropdown_item, android.R.id.text1, listOf())

        trainerPlatinumRadio.isEnabled = false
        trainerGoldRadio.isEnabled = false
        networkViewModel.referralTrainer.observe(viewLifecycleOwner, Observer {
            if (it.coach == null) {
                sponsor.error = getString(R.string.invalid_sponsor)
            }
            platinumTrainerAdapter = TrainerArrayAdapter(requireContext(), R.layout.partials_dropdown_item, android.R.id.text1, it.platinumTrainers)
            goldTrainerAdapter = TrainerArrayAdapter(requireContext(), R.layout.partials_dropdown_item, android.R.id.text1, it.goldTrainers)
            trainerPlatinumRadio.isChecked = false
            trainerGoldRadio.isChecked = false
            updateTrainerUI()
        })

//        networkViewModel.trainerFirstLevel.observe(viewLifecycleOwner, Observer {  firstLevel ->
//            firstLevel.forEach {
//                if (it.binaryPosition == 0) {
//                    trainerPlatinumRadio.isChecked = false
//                    trainerPlatinumRadio.isEnabled = false
//                    trainerPlatinumRadio.alpha = 0.5F
//                    trainerGoldRadio.isChecked = true
//                    trainerGoldRadio.isEnabled = true
//                }
//                if (it.binaryPosition == 1) {
//                    trainerPlatinumRadio.isChecked = true
//                    trainerPlatinumRadio.isEnabled = true
//                    trainerGoldRadio.isChecked = false
//                    trainerGoldRadio.isEnabled = false
//                    trainerGoldRadio.alpha = 0.5F
//                }
//            }
//        })

        trainer.setOnItemClickListener { parent, _, position, _ ->
            try{
                networkViewModel.selectedTrainer = parent.getItemAtPosition(position) as Trainer

                networkViewModel.selectedTrainer?.let{
                    trainerId.text = it.referralId
                    networkViewModel.trainerChanged(it.referralId)
                }
            }catch(e: Exception){
                Log.d("REGISTER", e.message.toString())
                networkViewModel.selectedTrainer = null
                trainerId.text = ""
            }
        }

        trainer.afterTextChanged {
            if (!trainer.isPerformingCompletion){
                networkViewModel.selectedTrainer = null
                trainerId.text = ""
            }
        }

        trainer.setOnClickListener {
            if (trainer.adapter != null){
                trainer.showDropDown()
            }
        }

        trainer.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus && trainer.adapter != null){
                trainer.showDropDown()
            }
            if(!hasFocus){
                if (networkViewModel.selectedTrainer == null) {
                    trainer.error = "Please select trainer"
                }else{
                    trainer.error = null
                }
            }
        }

        platinumRadio.setOnClickListener {
            platinumRadio.isChecked = true
            trainerPlatinumRadio.isChecked = true
            goldRadio.isChecked = false
            trainerGoldRadio.isChecked = false
            selectedPosition = 0
            selectedTrainerPosition = 0
            updateTrainerUI()
        }

        goldRadio.setOnClickListener {
            platinumRadio.isChecked = false
            trainerPlatinumRadio.isChecked = false
            goldRadio.isChecked = true
            trainerGoldRadio.isChecked = true
            selectedPosition = 1
            selectedTrainerPosition = 1
            updateTrainerUI()
        }

//        trainerPlatinumRadio.setOnClickListener {
//            trainerPlatinumRadio.isChecked = true
//            trainerGoldRadio.isChecked = false
//            selectedTrainerPosition = 0
//        }
//
//        trainerGoldRadio.setOnClickListener {
//            trainerPlatinumRadio.isChecked = false
//            trainerGoldRadio.isChecked = true
//            selectedTrainerPosition = 1
//        }

        nextButton.setOnClickListener {
            loggedInUser?.let{
                nextButton.isEnabled = false
                networkViewModel.addNetwork(
                        sponsorId = sponsor.text.toString(),
                        trainerId = trainerId.text.toString(),
                        email = it.email!!,
                        username = it.displayName!!,
                        position = selectedTrainerPosition)
            }
        }

        networkViewModel.addNetworkResult.observe(viewLifecycleOwner, Observer {
            nextButton.isEnabled = true

            val result = it ?: return@Observer

            if (result.error != null) {
                Toast.makeText(activity, result.error, Toast.LENGTH_LONG).show()
            }
            if (result.success != null) {
                MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                        .setTitle("Network Created")
                        .setMessage("Network created successfully. Your network will be put ON-HOLD until you activate your network. Create node(s) to activate it. ")
                        .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                            findNavController().popBackStack()
                        }
                        .setCancelable(false)
                        .show()
            }
        })

        networkViewModel.referralError.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })

        return root
    }

    private fun updateTrainerUI(){
        networkViewModel.selectedTrainer = null
        trainerId.text = ""
        trainer.setText("")

        if (selectedPosition == 0){
            trainer.setAdapter(platinumTrainerAdapter)
        }
        if (selectedPosition == 1){
            trainer.setAdapter(goldTrainerAdapter)
        }

        trainer.invalidate()
    }
}