package com.goldgainer.glcc.ui.login

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.goldgainer.glcc.R

class TrainerArrayAdapter(context: Context, resource: Int, textViewResourceId: Int, objects: List<Trainer>) : ArrayAdapter<Trainer>(context, resource, textViewResourceId, objects), Filterable {

    private val networkArr = objects.toTypedArray()
    private var filteredArr = objects.toTypedArray()

    override fun getCount(): Int {
        return filteredArr.size
    }

    override fun getItem(position: Int): Trainer {
        return filteredArr[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val superView = super.getView(position, convertView, parent)
        val textView = superView as TextView
        val trainer = filteredArr[position]
        textView.text = trainer.displayName
        return textView
    }

    override fun getFilter() = filter

    private val filter: Filter = object : Filter() {

        @Synchronized
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = FilterResults()
            filteredArr = if(constraint == null || constraint.isEmpty()){
                networkArr.copyOf()
            }else{
                filteredArr.filter { it ->
                    it.displayName.contains(constraint, true)
                }.toTypedArray()
            }
            results.values = filteredArr
            results.count = filteredArr.size

            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            if(results?.values != null){
                if(results.count > 0){
                    notifyDataSetChanged()
                }else{
                    notifyDataSetInvalidated()
                }
            }
        }
    }
}