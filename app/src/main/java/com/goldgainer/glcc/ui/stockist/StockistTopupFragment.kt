package com.goldgainer.glcc.ui.stockist

import android.Manifest
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.MainActivity
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.toDigitalCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class StockistTopupFragment : AuthenticatedFragment() {

    private lateinit var viewModel: StockistViewModel

    private lateinit var networkAdapter : ArrayAdapter<String>

    private lateinit var nodeTypeAdapter : ArrayAdapter<String>

    private lateinit var packageAdapter : ArrayAdapter<String>

    private var imageCapture: ImageCapture? = null
    private lateinit var outputDirectory: File
    private lateinit var cameraExecutor: ExecutorService
    private var paymentFile : File? = null
    private lateinit var paymentImage : ImageView

    private var isCapturing = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_stockist_topup, container, false)
        setUserHeader(root.findViewById(R.id.stockist_user_header))
        viewModel = ViewModelProvider(this).get(StockistViewModel::class.java)

        val networkSpinner = root.findViewById<Spinner>(R.id.stockist_network_spinner)
        val nodeSpinner = root.findViewById<Spinner>(R.id.edit_node_type)
        val packageSpinner = root.findViewById<Spinner>(R.id.edit_voucher_package)

        val subTotal = root.findViewById<TextView>(R.id.sub_total_text)
        val tax = root.findViewById<TextView>(R.id.tax_text)
        val total = root.findViewById<TextView>(R.id.total_text)

        val paymentAccountLabel = root.findViewById<TextView>(R.id.payment_name_label)
        val paymentAccount = root.findViewById<TextView>(R.id.payment_name_text)
        val paymentAddressLabel = root.findViewById<TextView>(R.id.payment_address_label)
        val paymentAddress = root.findViewById<EditText>(R.id.payment_address_edit)
        val bankNameLabel = root.findViewById<TextView>(R.id.payment_bank_name_label)
        val bankName = root.findViewById<TextView>(R.id.payment_bank_name_text)

        val copyBtn = root.findViewById<Button>(R.id.payment_copy_btn)

        paymentImage = root.findViewById(R.id.payment_image)
        val paymentPreview = root.findViewById<PreviewView>(R.id.payment_preview)
        val paymentBtn = root.findViewById<ImageButton>(R.id.payment_btn)

        val submitBtn = root.findViewById<Button>(R.id.topup_save)

        cameraExecutor = Executors.newSingleThreadExecutor()
        outputDirectory = getOutputDirectory()

        networkAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        networkAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        networkSpinner.adapter = networkAdapter

        viewModel.networkCategories.observe(viewLifecycleOwner, Observer { categories ->
            if (categories.isNotEmpty()) {
                networkAdapter.clear()
                categories.map { it.description }.forEach { category ->
                    networkAdapter.add(category)
                }
                networkAdapter.notifyDataSetChanged()
                networkSpinner.setSelection(viewModel.selectedNetworkIndex)
            }
        })

        networkSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                viewModel.selectedNetworkIndex = position
                viewModel.getVoucherPackages()
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        viewModel.getActiveCategories()

        nodeTypeAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        nodeTypeAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        nodeSpinner.adapter = nodeTypeAdapter

        packageAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        packageAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        packageSpinner.adapter = packageAdapter

        packageSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                val packages = viewModel.voucherPackages.value
                packages?.let { p ->
                    val nodeTypes = p.map { it.nodeType }.distinct().sorted()
                    val type = nodeTypes[nodeSpinner.selectedItemPosition]
                    val filteredPackage = p.filter { it.nodeType == type }
                    val voucherPackage = filteredPackage[position]
                    viewModel.selectedVoucherPackage = voucherPackage
                    subTotal.text = voucherPackage.price.toDigitalCurrency(voucherPackage.currency)
                    val calTax = voucherPackage.price * 0.1
                    if (voucherPackage.currency == "IDR") {
                        tax.text = calTax.toDigitalCurrency(voucherPackage.currency)
                        val calTotal = voucherPackage.price + calTax
                        total.text = calTotal.toDigitalCurrency(voucherPackage.currency)
                    }else{
                        tax.text = "0"
                        total.text = voucherPackage.price.toDigitalCurrency(voucherPackage.currency)
                    }
                }
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        nodeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                packageAdapter.clear()
                val packages = viewModel.voucherPackages.value
                packages?.let { p ->
                    val nodeTypes = p.map { it.nodeType }.distinct().sorted()
                    val type = nodeTypes[position]
                    val filteredPackage = p.filter { it.nodeType == type }
                    filteredPackage.forEach {
                        packageAdapter.add("${it.price.toDigitalCurrency(it.currency)} : Qty = ${it.amount} Vouchers")
                    }
                }
                packageAdapter.notifyDataSetChanged()
                packageSpinner.setSelection(0)
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        viewModel.voucherPackages.observe(viewLifecycleOwner, Observer { packages ->
            if (packages.isEmpty()) {
                MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage("No Voucher Packages available.")
                    .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
                        findNavController().popBackStack()
                    }
                    .show()
                return@Observer
            }

            nodeTypeAdapter.clear()
            val nodeTypes = packages.map { it.nodeType }.distinct().sorted()
            nodeTypes.forEach {
                nodeTypeAdapter.add("$it Node")
            }
            nodeTypeAdapter.notifyDataSetChanged()

            packageAdapter.clear()
            val type = nodeTypes[0]
            val filteredPackage = packages.filter { it.nodeType == type }
            filteredPackage.forEach {
                packageAdapter.add("${it.price.toDigitalCurrency(it.currency)} : Qty = ${it.amount} Vouchers")
            }
            packageAdapter.notifyDataSetChanged()

            val voucherPackage = filteredPackage[0]
            viewModel.selectedVoucherPackage = voucherPackage
            subTotal.text = voucherPackage.price.toDigitalCurrency(voucherPackage.currency)
            val calTax = voucherPackage.price * 0.1
            if (voucherPackage.currency == "IDR") {
                tax.text = calTax.toDigitalCurrency(voucherPackage.currency)
                val calTotal = voucherPackage.price + calTax
                total.text = calTotal.toDigitalCurrency(voucherPackage.currency)
            }else{
                tax.text = "0"
                total.text = voucherPackage.price.toDigitalCurrency(voucherPackage.currency)
            }
            viewModel.getPaymentInfo(voucherPackage.currency)
        })

        viewModel.getVoucherPackages()

        viewModel.paymentInfo.observe(viewLifecycleOwner, Observer { paymentInfo ->
            if (paymentInfo.type == 0) {
                paymentAccountLabel.text = "ACCOUNT NAME"
                paymentAddressLabel.text = "ACCOUNT NUMBER"
                bankNameLabel.visibility = View.VISIBLE
                bankName.visibility = View.VISIBLE

                paymentAccount.text = paymentInfo.account
                paymentAddress.setText(paymentInfo.number)
                bankName.text = paymentInfo.name
            } else {
                paymentAccountLabel.text = "NETWORK"
                paymentAddressLabel.text = "${paymentInfo.account} ADDRESS"
                bankNameLabel.visibility = View.GONE
                bankName.visibility = View.GONE

                paymentAccount.text = paymentInfo.account
                paymentAddress.setText(paymentInfo.number)
            }
        })

        copyBtn.setOnClickListener {
            Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_LONG).show()
            val mainActivity = activity as MainActivity
            val clipboard = mainActivity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Payment Address", paymentAddress.text)
            clipboard.setPrimaryClip(clip)
        }

        paymentBtn.setOnClickListener {
            if (allPermissionsGranted()) {
                if (isCapturing) {
                    paymentImage.setImageResource(android.R.color.transparent)
                    paymentImage.alpha = 1.0f
                    paymentPreview.alpha = 0.0f
                    val sdf = SimpleDateFormat("dd_M_yyyy_hh_mm_ss")
                    val currentDate = sdf.format(Date())
                    takePhoto("topup_$currentDate")
                } else {
                    paymentImage.alpha = 0.0f
                    paymentPreview.alpha = 1.0f
                    startCamera(paymentPreview, CameraSelector.DEFAULT_BACK_CAMERA)
                    isCapturing = true
                }
            } else {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    REQUIRED_PERMISSIONS,
                    REQUEST_CODE_PERMISSIONS
                )
            }
        }

        submitBtn.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                val errorString = viewModel.requestTopup(paymentFile)
                if (errorString != null) {
                    MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                        .setTitle("Error")
                        .setMessage(errorString)
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                } else {
                  MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                        .setTitle("Info")
                        .setMessage("Thank you for Top Up Vouchers. Your payment is being approved by Admin.")
                        .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
                            findNavController().popBackStack()
                        }
                        .show()
                }
            }
        }

        return root
    }

    private fun startCamera(viewFinder: PreviewView, cameraSelector: CameraSelector) {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .setTargetResolution(Size(viewFinder.width, viewFinder.height))
                .build()
                .also {
                    it.setSurfaceProvider(viewFinder.surfaceProvider)
                }

            imageCapture = ImageCapture.Builder()
                .setTargetResolution(Size(viewFinder.width, viewFinder.height))
                .build()

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    viewLifecycleOwner,
                    cameraSelector,
                    preview,
                    imageCapture
                )

            } catch (exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun takePhoto(filename: String) {
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        val photoFile = File(outputDirectory, "$filename.jpg")

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        Toast.makeText(context, "Taking a photo", Toast.LENGTH_SHORT).show()
        // Set up image capture listener, which is triggered after photo has
        // been taken
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Toast.makeText(context, exc.message, Toast.LENGTH_SHORT).show()
                    Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
                    // Reset Camera After
                    ProcessCameraProvider.getInstance(requireContext()).get().unbindAll()
                    isCapturing = false
                }
                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = Uri.fromFile(photoFile)
                    Toast.makeText(context, "Done!", Toast.LENGTH_SHORT).show()
                    paymentFile = photoFile
                    Picasso.get().load(savedUri).memoryPolicy(MemoryPolicy.NO_CACHE).resize(WIDTH, HEIGHT).centerCrop().into(paymentImage)
                    // Reset Camera After
                    isCapturing = false
                    ProcessCameraProvider.getInstance(requireContext()).get().unbindAll()
                    viewModel.compressPhoto(photoFile, requireContext())
                }
            }
        )
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            requireContext(), it
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun getOutputDirectory(): File {
        val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name)).apply { mkdirs() } }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else requireActivity().filesDir
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    companion object {
        private const val TAG = "GLCC Community"
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
        private const val WIDTH = 300
        private const val HEIGHT = 300
    }
}