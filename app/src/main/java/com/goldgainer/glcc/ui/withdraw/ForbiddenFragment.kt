package com.goldgainer.glcc.ui.withdraw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.button.MaterialButton

class ForbiddenFragment: AuthenticatedFragment() {

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		val root = inflater.inflate(R.layout.fragment_withdraw_forbidden, container, false)
		setUserHeader(root.findViewById(R.id.withdraw_user_header))

		val transactionBtn = root.findViewById<MaterialButton>(R.id.transaction_tab)
		val profileBtn = root.findViewById<Button>(R.id.profile_btn)

		transactionBtn.setOnClickListener {
			findNavController().navigate(R.id.action_nav_wallet_forbidden_to_nav_transactions)
		}

		profileBtn.setOnClickListener {
			findNavController().navigate(R.id.action_global_nav_profile)
		}

		return root
	}
}