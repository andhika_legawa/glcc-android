package com.goldgainer.glcc.ui.withdraw

data class WithdrawFormState (
		val amountError:String? = null,
		val minWithdraw:String,
		val maxWithdraw:String,
		val taxPercentage:String
		)