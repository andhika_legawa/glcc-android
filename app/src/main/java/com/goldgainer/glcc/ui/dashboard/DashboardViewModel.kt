package com.goldgainer.glcc.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.Network
import com.goldgainer.glcc.data.model.NetworkCategory
import com.goldgainer.glcc.data.model.Notification
import com.goldgainer.glcc.data.model.UserData
import com.goldgainer.glcc.data.toCurrency
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min

class DashboardViewModel : ViewModel() {

    private val _selectedType = MutableLiveData<String>("I")
    val selectedType: LiveData<String> = _selectedType

    private val _networkOnHold = MutableLiveData<Network>()
    val networkOnHold: LiveData<Network> = _networkOnHold

    private val _totalAsset = MutableLiveData<TotalAsset>()
    val totalAsset: LiveData<TotalAsset> = _totalAsset

    private val _pairConnection = MutableLiveData<List<PairConnection>>()
    val pairConnection: LiveData<List<PairConnection>> = _pairConnection

    private val _notifications = MutableLiveData<List<Notification>>()
    val notifications: LiveData<List<Notification>> = _notifications

    private val _networkCategorySet = MutableLiveData<List<NetworkCategory>>()
    val networkCategories: LiveData<List<NetworkCategory>> = _networkCategorySet

    private val _userData = MutableLiveData<UserData>()
    val userData : LiveData<UserData> = _userData

    private val _database = FirebaseDataSource()

    fun selectNetworkType(email: String, type: String){
        _selectedType.value = type
        viewModelScope.launch {
            val userNetworks = _database.getUserNetworks(email, type)
            val onHoldNetworks = _database.getOnHoldNetworks(email)
            //get networks category
            val categories = _database.getNetworkCategories(type)
            categories.forEach {
                it.filled = userNetworks.find { network ->
                    it.number == network.number
                }
                it.onhold = onHoldNetworks.find { onholdnetwork ->
                    it.number == onholdnetwork.number
                }
            }
            _networkCategorySet.value = categories.sortedBy { it.number }
        }
        countAsset(email)
    }

    fun checkNetworkOnHold(owner: String) {
        viewModelScope.launch {
            //reload userdata
            val userData = FirebaseDataSource().getUserDetail(owner)
            if (userData!=null) {
                Helper.setLocalUser(owner, userData)
                _userData.value = userData
            }

            val activeNetworks = _database.getUserNetworks(owner, _selectedType.value)
            if (activeNetworks.isNotEmpty()) {
                return@launch
            }
            val currentOnHold = _networkOnHold.value
            if (currentOnHold != null) {
                return@launch
            }
            val onHoldNetworks = _database.getOnHoldNetworks(owner)
            if (onHoldNetworks.isNotEmpty()) {
                _networkOnHold.value = onHoldNetworks.first()
            }
        }
    }

    fun updateNotification(email: String) {
        viewModelScope.launch {
            val notifications = mutableListOf<Notification>()
            val onHoldNetworks = _database.getOnHoldNetworks(email)
            if (onHoldNetworks.isNotEmpty()) {
                onHoldNetworks.forEach {
                    var type = "Indonesia"
                    if (it.type == "G") {
                        type = "Global"
                    }

                    notifications.add(
                        Notification(
                            title = "Network On-Hold",
                            message = "Your $type Network N${it.number} is on-hold. Click here to activate your network.",
                            type = "ONHOLD",
                            networkonhold = it
                        ))
                }
            }
            val todayNotifications = _database.getLatestNotifications(email)
            if (todayNotifications.isNotEmpty()) {
                notifications.addAll(todayNotifications)
            }
            notifications.sortByDescending { it.date }
            _notifications.value = notifications
        }
    }

    private fun countAsset(owner: String){
        viewModelScope.launch {
            val networks = _database.getUserNetworks(owner, _selectedType.value)

            var totalNodes = 0L
            var totalBonus = 0.0
            var totalBSponsor = 0.0
            var totalBPairing = 0.0
            var totalBRollup = 0.0
            val pairConnections = arrayListOf<PairConnection>()

            networks.forEach {
                val snapshot = _database.getLatestSnapshot(it.referralId)
                snapshot?.let { snap ->
                    totalBonus += snap.totalAsset
                    totalBSponsor += snap.platinumBSponsor + snap.goldBSponsor
                    totalBPairing += snap.platinumBPairing + snap.goldBPairing
                    totalBRollup += snap.platinumBRollup + snap.goldBRollup
                    totalNodes += snap.goldNodes + snap.platinumNodes

                    val div = min(snap.platinumNodes, snap.goldNodes)
                    pairConnections.add(
                        PairConnection(
                            networkType = snap.type,
                            networkNumber = snap.number,
                            networkPair = "$div:$div"
                        )
                    )
                }
            }

            val assetName = if (_selectedType.value == "I") "INDONESIA NETWORK - IDR" else "GLOBAL NETWORK - USD"
            val currency = if (_selectedType.value == "I") "IDR" else "USD"
            _totalAsset.value = TotalAsset(
                assetNetwork = assetName,
                totalNodes = totalNodes.toString(),
                totalBSponsor = totalBSponsor.toCurrency(currency),
                totalBPairing = totalBPairing.toCurrency(currency),
                totalBRollup = totalBRollup.toCurrency(currency),
                totalBonus = totalBonus.toCurrency(currency)
            )
            _pairConnection.value = pairConnections.toList()
        }
    }

    data class TotalAsset(
        val assetNetwork: String = "",
        val totalNodes : String = "",
        val totalBSponsor : String = "",
        val totalBPairing : String = "",
        val totalBRollup : String = "",
        val totalBonus : String = ""
    )
}

data class PairConnection(
    val networkType: String = "",
    val networkNumber: Int = 0,
    val networkPair: String = ""
)

class PairConnectionAdapter(private val pairSet : List<PairConnection>, private val listener:(PairConnection) -> Unit) : RecyclerView.Adapter<PairConnectionAdapter.PairViewHolder>(){

    class PairViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PairViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.partials_network_pair, parent, false) as View
        return PairViewHolder(view)
    }

    override fun onBindViewHolder(holder: PairViewHolder, position: Int) {
        val item = pairSet[position]
        val networkText = holder.itemView.findViewById<TextView>(R.id.network_text)
        val pairText = holder.itemView.findViewById<TextView>(R.id.pair_text)
        val pairBtn = holder.itemView.findViewById<Button>(R.id.pair_btn)

        networkText.text = "N${item.networkNumber} PAIR CONNECTION"
        pairText.text = item.networkPair
        pairBtn.text = item.networkPair
        pairBtn.setOnClickListener { listener(item) }

        if ( item.networkType == "I") {
            pairText.visibility = View.GONE
            pairBtn.visibility = View.VISIBLE
        } else {
            pairText.visibility = View.VISIBLE
            pairBtn.visibility = View.GONE
        }
    }

    override fun getItemCount() = pairSet.size
}