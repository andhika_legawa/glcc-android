package com.goldgainer.glcc.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class StockistTrxFragment : AuthenticatedFragment() {

    private lateinit var viewModel: StockistViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_stockist_trx, container, false)
        setUserHeader(root.findViewById(R.id.stockist_user_header))
        viewModel = ViewModelProvider(this).get(StockistViewModel::class.java)

        val yearSpinner = root.findViewById<Spinner>(R.id.stockist_trx_year)
        val monthSpinner = root.findViewById<Spinner>(R.id.stockist_trx_month)
        val topupCheck = root.findViewById<CheckBox>(R.id.stockist_topup_checkbox)
        val sellCheck = root.findViewById<CheckBox>(R.id.stockist_sell_checkbox)
        val trxHeader = root.findViewById<LinearLayout>(R.id.stockist_trx_header)
        val trxList = root.findViewById<ListView>(R.id.stockist_trx_list)
        val trxEmpty = root.findViewById<TextView>(R.id.stockist_trx_empty)

        val searchBtn = root.findViewById<Button>(R.id.stockist_search_btn)

        val yearAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        yearAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        val start = 2021
        var currentYear = Calendar.getInstance().get(Calendar.YEAR)
        while ( currentYear >= start ) {
            yearAdapter.add(currentYear.toString())
            currentYear -= 1
        }
        yearSpinner.adapter = yearAdapter

        val monthAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        monthAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        var currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        var january = 0
        val sdf = SimpleDateFormat("MMM")
        while ( january <= 11) {
            val cal = Calendar.getInstance()
            cal.set(currentYear, january, 1)
            monthAdapter.add(sdf.format(cal.time))
            january += 1
        }
        monthSpinner.adapter = monthAdapter
        monthSpinner.setSelection(currentMonth)

        trxHeader.visibility = View.GONE
        trxList.visibility = View.GONE
        trxEmpty.visibility = View.GONE

        val trxAdapter = StockistTrxAdapter(requireContext(), arrayListOf())
        trxList.adapter = trxAdapter

        searchBtn.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                val yearStr = yearSpinner.selectedItem as String
                val year = yearStr.toInt()
                val month = monthSpinner.selectedItemPosition
                val cal = Calendar.getInstance()
                cal.set(year, month, 1, 0, 0 ,0)
                val start = cal.time
                cal.set(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59)
                val end = cal.time

                val list = viewModel.findTransaction(start, end, topupCheck.isChecked, sellCheck.isChecked)
                if (list.isEmpty()) {
                    trxHeader.visibility = View.GONE
                    trxList.visibility = View.GONE
                    trxEmpty.visibility = View.VISIBLE
                } else {
                    trxHeader.visibility = View.VISIBLE
                    trxList.visibility = View.VISIBLE
                    trxEmpty.visibility = View.GONE
                    trxAdapter.clear()
                    trxAdapter.addAll(list)
                    trxAdapter.notifyDataSetChanged()
                }
            }
        }

        return root
    }

}