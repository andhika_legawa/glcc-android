package com.goldgainer.glcc.ui.login

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.LoginRepository
import com.goldgainer.glcc.data.Result
import com.goldgainer.glcc.data.model.UserData
import com.goldgainer.glcc.ui.network.NetworkViewModel
import kotlinx.coroutines.launch

class RegisterViewModel(private val loginRepository: LoginRepository) : NetworkViewModel() {

    private val _registerForm = MutableLiveData<RegisterFormState>()
    val registerFormState: LiveData<RegisterFormState> = _registerForm

    private val _registerResult = MutableLiveData<LoginResult>()
    val registerResult: LiveData<LoginResult> = _registerResult

    private val _latestVersion = MutableLiveData<Int>()
    val latestVersion: LiveData<Int> = _latestVersion

    fun register(username: String, email: String, country: String, sponsor: String, position: Int, trainer: String?, password: String, retype: String){
        val lowerCaseEmail = email.toLowerCase()
        viewModelScope.launch {
            isLoading.value = true
            try{
                val result = loginRepository.register(lowerCaseEmail, password, username)
                if (result is Result.Success) {

                    val userDetail = UserData()
                    result.data?.userId?.let{
                        userDetail.userId = it
                    }
                    userDetail.name = username
                    userDetail.email = lowerCaseEmail
                    userDetail.country = country

                    if( ! dataSource.saveUserDetail(userDetail) ){
                        throw java.lang.Exception("failed to save user data")
                    }

                    val trainerId = trainer?:throw java.lang.Exception("no trainer id")

                    createNetwork(sponsor, trainerId, lowerCaseEmail, username, position)

                    result.data?.let{
                        _registerResult.value = LoginResult(success = LoggedInUserView(it))
                    }

                }
                if (result is Result.Error) {
                    result.exception?.let {
                        _registerResult.value = LoginResult(error = R.string.register_failed, errorMessage = it.message)
                    }
                }
            }catch(e: Exception) {
                e.message?.let { Log.d("REGISTER", it) }
                _registerResult.value = LoginResult(error = R.string.register_failed)
            }
            isLoading.value = false
        }
    }

    fun registerDataChanged(username: String, email: String, password: String, retype: String) {
        viewModelScope.launch {
            isLoading.value = true

            if (!isUserNameValid(username)) {
                _registerForm.value = RegisterFormState(usernameError = R.string.invalid_username)
            } else if(dataSource.checkUsernameExist(username)) {
                _registerForm.value = RegisterFormState(usernameError = R.string.invalid_username_exist)
            } else if (!isEmailValid(email)) {
                _registerForm.value = RegisterFormState(emailError = R.string.invalid_email)
            } else if (dataSource.checkEmailExist(email)) {
                _registerForm.value = RegisterFormState(emailError = R.string.invalid_email_exist)
            } else if (!isPasswordValid(password)) {
                _registerForm.value = RegisterFormState(passwordError = R.string.invalid_password)
            } else if (!isRetypeValid(password, retype)) {
                _registerForm.value = RegisterFormState(retypePasswordError = R.string.invalid_retype)
            } else if (selectedTrainer == null) {
                _registerForm.value = RegisterFormState(trainerError = R.string.invalid_trainer)
            } else {
                _registerForm.value = RegisterFormState(isDataValid = true)
            }
            isLoading.value = false
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return username.isNotBlank()
    }

    private fun isEmailValid(email: String): Boolean {
        return if (email.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(email).matches()
        } else {
            email.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }

    private fun isRetypeValid(password: String, retype: String): Boolean {
        return password == retype
    }

    fun checkVersion(){
        viewModelScope.launch {
            val helper = Helper()
            _latestVersion.value = helper.getGlobalSettings("latest_version", 1) as Int
        }
    }
}