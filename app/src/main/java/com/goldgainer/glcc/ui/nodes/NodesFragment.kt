package com.goldgainer.glcc.ui.nodes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.text.NumberFormat

class NodesFragment : AuthenticatedFragment() {

    private lateinit var nodesViewModel: NodesViewModel

    private lateinit var instruction: TextView

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        nodesViewModel = ViewModelProvider(this).get(NodesViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_nodes_create, container, false)
        setUserHeader(root.findViewById(R.id.node_user_header))

        instruction = root.findViewById(R.id.node_instruction_textview)

        arguments?.let {
            nodesViewModel.selectedNetwork = it.getString("selectedNetwork", "")
        }

        val availableNode = root.findViewById<TextView>(R.id.node_available)

        val indonesiaTextView = root.findViewById<TextView>(R.id.indonesia_textview)
        val idrTextView = root.findViewById<TextView>(R.id.idr_textview)

        val node1 = root.findViewById<LinearLayout>(R.id.node_option_1)
        val node2 = root.findViewById<LinearLayout>(R.id.node_option_2)
        val node3 = root.findViewById<LinearLayout>(R.id.node_option_3)
        val node4 = root.findViewById<LinearLayout>(R.id.node_option_4)

        val buy1node = root.findViewById<TextView>(R.id.node_1node)
        val buy3node = root.findViewById<TextView>(R.id.node_3node)
        val buy7node = root.findViewById<TextView>(R.id.node_7node)
        val buy15node = root.findViewById<TextView>(R.id.node_15node)

        nodesViewModel.nodesAvailable.observe(viewLifecycleOwner, Observer {

            val category = nodesViewModel.currentCategory

            indonesiaTextView.text = category.description
            idrTextView.text = "(${category.currency})"

            buy1node.text = NumberFormat.getIntegerInstance().format((category.nodevalue * 1))
            buy3node.text = NumberFormat.getIntegerInstance().format((category.nodevalue * 3))
            buy7node.text = NumberFormat.getIntegerInstance().format((category.nodevalue * 7))
            buy15node.text = NumberFormat.getIntegerInstance().format((category.nodevalue * 15))

            node1.setOnClickListener {
                pickNodeCategory(category.nodevalue, 1)
            }
            node2.setOnClickListener {
                pickNodeCategory(category.nodevalue, 3)
            }
            node3.setOnClickListener {
                pickNodeCategory(category.nodevalue, 7)
            }
            node4.setOnClickListener {
                pickNodeCategory(category.nodevalue, 15)
            }

            val nodesAvailable = it
            availableNode.text = nodesAvailable.toString()

            if (nodesAvailable < 15) {
                node4.alpha = 0.5F
                node4.setOnClickListener {  }
            }

            if (nodesAvailable < 7) {
                node3.alpha = 0.5F
                node3.setOnClickListener {  }
            }

            if (nodesAvailable < 3) {
                node2.alpha = 0.5F
                node2.setOnClickListener {  }
            }

            if (nodesAvailable == 0) {
                node1.alpha = 0.5F
                node1.setOnClickListener {  }
                instruction.text = "You already maxed out your nodes in this network."
            }

            if (nodesAvailable == -1) {
                availableNode.text = "N/A"
                node1.alpha = 0.5F
                node1.setOnClickListener {  }
                MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                    .setTitle("Nodes Unavailable")
                    .setMessage("There is no nodes available now.")
                    .setPositiveButton(android.R.string.ok, null)
                    .show()
            }

            if(nodesAvailable > 1) {
                updateInstructions()
            }
        })

        nodesViewModel.nodeError.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })

        return root
    }

    override fun onResume() {
        super.onResume()
        nodesViewModel.checkNodesAvailability()
    }

    private fun updateInstructions(){
        val instructionText = "To activate/upgrade your network, choose the amount of node(s) you want to create."
        instruction.text = instructionText
    }

    private fun pickNodeCategory(value: Int, quantity: Int){
        if (nodesViewModel.nodesAvailable.value == null) {
            return
        }

        if (nodesViewModel.nodesAvailable.value!! < quantity) {
            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                .setTitle("Nodes Unavailable")
                .setMessage("Not enough nodes available for your request. Please choose smaller Node Package")
                .setPositiveButton(android.R.string.ok, null)
                .show()
        }else{
            val bundle = bundleOf("selectedNetwork" to nodesViewModel.selectedNetwork, "value" to value, "quantity" to quantity, "type" to nodesViewModel.currentCategory.type, "selectedCategory" to nodesViewModel.currentCategory.description)
            findNavController().navigate(R.id.action_nav_nodes_create_to_nav_voucher, bundle)
        }
    }
}