package com.goldgainer.glcc.ui.login

data class SendingResult(
    val success: Int? = null,
    val error: Int? = null
)