package com.goldgainer.glcc.ui.static

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment

class PrivacyFragment : AuthenticatedFragment() {

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		val root = inflater.inflate(R.layout.fragment_privacy, container, false)
		setUserHeader(root.findViewById(R.id.user_header))

		val webView = root.findViewById<WebView>(R.id.webview)
		webView.loadUrl("file:///android_asset/privacy.html")

		return root
	}
}