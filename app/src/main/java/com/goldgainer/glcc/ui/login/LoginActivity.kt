package com.goldgainer.glcc.ui.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ActivityNavigator
import androidx.navigation.findNavController

import com.goldgainer.glcc.R
import io.alterac.blurkit.BlurKit
import io.alterac.blurkit.BlurLayout

class LoginActivity : AppCompatActivity() {

//    private lateinit var blurView: BlurLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val navController = findNavController(R.id.login_nav_host_fragment)
//        blurView = findViewById(R.id.login_blurview)

//        BlurKit.init(this)
    }

    override fun onStart() {
        super.onStart()
//        val loginFrame = findViewById<FrameLayout>(R.id.login_frame)
//        BlurKit.getInstance().fastBlur(loginFrame, 10, 0.12F)
//        blurView.startBlur()
//        blurView.lockView()
    }

    override fun onStop() {
//        blurView.pauseBlur()

        super.onStop()
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }
}

