package com.goldgainer.glcc.ui.withdraw

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment
import androidx.appcompat.app.AlertDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class GoldWithdrawFragment: AuthenticatedFragment() {

	private lateinit var withdrawViewModel: WithdrawViewModel
	private lateinit var disabledDialog: AlertDialog
	private lateinit var goldGramAdapter: ArrayAdapter<String>

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		withdrawViewModel = ViewModelProvider(this).get(WithdrawViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_withdraw_gold, container, false)
		setUserHeader(root.findViewById(R.id.withdraw_user_header))

		val networkDescription = root.findViewById<TextView>(R.id.network_textview)
		val availableReward = root.findViewById<TextView>(R.id.quantity_textview)
		val amountSpinner = root.findViewById<Spinner>(R.id.amount_spinner)
		val minDraw = root.findViewById<TextView>(R.id.min_draw_textview)
		val maxDraw = root.findViewById<TextView>(R.id.max_draw_textview)

		val goldPrice = root.findViewById<TextView>(R.id.goldprice_textview)
		val transferFee = root.findViewById<TextView>(R.id.transferfee_textview)
		val adminFee = root.findViewById<TextView>(R.id.adminfee_textview)
		val packingFee = root.findViewById<TextView>(R.id.packingfee_textview)
		val goldMaximum = root.findViewById<TextView>(R.id.avamount_textview)

		val pinEdit = root.findViewById<EditText>(R.id.pin_edit)

		val sendPIN = root.findViewById<Button>(R.id.sendpin_btn)
		val proceedBtn = root.findViewById<Button>(R.id.proceed_btn)
		val cancelBtn = root.findViewById<Button>(R.id.cancel_btn)

		goldGramAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
		goldGramAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
		amountSpinner.adapter = goldGramAdapter

		sendPIN.setOnClickListener {
			withdrawViewModel.sendPIN()
		}

		proceedBtn.isEnabled = false
		proceedBtn.setOnClickListener {
			MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
					.setTitle("Request Withdraw")
					.setMessage("Are you sure you want to withdraw?")
					.setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
						val pin = pinEdit.text.toString()
						withdrawViewModel.submitWithdrawal(pin)
					}
					.setNegativeButton(android.R.string.cancel, null)
					.setCancelable(false)
					.show()
		}

		cancelBtn.setOnClickListener {
			withdrawViewModel.cancelRequest()
		}

		disabledDialog = MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
				.setTitle("Withdraw disabled")
				.setMessage("This feature is temporary disabled. Please check again later.")
				.setPositiveButton(android.R.string.ok) { dialog: DialogInterface, _: Int ->
					findNavController().navigate(R.id.action_global_nav_dashboard)
				}
				.setCancelable(false)
				.create()

		amountSpinner.isEnabled = false
		withdrawViewModel.status.observe(viewLifecycleOwner, Observer { status ->
			if (status == "FORBIDDEN") {
				findNavController().navigate(R.id.action_global_nav_wallet_forbidden)
			}

			if (status == "DISABLED") {
				if (!disabledDialog.isShowing) {
					disabledDialog.show()
				}
			}

			if (status == "CANCELED") {
				findNavController().navigate(R.id.action_global_nav_wallet)
			}

			if (status == "SUBMITTED") {
				findNavController().navigate(R.id.action_nav_withdraw_gold_to_nav_withdraw_status)
			}

			if (status == "PENDING") {
				amountSpinner.isEnabled = true
				val request = withdrawViewModel.currentRequest?:return@Observer
				networkDescription.text = request.networkCategory
				availableReward.text = request.available.toCurrency("IDR")
				transferFee.text = request.transferFee.toCurrency("IDR")
				packingFee.text = request.packingFee.toCurrency("IDR")
				adminFee.text = request.adminFee.toCurrency("IDR")
				withdrawViewModel.updateGoldRate()
			}
		})

		withdrawViewModel.checked.observe(viewLifecycleOwner, Observer { checked ->
			val request = withdrawViewModel.currentRequest?:return@Observer
			proceedBtn.isEnabled = checked.amountError == null
			if (request.amount <= 0) {
				proceedBtn.isEnabled = false
			}
			networkDescription.text = request.networkCategory
			availableReward.text = request.available.toCurrency("IDR")
			transferFee.text = request.transferFee.toCurrency("IDR")
			packingFee.text = request.packingFee.toCurrency("IDR")
			adminFee.text = request.adminFee.toCurrency("IDR")
		})

		withdrawViewModel.error.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
			override fun handleOnBackPressed() {
				withdrawViewModel.cancelRequest()
			}
		}
		requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback);

		withdrawViewModel.goldRate.observe(viewLifecycleOwner, Observer { goldRate ->
			try {
				if (goldRate.availableGrams.isEmpty()) {
					goldMaximum.text = "Not enough balance"
					Toast.makeText(requireContext(), "Not enough balance", Toast.LENGTH_LONG).show()
					return@Observer
				}
				minDraw.text = goldRate.availableGrams.first().toString() + " Grams"
				maxDraw.text = goldRate.availableGrams.last().toString() + " Grams"
				goldGramAdapter.clear()
				goldRate.availableGrams.forEach { gram ->
					goldGramAdapter.add(gram.toString() + " grams - " + (goldRate.goldprice * gram).toCurrency("IDR") )
				}
				goldGramAdapter.notifyDataSetChanged()
				goldPrice.text = goldRate.goldprice.toCurrency("IDR")
				goldMaximum.text = goldRate.availableIDR.toCurrency("IDR")
			} catch (e:Exception) {
				Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
			}
		})

		amountSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
			override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
				val goldRate = withdrawViewModel.goldRate.value?:return
				withdrawViewModel.prepareGoldWithdrawal(goldRate.availableGrams[position])
			}
			override fun onNothingSelected(parentView: AdapterView<*>?) {
			}
		}

		return root
	}

	override fun onResume() {
		super.onResume()
		withdrawViewModel.checkWithdrawStatus()
	}
}