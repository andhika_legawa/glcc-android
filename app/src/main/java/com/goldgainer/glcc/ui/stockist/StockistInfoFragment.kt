package com.goldgainer.glcc.ui.stockist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.ui.AuthenticatedFragment
import kotlinx.coroutines.launch

class StockistInfoFragment : AuthenticatedFragment() {

    private lateinit var viewModel: StockistViewModel

    private lateinit var networkAdapter : ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_stockist_info, container, false)
        setUserHeader(root.findViewById(R.id.stockist_user_header))
        viewModel = ViewModelProvider(this).get(StockistViewModel::class.java)

        val findBtn = root.findViewById<Button>(R.id.stockist_find_button)

        val info = root.findViewById<TextView>(R.id.stockist_info_txt)
        val desc = root.findViewById<TextView>(R.id.stockist_description_txt)
        val networkText = root.findViewById<TextView>(R.id.stockist_network_text)

        val dashboardBtn = root.findViewById<Button>(R.id.stockist_dashboard_btn)
        val personalBtn = root.findViewById<Button>(R.id.stockist_profile_btn)
        val locationBtn = root.findViewById<Button>(R.id.stockist_location_btn)
        val topupBtn = root.findViewById<Button>(R.id.stockist_topup_btn)
        val sellBtn = root.findViewById<Button>(R.id.stockist_sell_btn)
        val transactionBtn = root.findViewById<Button>(R.id.stockist_trx_btn)

        val networkSpinner = root.findViewById<Spinner>(R.id.stockist_network_spinner)

        val voucherStock = root.findViewById<LinearLayout>(R.id.stockist_voucher_stock)
        val node1 = root.findViewById<TextView>(R.id.stockist_node_1)
        val node3 = root.findViewById<TextView>(R.id.stockist_node_3)
        val node7 = root.findViewById<TextView>(R.id.stockist_node_7)
        val node15 = root.findViewById<TextView>(R.id.stockist_node_15)

        desc.visibility = View.GONE
        networkText.visibility = View.GONE
        networkSpinner.visibility = View.GONE
        voucherStock.visibility = View.GONE
        dashboardBtn.visibility = View.GONE
        personalBtn.visibility = View.GONE
        locationBtn.visibility = View.GONE
        topupBtn.visibility = View.GONE
        sellBtn.visibility = View.GONE
        transactionBtn.visibility = View.GONE

        findBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_stockist_info_to_nav_stockist_find)
        }

        dashboardBtn.setOnClickListener {
            findNavController().navigate(R.id.action_global_nav_dashboard)
        }

        personalBtn.setOnClickListener {
            findNavController().navigate(R.id.action_global_nav_profile)
        }

        locationBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_stockist_info_to_nav_stockist_location)
        }

        topupBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_stockist_info_to_nav_stockist_topup)
        }

        sellBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_stockist_info_to_nav_stockist_sell)
        }

        transactionBtn.setOnClickListener {
            findNavController().navigate(R.id.action_nav_stockist_info_to_nav_stockist_trx)
        }

        networkAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        networkAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        networkSpinner.adapter = networkAdapter

        viewModel.networkCategories.observe(viewLifecycleOwner, Observer { categories ->
            if (categories.isNotEmpty()) {
                networkAdapter.clear()
                categories.map { it.description }.forEach { category ->
                    networkAdapter.add(category)
                }
                networkAdapter.notifyDataSetChanged()
                networkSpinner.setSelection(viewModel.selectedNetworkIndex)
            }
        })

        networkSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                viewModel.selectedNetworkIndex = position
                viewModel.loadVoucherStock()
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        viewModel.voucherStock.observe(viewLifecycleOwner, Observer { voucherStock ->
            node1.text = voucherStock.node1.toString()
            node3.text = voucherStock.node3.toString()
            node7.text = voucherStock.node7.toString()
            node15.text = voucherStock.node15.toString()

            val total = voucherStock.node1 + voucherStock.node3 + voucherStock.node7 + voucherStock.node15
            if ( total <= 0 ) {
                sellBtn.alpha = 0.5F
                sellBtn.setOnClickListener {  }
            } else {
                sellBtn.alpha = 1.0F
                sellBtn.setOnClickListener {
                    val bundle = bundleOf( "selectedNetwork" to viewModel.selectedNetworkIndex)
                    findNavController().navigate(R.id.action_nav_stockist_info_to_nav_stockist_sell, bundle)
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launch {

            var canBeStockist = true
            val localUser = Helper.getLocalUser()
            if (!localUser.verified) {
                canBeStockist = false
            }

            val isActive = viewModel.networkActivated()
            if (!isActive) {
                canBeStockist = false
            }

            if (canBeStockist) {
                val isStockist = viewModel.isStockist()

                info.text = "STOCKIST INFORMATION"
                desc.visibility = View.VISIBLE
                desc.text = "You are eligible to become Stockist. Please fill your Location and Contact Information below to become Stockist"

                if (isStockist) {
                    desc.visibility = View.GONE
                    voucherStock.visibility = View.VISIBLE
                    networkText.visibility = View.VISIBLE
                    networkSpinner.visibility = View.VISIBLE
                }

                locationBtn.visibility = View.VISIBLE
                topupBtn.visibility = View.VISIBLE
                sellBtn.visibility = View.VISIBLE
                transactionBtn.visibility = View.VISIBLE

                if (!isStockist) {
                    topupBtn.alpha = 0.5F
                    topupBtn.setOnClickListener {  }
                    sellBtn.alpha = 0.5F
                    sellBtn.setOnClickListener {  }
                    transactionBtn.alpha = 0.5F
                    transactionBtn.setOnClickListener {  }
                }

                viewModel.getActiveCategories()
                viewModel.loadVoucherStock()
            } else {
                info.text = "INFORMATION"
                desc.visibility = View.VISIBLE
                desc.text = "To Become Stockist, you must connected to the network, buy minimum 1 Node and your Personal Information is approved by admin"

                dashboardBtn.visibility = View.VISIBLE
                personalBtn.visibility = View.VISIBLE
            }
        }

        return root
    }
}