package com.goldgainer.glcc.ui.payment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.model.Transaction
import com.goldgainer.glcc.data.model.Voucher
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class VoucherViewModel : ViewModel() {

    var selectedNetwork: String = ""
    var selectedQuantity: Int = 0

    private val _validVoucher = MutableLiveData<Voucher?>()
    val validVoucher : LiveData<Voucher?> = _validVoucher

    private val _voucherTransaction = MutableLiveData<Transaction?>()
    val voucherTransaction : LiveData<Transaction?> = _voucherTransaction

    private val _transactionError = MutableLiveData<String?>()
    val transactionError: LiveData<String?> = _transactionError

    private val _database = FirebaseDataSource()

    private var _selectedNumber: Int = 0;

    fun checkVoucher(code: String) {
        val trimmedCode = code.trim()
        viewModelScope.launch {
            try {
                if (selectedNetwork == "") { throw Exception("Invalid network") }
                var network = _database.getNetwork(selectedNetwork)
                if (network == null) {
                    network = _database.getOnHoldNetwork(selectedNetwork)?:throw Exception("Cannot found network!")
                }
                val voucher = _database.getVoucherByCode(trimmedCode)?:throw Exception("Invalid voucher")
                if (voucher.redeemedBy != null) { throw Exception("Voucher already redeemed") }
                if (network.type != voucher.type) { throw Exception("Invalid voucher region") }
                if (voucher.quantity != selectedQuantity) { throw Exception("Invalid voucher quantity") }

                _selectedNumber = network.number;
                _validVoucher.value = voucher
            } catch (e: Exception) {
                _transactionError.value = e.message
                _validVoucher.value = null
            }
        }
    }

    fun performVoucherPayment() {
        val voucher = _validVoucher.value
        if (voucher == null){
            _transactionError.value = "Please input valid voucher."
        }
        if (selectedNetwork == null){
            _transactionError.value = "No network selected."
        }
        viewModelScope.launch {
            val functions = Firebase.functions(regionOrCustomDomain = "asia-southeast2")
            try {
                if (voucher != null){
                    val transactionId = functions.getHttpsCallable("createTransaction").call().await().data as String
                    val transaction = _database.getCreatedTransaction(transactionId)?: throw java.lang.Exception("failed creating transaction")
                    transaction.type = "TPN-V"
                    transaction.value = voucher.quantity.toLong() * voucher.value.toLong()
                    transaction.data = mapOf(
                            "network" to selectedNetwork!!,
                            "networkNumber" to _selectedNumber.toString(),
                            "code" to voucher.code,
                            "nodeQuantity" to voucher.quantity.toString(),
                            "nodeValue" to voucher.value.toString(),
                            "nodeType" to voucher.type)
                    transaction.status = "SUCCESS"
                    _database.saveCreatedTransaction(transaction)
                    functions.getHttpsCallable("processTransaction").call(hashMapOf("transactionId" to transactionId))
                    _voucherTransaction.value = transaction
                }
            }catch(e: Exception){
                _transactionError.value = e.message
            }
        }
    }
}