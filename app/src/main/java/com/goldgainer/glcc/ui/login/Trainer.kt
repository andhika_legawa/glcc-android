package com.goldgainer.glcc.ui.login

data class Trainer (
    val referralId: String,
    val displayName: String,
    val binaryState: Int = 0
    ){
    override fun toString(): String {
        return displayName
    }
}