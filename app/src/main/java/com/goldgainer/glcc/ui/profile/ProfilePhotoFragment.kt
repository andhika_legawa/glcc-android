package com.goldgainer.glcc.ui.profile

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class ProfilePhotoFragment : AuthenticatedFragment() {

	private lateinit var profileViewModel: ProfileViewModel

	private var imageCapture: ImageCapture? = null

	private lateinit var outputDirectory: File
	private lateinit var cameraExecutor: ExecutorService

	private var isCapturing : String? = null
	private var capturedId : File? = null
	private var capturedSelfie : File? = null

	private lateinit var imageviewId : ImageView
	private lateinit var imageviewSelfie : ImageView

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_profile_photo, container, false)
		setUserHeader(root.findViewById(R.id.profile_user_header))

		cameraExecutor = Executors.newSingleThreadExecutor()

		outputDirectory = getOutputDirectory()

		loggedInUser?.let {
			profileViewModel.currentOwner = it.email!!
		}

		val previewId = root.findViewById<PreviewView>(R.id.preview_id)
		val btnCaptureId = root.findViewById<Button>(R.id.btn_capture_id)

		val previewSelfie = root.findViewById<PreviewView>(R.id.preview_selfie)
		val btnCaptureSelfie = root.findViewById<Button>(R.id.btn_capture_selfie)

		val nationalidText = root.findViewById<TextView>(R.id.text_nationalid)
		val nationalidPhotoText = root.findViewById<TextView>(R.id.text_nationalid_photo)
		val selfieText = root.findViewById<TextView>(R.id.text_selfie)
		val selfiePhotoText = root.findViewById<TextView>(R.id.text_selfie_photo)

		val country = Helper.getLocalUser().country
		country?.let {
			profileViewModel.currentCountry = it
		}

		if (country != "ID")  {
			nationalidText.text = "Photo your Passport:"
			nationalidPhotoText.text = "Passport"
			selfieText.text = "Photo selfie while holding your Passport:"
			selfiePhotoText.text = "Photo selfie and hold your Passport"
			btnCaptureId.text = "Passport"
		}

		imageviewId = root.findViewById(R.id.imageview_id)
		imageviewSelfie = root.findViewById(R.id.imageview_selfie)

		val confirm = root.findViewById<TextView>(R.id.photo_text_confirm)
		val save = root.findViewById<Button>(R.id.btn_save_photo)
		val loading = root.findViewById<ProgressBar>(R.id.loading_photo)
		loading.visibility = View.INVISIBLE

		profileViewModel.currentProfile.observe(viewLifecycleOwner, Observer { profile ->

			val storageRef = Firebase.storage("gs://glcc-app-users/").reference

			profile.nationalIdPhoto?.let { it ->
				imageviewId.alpha = 1.0f
				val photoFile = File(outputDirectory, "nationalid.jpg")
				storageRef.child(it).getFile(photoFile)
					.addOnSuccessListener {
						Picasso.get().load(photoFile).resize(WIDTH, HEIGHT).centerCrop().into(imageviewId)
					}
					.addOnFailureListener{ e ->
						Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
					}
			}

			profile.selfiePhoto?.let {
				imageviewSelfie.alpha = 1.0f
				val photoFile = File(outputDirectory, "selfie.jpg")
				storageRef.child(it).getFile(photoFile)
					.addOnSuccessListener {
						Picasso.get().load(photoFile).resize(WIDTH, HEIGHT).centerCrop().into(imageviewSelfie)
					}
					.addOnFailureListener{ e ->
						Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
					}
			}

			save.isEnabled = true
			if (profile.status == "PA" || profile.status == "AV") {
				btnCaptureId.isEnabled = false
				btnCaptureId.visibility = View.INVISIBLE
				btnCaptureSelfie.isEnabled = false
				btnCaptureSelfie.visibility = View.INVISIBLE
				save.isEnabled = false
				confirm.visibility = View.INVISIBLE
				save.visibility = View.INVISIBLE
			}
		})

		profileViewModel.getProfile()

		btnCaptureId.setOnClickListener {
			if (allPermissionsGranted()) {
				if (isCapturing != null) {
					imageviewId.setImageResource(android.R.color.transparent)
					imageviewId.alpha = 1.0f
					previewId.alpha = 0.0f
					takePhoto("nationalid")
				} else {
					imageviewId.alpha = 0.0f
					previewId.alpha = 1.0f
					startCamera(previewId, CameraSelector.DEFAULT_BACK_CAMERA)
					isCapturing = "id"
				}
			} else {
				ActivityCompat.requestPermissions(
					requireActivity(),
					REQUIRED_PERMISSIONS,
					REQUEST_CODE_PERMISSIONS
				)
			}
		}

		btnCaptureSelfie.setOnClickListener {
			if (allPermissionsGranted()) {
				if (isCapturing != null) {
					imageviewSelfie.setImageResource(android.R.color.transparent)
					imageviewSelfie.alpha = 1.0f
					previewSelfie.alpha = 0.0f
					takePhoto("selfie")
				} else {
					imageviewSelfie.alpha = 0.0f
					previewSelfie.alpha = 1.0f
					startCamera(previewSelfie, CameraSelector.DEFAULT_FRONT_CAMERA)
					isCapturing = "selfie"
				}
			} else {
				ActivityCompat.requestPermissions(
					requireActivity(),
					REQUIRED_PERMISSIONS,
					REQUEST_CODE_PERMISSIONS
				)
			}
		}

		profileViewModel.profileError.observe(viewLifecycleOwner, Observer {
			loading.visibility = View.INVISIBLE
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		profileViewModel.profileSaved.observe(viewLifecycleOwner, Observer { success ->
			if (success) {
				loading.visibility = View.INVISIBLE
				findNavController().popBackStack()
			}
		})

		save.setOnClickListener {
			loading.visibility = View.VISIBLE
			profileViewModel.savePhoto(capturedId, capturedSelfie)
		}

		return root
	}

	private fun startCamera(viewFinder: PreviewView, cameraSelector: CameraSelector) {
		val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

		cameraProviderFuture.addListener(Runnable {
			try {

				// Used to bind the lifecycle of cameras to the lifecycle owner
				val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

				// Preview
				val preview = Preview.Builder()
					.setTargetResolution(Size(viewFinder.width, viewFinder.height))
					.build()
					.also {
						it.setSurfaceProvider(viewFinder.surfaceProvider)
					}

				imageCapture = ImageCapture.Builder()
					.setTargetResolution(Size(viewFinder.width, viewFinder.height))
					.build()


				// Unbind use cases before rebinding
				cameraProvider.unbindAll()

				// Bind use cases to camera
				cameraProvider.bindToLifecycle(
					viewLifecycleOwner,
					cameraSelector,
					preview,
					imageCapture
				)

			} catch (exc: Exception) {
				Log.e(TAG, "Use case binding failed", exc)
			}

		}, ContextCompat.getMainExecutor(requireContext()))
	}

	private fun takePhoto(filename: String) {
		// Get a stable reference of the modifiable image capture use case
		val imageCapture = imageCapture ?: return

		val photoFile = File(outputDirectory, "$filename.jpg")

		// Create output options object which contains file + metadata
		val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

		Toast.makeText(context, "Taking a photo", Toast.LENGTH_SHORT).show()
		// Set up image capture listener, which is triggered after photo has
		// been taken
		imageCapture.takePicture(
			outputOptions,
			ContextCompat.getMainExecutor(requireContext()),
			object : ImageCapture.OnImageSavedCallback {
				override fun onError(exc: ImageCaptureException) {
					Toast.makeText(context, exc.message, Toast.LENGTH_SHORT).show()
					Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
					// Reset Camera After
					isCapturing = null
					ProcessCameraProvider.getInstance(requireContext()).get().unbindAll()
				}

				override fun onImageSaved(output: ImageCapture.OutputFileResults) {
					val savedUri = Uri.fromFile(photoFile)
					Toast.makeText(context, "Done!", Toast.LENGTH_SHORT).show()
					if (isCapturing == "id") {
						capturedId = photoFile
						Picasso.get().load(savedUri).memoryPolicy(MemoryPolicy.NO_CACHE).resize(WIDTH, HEIGHT).centerCrop().into(imageviewId)
					}
					if (isCapturing == "selfie") {
						capturedSelfie = photoFile
						Picasso.get().load(savedUri).memoryPolicy(MemoryPolicy.NO_CACHE).resize(WIDTH, HEIGHT).centerCrop().into(imageviewSelfie)
					}
					// Reset Camera After
					isCapturing = null
					ProcessCameraProvider.getInstance(requireContext()).get().unbindAll()
					profileViewModel.compressPhoto(photoFile, requireContext())
				}
			})
	}

	private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
		ContextCompat.checkSelfPermission(
			requireContext(), it
		) == PackageManager.PERMISSION_GRANTED
	}

	private fun getOutputDirectory(): File {
		val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
			File(it, resources.getString(R.string.app_name)).apply { mkdirs() } }
		return if (mediaDir != null && mediaDir.exists())
			mediaDir else requireActivity().filesDir
	}

	override fun onDestroy() {
		super.onDestroy()
		cameraExecutor.shutdown()
	}

	companion object {
		private const val TAG = "GLCC Community"
		private const val REQUEST_CODE_PERMISSIONS = 10
		private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
		private const val WIDTH = 320
		private const val HEIGHT = 200
	}
}