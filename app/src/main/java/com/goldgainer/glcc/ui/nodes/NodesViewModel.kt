package com.goldgainer.glcc.ui.nodes

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.NetworkCategory
import kotlinx.coroutines.launch
import java.lang.Exception

class NodesViewModel : ViewModel() {

    var selectedNetwork: String = ""

    var currentCategory: NetworkCategory = NetworkCategory()

    private val _nodesAvailable = MutableLiveData<Int>()
    val nodesAvailable: LiveData<Int> = _nodesAvailable

    private val _nodeError = MutableLiveData<String?>()
    val nodeError: LiveData<String?> = _nodeError

    private val _database = FirebaseDataSource()

    fun checkNodesAvailability() {
        viewModelScope.launch {
            try {
                val helper = Helper()
                val globalNodesLimit = helper.getGlobalSettings("global_node_limit", 15) as Int
                val indonesiaNodesLimit = helper.getGlobalSettings("indonesia_node_limit", 15) as Int

                val currentNodes = _database.getNodes(selectedNetwork)
                val totalNodes = currentNodes.size

                var network = _database.getNetwork(selectedNetwork)
                if (network == null) {
                    network = _database.getOnHoldNetwork(selectedNetwork)?:throw Exception("Cannot found network!")
                }
                val categories = _database.getNetworkCategories(network.type)
                currentCategory = categories.find { it.number == network.number }?:throw Exception("Invalid Network Category!")

                if (currentCategory.type == "I") {
                    when {
                        indonesiaNodesLimit == 0 -> {
                            _nodesAvailable.value = 15
                        }
                        indonesiaNodesLimit == -1 -> {
                            _nodesAvailable.value = -1
                        }
                        indonesiaNodesLimit <= totalNodes -> {
                            _nodesAvailable.value = 0
                        }
                        else -> {
                            _nodesAvailable.value = indonesiaNodesLimit - totalNodes
                        }
                    }
                }

                if (currentCategory.type == "G") {
                    when {
                        globalNodesLimit == 0 -> {
                            _nodesAvailable.value = 15
                        }
                        globalNodesLimit == -1 -> {
                            _nodesAvailable.value = -1
                        }
                        globalNodesLimit <= totalNodes -> {
                            _nodesAvailable.value = 0
                        }
                        else -> {
                            _nodesAvailable.value = globalNodesLimit - totalNodes
                        }
                    }
                }

            }catch (e: Exception) {
                Log.d("NODES", e.message.toString())
                _nodeError.value = e.message
            }
        }
    }
}