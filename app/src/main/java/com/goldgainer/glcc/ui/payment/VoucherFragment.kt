package com.goldgainer.glcc.ui.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.afterTextChanged
import com.goldgainer.glcc.data.pluralize
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment
import java.util.*

class VoucherFragment : AuthenticatedFragment() {

    private lateinit var voucherViewModel: VoucherViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        voucherViewModel = ViewModelProvider(this).get(VoucherViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_voucher, container, false)
        setUserHeader(root.findViewById(R.id.voucher_user_header))

        val bundle = arguments
        if (bundle != null) {
            voucherViewModel.selectedNetwork = bundle.getString("selectedNetwork", "")
            voucherViewModel.selectedQuantity = bundle.getInt("quantity", 0)
        }

        val value = arguments?.getInt("value", 0)
        val quantity = arguments?.getInt("quantity", 1)
        val type = arguments?.getString("type", "I")

        val voucherSummary = root.findViewById<TextView>(R.id.voucher_summary_textview)

        var preText = "INDONESIA NETWORK"
        var currency = "IDR"
        if (type.equals("G")){
            preText = "GLOBAL NETWORK"
            currency = "USD"
        }

        val categoryDesc = arguments?.getString("selectedCategory", preText)

        voucherSummary.text = categoryDesc

        val nodeText = root.findViewById<TextView>(R.id.voucher_node_textview)
        val valueText = root.findViewById<TextView>(R.id.voucher_value_textview)

        nodeText.text = "$quantity Node".pluralize(quantity!!)
        valueText.text = (quantity!! * value!!).toDouble().toCurrency(currency)

        val voucher = root.findViewById<EditText>(R.id.voucher_code_textview)
        val proceed = root.findViewById<Button>(R.id.voucher_proceed_button)

        proceed.setOnClickListener {
            proceed.isEnabled = false
            voucherViewModel.performVoucherPayment()
        }

        voucher.afterTextChanged {
            if (it.isNotBlank()) {
                voucherViewModel.checkVoucher(it)
            }
        }

        voucherViewModel.validVoucher.observe(viewLifecycleOwner, Observer { validVoucher ->
            val codeText = voucher.text.toString()
            if (codeText.isBlank() || codeText.isEmpty()) {
                return@Observer
            }

            if(validVoucher == null){
                voucher.error = "Voucher Invalid!"
                proceed.isEnabled = false
                return@Observer
            }

            proceed.isEnabled = true

            //match with request

            var currency = "IDR"
            if (validVoucher.type == "G"){
                currency = "USD"
            }

//            voucherSummary.text = validVoucher.description
//            voucherSummary.animation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
//            voucherSummary.animate()

            nodeText.text = "${validVoucher.quantity} Node".pluralize(validVoucher.quantity)
            nodeText.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_green))
            nodeText.animation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
            nodeText.animate()

            valueText.text = (validVoucher.quantity * validVoucher.value).toDouble().toCurrency(currency)
            valueText.setTextColor(ContextCompat.getColor(requireContext(), R.color.glcc_green))
            valueText.animation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
            valueText.animate()

        })

        voucherViewModel.voucherTransaction.observe(viewLifecycleOwner, Observer {
            proceed.isEnabled = true
            if (it != null && it.status == "SUCCESS"){
                val status = "COMPLETED"
                val desc = "Thank you for your transaction. Your Transaction ID: ${it.id}"

                var region = "INDONESIA NETWORK"
                val type = it.data["nodeType"] as String
                var currency = "IDR"
                if (type == "G") {
                    region = "GLOBAL NETWORK"
                    currency = "USD"
                }
                val number = it.data["networkNumber"] as String
                val qty = it.data["nodeQuantity"] as String

                val details = region + " - N$number"
                val details2 = it.value.toDouble().toCurrency(currency) + " - $qty NODES"
                val bundle = bundleOf("transactionId" to it.id,
                                            "status" to status,
                                            "description" to desc,
                                            "details" to details,
                                            "details2" to details2
                                        )
                findNavController().navigate(R.id.action_nav_voucher_to_nav_status, bundle)
            } else {
                Toast.makeText(context, "Redeem voucher failed. Please try again later.", Toast.LENGTH_LONG).show()
            }
        })

        voucherViewModel.transactionError.observe(viewLifecycleOwner, Observer {
            if (it != null){
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })

        val orTextView = root.findViewById<TextView>(R.id.voucher_or_textview)
        val creditCard = root.findViewById<Button>(R.id.voucher_card_button)

        if (type == "G") {
            orTextView.visibility = View.GONE
            creditCard.visibility = View.GONE
        }

        creditCard.setOnClickListener {
            findNavController().navigate(R.id.action_nav_voucher_to_nav_payment, arguments)
        }

        return root
    }

}