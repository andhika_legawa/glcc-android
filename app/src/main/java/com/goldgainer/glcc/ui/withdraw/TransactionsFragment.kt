package com.goldgainer.glcc.ui.withdraw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.text.SimpleDateFormat
import java.util.*

class TransactionsFragment : AuthenticatedFragment() {

    private lateinit var withdrawViewModel: WithdrawViewModel
    private lateinit var transactionAdapter: TransactionAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        withdrawViewModel = ViewModelProvider(this).get(WithdrawViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_withdraw_transactions, container, false)
        setUserHeader(root.findViewById(R.id.withdraw_user_header))

        val networkSpinner = root.findViewById<Spinner>(R.id.transaction_network_spinner)
        val yearSpinner = root.findViewById<Spinner>(R.id.transaction_year)
        val monthSpinner = root.findViewById<Spinner>(R.id.transaction_month)
        val searchBtn = root.findViewById<Button>(R.id.transactions_search_btn)
        val trxHeader = root.findViewById<LinearLayout>(R.id.transactions_trx_header)
        val trxEmpty = root.findViewById<TextView>(R.id.transaction_empty)
        val trxList = root.findViewById<ListView>(R.id.transactions_list)
        val progressCircular = root.findViewById<ProgressBar>(R.id.progress_circular)

        val withdrawBtn = root.findViewById<MaterialButton>(R.id.withdraw_tab)

        withdrawBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        withdrawViewModel.isBusy.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressCircular.visibility = View.VISIBLE
            } else {
                progressCircular.visibility = View.GONE
            }
        })

        withdrawViewModel.reset()

        withdrawViewModel.error.observe(viewLifecycleOwner, Observer { errorString ->
            if (errorString != null) {
                MaterialAlertDialogBuilder(requireContext(), R.style.Theme_GLCC_Dialog_Alert)
                    .setTitle("Info")
                    .setMessage(errorString)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            }
        })

        val networkAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, arrayListOf())
        networkAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        networkSpinner.adapter = networkAdapter

        transactionAdapter = TransactionAdapter(requireContext(), arrayListOf())
        trxList.adapter = transactionAdapter
        trxList.emptyView = trxEmpty

        withdrawViewModel.networkCategories.observe(viewLifecycleOwner, Observer { categories ->
            networkAdapter.clear()
            if (categories.isNotEmpty()) {
                categories.forEach {
                    networkAdapter.add(it.description)
                }
                networkAdapter.notifyDataSetChanged()
                networkSpinner.setSelection(0)
            }
        })

        withdrawViewModel.transactions.observe(viewLifecycleOwner, Observer { histories ->
            transactionAdapter.clear()
            if (histories.isNotEmpty()) {
                histories.forEach {
                    transactionAdapter.add(it)
                }
                transactionAdapter.notifyDataSetChanged()
                trxHeader.visibility = View.VISIBLE
            } else {
                trxHeader.visibility = View.GONE
            }
        })

        val yearAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        yearAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        val start = 2021
        var currentYear = Calendar.getInstance().get(Calendar.YEAR)
        while ( currentYear >= start ) {
            yearAdapter.add(currentYear.toString())
            currentYear -= 1
        }
        yearSpinner.adapter = yearAdapter

        val monthAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        monthAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        var currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        var january = 0
        val sdf = SimpleDateFormat("MMM")
        while ( january <= 11) {
            val cal = Calendar.getInstance()
            cal.set(currentYear, january, 1)
            monthAdapter.add(sdf.format(cal.time))
            january += 1
        }
        monthSpinner.adapter = monthAdapter
        monthSpinner.setSelection(currentMonth)

        trxHeader.visibility = View.GONE
        trxList.visibility = View.GONE

        searchBtn.setOnClickListener {
            val yearStr = yearSpinner.selectedItem as String
            val year = yearStr.toInt()
            val month = monthSpinner.selectedItemPosition
            val cal = Calendar.getInstance()
            cal.set(year, month, 1, 0, 0 ,0)
            val start = cal.time
            cal.set(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59)
            val end = cal.time

            withdrawViewModel.searchTransaction(networkSpinner.selectedItemPosition, start, end)
        }

        withdrawViewModel.loadNetworks()

        return root
    }
}