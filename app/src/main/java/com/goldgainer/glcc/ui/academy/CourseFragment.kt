package com.goldgainer.glcc.ui.academy

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.Display
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.model.Course
import com.goldgainer.glcc.data.model.Prodi
import com.goldgainer.glcc.data.model.ProdiStatic
import com.goldgainer.glcc.data.model.Semester
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.selects.select

class CourseFragment : AuthenticatedFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_academy_course, container, false)
        setUserHeader(root.findViewById(R.id.user_header))

        val prodiName = root.findViewById<TextView>(R.id.prodi_name)
        val prodiDetail = root.findViewById<TextView>(R.id.prodi_detail)

        var prodi = Prodi()
        arguments?.let {
            when (it.getInt("prodi", 1)) {
                1 -> prodi = ProdiStatic.getProdi1()
                2 -> prodi = ProdiStatic.getProdi2()
                3 -> prodi = ProdiStatic.getProdi3()
                4 -> prodi = ProdiStatic.getProdi4()
                5 -> prodi = ProdiStatic.getProdi5()
            }
        }

        prodiName.text = prodi.name
        prodiDetail.text = "${prodi.semesterTotal} Semester, ${prodi.courseTotal} Courses and ${prodi.sksTotal} SKS"

        val semesterAdapter = SemesterExpandableListAdapter(requireContext(), prodi.semesterList)

        val semesterExpandable = root.findViewById<ExpandableListView>(R.id.semester_expandable)
        semesterExpandable.setAdapter(semesterAdapter)
        prodi.semesterList.forEachIndexed { index, _ ->
            semesterExpandable.expandGroup(index)
        }
        semesterExpandable.setOnChildClickListener { _, _, _, _, _ ->
            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                .setTitle("")
                .setMessage("The course is locked.")
                .setPositiveButton(android.R.string.ok, null)
                .setCancelable(false)
                .create()
                .show()
            return@setOnChildClickListener true
        }
        return root
    }
}

class SemesterExpandableListAdapter(
    context: Context, semesterList: List<Semester>
) : BaseExpandableListAdapter() {
    private val context: Context = context
    private val semesterList: List<Semester> = semesterList

    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        val semester = semesterList[listPosition]
        return semester.courseList[expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int, expandedListPosition: Int,
        isLastChild: Boolean, convertView: View?, parent: ViewGroup?
    ): View? {
        var convertView = convertView
        val course = getChild(listPosition, expandedListPosition) as Course
        if (convertView == null) {
            val layoutInflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.fragment_academy_course_item, null)
        }

        val nameText = convertView?.findViewById<TextView>(R.id.course_name)

        nameText?.text = "${course.order}. ${course.name} - ${course.sks}"

        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        val semester = semesterList[listPosition]
        return semester.courseList.size
    }

    override fun getGroup(listPosition: Int): Any {
        return semesterList[listPosition]
    }

    override fun getGroupCount(): Int {
        return semesterList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        listPosition: Int, isExpanded: Boolean,
        convertView: View?, parent: ViewGroup?
    ): View? {
        var convertView = convertView
        val semester = getGroup(listPosition) as Semester
        if (convertView == null) {
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.fragment_academy_course_group, null)
        }

        val nameText = convertView?.findViewById<TextView>(R.id.semester_name)
        val detailText= convertView?.findViewById<TextView>(R.id.semester_detail)

        nameText?.text = semester.name
        detailText?.text = "${semester.courseTotal} Courses and ${semester.sksTotal} SKS"

        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }

}