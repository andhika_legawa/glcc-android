package com.goldgainer.glcc.ui.withdraw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.data.toDigitalCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment

class StatusFragment: AuthenticatedFragment() {

	private lateinit var withdrawViewModel: WithdrawViewModel

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		withdrawViewModel = ViewModelProvider(this).get(WithdrawViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_withdraw_status, container, false)
		setUserHeader(root.findViewById(R.id.withdraw_user_header))

		val trxId = root.findViewById<TextView>(R.id.trxid_textview)
		val received = root.findViewById<TextView>(R.id.amount_textview)
		val walletBtn = root.findViewById<Button>(R.id.wallet_btn)

		walletBtn.setOnClickListener {
			findNavController().navigate(R.id.action_nav_withdraw_status_to_nav_wallet)
		}

		withdrawViewModel.status.observe(viewLifecycleOwner, Observer { status ->
			if (status == "SUBMITTED") {
				val request = withdrawViewModel.currentRequest?:return@Observer
				trxId.text = "Your Transaction ID: #" + request.trxId
				if (request.subtype == "GC") {
					received.text = request.savingGoldcoin.toDigitalCurrency("GLC")
				} else if (request.subtype == "PG") {
					received.text = request.savingGold.toString() + " Grams"
				} else {
					received.text = request.amountReceived.toCurrency("IDR")
				}
			}
		})

		return root
	}

	override fun onResume() {
		super.onResume()
		withdrawViewModel.checkWithdrawStatus()
	}
}