package com.goldgainer.glcc.ui.login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.BuildConfig
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.afterTextChanged
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.hbb20.CountryCodePicker

class RegisterFragment : Fragment() {

    private lateinit var registerViewModel: RegisterViewModel

    private lateinit var platinumTrainerAdapter : TrainerArrayAdapter
    private lateinit var goldTrainerAdapter : TrainerArrayAdapter
    private lateinit var trainer : AutoCompleteTextView
    private lateinit var trainerId: TextView
    private var selectedPosition : Int = 0
    private var selectedTrainerPosition : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_register, container, false)

        val username = root.findViewById<EditText>(R.id.register_username)
        val email = root.findViewById<EditText>(R.id.register_email)
        val sponsor = root.findViewById<EditText>(R.id.register_sponsor)
        val password = root.findViewById<EditText>(R.id.register_password)
        val retype = root.findViewById<EditText>(R.id.register_retype)
        val loading = root.findViewById<ProgressBar>(R.id.register_loading)
        val register = root.findViewById<Button>(R.id.register_button)
        trainer = root.findViewById(R.id.register_trainer)
        trainerId = root.findViewById(R.id.register_trainer_id)
        val platinumRadio = root.findViewById<RadioButton>(R.id.platinum_radio)
        val goldRadio = root.findViewById<RadioButton>(R.id.gold_radio)
        val trainerPlatinumRadio = root.findViewById<RadioButton>(R.id.platinum_radio2)
        val trainerGoldRadio = root.findViewById<RadioButton>(R.id.gold_radio2)
        var ccp = root.findViewById<CountryCodePicker>(R.id.register_country)
        val loadContainer = root.findViewById<LinearLayout>(R.id.load_container)
        loadContainer.visibility = View.GONE

        registerViewModel = ViewModelProvider(this, RegisterViewModelFactory()).get(RegisterViewModel::class.java)

        register.isEnabled = false

        registerViewModel.referralValid.observe(viewLifecycleOwner, Observer { valid ->
            sponsor.error = null
            if (!valid) {
                sponsor.error = getString(R.string.invalid_sponsor)
            }
            register.isEnabled = valid
        })

        registerViewModel.registerFormState.observe(viewLifecycleOwner, Observer {
            val registerState = it ?: return@Observer

            // disable register button unless valid
            register.isEnabled = registerState.isDataValid

            username.error = null
            if (registerState.usernameError != null) {
                username.error = getString(registerState.usernameError)
            }

            email.error = null
            if (registerState.emailError != null) {
                email.error = getString(registerState.emailError)
            }

            trainer.error = null
            if (registerState.trainerError != null) {
                trainer.error = getString(registerState.trainerError)
            }

            password.error = null
            if (registerState.passwordError != null) {
                password.error = getString(registerState.passwordError)
            }

            retype.error = null
            if(registerState.retypePasswordError != null){
                retype.error = getString(registerState.retypePasswordError)
            }

        })

        registerViewModel.registerResult.observe(viewLifecycleOwner, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                if (loginResult.errorMessage != null) {
                    MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                            .setTitle("Register Failed")
                            .setMessage(loginResult.errorMessage)
                            .setPositiveButton(android.R.string.ok, null)
                            .show()
                } else {
                    Toast.makeText(activity, loginResult.error, Toast.LENGTH_SHORT).show()
                }
            }
            if (loginResult.success != null) {
                Toast.makeText(activity, "Register Success", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_nav_register_to_nav_validation)
            }
        })

        registerViewModel.loading.observe(viewLifecycleOwner, Observer {
            val isLoading = it ?: return@Observer
            if (isLoading){
                loading.visibility = View.VISIBLE
                register.isEnabled = false
            }else{
                loading.visibility = View.GONE
                register.isEnabled = true
            }
        })

        registerViewModel.trainerLoading.observe(viewLifecycleOwner, Observer {
            val isLoading = it ?: return@Observer
            if (isLoading){
                loadContainer.visibility = View.VISIBLE
                trainer.visibility = View.GONE
                trainerId.visibility = View.GONE
            }else{
                loadContainer.visibility = View.GONE
                trainer.visibility = View.VISIBLE
                trainerId.visibility = View.VISIBLE
            }
        })

        platinumTrainerAdapter = TrainerArrayAdapter(requireContext(), R.layout.partials_dropdown_item, android.R.id.text1, listOf())
        goldTrainerAdapter = TrainerArrayAdapter(requireContext(), R.layout.partials_dropdown_item, android.R.id.text1, listOf())

        trainerPlatinumRadio.isEnabled = false
        trainerGoldRadio.isEnabled = false
        registerViewModel.referralTrainer.observe(viewLifecycleOwner, Observer {
            platinumTrainerAdapter = TrainerArrayAdapter(requireContext(), R.layout.partials_dropdown_item, android.R.id.text1, it.platinumTrainers)
            goldTrainerAdapter = TrainerArrayAdapter(requireContext(), R.layout.partials_dropdown_item, android.R.id.text1, it.goldTrainers)
            trainerPlatinumRadio.isChecked = false
            trainerGoldRadio.isChecked = false
            updateTrainerUI()
        })

//        registerViewModel.trainerFirstLevel.observe(viewLifecycleOwner, Observer {  firstLevel ->
//            firstLevel.forEach {
//                if (it.binaryPosition == 0) {
//                    trainerPlatinumRadio.isChecked = false
//                    trainerPlatinumRadio.isEnabled = false
//                    trainerPlatinumRadio.alpha = 0.5F
//                    trainerGoldRadio.isChecked = true
//                    trainerGoldRadio.isEnabled = true
//                    selectedTrainerPosition = 1
//                }
//                if (it.binaryPosition == 1) {
//                    trainerPlatinumRadio.isChecked = true
//                    trainerPlatinumRadio.isEnabled = true
//                    trainerGoldRadio.isChecked = false
//                    trainerGoldRadio.isEnabled = false
//                    trainerGoldRadio.alpha = 0.5F
//                    selectedTrainerPosition = 0
//                }
//            }
//        })

        trainer.setOnItemClickListener { parent, _, position, _ ->
            try{
                registerViewModel.selectedTrainer = parent.getItemAtPosition(position) as Trainer

                registerViewModel.selectedTrainer?.let{
                    trainerId.text = it.referralId
                    registerViewModel.trainerChanged(it.referralId)
                }
            }catch(e: Exception){
                Log.d("REGISTER", e.message.toString())
                registerViewModel.selectedTrainer = null
                trainerId.text = ""
            }
        }

        trainer.afterTextChanged {
            if (!trainer.isPerformingCompletion){
                registerViewModel.selectedTrainer = null
                trainerId.text = ""
            }
        }

        trainer.setOnClickListener {
            if (trainer.adapter != null){
                trainer.showDropDown()
            }
        }

        trainer.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus && trainer.adapter != null){
                trainer.showDropDown()
            }
            if(!hasFocus){
                if (registerViewModel.selectedTrainer == null) {
                    trainer.error = "Please select trainer"
                }else{
                    trainer.error = null
                }
            }
        }

        username.afterTextChanged {
            registerViewModel.registerDataChanged(
                username = username.text.toString(),
                email = email.text.toString(),
                password = password.text.toString(),
                retype = retype.text.toString())
        }

        email.afterTextChanged {
            registerViewModel.registerDataChanged(
                username = username.text.toString(),
                email = email.text.toString(),
                password = password.text.toString(),
                retype = retype.text.toString())
        }

        sponsor.afterTextChanged {
            registerViewModel.referralDataChanged(sponsor.text.toString(), ccp.selectedCountryNameCode)
        }

        ccp.setOnCountryChangeListener {
            val sponsorId = sponsor.text.toString()
            if (!sponsorId.isNullOrBlank()) {
                registerViewModel.referralDataChanged(sponsorId, ccp.selectedCountryNameCode)
            }
        }

        password.afterTextChanged {
            registerViewModel.registerDataChanged(
                username = username.text.toString(),
                email = email.text.toString(),
                password = password.text.toString(),
                retype = retype.text.toString())
        }

        retype.afterTextChanged {
            registerViewModel.registerDataChanged(
                username = username.text.toString(),
                email = email.text.toString(),
                password = password.text.toString(),
                retype = retype.text.toString())
        }

        platinumRadio.setOnClickListener {
            platinumRadio.isChecked = true
            trainerPlatinumRadio.isChecked = true
            goldRadio.isChecked = false
            trainerGoldRadio.isChecked = false
            selectedPosition = 0
            selectedTrainerPosition = 0
            updateTrainerUI()
        }

        goldRadio.setOnClickListener {
            platinumRadio.isChecked = false
            trainerPlatinumRadio.isChecked = false
            goldRadio.isChecked = true
            trainerGoldRadio.isChecked = true
            selectedPosition = 1
            selectedTrainerPosition = 1
            updateTrainerUI()
        }

//        trainerPlatinumRadio.setOnClickListener {
//            trainerPlatinumRadio.isChecked = true
//            trainerGoldRadio.isChecked = false
//            selectedTrainerPosition = 0
//        }
//
//        trainerGoldRadio.setOnClickListener {
//            trainerPlatinumRadio.isChecked = false
//            trainerGoldRadio.isChecked = true
//            selectedTrainerPosition = 1
//        }

        val loginTabButton = root.findViewById<Button>(R.id.register_first_tab_button)

        loginTabButton.setOnClickListener {
            findNavController().navigate(R.id.action_nav_register_to_nav_login)
        }

        register.setOnClickListener {
            registerViewModel.register(
                username = username.text.toString(),
                email = email.text.toString(),
                country = ccp.selectedCountryNameCode,
                sponsor = sponsor.text.toString(),
                position = selectedTrainerPosition,
                trainer = registerViewModel.selectedTrainer?.referralId,
                password = password.text.toString(),
                retype = retype.text.toString())
        }

        registerViewModel.referralError.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })

        registerViewModel.latestVersion.observe(viewLifecycleOwner, Observer {
            if (BuildConfig.VERSION_CODE < it) {
                MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                        .setTitle("Application Need Update")
                        .setMessage("There is a new update available, please update to continue using GLCC app.")
                        .setPositiveButton(android.R.string.ok, null)
                        .setCancelable(false)
                        .show()
            }
        })

        return root
    }

    private fun updateTrainerUI(){
        registerViewModel.selectedTrainer = null
        trainerId.text = ""
        trainer.setText("")

        if (selectedPosition == 0){
            trainer.setAdapter(platinumTrainerAdapter)
        }
        if (selectedPosition == 1){
            trainer.setAdapter(goldTrainerAdapter)
        }

        trainer.invalidate()
    }

    override fun onResume() {
        super.onResume()
        registerViewModel.checkVersion()
    }
}