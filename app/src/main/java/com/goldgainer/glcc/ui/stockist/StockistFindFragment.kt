package com.goldgainer.glcc.ui.stockist

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch

class StockistFindFragment : AuthenticatedFragment() {

    private lateinit var viewModel: StockistViewModel

    private lateinit var provinceAdapter : ArrayAdapter<String>
    private lateinit var cityAdapter : ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_stockist_find, container, false)
        setUserHeader(root.findViewById(R.id.stockist_user_header))
        viewModel = ViewModelProvider(this).get(StockistViewModel::class.java)

        val provinceSpinner = root.findViewById<Spinner>(R.id.stockist_find_province_spinner)
        val citySpinner = root.findViewById<Spinner>(R.id.stockist_find_city_spinner)
        val myStockistBtn = root.findViewById<Button>(R.id.stockist_my_button)

        val searchBtn = root.findViewById<Button>(R.id.stockist_search_btn)
        val stockistHeader = root.findViewById<LinearLayout>(R.id.stockist_header)
        val stockistList = root.findViewById<ListView>(R.id.stockist_list)
        val stockistEmpty = root.findViewById<TextView>(R.id.stockist_empty)

        myStockistBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        provinceAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        provinceAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        provinceSpinner.adapter = provinceAdapter

        cityAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        cityAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        citySpinner.adapter = cityAdapter

        provinceSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                cityAdapter.clear()
                val regions = viewModel.regions.value
                regions?.let {
                    val province = it.getJSONObject(position)
                    val cities = province.getJSONArray("kota")
                    var cityIndex = 0
                    for (i in 0 until cities.length() step 1) {
                        val cityName = cities.getString(i)
                        cityAdapter.add(cityName)
                    }
                    cityAdapter.notifyDataSetChanged()
                    citySpinner.setSelection(cityIndex)
                }
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        viewModel.regions.observe(viewLifecycleOwner, Observer { provinces ->
            provinceAdapter.clear()
            var provinceIndex = 0
            for (i in 0 until provinces.length() step 1) {
                val jsonObject = provinces.getJSONObject(i)
                val provinceName = jsonObject.getString("provinsi")
                provinceAdapter.add(provinceName)
            }
            provinceAdapter.notifyDataSetChanged()
            provinceSpinner.setSelection(provinceIndex)
        })

        viewModel.getProvinces(requireContext())

        stockistHeader.visibility = View.GONE
        stockistList.visibility = View.GONE
        stockistEmpty.visibility = View.GONE

        val stockistAdapter = StockistAdapter(requireContext(), arrayListOf(),
            { whatsapp ->
                val packageManager = requireActivity().packageManager
                val i = Intent(Intent.ACTION_VIEW)
                val url = "https://api.whatsapp.com/send?phone=$whatsapp"
                i.setPackage("com.whatsapp")
                i.data = Uri.parse(url)
                if (i.resolveActivity(packageManager) != null) {
                    startActivity(i)
                } else {
                    MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                        .setTitle("Info")
                        .setMessage("No whatsapp detected. Stockist Whatsapp number : $whatsapp")
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                }
            },
            { telegram ->
                val telegramHandle = telegram.substringAfter("@")
                val packageManager = requireActivity().packageManager
                val i = Intent(Intent.ACTION_VIEW)
                val url = "http://telegram.me/$telegramHandle"
                i.setPackage("org.telegram.messenger")
                i.data = Uri.parse(url)
                if (i.resolveActivity(packageManager) != null) {
                    startActivity(i)
                } else {
                    MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                        .setTitle("Info")
                        .setMessage("No telegram detected. Stockist Telegram User : $telegramHandle")
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                }
            })
        stockistList.adapter = stockistAdapter

        searchBtn.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                val province = provinceSpinner.selectedItem as String
                val city = citySpinner.selectedItem as String
                val list = viewModel.findStockist(province, city)
                if (list.isEmpty()) {
                    stockistHeader.visibility = View.GONE
                    stockistList.visibility = View.GONE
                    stockistEmpty.visibility = View.VISIBLE
                    stockistEmpty.text = "Cannot find stockist on $province, $city"
                } else {
                    stockistHeader.visibility = View.VISIBLE
                    stockistList.visibility = View.VISIBLE
                    stockistEmpty.visibility = View.GONE
                    stockistAdapter.clear()
                    stockistAdapter.addAll(list)
                    stockistAdapter.notifyDataSetChanged()
                }
            }
        }

        return root
    }
}