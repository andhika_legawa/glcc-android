package com.goldgainer.glcc.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.hbb20.CCPCountry
import com.hbb20.CountryCodePicker

class ProfileDetailFragment : AuthenticatedFragment() {

	private lateinit var profileViewModel: ProfileViewModel

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_profile_detail, container, false)
		setUserHeader(root.findViewById(R.id.profile_user_header))

		loggedInUser?.let {
			profileViewModel.currentOwner = it.email!!
		}

		val userData = Helper.getLocalUser()

		val fullname = root.findViewById<EditText>(R.id.edit_fullname)
		val username = root.findViewById<TextView>(R.id.edit_username)
		val email = root.findViewById<TextView>(R.id.edit_email)
		val country = root.findViewById<TextView>(R.id.edit_country)
		val address = root.findViewById<EditText>(R.id.edit_address)
		val phone = root.findViewById<EditText>(R.id.edit_phone)
		val nationalid = root.findViewById<EditText>(R.id.edit_national_id)
		val taxid = root.findViewById<EditText>(R.id.edit_tax_id)

		val nationalidText = root.findViewById<TextView>(R.id.detail_text_nationalid)
		val taxidText = root.findViewById<TextView>(R.id.detail_text_tax)
		val confirm = root.findViewById<TextView>(R.id.text_confirm)
		val save = root.findViewById<Button>(R.id.btn_save_detail)

		if (userData.name != null) {
			username.text = userData.name
		}
		if (userData.email != null) {
			email.text = userData.email
		}
		userData.country?.let {
			profileViewModel.currentCountry = it
			if (it != "ID") {
				nationalidText.text = "Passport Number:"
				nationalid.hint = resources.getText(R.string.passport)
				taxidText.visibility = View.GONE
				taxid.visibility = View.GONE
			}
			val ccpCountry = CCPCountry.getCountryForNameCodeFromLibraryMasterList(requireContext(), CountryCodePicker.Language.ENGLISH, it)
			country.text = ccpCountry.englishName
		}

		profileViewModel.currentProfile.observe(viewLifecycleOwner, Observer {  profile ->
			profile.fullname?.let {
				fullname.setText(it)
			}
			profile.address?.let {
				address.setText(it)
			}
			profile.phone?.let {
				phone.setText(it)
			}
			profile.nationalId?.let {
				nationalid.setText(it)
			}
			profile.taxId?.let {
				taxid.setText(it)
			}
			save.isEnabled = true
			if (profile.status == "PA" || profile.status == "AV") {
				fullname.isEnabled = false
				address.isEnabled = false
				phone.isEnabled = false
				nationalid.isEnabled = false
				taxid.isEnabled = false
				save.isEnabled = false
				confirm.visibility = View.INVISIBLE
				save.visibility = View.INVISIBLE
			}
		})

		profileViewModel.profileError.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		profileViewModel.getProfile()

		profileViewModel.profileSaved.observe(viewLifecycleOwner, Observer { success ->
			if (success) {
				findNavController().popBackStack()
			}
		})

		save.setOnClickListener {
			profileViewModel.saveDetail(
					fullname = fullname.text.toString(),
					address = address.text.toString(),
					phone = phone.text.toString(),
					nationalid = nationalid.text.toString(),
					taxid = taxid.text.toString())
		}

		return root
	}

}