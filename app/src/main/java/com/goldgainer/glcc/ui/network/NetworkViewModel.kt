package com.goldgainer.glcc.ui.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.Network
import com.goldgainer.glcc.ui.login.Trainer
import kotlinx.coroutines.launch

open class NetworkViewModel : ViewModel() {

    var selectedType : String? = null
    var selectedNumber : Int? = null

    private val _addNetworkResult = MutableLiveData<NetworkResult>()
    val addNetworkResult: LiveData<NetworkResult> = _addNetworkResult

    protected val dataSource = FirebaseDataSource()

    protected val isLoading = MutableLiveData<Boolean>(false)
    val loading: LiveData<Boolean> = isLoading

    protected val isTrainerLoading = MutableLiveData<Boolean>(false)
    val trainerLoading: LiveData<Boolean> = isTrainerLoading

    private var _referralValid = MutableLiveData<Boolean>()
    val referralValid: LiveData<Boolean> = _referralValid

    private val _referralTrainer = MutableLiveData<ReferralTrainer>()
    val referralTrainer: LiveData<ReferralTrainer> = _referralTrainer

    private val _referralError = MutableLiveData<String?>()
    val referralError: LiveData<String?> = _referralError

    private val _trainerFirstLevel = MutableLiveData<List<Network>>()
    val trainerFirstLevel: LiveData<List<Network>> = _trainerFirstLevel

    var selectedTrainer: Trainer? = null

    fun addNetwork(sponsorId: String, trainerId: String, email: String, username: String, position: Int) {
        viewModelScope.launch {
            try {
                val newNetwork = createNetwork(sponsorId, trainerId, email, username, position)
                _addNetworkResult.value = NetworkResult(success = newNetwork)
            }catch(e: Exception) {
                e.message?.let { Log.d("REGISTER", it) }
                _addNetworkResult.value = NetworkResult(error = R.string.network_creation_failed)
            }
        }
    }

    protected suspend fun createNetwork(sponsorId: String, trainerId: String, email: String, username: String, position: Int) : Network {

        val trainerNetwork = dataSource.getNetwork(trainerId)?:throw Exception("Invalid trainer data")
        val networkCategories = dataSource.getNetworkCategories(trainerNetwork.type)
        val selectedCategory = networkCategories.find { it.number == trainerNetwork.number }?:throw Exception("Invalid network category")

        if (!selectedCategory.active) {
            throw Exception("Network already closed!")
        }

        if (selectedCategory.locked) {
            throw Exception("Network is currently locked")
        }

        val helper = Helper()
        val prefix = "N" + trainerNetwork.type
        var referralId = helper.nodeIDGenerator(prefix)
        var retry = 10
        while (dataSource.checkReferralExist(referralId)) {
            if (retry <= 0) {
                throw Exception("ID exist")
            }
            referralId = helper.nodeIDGenerator(prefix)
            retry--
        }

        //check position
        val firstLevel = dataSource.getNetworkFirstLevel(trainerId)
        firstLevel.forEach {
            if(it.binaryPosition == position) {
                throw Exception("Position already filled")
            }
        }

        //create new network
        val newNetwork = Network(
            owner = email,
            ownerName = username,
            referralId = referralId,
            type = trainerNetwork.type,
            number = trainerNetwork.number,
            sponsorId = sponsorId,
            trainerId = trainerId,
            category = selectedCategory.id,
            binaryPosition = position
        )

        if( ! dataSource.createNetwork(newNetwork) ){
            throw java.lang.Exception("failed to save network")
        }

        return newNetwork
    }

    fun referralDataChanged(referral: String, country: String?) {
        var networkType = selectedType

        if (country != null && country != "ID") {
            //force to G
            networkType = "G"
        }
        //check against db
        viewModelScope.launch {
            isTrainerLoading.value = true
            try{
                val isValid = dataSource.checkReferralExist(referral, networkType, selectedNumber)
                if (!isValid) {
                    _referralValid.value = false
                    _referralTrainer.value = ReferralTrainer()
                    throw Exception("Invalid Coach ID")
                }

                val tree = dataSource.getNetworkTree(referral)?:throw Exception("Invalid Coach ID")

                if (isValid) {
                    val networkCategories = dataSource.getNetworkCategories(tree.root.type)
                    val selectedCategory = networkCategories.find { it.number == tree.root.number }?:throw Exception("Invalid network category")

                    if (!selectedCategory.active) {
                        _referralValid.value = false
                        throw Exception("Network already closed!")
                    }

                    if (selectedCategory.locked) {
                        _referralValid.value = false
                        throw Exception("Network is currently locked")
                    }
                }
                val aPlatinumTrainers = tree.platinumTrainers.filter { it.binaryState < 2 }
                aPlatinumTrainers.onEach { network ->
                    network.platinumHead = tree.platinumTrainers.find { it.binaryPosition == 0 && it.trainerId == network.referralId }
                }
                val platinumTrainers = aPlatinumTrainers.filter { it.platinumHead == null }
                val aGoldTrainers = tree.goldTrainers.filter { it.binaryState < 2 }
                aGoldTrainers.onEach { network ->
                    network.goldHead = tree.goldTrainers.find { it.binaryPosition == 1 &&  it.trainerId == network.referralId }
                }
                val goldTrainers = aGoldTrainers.filter { it.goldHead == null }

                val trainers = ReferralTrainer(
                    coach = tree.root,
                    platinumTrainers = platinumTrainers.map { Trainer(it.referralId, it.ownerName, it.binaryState) },
                    goldTrainers = goldTrainers.map { Trainer(it.referralId, it.ownerName, it.binaryState) }
                )
                if(trainers.platinumTrainers.isNullOrEmpty()){
                    trainers.platinumTrainers = listOf(Trainer(referral, "Coach is your trainer"))
                }
                if(trainers.goldTrainers.isNullOrEmpty()){
                    trainers.goldTrainers = listOf(Trainer(referral, "Coach is your trainer"))
                }
                _referralTrainer.value = trainers
                _referralValid.value = true
            }catch(e: Exception){
                _referralTrainer.value = ReferralTrainer()
                _referralError.value = e.message
            }
            isTrainerLoading.value = false
        }
    }

    fun trainerChanged(referral: String) {
        viewModelScope.launch {
            _trainerFirstLevel.value = dataSource.getNetworkFirstLevel(referral)
        }
    }

    data class ReferralTrainer(
        val coach: Network? = null,
        var platinumTrainers: List<Trainer> = listOf(),
        var goldTrainers: List<Trainer> = listOf())
}