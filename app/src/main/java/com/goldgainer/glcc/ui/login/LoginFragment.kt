package com.goldgainer.glcc.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.BuildConfig
import com.goldgainer.glcc.MainActivity
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.afterTextChanged
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class LoginFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_login, container, false)

        val username = root.findViewById<EditText>(R.id.login_username)
        val password = root.findViewById<EditText>(R.id.login_password)
        val login = root.findViewById<Button>(R.id.login_button)
        val loading = root.findViewById<ProgressBar>(R.id.login_loading)

        loginViewModel = ViewModelProvider(this, LoginViewModelFactory()).get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(viewLifecycleOwner, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(viewLifecycleOwner, Observer {
            login.isEnabled = true

            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                if (loginResult.errorMessage != null) {
                    MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                            .setTitle("Login Failed")
                            .setMessage(loginResult.errorMessage)
                            .setPositiveButton(android.R.string.ok, null)
                            .show()
                } else {
                    if (loginResult.error == R.string.verify_email) {
                        findNavController().navigate(R.id.action_nav_login_to_nav_validation)
                        return@Observer
                    } else {
                        showLoginFailed(loginResult.error)
                    }
                }
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                        username.text.toString(),
                        password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> {
                        login.isEnabled = false
                        loading.visibility = View.VISIBLE
                        loginViewModel.login(username.text.toString(), password.text.toString())
                    }
                }
                false
            }
        }

        login.setOnClickListener {
            login.isEnabled = false
            loading.visibility = View.VISIBLE
            loginViewModel.login(username.text.toString(), password.text.toString())
        }

//        var loginTabButton = root.findViewById<Button>(R.id.login_first_tab_button)
        val registerTabButton = root.findViewById<Button>(R.id.login_second_tab_button)
        val forgotTextView = root.findViewById<TextView>(R.id.login_forgot_textview)

        forgotTextView.setOnClickListener {
            findNavController().navigate(R.id.action_nav_login_to_nav_forgot)
        }

        loginViewModel.latestVersion.observe(viewLifecycleOwner, Observer {
            if (BuildConfig.VERSION_CODE < it) {
                MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                    .setTitle("Application Need Update")
                    .setMessage("There is a new update available, please update to continue using GLCC app.")
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show()
            } else {
                registerTabButton.setOnClickListener {
                    findNavController().navigate(R.id.action_nav_login_to_nav_register)
                }
            }
        })

        return root
    }

    override fun onStart() {
        super.onStart()

        loginViewModel.logout()
    }

    override fun onResume() {
        super.onResume()
        loginViewModel.logout()
        loginViewModel.checkVersion()
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        if (!model.user.emailVerified) {
            findNavController().navigate(R.id.action_nav_login_to_nav_validation)
        }else{
            startActivity(Intent(activity, MainActivity::class.java))
        }
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(activity, errorString, Toast.LENGTH_SHORT).show()
    }
}