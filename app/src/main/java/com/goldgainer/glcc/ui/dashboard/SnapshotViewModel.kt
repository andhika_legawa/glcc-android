package com.goldgainer.glcc.ui.dashboard

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.model.Snapshot
import kotlinx.coroutines.launch

class SnapshotViewModel : ViewModel() {

    var selectedNetwork: String = ""

    private val _latestSnapshot = MutableLiveData<Snapshot>()
    val latestSnapshot : LiveData<Snapshot> = _latestSnapshot

    private val _database = FirebaseDataSource()

    private val _snapshotError = MutableLiveData<String>()
    val snapshotError : LiveData<String> = _snapshotError

    fun getLatestSnapshot() {
        viewModelScope.launch {
            try {
                val network = _database.getNetwork(selectedNetwork)?:throw java.lang.Exception("Network not exist!")
                val snapshot = _database.getLatestSnapshot(network.referralId)
                if( snapshot != null ){
                    _latestSnapshot.value = snapshot
                }else{
                    _latestSnapshot.value = Snapshot(
                            owner = network.owner,
                            referralId = network.referralId,
                            type = network.type,
                            number = network.number
                        )
                }
            }catch (e: Exception){
                e.message?.let { Log.d("SNAPSHOT", it) }
                _snapshotError.value = e.message
            }
        }
    }
}