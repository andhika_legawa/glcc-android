package com.goldgainer.glcc.ui.payment

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.model.Transaction
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.xendit.Models.Card
import com.xendit.Models.Token
import com.xendit.Models.XenditError
import com.xendit.TokenCallback
import com.xendit.Xendit
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class PaymentViewModel : ViewModel() {

    private val publicKey = "xnd_public_production_OYuFfL5w0+SrncM4KLMYGj+Xb9SnooMpkX3l+Rxn/WLe+rGpDgBzhg=="
    private val isDevelopmentKey = false

    var context : Context? = null

    private val _creditCardTransaction = MutableLiveData<Transaction?>()
    val creditCardTransaction : LiveData<Transaction?> = _creditCardTransaction

    private val _transactionError = MutableLiveData<String?>()
    val transactionError: LiveData<String?> = _transactionError

    private val _loading = MutableLiveData<Boolean>().apply { value = false }
    val loading: LiveData<Boolean> = _loading

    var selectedNetwork: String = ""
    var nodeValue: Int = 0
    var nodeType: String = "I"
    var nodeQuantity: Int = 0

    var creditCardCharge : Int = 0
    var creditCardNumber : String = ""
    var creditCardExpired : String = ""
    var creditCardSecret : String = ""
    var creditCardName : String = ""

    var xenditToken : Token? = null

    private val _database = FirebaseDataSource()

    fun proceedCreditCardPayment(){
        if (creditCardExpired.length < 6) {
            _transactionError.value = "Please fill all the credit card fields"
            return
        }
        val month = creditCardExpired.substring(0, 2)
        val year = creditCardExpired.substring(2, 6)
        val xendit = Xendit(context, publicKey)
        val card = Card(creditCardNumber, month, year, creditCardSecret)

        _loading.value = true
        xendit.createSingleUseToken(card, creditCardCharge, object: TokenCallback(){
            override fun onSuccess(token: Token?) {
                if (token != null) {
                    Log.d("XENDIT", token.id)
                    xenditToken = token
                    _loading.value = false
                    createTransaction()
                }
            }

            override fun onError(error: XenditError?) {
                _loading.value = false
                _transactionError.value = error?.errorMessage
            }
        })
    }

    private fun createTransaction() {
        if (selectedNetwork == null){
            _transactionError.value = "No network selected."
        }
        if (xenditToken == null){
            _transactionError.value = "No authentication"
        }
        viewModelScope.launch {
            _loading.value = true
            val functions = Firebase.functions(regionOrCustomDomain = "asia-southeast2")
            try {
                val transactionId = functions.getHttpsCallable("createTransaction").call().await().data as String
                val transaction = _database.getCreatedTransaction(transactionId)?: throw java.lang.Exception("failed creating transaction")
                transaction.type = "TPN-X"
                transaction.value = creditCardCharge.toLong()
                val data = mapOf(
                        "network" to selectedNetwork!!,
                        "tokenId" to xenditToken!!.id,
                        "authenticationId" to xenditToken!!.authenticationId,
                        "nodeValue" to nodeValue.toString(),
                        "nodeType" to nodeType,
                        "nodeQuantity" to nodeQuantity.toString()
                )
                transaction.data = data
                transaction.status = "PENDING"
                _database.saveCreatedTransaction(transaction)

                var currency = "IDR"
                if (nodeType == "G") currency = "USD"
                //charge
                val chargeId = functions.getHttpsCallable("chargeXendit").call(
                        hashMapOf(
                                "transactionId" to transactionId,
                                "tokenId" to xenditToken!!.id,
                                "authenticationId" to xenditToken!!.authenticationId,
                                "amount" to creditCardCharge.toLong(),
                                "dev" to isDevelopmentKey,
                                "cvv" to creditCardSecret,
                                "currency" to currency
                        )
                ).await().data as String

                val xenditCharge = _database.getXenditCharge(chargeId)

                if (xenditCharge?.status == "FAILED") {
                    transaction.status = "FAILED"
                    // get failure reason
                    if (xenditCharge.request.containsKey("failure_reason")) {
                        val failureMsg = xenditCharge.request["failure_reason"] as String
                        _transactionError.value = failureMsg
                    }
                }

                if (xenditCharge?.status == "CAPTURED") {
                    transaction.status = "SUCCESS"
                }

                _database.saveCreatedTransaction(transaction)

                functions.getHttpsCallable("processTransaction").call(hashMapOf("transactionId" to transactionId))
                _creditCardTransaction.value = transaction
                _loading.value = false
            }catch(e: Exception){
                _loading.value = false
                _transactionError.value = e.message
            }
        }
    }
}