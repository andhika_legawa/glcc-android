package com.goldgainer.glcc.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R

class ValidationFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_validation, container, false)

        val resend = root.findViewById<Button>(R.id.validation_resend_button)
        val login = root.findViewById<Button>(R.id.validation_login_button)

        loginViewModel = ViewModelProvider(this, LoginViewModelFactory()).get(LoginViewModel::class.java)

        loginViewModel.sendResult.observe(viewLifecycleOwner, Observer {
            val sendResult = it ?: return@Observer

            if (sendResult.success != null){
                Toast.makeText(activity, sendResult.success, Toast.LENGTH_SHORT).show()
            }
            if (sendResult.error != null){
                Toast.makeText(activity, sendResult.error, Toast.LENGTH_SHORT).show()
            }
        })

        resend.setOnClickListener {
            loginViewModel.resendVerification()
        }

        login.setOnClickListener {
            findNavController().navigate(R.id.action_nav_validation_to_nav_login)
        }

        return root
    }
}