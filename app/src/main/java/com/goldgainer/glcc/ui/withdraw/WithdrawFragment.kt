package com.goldgainer.glcc.ui.withdraw

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.afterTextChanged
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class WithdrawFragment: AuthenticatedFragment() {

	private lateinit var withdrawViewModel: WithdrawViewModel
	private lateinit var disabledDialog: AlertDialog

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		withdrawViewModel = ViewModelProvider(this).get(WithdrawViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_withdraw, container, false)
		setUserHeader(root.findViewById(R.id.withdraw_user_header))

		val networkDescription = root.findViewById<TextView>(R.id.network_textview)
		val rewardText = root.findViewById<TextView>(R.id.bonus_textview)
		val availableReward = root.findViewById<TextView>(R.id.quantity_textview)
		val amountEdit = root.findViewById<EditText>(R.id.amount_edit)
		val minDraw = root.findViewById<TextView>(R.id.min_draw_textview)
		val maxDraw = root.findViewById<TextView>(R.id.max_draw_textview)

		val goldSaving = root.findViewById<TextView>(R.id.pgold_textview)
		val coinSaving = root.findViewById<TextView>(R.id.goldcoin_textview)
		val transferFee = root.findViewById<TextView>(R.id.transferfee_textview)
		val adminFee = root.findViewById<TextView>(R.id.adminfee_textview)
		val bonusTaxText = root.findViewById<TextView>(R.id.bonus_tax_textview)
		val tax = root.findViewById<TextView>(R.id.tax_textview)
		val received = root.findViewById<TextView>(R.id.received_textview)

		val vaNumber = root.findViewById<TextView>(R.id.vanumber_textview)
		val bankName = root.findViewById<TextView>(R.id.bankname_textview)
		val bankAccount = root.findViewById<TextView>(R.id.bankaccount_textview)
		val bankNumber = root.findViewById<TextView>(R.id.banknumber_textview)
		val vaTextView = root.findViewById<TextView>(R.id.nova_textview)

		val pinEdit = root.findViewById<EditText>(R.id.pin_edit)

		val sendPIN = root.findViewById<Button>(R.id.sendpin_btn)
		val proceedBtn = root.findViewById<Button>(R.id.proceed_btn)
		val cancelBtn = root.findViewById<Button>(R.id.cancel_btn)

		sendPIN.setOnClickListener {
			withdrawViewModel.sendPIN()
		}

		proceedBtn.isEnabled = false
		proceedBtn.setOnClickListener {
			MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
					.setTitle("Request Withdraw")
					.setMessage("Are you sure you want to withdraw?")
					.setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
						val pin = pinEdit.text.toString()
						withdrawViewModel.submitWithdrawal(pin)
					}
					.setNegativeButton(android.R.string.cancel, null)
					.setCancelable(false)
					.show()
		}

		cancelBtn.setOnClickListener {
			withdrawViewModel.cancelRequest()
		}

		disabledDialog = MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
				.setTitle("Withdraw disabled")
				.setMessage("This feature is temporary disabled. Please check again later.")
				.setPositiveButton(android.R.string.ok) { dialog: DialogInterface, _: Int ->
					findNavController().navigate(R.id.action_global_nav_dashboard)
				}
				.setCancelable(false)
				.create()

		amountEdit.isEnabled = false
		withdrawViewModel.status.observe(viewLifecycleOwner, Observer { status ->
			if (status == "FORBIDDEN") {
				findNavController().navigate(R.id.action_global_nav_wallet_forbidden)
			}

			if (status == "DISABLED") {
				if (!disabledDialog.isShowing) {
					disabledDialog.show()
				}
			}

			if (status == "CANCELED") {
				findNavController().navigate(R.id.action_global_nav_wallet)
			}

			if (status == "SUBMITTED") {
				findNavController().navigate(R.id.action_nav_withdraw_to_nav_withdraw_status)
			}

			if (status == "PENDING") {
				amountEdit.isEnabled = true
				val request = withdrawViewModel.currentRequest?:return@Observer
				networkDescription.text = request.networkCategory
				availableReward.text = request.available.toCurrency("IDR")
				amountEdit.setText(request.amount.toString())
				goldSaving.text = request.savingGoldValue.toCurrency("IDR")
				coinSaving.text = request.savingGoldcoinValue.toCurrency("IDR")
				transferFee.text = request.transferFee.toCurrency("IDR")
				adminFee.text = request.adminFee.toCurrency("IDR")
				tax.text = request.tax.toCurrency("IDR")
				received.text = request.amountReceived.toCurrency("IDR")

				if (request.subtype == "BS") {
					rewardText.text = "R.R"
				}

				if (request.subtype == "BP") {
					rewardText.text = "C.R"
				}

				if (request.subtype == "BR") {
					rewardText.text = "A.R"
				}

				vaNumber.text = request.vanumber
				if (request.vanumber.isNullOrEmpty()) {
					vaNumber.text = "-"
					vaTextView.visibility = View.VISIBLE
				} else {
					vaTextView.visibility = View.GONE
				}
				bankName.text = request.bankName
				bankAccount.text = request.bankAccount
				bankNumber.text = request.bankNumber

				withdrawViewModel.prepareWithdrawal(request.amount)
			}
		})

		withdrawViewModel.checked.observe(viewLifecycleOwner, Observer { checked ->
			if (checked.amountError != null) {
				amountEdit.error = checked.amountError
				proceedBtn.isEnabled = false
			}else{
				amountEdit.error = null
				proceedBtn.isEnabled = true
			}
			minDraw.text = checked.minWithdraw
			maxDraw.text = checked.maxWithdraw
			bonusTaxText.text = "Reward Tax (${checked.taxPercentage})"

			val request = withdrawViewModel.currentRequest?:return@Observer
			if (request.amount <= 0) {
				proceedBtn.isEnabled = false
			}
			networkDescription.text = request.networkCategory
			availableReward.text = request.available.toCurrency("IDR")
			goldSaving.text = request.savingGoldValue.toCurrency("IDR")
			coinSaving.text = request.savingGoldcoinValue.toCurrency("IDR")
			transferFee.text = request.transferFee.toCurrency("IDR")
			adminFee.text = request.adminFee.toCurrency("IDR")
			tax.text = request.tax.toCurrency("IDR")
			received.text = request.amountReceived.toCurrency("IDR")
		})

		withdrawViewModel.error.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		amountEdit.afterTextChanged {
			try {
				val amount = it.toDouble()
				withdrawViewModel.prepareWithdrawal(amount)
			}catch (e:Exception) {
				//
			}
		}

		val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
			override fun handleOnBackPressed() {
				withdrawViewModel.cancelRequest()
			}
		}
		requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback);

		return root
	}

	override fun onResume() {
		super.onResume()
		withdrawViewModel.checkWithdrawStatus()
	}
}