package com.goldgainer.glcc.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.model.NetworkCategory

class NetworkGridAdapter(private val nodeSet : List<NetworkCategory>, private val listener:(NetworkCategory) -> Unit) : RecyclerView.Adapter<NetworkGridAdapter.NodeViewHolder>(){

    class NodeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NodeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.partials_networkgrid_item, parent, false) as View
        return NodeViewHolder(view)
    }

    override fun onBindViewHolder(holder: NodeViewHolder, position: Int) {
        val item = nodeSet[position]
        val textView = holder.itemView.findViewById<TextView>(R.id.node_textview)
        if (!item.locked){
            textView.setBackgroundResource(0)
            textView.text = item.name
            var color = ContextCompat.getColor(textView.context, R.color.glcc_disabled)
            if (item.active) {
                color = ContextCompat.getColor(textView.context, R.color.glcc_orange)
            }
            if(item.filled != null) {
                color = ContextCompat.getColor(textView.context, R.color.glcc_green)
            }
            textView.setBackgroundColor(color)
        }

        holder.itemView.setOnClickListener { listener(item) }
    }

    override fun getItemCount() = nodeSet.size
}