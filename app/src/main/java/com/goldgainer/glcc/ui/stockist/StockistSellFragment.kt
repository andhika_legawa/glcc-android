package com.goldgainer.glcc.ui.stockist

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch

class StockistSellFragment : AuthenticatedFragment() {

    private lateinit var viewModel: StockistViewModel

    private lateinit var networkAdapter : ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_stockist_sell, container, false)
        setUserHeader(root.findViewById(R.id.stockist_user_header))
        viewModel = ViewModelProvider(this).get(StockistViewModel::class.java)

        arguments?.let {
            viewModel.selectedNetworkIndex = it.getInt("selectedNetwork", 0)
        }

        val nodeType = root.findViewById<Spinner>(R.id.edit_node_type)

        val networkSpinner = root.findViewById<Spinner>(R.id.stockist_network_spinner)
        val node1 = root.findViewById<TextView>(R.id.stockist_node_1)
        val node3 = root.findViewById<TextView>(R.id.stockist_node_3)
        val node7 = root.findViewById<TextView>(R.id.stockist_node_7)
        val node15 = root.findViewById<TextView>(R.id.stockist_node_15)

        val amount = root.findViewById<EditText>(R.id.edit_voucher_amount)
        val email = root.findViewById<EditText>(R.id.edit_buyer_email)

        val sendPin = root.findViewById<Button>(R.id.sell_sendpin_btn)
        val pinEdit = root.findViewById<EditText>(R.id.sell_pin_edit)

        val sendBtn = root.findViewById<Button>(R.id.stockist_sell_btn)

        networkAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item)
        networkAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        networkSpinner.adapter = networkAdapter
        networkSpinner.isEnabled = false

        viewModel.networkCategories.observe(viewLifecycleOwner, Observer { categories ->
            if (categories.isNotEmpty()) {
                networkAdapter.clear()
                categories.map { it.description }.forEach { category ->
                    networkAdapter.add(category)
                }
                networkAdapter.notifyDataSetChanged()
                networkSpinner.setSelection(viewModel.selectedNetworkIndex)
                viewModel.loadVoucherStock()
            }
        })

        networkSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                viewModel.selectedNetworkIndex = position
                viewModel.loadVoucherStock()
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        val nodeAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item)
        nodeAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
        nodeType.adapter = nodeAdapter

        viewModel.voucherStock.observe(viewLifecycleOwner, Observer { voucherStock ->
            node1.text = voucherStock.node1.toString()
            node3.text = voucherStock.node3.toString()
            node7.text = voucherStock.node7.toString()
            node15.text = voucherStock.node15.toString()

            nodeAdapter.clear()
            nodeAdapter.add("1 Node")
            nodeAdapter.add("3 Node")
            nodeAdapter.add("7 Node")
            nodeAdapter.add("15 Node")
            nodeAdapter.notifyDataSetChanged()
        })

        viewModel.getActiveCategories()

        viewModel.error.observe(viewLifecycleOwner, Observer { error ->
            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                .setTitle("Info")
                .setMessage(error)
                .setPositiveButton(android.R.string.ok, null)
                .show()
        })

        sendPin.setOnClickListener {
            viewModel.sendPIN()
        }

        sendBtn.setOnClickListener {
            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                .setTitle("Sell Voucher")
                .setMessage("Are you sure the buyer Email Address is correct?")
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    viewLifecycleOwner.lifecycleScope.launch {
                        var type = 0
                        if (nodeType.selectedItemPosition == 0) {
                            type = 1
                        }
                        if (nodeType.selectedItemPosition == 1) {
                            type = 3
                        }
                        if (nodeType.selectedItemPosition == 2) {
                            type = 7
                        }
                        if (nodeType.selectedItemPosition == 3) {
                            type = 15
                        }
                        val amountStr = amount.text.toString()
                        val amountInt = amountStr.toInt()
                        val email = email.text.toString()
                        val pinInput = pinEdit.text.toString()
                        val errorString = viewModel.sellVoucher(type, amountInt, email, pinInput)
                        if (errorString != null) {
                            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                                .setTitle("Error")
                                .setMessage(errorString)
                                .setPositiveButton(android.R.string.ok, null)
                                .show()
                        } else {
                            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                                .setTitle("Info")
                                .setMessage("Your voucher has been sent to Buyer Email Address.")
                                .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
                                    findNavController().popBackStack()
                                }
                                .show()
                        }
                    }
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        return root
    }
}