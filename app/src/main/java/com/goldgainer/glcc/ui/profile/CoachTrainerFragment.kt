package com.goldgainer.glcc.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.button.MaterialButton

class CoachTrainerFragment : AuthenticatedFragment() {

    private lateinit var viewModel: ProfileViewModel

    private lateinit var coachAdapter: CoachTrainerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_coach, container, false)
        setUserHeader(root.findViewById(R.id.profile_user_header))

        val profileTab = root.findViewById<MaterialButton>(R.id.profile_tab)
        val coachList = root.findViewById<ListView>(R.id.coach_trainer_list)

        loggedInUser?.let {
            viewModel.currentOwner = it.email!!
        }

        coachAdapter = CoachTrainerAdapter(requireContext(), arrayListOf())
        coachList.adapter = coachAdapter
        coachList.emptyView = root.findViewById(R.id.empty_textview)

        viewModel.coachTrainers.observe(viewLifecycleOwner, Observer { trainers ->
            coachAdapter.clear()
            if (trainers.isNotEmpty()) {
                trainers.forEach {
                    coachAdapter.add(it)
                }
            }
            coachAdapter.notifyDataSetChanged()
        })

        profileTab.setOnClickListener {
            findNavController().popBackStack()
        }

        viewModel.loadCoachTrainer()

        return root
    }
}