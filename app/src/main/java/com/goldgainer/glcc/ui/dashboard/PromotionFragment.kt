package com.goldgainer.glcc.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.VideoView
import androidx.fragment.app.DialogFragment
import com.goldgainer.glcc.R

class PromotionFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_promotion, container, false)
        val promotionVideo = root.findViewById<VideoView>(R.id.promotion_video)
        val path = "android.resource://" + context?.packageName + "/" + R.raw.glcc_reward_video
        promotionVideo.setVideoPath(path)
        promotionVideo.setOnPreparedListener {
            it.isLooping = true
        }
        promotionVideo.start()

        val closeBtn = root.findViewById<ImageButton>(R.id.close_btn)
        closeBtn.setOnClickListener {
            dismiss()
        }
        return root
    }
}