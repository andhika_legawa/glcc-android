package com.goldgainer.glcc.ui.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.sapereaude.maskedEditText.MaskedEditText
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.afterTextChanged
import com.goldgainer.glcc.data.pluralize
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment

class PaymentFragment : AuthenticatedFragment() {

    private lateinit var paymentViewModel: PaymentViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        paymentViewModel = ViewModelProvider(this).get(PaymentViewModel::class.java)
        paymentViewModel.context = context
        val root = inflater.inflate(R.layout.fragment_payment, container, false)
        setUserHeader(root.findViewById(R.id.payment_user_header))

        val cancelButton = root.findViewById<Button>(R.id.pay_cancel_button)
        cancelButton.setOnClickListener {
            findNavController().popBackStack()
        }

        val region = root.findViewById<TextView>(R.id.pay_region_textview)
        val nodeQty = root.findViewById<TextView>(R.id.pay_totalnodes_textview)
        val valueToPay = root.findViewById<TextView>(R.id.pay_totalvalue_textview)
        val feeToPay = root.findViewById<TextView>(R.id.pay_ccfee_textview)
        val subToPay = root.findViewById<TextView>(R.id.pay_subtotal_textview)
        val taxToPay = root.findViewById<TextView>(R.id.pay_tax_textview)
        val totalToPay = root.findViewById<TextView>(R.id.pay_total_textview)

        var totalToCharge = 0

        arguments?.let{
            val network = it.getString("network", "")
            val value = it.getInt("value", 0)
            val quantity = it.getInt("quantity", 0)
            val type = it.getString("type", "I")

            paymentViewModel.selectedNetwork = network
            paymentViewModel.nodeValue = value
            paymentViewModel.nodeQuantity = quantity
            paymentViewModel.nodeType = type

            var currency = "IDR"
            var selectedCategory = "INDONESIA NETWORK"
            if (type == "G"){
                selectedCategory = "GLOBAL NETWORK"
                currency = "USD"
            }
            region.text = arguments?.getString("selectedCategory", selectedCategory)

            nodeQty.text = "$quantity Node".pluralize(quantity!!)

            val nodeValue = quantity * value

            valueToPay.text = nodeValue.toDouble().toCurrency(currency)

            var duaribu = 2000
            if (type == "G") duaribu = 0

            val convenienceFee = ( 0.029 * nodeValue ) + duaribu

            feeToPay.text = convenienceFee.toCurrency(currency)

            totalToCharge = nodeValue + convenienceFee.toInt()

            subToPay.text = totalToCharge.toDouble().toCurrency(currency)

            val tax = 0.1 * totalToCharge

            taxToPay.text = tax.toCurrency(currency)

            totalToCharge += tax.toInt()

            totalToPay.text = totalToCharge.toDouble().toCurrency(currency)

            paymentViewModel.creditCardCharge = totalToCharge
        }

        val ccNumber = root.findViewById<MaskedEditText>(R.id.pay_ccnumber_edittext)
        val ccExpired = root.findViewById<MaskedEditText>(R.id.pay_expireddate_edittext)
        val ccSecret = root.findViewById<MaskedEditText>(R.id.pay_cvv_edittext)
        val ccName = root.findViewById<EditText>(R.id.pay_nameoncard_edittext)

        ccNumber.afterTextChanged {
            paymentViewModel.creditCardNumber = it
        }
        ccExpired.afterTextChanged {
            paymentViewModel.creditCardExpired = it
        }
        ccSecret.afterTextChanged {
            paymentViewModel.creditCardSecret = it
        }
        ccName.afterTextChanged {
            paymentViewModel.creditCardName = it
        }

        val proceedButton = root.findViewById<Button>(R.id.pay_proceed_button)
        val loading = root.findViewById<ProgressBar>(R.id.pay_loading)

        proceedButton.setOnClickListener {
            paymentViewModel.proceedCreditCardPayment()
        }

        paymentViewModel.creditCardTransaction.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                val bundle = bundleOf("transactionId" to it.id,
                        "status" to it.status
                )
                findNavController().navigate(R.id.action_nav_payment_to_nav_status, bundle)
            }
        })

        paymentViewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                proceedButton.isEnabled = false
                loading.visibility = View.VISIBLE
            } else {
                proceedButton.isEnabled = true
                loading.visibility = View.INVISIBLE
            }
        })

        paymentViewModel.transactionError.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })

        return root
    }

}