package com.goldgainer.glcc.ui.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.pluralize
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment

class StatusFragment : AuthenticatedFragment() {

    private lateinit var statusViewModel: StatusViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        statusViewModel = ViewModelProvider(this).get(StatusViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_trxstatus, container, false)
        setUserHeader(root.findViewById(R.id.status_user_header))

        val statusText = arguments?.getString("status", "STATUS")
        statusViewModel.transactionId = arguments?.getString("transactionId")
        val descriptionText = arguments?.getString("description", "")
        val detailsText = arguments?.getString("details", "")
        val details2Text = arguments?.getString("details2", "")
        val okText = arguments?.getString("ok", "DASHBOARD")

        val status = root.findViewById<TextView>(R.id.status_textview)
        status.text = statusText

        val description = root.findViewById<TextView>(R.id.status_description_textview)
        description.text = descriptionText

        val details = root.findViewById<TextView>(R.id.status_details_textview)
        details.text = detailsText

        val details2 = root.findViewById<TextView>(R.id.status_details_textview2)
        details2.text = details2Text

        val ok = root.findViewById<Button>(R.id.status_ok_button)
        ok.text = okText

        ok.setOnClickListener {
            findNavController().navigate(R.id.action_nav_status_to_nav_dashboard)
        }

        statusViewModel.transaction.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            description.text = "Thank you for your transaction. Your Transaction ID : ${it.id}. You just bought :"
            val dataMap = it.data
            if (it.type == "TPN-X" || it.type == "TPN-V") {
                details.text = "NETWORK INDONESIA"
                var currency = "IDR"
                dataMap["nodeType"]?.let { type ->
                    if (type == "G") {
                        details.text = "GLOBAL NETWORK"
                        currency = "USD"
                    }
                }
                val nodes = dataMap["nodeQuantity"]?:"0"
                val value = dataMap["nodeValue"]?:"0"
                val total = nodes.toDouble() * value.toDouble()
                details2.text = total.toCurrency(currency) + " - " + nodes + " Node".pluralize(nodes.toInt())
            }
        })

        statusViewModel.getTransaction()

        return root
    }

}