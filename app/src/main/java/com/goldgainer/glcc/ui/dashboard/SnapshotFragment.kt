package com.goldgainer.glcc.ui.dashboard

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.MainActivity
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment
import kotlin.math.min

class SnapshotFragment : AuthenticatedFragment() {

    private lateinit var snapshotViewModel: SnapshotViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        snapshotViewModel = ViewModelProvider(this).get(SnapshotViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_snapshot, container, false)
        setUserHeader(root.findViewById(R.id.snapshot_user_header))

        arguments?.let {
            snapshotViewModel.selectedNetwork = it.getString("selectedNetwork", "")
        }

        val createNode = root.findViewById<Button>(R.id.snapshot_add_button)
        createNode.setOnClickListener {
            val bundle = arguments
            findNavController().navigate(R.id.action_nav_snapshot_to_nav_nodes_create, bundle)
        }

        val referralId = root.findViewById<TextView>(R.id.snapshot_referralid_textview)
        referralId.text = snapshotViewModel.selectedNetwork
        val copy = root.findViewById<Button>(R.id.snapshot_copy_button)

        copy.setOnClickListener {
            Toast.makeText(context, "Referral Id Copied to clipboard!", Toast.LENGTH_LONG).show()
            val mainActivity = activity as MainActivity
            val clipboard = mainActivity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Referral Id", referralId.text)
            clipboard.setPrimaryClip(clip)
        }

        val region = root.findViewById<TextView>(R.id.snapshot_region_textview)
        val networkNumber = root.findViewById<TextView>(R.id.snapshot_networknumber_textview)
        val totalAsset = root.findViewById<TextView>(R.id.snapshot_total_textview)
        val nodes = root.findViewById<TextView>(R.id.snapshot_nodes_textview)
        val nodeLimit = root.findViewById<TextView>(R.id.snapshot_limitbonus)

        val platinumMB = root.findViewById<TextView>(R.id.platinum_mb)
        val platinumND = root.findViewById<TextView>(R.id.platinum_nd)
        val platinumBS = root.findViewById<TextView>(R.id.platinum_bs)
        val platinumBP = root.findViewById<TextView>(R.id.platinum_bp)
        val platinumBR = root.findViewById<TextView>(R.id.platinum_br)
        val platinumBT = root.findViewById<TextView>(R.id.platinum_btotal)

        val goldMB = root.findViewById<TextView>(R.id.gold_mb)
        val goldND = root.findViewById<TextView>(R.id.gold_nd)
        val goldBS = root.findViewById<TextView>(R.id.gold_bs)
        val goldBP = root.findViewById<TextView>(R.id.gold_bp)
        val goldBR = root.findViewById<TextView>(R.id.gold_br)
        val goldBT = root.findViewById<TextView>(R.id.gold_btotal)

        val pairText = root.findViewById<TextView>(R.id.pair_string)

        val pairBtn = root.findViewById<Button>(R.id.pair_btn)

        var networkType = "I"
        var networkNum = 1
        snapshotViewModel.latestSnapshot.observe(viewLifecycleOwner, Observer {
            var currency = "IDR"
            if (it.type == "I") {
                region.text = "INDONESIA NETWORK"
            }
            if (it.type == "G") {
                region.text = "GLOBAL NETWORK"
                currency = "USD"
            }
            networkNumber.text = "NETWORK: N${it.number}"
            networkNum = it.number
            totalAsset.text = it.totalAsset.toDouble().toCurrency(currency)
            nodes.text = it.nodes.toString()
            val maxNode = it.nodes * 16
            nodeLimit.text = "$maxNode:$maxNode"

            platinumMB.text = it.platinumNetworks.toString()
            platinumND.text = it.platinumNodes.toString()
            platinumBS.text = it.platinumBSponsor.toDouble().toCurrency(currency)
            platinumBP.text = it.platinumBPairing.toDouble().toCurrency(currency)
            platinumBR.text = it.platinumBRollup.toDouble().toCurrency(currency)
            val platinumTotal = it.platinumBSponsor + it.platinumBPairing + it.platinumBRollup
            platinumBT.text = platinumTotal.toDouble().toCurrency(currency)

            goldMB.text = it.goldNetworks.toString()
            goldND.text = it.goldNodes.toString()
            goldBS.text = it.goldBSponsor.toDouble().toCurrency(currency)
            goldBP.text = it.goldBPairing.toDouble().toCurrency(currency)
            goldBR.text = it.goldBRollup.toDouble().toCurrency(currency)
            val goldTotal = it.goldBSponsor + it.goldBPairing + it.goldBRollup
            goldBT.text = goldTotal.toDouble().toCurrency(currency)

            val div = min(it.platinumNodes, it.goldNodes)
            pairText.text = "$div:$div"
            if (it.type != "I") {
                pairBtn.visibility = View.GONE
            }
        })


        pairBtn.setOnClickListener {
            val bundle = bundleOf(networkType to "type", networkNum.toString() to "number")
            findNavController().navigate(R.id.action_global_nav_reward_journey, bundle)
        }

        return root
    }

    override fun onStart() {
        super.onStart()
        snapshotViewModel.getLatestSnapshot()
    }
}