package com.goldgainer.glcc.ui

import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.goldgainer.glcc.MainActivity
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.LoggedInUser
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

open class AuthenticatedFragment : Fragment() {

	protected lateinit var mainActivity : MainActivity
	protected var loggedInUser : LoggedInUser? = null

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		mainActivity = activity as MainActivity
		loggedInUser= mainActivity.getCurrentUser()
		return super.onCreateView(inflater, container, savedInstanceState)
	}

	protected fun setUserHeader(userHeader: ConstraintLayout) {
		val userTextView = userHeader.findViewById<TextView>(R.id.uh_username_textview)
		val dateTextView = userHeader.findViewById<TextView>(R.id.uh_date_textview)
		loggedInUser?.let {
			val userData = Helper.getLocalUser()
			userTextView.text = userData.name + " "
			userTextView.setTypeface(null, Typeface.ITALIC)
			if (userData.verified) {
				userTextView.setCompoundDrawablesWithIntrinsicBounds (0, 0, R.drawable.check, 0)
			}
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			dateTextView.text = LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM))
		}else{
			val sdf = SimpleDateFormat("MMM dd, yyyy")
			dateTextView.text = sdf.format(Date()).toString()
		}
		dateTextView.setTypeface(null, Typeface.ITALIC)
	}
}