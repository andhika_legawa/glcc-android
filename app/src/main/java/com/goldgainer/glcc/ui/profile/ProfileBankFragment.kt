package com.goldgainer.glcc.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment

class ProfileBankFragment : AuthenticatedFragment() {

	private lateinit var profileViewModel: ProfileViewModel

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_profile_bank, container, false)
		setUserHeader(root.findViewById(R.id.profile_user_header))

		loggedInUser?.let {
			profileViewModel.currentOwner = it.email!!
		}

		val bankname = root.findViewById<Spinner>(R.id.edit_bank_name)
		val bankaccount = root.findViewById<EditText>(R.id.edit_bank_account_name)
		val banknumber = root.findViewById<EditText>(R.id.edit_bank_account_number)

		val confirm = root.findViewById<TextView>(R.id.bank_text_confirm)
		val save = root.findViewById<Button>(R.id.btn_save_bank)

		ArrayAdapter.createFromResource(requireContext(), R.array.bank_array, android.R.layout.simple_spinner_item).also {
			it.setDropDownViewResource(R.layout.partials_dropdown_item)
			bankname.adapter = it
		}

		profileViewModel.currentProfile.observe(viewLifecycleOwner, Observer { profile ->
			if (profileViewModel.dBankArray.isNotEmpty()) {
				ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, profileViewModel.dBankArray).also {
					it.setDropDownViewResource(R.layout.partials_dropdown_item)
					bankname.adapter = it
				}
			}
			profile.bankName?.let {
				val bankArr = context?.resources?.getStringArray(R.array.bank_array)
				bankArr?.let { arr ->
					bankname.setSelection(arr.indexOf(it))
				}
			}
			profile.bankAccount?.let {
				bankaccount.setText(it)
			}
			profile.bankNumber?.let {
				banknumber.setText(it)
			}
			save.isEnabled = true
			if (profile.status == "PA" || profile.status == "AV") {
				bankname.isEnabled = false
				bankaccount.isEnabled = false
				banknumber.isEnabled = false
				save.isEnabled = false
				confirm.visibility = View.INVISIBLE
				save.visibility = View.INVISIBLE
			}
		})

		profileViewModel.profileError.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		profileViewModel.getProfile()

		profileViewModel.profileSaved.observe(viewLifecycleOwner, Observer { success ->
			if (success) {
				findNavController().popBackStack()
			}
		})

		save.setOnClickListener {
			profileViewModel.saveBank(
					bankname = bankname.selectedItem.toString(),
					bankaccount = bankaccount.text.toString(),
					banknumber = banknumber.text.toString())
		}


		return root
	}

}