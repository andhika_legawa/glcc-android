package com.goldgainer.glcc.ui.withdraw

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.model.*
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.data.toDigitalCurrency
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class WithdrawViewModel() : ViewModel() {

    private val _isBusy = MutableLiveData<Boolean>()
    val isBusy: LiveData<Boolean> = _isBusy

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _status = MutableLiveData<String>()
    val status: LiveData<String> = _status
    private val _database = FirebaseDataSource()
    private val _wallet = MutableLiveData<Wallet>()
    val currentWallet: LiveData<Wallet> = _wallet
    var currentRequest: Withdraw? = null
    private val localUser = Helper.getLocalUser()

    var userNetworks:List<Network> = listOf()
    private val _networkCategory = MutableLiveData<List<NetworkCategory>>()
    val networkCategories: LiveData<List<NetworkCategory>> = _networkCategory
    private var selectedNetwork:Network? = null

    private val _checked = MutableLiveData<WithdrawFormState>()
    val checked: LiveData<WithdrawFormState> = _checked

    private val _transactions = MutableLiveData<List<TransactionHistory>>()
    val transactions: LiveData<List<TransactionHistory>> = _transactions

    private var sendPINDate: Date? = null

    private val _glcRate = MutableLiveData<GLCRate>()
    val glcRate: LiveData<GLCRate> = _glcRate

    private val _goldRate = MutableLiveData<GoldRate>()
    val goldRate: LiveData<GoldRate> = _goldRate

    fun reset() {
        _error.value = null
        _isBusy.value = false
    }

    fun loadNetworks() {
        viewModelScope.launch {
            try {
                var iCategories = _database.getNetworkCategories("I")
                localUser.country?.let { country ->
                    if (country != "ID") {
                        iCategories = listOf()
                    }
                }
                localUser.email?.let {
                    userNetworks = _database.getUserNetworks(it)
                }
                val gCategories = _database.getNetworkCategories("G")
                var activeCategories = iCategories.filter { it.active && !it.locked } + gCategories.filter {  it.active && !it.locked }
                _networkCategory.value = activeCategories.sortedBy { it.number }
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    private suspend fun doCheckWithdrawStatus(){
        val owner = localUser.email ?: throw Exception("Null user")
        val helper = Helper()

        if (_database.isBlacklisted(owner)) {
            _status.value = "FORBIDDEN"
        }

        if (!localUser.verified) {
            _status.value = "FORBIDDEN"
        }

        val enabled = helper.getGlobalSettings("withdraw_enabled", 1) as Int
        if (enabled == 1) {
            _status.value = "ENABLED"
        }
        if (enabled == 0) {
            _status.value = "DISABLED"
        }

        currentRequest = _database.getWithdrawRequest(owner)

        val request = currentRequest
        if (request != null) {
            _status.value = request.status
        }
    }

    fun checkWithdrawStatus(){
        viewModelScope.launch {
            try {
                doCheckWithdrawStatus()
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    fun loadNetworkSnapshot(networkPos: Int) {
        viewModelScope.launch {
            try {
                val categories = _networkCategory.value ?: throw Exception("Network Categories Issue")
                val selectedCategory = categories[networkPos]
                val network = userNetworks.find { it.type == selectedCategory.type && it.number == selectedCategory.number } ?: throw Exception("No matching user network")
                val snapshot = _database.getLatestSnapshot(network.referralId)
                val owner = localUser.email ?: throw Error("Null user")
                val withdraws = _database.getWithdraws(owner, network.referralId).filter { it.status == "SUCCESS" }
                val wBonusSponsor = withdraws.filter { it.subtype == "BS" }.map { it.amount }.sum()
                val wBonusPairing = withdraws.filter { it.subtype == "BP" }.map { it.amount }.sum()
                val wBonusRollup = withdraws.filter { it.subtype == "BR" }.map { it.amount }.sum()
                val wPGold = withdraws.filter { it.subtype == "PG" }.map { it.amount }.sum()
                val wGoldcoin = withdraws.filter { it.subtype == "GC" }.map { it.amount }.sum()

                val withdrawsPendingTax = _database.getWithdrawsPendingTax(owner, network.referralId).filter { it.status == "SUCCESS" }
                val tBonusSponsor = withdrawsPendingTax.filter { it.subtype == "BS" }.map { it.tax }.sum()
                val tBonusPairing = withdrawsPendingTax.filter { it.subtype == "BP" }.map { it.tax }.sum()
                val tBonusRollup = withdrawsPendingTax.filter { it.subtype == "BR" }.map { it.tax }.sum()

                val sPGold = withdraws.filter { it.subtype != "PG" }.map { it.savingGoldValue }.sum()
                val sGoldcoin = withdraws.filter { it.subtype != "GC" }.map { it.savingGoldcoinValue }.sum()
                snapshot?.let {
                    _wallet.value = Wallet(
                        referralId = it.referralId,
                        bonusSponsor = it.platinumBSponsor + it.goldBSponsor - wBonusSponsor - tBonusSponsor,
                        tBonusSponsor = tBonusSponsor - (it.platinumBSponsor + it.goldBSponsor - wBonusSponsor),
                        bonusPairing = it.platinumBPairing + it.goldBPairing - wBonusPairing - tBonusPairing,
                        tBonusPairing = tBonusPairing - (it.platinumBPairing + it.goldBPairing - wBonusPairing),
                        bonusRollup = it.platinumBRollup + it.goldBRollup - wBonusRollup - tBonusRollup,
                        tBonusRollup = tBonusRollup - (it.platinumBRollup + it.goldBRollup - wBonusRollup),
                        savingGold = sPGold - wPGold,
                        savingGoldcoin = sGoldcoin - wGoldcoin
                    )
                    selectedNetwork = network
                }
            }catch(e:Exception) {
                _wallet.value = Wallet(
                        referralId = "",
                        bonusSponsor = 0.0,
                        bonusPairing = 0.0,
                        bonusRollup = 0.0,
                        savingGold = 0.0,
                        savingGoldcoin = 0.0
                )
                selectedNetwork = null
                _error.value = e.message
            }
        }
    }

    fun makeRequest(type: String, subtype: String, available:Double){
        viewModelScope.launch {
            try {
                val owner = localUser.email?:throw Exception("Null email")
                val network = selectedNetwork?:throw Exception("No network selected")
                val profile = _database.getProfile(owner)?:throw Exception("Profile not available")
                val withdraw = Withdraw(
                        owner = owner, trxId = generateTrxId(),
                        networkId = network.referralId, networkNumber = network.number, networkType = network.type, networkCategory = network.category,
                        type = type, subtype = subtype,
                        bankName = profile.bankName, bankAccount = profile.bankAccount, bankNumber = profile.bankNumber, status = "PENDING",
                        available = available,
                        vanumber = profile.virtualAccount,
                        glcWallet = profile.glcWallet,
                        usdtWallet = profile.usdtWallet
                )
                if (_database.saveWithdrawRequest(owner, withdraw)){
                    currentRequest = withdraw
                    _status.value = "PENDING"
                }else{
                    throw Exception("Failed to make withdraw request")
                }
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    fun cancelRequest(){
        viewModelScope.launch {
            try {
                val owner = localUser.email?:throw Exception("Null email")
                _database.deleteWithdrawRequest(owner)
                _status.value = "CANCELED"
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    fun prepareWithdrawal(amount:Double) {
        viewModelScope.launch {
            try {
                currentRequest?.amount = amount
                validateRequest()
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    fun prepareWithdrawal(amount:Double, glcReceived:Double) {
        viewModelScope.launch {
            try {
                currentRequest?.amount = amount
                currentRequest?.savingGoldcoin = glcReceived
                validateRequest()
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    fun prepareGoldWithdrawal(goldAmount:Double){
        viewModelScope.launch {
            try {
                val goldRate = _goldRate.value?:throw Exception("Gold rate not available")
                currentRequest?.amount = goldAmount * goldRate.goldprice
                currentRequest?.savingGold = goldAmount
                validateRequest()
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    fun sendPIN() {
        viewModelScope.launch {
            try {
                val lastSend = sendPINDate
                if (lastSend != null ) {
                    val diff = Date().time - lastSend.time
                    val seconds =  diff / 3600
                    val minutes = seconds / 60
                    if (minutes < 1) throw Exception("Please try again in a minute.")
                }

                val owner = localUser.email?:throw Exception("Null email")
                val functions = Firebase.functions(regionOrCustomDomain = "asia-southeast2")
                val pin = functions.getHttpsCallable("sendPIN").call(hashMapOf("email" to owner)).await().data as String
                currentRequest?.pin = pin
                sendPINDate = Date()
                _error.value = "PIN sent to $owner"
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    fun submitWithdrawal(pin:String) {
        viewModelScope.launch {
            try {
                val status = status.value?:throw Exception("Status check failed!")
                if (status == "PENDING") {
                    // continue
                    val isValid = validateRequest()
                    if (!isValid) {
                        throw Exception("Invalid Withdraw")
                    }
                    // check PIN
                    if (pin.isBlank()) {
                        throw Exception("Please input PIN")
                    }
                    val owner = localUser.email?:throw Exception("Null email")
                    val request = currentRequest?:throw Exception("Withdraw request failed!")
                    if (request.pin != pin) throw Exception("Wrong PIN!")
                    request.status = "SUBMITTED"
                    if (_database.saveWithdrawRequest(owner, request)) {
                        _status.value = "SUBMITTED"
                    }else{
                        throw Exception("Withdraw submission failed!")
                    }
                }
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

    private fun generateTrxId(): String {
        val sdf = SimpleDateFormat("MMyy")
        val trxIdBuilder = StringBuilder()
        trxIdBuilder.append("WB")
                .append("-")
                .append(sdf.format(Date()))
                .append(randomString(10))
        return trxIdBuilder.toString()
    }

    private fun randomString(strLength:Int): String {
        val source = ('0'..'9')
        return (1..strLength)
                .map { source.random() }
                .joinToString("")
    }

    private suspend fun validateRequest() : Boolean {
        val request = currentRequest?:throw Exception("Withdraw request failed!")

        val helper = Helper()
        val minDraw = helper.getGlobalSettings("min_withdraw", 50000) as Int
        val maxDraw = helper.getGlobalSettings("max_withdraw", 10000000) as Int
        val transferFee = helper.getGlobalSettings("transfer_fee", 5000) as Int
        val adminFee = helper.getGlobalSettings("admin_fee", 1000) as Int

        val goldTransferFee = helper.getGlobalSettings("gold_transfer_fee", 5000) as Int
        val goldPackingFee = helper.getGlobalSettings("gold_packing_fee", 5000) as Int
        val goldAdminFee = helper.getGlobalSettings("gold_admin_fee", 1000) as Int

        val tax1 = helper.getGlobalSettings("tax_one", 5) as Int
        val tax2 = helper.getGlobalSettings("tax_two", 15) as Int
        val tax3 = helper.getGlobalSettings("tax_three", 25) as Int
        val tax4 = helper.getGlobalSettings("tax_four", 30) as Int

        var amountError:String? = null
        var taxPercentage = "$tax1%"
        if (request.amount > 0) {
            if (request.amount > request.available) {
                amountError = "Available reward is " + request.available.toCurrency("IDR")
            }
            if (request.amount < minDraw) {
                amountError = "Minimum withdraw is " + minDraw.toCurrency("IDR")
            }
            if (request.amount > maxDraw) {
                amountError = "Maximum withdraw is " + maxDraw.toCurrency("IDR")
            }

            request.transferFee = transferFee.toDouble()
            request.adminFee = adminFee.toDouble()

            if (request.amount <= 50000000) {
                request.tax = request.amount * (tax1.toDouble()/100)
                taxPercentage = "$tax1%"
            }

            if (request.amount > 50000000 && request.amount <= 250000000) {
                request.tax = request.amount * (tax2.toDouble()/100)
                taxPercentage = "$tax2%"
            }

            if (request.amount > 250000000 && request.amount <= 500000000) {
                request.tax = request.amount * (tax3.toDouble()/100)
                taxPercentage = "$tax3%"
            }

            if (request.amount > 500000000) {
                request.tax = request.amount * (tax4/100)
                taxPercentage = "$tax4%"
            }

            if (request.subtype == "BS" || request.subtype == "BP" || request.subtype == "BR") {
                request.savingGoldValue = request.amount * 0.1
                request.savingGoldcoinValue = request.amount * 0.1
                request.amountReceived = request.amount - request.savingGoldValue - request.savingGoldcoinValue - request.transferFee - request.adminFee - request.tax
            }

            if (request.subtype == "GC") {
                request.amountReceived = request.amount - request.transferFee - request.adminFee
                request.savingGoldcoinValue = request.amountReceived
            }

            if (request.subtype == "PG") {
                request.transferFee = goldTransferFee.toDouble()
                request.packingFee = goldPackingFee.toDouble()
                request.adminFee = goldAdminFee.toDouble()
                request.amountReceived = request.amount
                request.amount = request.amount + request.transferFee + request.packingFee + request.adminFee
                request.savingGoldcoinValue = request.amountReceived
            }

            currentRequest = request

        }

        if (request.amountReceived <= 0) {
            amountError = "Invalid amount received"
        }

        _checked.value = WithdrawFormState(amountError = amountError, minWithdraw = minDraw.toCurrency("IDR"), maxWithdraw = maxDraw.toCurrency("IDR"), taxPercentage = taxPercentage)

        return amountError == null
    }

    fun searchTransaction(networkPos: Int, start: Date, end: Date) {
        viewModelScope.launch {
            _isBusy.value = true
            try {
                val categories = _networkCategory.value?: throw Exception("Network Categories Issue")
                val selectedCategory = categories[networkPos]
                val email = localUser.email?: throw Exception("User email issue")
                val createdTransactionHistories = _database.getCreatedTransactionList(email, start, end)
                val transactionHistories = _database.getTransactionList(email, start, end)
                val withdrawHistories = _database.getWithdrawList(email, start, end)
                val stockistHistories = _database.getStockistTransaction(email, start, end)
                val nCTxList = createdTransactionHistories.filter {
                    it.data.containsKey("nodeType") && it.data["nodeType"] == selectedCategory.type &&
                            it.data.containsKey("networkNumber") && it.data["networkNumber"] == selectedCategory.number.toString()
                }
                val nTxList = transactionHistories.filter {
                    it.data.containsKey("nodeType") && it.data["nodeType"] == selectedCategory.type &&
                            it.data.containsKey("networkNumber") && it.data["networkNumber"] == selectedCategory.number.toString()
                }
                val nWdList = withdrawHistories.filter { it.networkType == selectedCategory.type && it.networkNumber == selectedCategory.number && it.status == "SUCCESS"}
                val nStList = stockistHistories.filter { it.networkType == selectedCategory.type && it.networkNumber == selectedCategory.number && it.type == 1 }
                val histories = arrayListOf<TransactionHistory>()
                nCTxList.forEach { trx ->
                    val code = trx.data["code"]?:""
                    var prefix = "GV"
                    if (code.startsWith("GVID-SS") || code.startsWith("GVUS-SS")) {
                        prefix = "SV"
                    }
                    val node = trx.data["nodeQuantity"]?:0

                    histories.add(
                        TransactionHistory(
                            date = trx.createdDate,
                            trx = "Redeemed " + prefix,
                            rcv =  node.toString() + " Node",
                            id = trx.id
                        )
                    )
                }
                nTxList.forEach { trx ->
                    val existing = histories.filter { it.id == trx.id }
                    if (existing.isNotEmpty()) { return@forEach }
                    val code = trx.data["code"]?:""
                    var prefix = "GV"
                    if (code.startsWith("GVID-SS") || code.startsWith("GVUS-SS")) {
                        prefix = "SV"
                    }
                    val node = trx.data["nodeQuantity"]?:0

                    histories.add(
                        TransactionHistory(
                            date = trx.createdDate,
                            trx = "Redeemed $prefix",
                            rcv = "$node Node",
                            id = trx.id
                        )
                    )
                }
                nWdList.forEach { wd ->
                    var type = wd.subtype
                    if (wd.subtype == "BS") { type = "RR" }
                    if (wd.subtype == "BP") { type = "CR" }
                    if (wd.subtype == "BR") { type = "AR" }
                    var amtReceived = wd.amountReceived.toDigitalCurrency("")
                    if (wd.subtype == "PG") {
                        amtReceived = wd.savingGold.toDigitalCurrency(" Gr")
                    }
                    histories.add(
                        TransactionHistory(
                            date = wd.requestDate,
                            trx = "$type Withdraw",
                            rcv = amtReceived
                        )
                    )
                }
                nStList.forEach { st ->
                    histories.add(
                        TransactionHistory(
                            date = st.date!!,
                            trx = "Sold SV",
                            rcv = "${st.nodeType} Node / ${st.amount}"
                        )
                    )
                }
                _isBusy.value = false
                _transactions.value = histories.sortedByDescending { it.date }
            }catch (e:Exception) {
                _error.value = e.message
                _isBusy.value = false
                _transactions.value = listOf()
            }
        }
    }

    suspend fun loadSavingSettings() : SavingSetting {
        val goldcoinEnabled = Helper().getGlobalSettings("goldcoin_enabled", 0) as Int
        val pgoldEnabled = Helper().getGlobalSettings("pgold_enabled", 0) as Int
        return SavingSetting(
            goldcoin = goldcoinEnabled == 1,
            pgold = pgoldEnabled == 1
        )
    }

    fun updateGLCRate() {
        viewModelScope.launch {
            val helper = Helper()
            val glcusd = helper.getGlobalSettings("glc_usdt", "0.0") as String
            val usdidr = helper.getGlobalSettings("usd_idr", "0.0") as String
            val slippage = helper.getGlobalSettings("slippage", "0.05") as String
            _glcRate.value = GLCRate(
                glcusd = glcusd.toDouble(),
                usdidr = usdidr.toDouble(),
                slippage = slippage.toDouble()
            )
        }
    }

    fun updateGoldRate() {
        viewModelScope.launch {
            try {
                val withdrawRequest = currentRequest?:throw Error("No withdraw request")
                val helper = Helper()
                val goldPrice = helper.getGlobalSettings("gold_idr", "0.0") as String
                val goldPriceDbl = goldPrice.toDouble()
                val goldTransferFee = helper.getGlobalSettings("gold_transfer_fee", 5000) as Int
                val goldPackingFee = helper.getGlobalSettings("gold_packing_fee", 5000) as Int
                val goldAdminFee = helper.getGlobalSettings("gold_admin_fee", 1000) as Int
                val availableIDR = withdrawRequest.available - goldTransferFee - goldPackingFee - goldAdminFee
                val availableGrams = arrayListOf<Double>()
                if (availableIDR >= (0.5 * goldPriceDbl) ) {
                    availableGrams.add(0.5)
                }
                if (availableIDR >= (1 * goldPriceDbl) ) {
                    availableGrams.add(1.0)
                }
                if (availableIDR >= (2 * goldPriceDbl) ) {
                    availableGrams.add(2.0)
                }
                if (availableIDR >= (3 * goldPriceDbl) ) {
                    availableGrams.add(3.0)
                }
                _goldRate.value = GoldRate(
                    goldprice = goldPriceDbl,
                    availableIDR = availableIDR,
                    availableGrams = availableGrams
                )
            }catch(e:Exception) {
                _error.value = e.message
            }
        }
    }

}

data class TransactionHistory (
    val date: Date,
    val trx: String,
    val rcv: String,
    val id: String? = null
    )

class TransactionAdapter(theContext: Context, transactionHistories: List<TransactionHistory>)
    : ArrayAdapter<TransactionHistory>(theContext,0, transactionHistories) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val transaction = getItem(position)?:return super.getView(position, convertView, parent)

        var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.partials_transaction_item, parent, false)
        val dateText = view.findViewById<TextView>(R.id.trx_item_date)
        val infoText = view.findViewById<TextView>(R.id.trx_item_info)
        val receiveText = view.findViewById<TextView>(R.id.trx_item_receive)

        val sdf = SimpleDateFormat("d MMM")
        dateText.text = sdf.format(transaction.date)
        infoText.text = transaction.trx
        receiveText.text = transaction.rcv

        return view
    }
}

data class SavingSetting (
    val goldcoin: Boolean = false,
    val pgold: Boolean = false
)

data class GLCRate (
    var glcusd: Double = 0.0,
    var usdidr: Double = 0.0,
    var slippage: Double = 0.05
)

data class GoldRate (
    var goldprice: Double = 0.0,
    var availableIDR: Double = 0.0,
    var availableGrams: ArrayList<Double> = arrayListOf()
)