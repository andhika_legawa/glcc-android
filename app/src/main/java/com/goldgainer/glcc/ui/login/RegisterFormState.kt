package com.goldgainer.glcc.ui.login

data class RegisterFormState (val usernameError: Int? = null,
                              val emailError: Int? = null,
                              val sponsorError: Int? = null,
                              val trainerError: Int? = null,
                              val passwordError: Int? = null,
                              val retypePasswordError: Int? = null,
                              val isDataValid: Boolean = false)