package com.goldgainer.glcc.ui.network

import com.goldgainer.glcc.data.model.Network

data class NetworkResult(
        val success: Network? = null,
        val error: Int? = null
)