package com.goldgainer.glcc.ui.network

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

class NetworkRPCRedeemFragment : AuthenticatedFragment() {

    lateinit var viewModel: NetworkRPCViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_network_rpc_redeem, container, false)
        setUserHeader(root.findViewById(R.id.user_header))

        viewModel = ViewModelProvider(this).get(NetworkRPCViewModel::class.java)

        val loading = root.findViewById<ProgressBar>(R.id.progress_circular)
        viewModel.isBusy.observe(viewLifecycleOwner, {
            if (it) {
                loading.visibility = View.VISIBLE
            } else {
                loading.visibility = View.GONE
            }
        })
        viewModel.reset()

        viewModel.error.observe(viewLifecycleOwner, {
            if (it != null) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })

        val networkText = root.findViewById<TextView>(R.id.network_text)
        val pairText = root.findViewById<TextView>(R.id.pair_string)

        arguments?.let {
            val networkType  = it.getString("networkType", "I")
            val networkNumber = it.getInt("networkNumber", 0)
            val pairString = it.getString("pairString", "000:000")
            viewModel.pairString = pairString
            viewModel.checkRedeem(networkType, networkNumber, pairString)
        }

        viewModel.selectedNetworkCategory.observe(viewLifecycleOwner, {
            pairText.text = viewModel.pairString
            networkText.text = it.description
        })

        val statusText = root.findViewById<TextView>(R.id.redeem_status)
        val descriptionText = root.findViewById<TextView>(R.id.redeem_description_text)
        val instructionText = root.findViewById<TextView>(R.id.redeem_status_text)
        val redeemBtn = root.findViewById<Button>(R.id.redeem_btn)
        val backBtn = root.findViewById<Button>(R.id.back_btn)
        viewModel.currentRedeem.observe(viewLifecycleOwner, {
            if (it.status.isEmpty()) {
                statusText.visibility = View.GONE
            } else {
                statusText.visibility = View.VISIBLE
            }
            if (it.requestDate != null) {
                redeemBtn.visibility = View.GONE
                backBtn.visibility = View.VISIBLE
                statusText.text = it.status
                if (it.status == "PENDING") {
                    descriptionText.text = "Your Pair Connection Reward for ${it.pairString} is being reviewed and processed, please wait or contact nearest regional office for more information."
                    instructionText.visibility = View.GONE
                }
                if (it.status == "APPROVED") {
                    descriptionText.text = "Your Pair Connection Reward for ${it.pairString} is already approved, you can contact nearest regional office to receive your reward."
                    instructionText.visibility = View.VISIBLE
                }
                if (it.isRedeemed) {
                    descriptionText.text = "You have received your Pair Connection Reward for ${it.pairString} on : "
                    if (it.redeemDate != null) {
                        val sdf = SimpleDateFormat("dd/MM/yyyy - HH:mm:ss")
                        instructionText.text = sdf.format(it.redeemDate)
                    }
                }
            } else {
                redeemBtn.visibility = View.VISIBLE
                backBtn.visibility = View.GONE
            }
        })

        redeemBtn.setOnClickListener {
            MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
                .setTitle("Redeem Reward")
                .setMessage("Are you sure you want to redeem your reward?")
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    viewLifecycleOwner.lifecycleScope.launch {
                        val success = viewModel.redeemReward()
                        if (success) {
                            viewModel.checkRedeem()
                        }
                    }
                }
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .show()
        }

        backBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        return root
    }
}