package com.goldgainer.glcc.ui.withdraw

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.Helper
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch

class WalletFragment: AuthenticatedFragment() {

	private lateinit var withdrawViewModel: WithdrawViewModel
	private lateinit var disabledDialog: AlertDialog

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		withdrawViewModel = ViewModelProvider(this).get(WithdrawViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_wallet, container, false)
		setUserHeader(root.findViewById(R.id.withdraw_user_header))

		val idWallet = root.findViewById<ConstraintLayout>(R.id.id_wallet)

		val localUser = Helper.getLocalUser()
		if (localUser.country != "ID") {
			idWallet.visibility = View.GONE
		}

		val niSpinner = root.findViewById<Spinner>(R.id.n_i_spinner)

		val bsValue = root.findViewById<TextView>(R.id.bs_wallet_textview)
		val bpValue = root.findViewById<TextView>(R.id.bp_wallet_textview)
		val brValue = root.findViewById<TextView>(R.id.br_wallet_textview)

		val tbsValue = root.findViewById<TextView>(R.id.tbs_wallet_textview)
		val tbpValue = root.findViewById<TextView>(R.id.tbp_wallet_textview)
		val tbrValue = root.findViewById<TextView>(R.id.tbr_wallet_textview)

		val prtTitle = root.findViewById<TextView>(R.id.prt_text_title)
		val prtDesc = root.findViewById<TextView>(R.id.prt_text_desc)

		val goldValue = root.findViewById<TextView>(R.id.pgold_saving_textview)
		val coinValue = root.findViewById<TextView>(R.id.goldcoin_saving_textview)

		val bsWithdrawBtn = root.findViewById<Button>(R.id.bs_withdraw_btn)
		val bpWithdrawBtn = root.findViewById<Button>(R.id.bp_withdraw_btn)
		val brWithdrawBtn = root.findViewById<Button>(R.id.br_withdraw_btn)

		val goldWithdrawBtn = root.findViewById<Button>(R.id.gold_withdraw_btn)
		val glcWithdrawBtn = root.findViewById<Button>(R.id.glc_withdraw_btn)

		val transactionBtn = root.findViewById<MaterialButton>(R.id.transaction_tab)

		niSpinner.onItemSelectedListener = object : OnItemSelectedListener {
			override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
				withdrawViewModel.loadNetworkSnapshot(position)
			}
			override fun onNothingSelected(parentView: AdapterView<*>?) {
			}
		}

		transactionBtn.setOnClickListener {
			findNavController().navigate(R.id.action_nav_wallet_to_nav_transactions)
		}

		val networkAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, arrayListOf())
		networkAdapter.setDropDownViewResource(R.layout.partials_dropdown_item)
		niSpinner.adapter = networkAdapter

		withdrawViewModel.networkCategories.observe(viewLifecycleOwner, Observer { categories ->
			if (categories.isNotEmpty()) {
				networkAdapter.clear()
				categories.forEach {
					networkAdapter.add(it.description)
				}
				networkAdapter.notifyDataSetChanged()
				niSpinner.setSelection(0)
			}
		})

		tbsValue.visibility = View.GONE
		tbpValue.visibility = View.GONE
		tbrValue.visibility = View.GONE

		prtTitle.visibility = View.GONE
		prtDesc.visibility = View.GONE

		withdrawViewModel.currentWallet.observe(viewLifecycleOwner, Observer { wallet ->
			tbsValue.visibility = View.GONE
			tbpValue.visibility = View.GONE
			tbrValue.visibility = View.GONE

			prtTitle.visibility = View.GONE
			prtDesc.visibility = View.GONE

			bsValue.text = wallet.bonusSponsor.toCurrency("IDR")
			bpValue.text = wallet.bonusPairing.toCurrency("IDR")
			brValue.text = wallet.bonusRollup.toCurrency("IDR")
			tbsValue.text = "- " + wallet.tBonusSponsor.toCurrency("IDR") + " *"
			tbpValue.text = "- " + wallet.tBonusPairing.toCurrency("IDR") + " *"
			tbrValue.text = "- " + wallet.tBonusRollup.toCurrency("IDR") + " *"
			goldValue.text = wallet.savingGold.toCurrency("IDR")
			coinValue.text = wallet.savingGoldcoin.toCurrency("IDR")

			if (wallet.tBonusSponsor > 0) {
				tbsValue.visibility = View.VISIBLE
				bsValue.text = "IDR0"
			}
			if (wallet.tBonusPairing > 0) {
				tbpValue.visibility = View.VISIBLE
				bpValue.text = "IDR0"
			}
			if (wallet.tBonusRollup > 0) {
				tbrValue.visibility = View.VISIBLE
				brValue.text = "IDR0"
			}
			if (wallet.tBonusSponsor > 0 ||
				wallet.tBonusPairing > 0 ||
				wallet.tBonusRollup > 0) {
				prtTitle.visibility = View.VISIBLE
				prtDesc.visibility = View.VISIBLE
			}

			bsWithdrawBtn.setOnClickListener {
				if (wallet.bonusSponsor > 0) {
					withdrawViewModel.makeRequest("IDR", "BS", wallet.bonusSponsor)
				}
			}

			bpWithdrawBtn.setOnClickListener {
				if (wallet.bonusPairing > 0) {
					withdrawViewModel.makeRequest("IDR", "BP", wallet.bonusPairing)
				}
			}

			brWithdrawBtn.setOnClickListener {
				if (wallet.bonusRollup > 0) {
					withdrawViewModel.makeRequest("IDR", "BR", wallet.bonusRollup)
				}
			}

			glcWithdrawBtn.setOnClickListener {
				if (wallet.savingGoldcoin > 0) {
					withdrawViewModel.makeRequest("IDR", "GC", wallet.savingGoldcoin)
				}
			}

			goldWithdrawBtn.setOnClickListener {
				if (wallet.savingGold > 0) {
					withdrawViewModel.makeRequest("IDR", "PG", wallet.savingGold)
				}
			}
		})

		disabledDialog = MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
				.setTitle("Withdraw disabled")
				.setMessage("This feature is temporary disabled. Please check again later.")
				.setPositiveButton(android.R.string.ok) { dialog: DialogInterface, _: Int ->
					findNavController().navigate(R.id.action_global_nav_dashboard)
				}
				.setCancelable(false)
				.create()

		withdrawViewModel.status.observe(viewLifecycleOwner, Observer { status ->
			if (status == "FORBIDDEN") {
				findNavController().navigate(R.id.action_global_nav_wallet_forbidden)
			}

			if (status == "DISABLED") {
				if (!disabledDialog.isShowing) {
					disabledDialog.show()
				}
			}

			if (status == "PENDING") {
				withdrawViewModel.currentRequest?.let {
					if (it.subtype == "BS" || it.subtype == "BP" || it.subtype == "BR") {
						findNavController().navigate(R.id.action_nav_wallet_to_nav_withdraw)
					}
					if (it.subtype == "GC") {
						findNavController().navigate(R.id.action_nav_wallet_to_nav_withdraw_glc)
					}
					if (it.subtype == "PG") {
						findNavController().navigate(R.id.action_nav_wallet_to_nav_withdraw_gold)
					}
				}
			}

			if (status == "SUBMITTED") {
				findNavController().navigate(R.id.action_nav_wallet_to_nav_withdraw_status)
			}
		})

		withdrawViewModel.error.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		withdrawViewModel.loadNetworks()

		viewLifecycleOwner.lifecycleScope.launch {
			val setting = withdrawViewModel.loadSavingSettings()
			if (setting.goldcoin) {
				glcWithdrawBtn.alpha = 1.0f
				glcWithdrawBtn.isEnabled = true
			}
			if (setting.pgold) {
				goldWithdrawBtn.alpha = 1.0f
				goldWithdrawBtn.isEnabled = true
			}
		}

		return root
	}

	override fun onResume() {
		super.onResume()
		withdrawViewModel.checkWithdrawStatus()
	}
}