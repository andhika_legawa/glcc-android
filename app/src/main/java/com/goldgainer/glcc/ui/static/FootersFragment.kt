package com.goldgainer.glcc.ui.static

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R

class FootersFragment : Fragment() {
	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		val root = inflater.inflate(R.layout.partials_disclaimer, container, false)

		val privacyLink = root.findViewById<TextView>(R.id.footer_privacy)
		privacyLink.setOnClickListener {
			findNavController().navigate(R.id.action_global_nav_privacy)
		}
		val termsLink = root.findViewById<TextView>(R.id.footer_terms)
		termsLink.setOnClickListener {
			findNavController().navigate(R.id.action_global_nav_terms)
		}

		return root
	}
}