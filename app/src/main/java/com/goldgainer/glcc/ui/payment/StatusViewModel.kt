package com.goldgainer.glcc.ui.payment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.model.Transaction
import kotlinx.coroutines.launch
import java.lang.Exception

class StatusViewModel : ViewModel() {

    var transactionId : String? = null

    private val _transaction = MutableLiveData<Transaction>()
    val transaction : LiveData<Transaction> = _transaction

    private val _database = FirebaseDataSource()

    fun getTransaction(){
        viewModelScope.launch {
            try{
                transactionId?.let {
                    _transaction.value = _database.getTransaction(it) ?: throw Exception("No transaction found")
                }
            }catch(e: Exception) {
                Log.d("Transaction", e.message.toString())
            }
        }
    }
}