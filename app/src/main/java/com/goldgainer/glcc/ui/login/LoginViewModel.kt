package com.goldgainer.glcc.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.goldgainer.glcc.data.LoginRepository
import com.goldgainer.glcc.data.Result

import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.FirebaseDataSource
import com.goldgainer.glcc.data.Helper
import kotlinx.coroutines.launch

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    private val _sendingResult = MutableLiveData<SendingResult>()
    val sendResult: LiveData<SendingResult> = _sendingResult

    private val _latestVersion = MutableLiveData<Int>()
    val latestVersion: LiveData<Int> = _latestVersion

    fun login(username: String, password: String) {
        viewModelScope.launch {
            val result = loginRepository.login(username, password)
            if (result is Result.Success) {
                result.data?.let{
                    if (!it.emailVerified) {
                        _loginResult.value = LoginResult(error = R.string.verify_email)
                        return@launch
                    }
                    if(Helper().isBlacklisted(it.email!!)){
                        _loginResult.value = LoginResult(error = R.string.account_banned)
                        return@launch
                    }
                    //reload userdata
                    val userData = FirebaseDataSource().getUserDetail(it.email!!)
                    if (userData!=null) {
                        Helper.setLocalUser(it.email!!, userData)
                        _loginResult.value = LoginResult(success = LoggedInUserView(it))
                    }else{
                        _loginResult.value = LoginResult(error = R.string.userdata_failed)
                    }
                }
            }
            if (result is Result.Error) {
                result.exception?.let {
                    _loginResult.value = LoginResult(error = R.string.login_failed, errorMessage = it.message)
                }
            }
        }
    }

    fun logout(){
        loginRepository.logout()

        //reset form
        _loginForm.value = LoginFormState()
    }

    fun resetPassword(email: String){
        viewModelScope.launch {
            val result = loginRepository.resetPassword(email)
            if (result is Result.Success) {
                _sendingResult.value = SendingResult(success = R.string.reset_success)
            }else{
                _sendingResult.value = SendingResult(error = R.string.reset_failed)
            }
        }
    }

    fun resendVerification(){
        viewModelScope.launch {
            val result = loginRepository.resendVerification()
            if (result is Result.Success) {
                _sendingResult.value = SendingResult(success = R.string.reverify_success)
            }else{
                _sendingResult.value = SendingResult(error = R.string.reverify_failed)
            }
        }
    }

    fun loginDataChanged(email: String, password: String) {
        if (!isUserNameValid(email)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_email)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    fun forgotDataChanged(email: String) {
        if (!isUserNameValid(email)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_email)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    fun checkVersion(){
        viewModelScope.launch {
            val helper = Helper()
            _latestVersion.value = helper.getGlobalSettings("latest_version", 1) as Int
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(username).matches() && username.isNotBlank()
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}