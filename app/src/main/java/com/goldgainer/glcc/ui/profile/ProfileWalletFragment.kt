package com.goldgainer.glcc.ui.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.ui.AuthenticatedFragment


class ProfileWalletFragment : AuthenticatedFragment(){

	private lateinit var profileViewModel: ProfileViewModel

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_profile_wallet, container, false)
		setUserHeader(root.findViewById(R.id.profile_user_header))

		loggedInUser?.let {
			profileViewModel.currentOwner = it.email!!
		}

		val txtGlcLink = root.findViewById<TextView>(R.id.txt_download_glc)
		val glcwallet = root.findViewById<EditText>(R.id.edit_glc_wallet)
		val usdtwallet = root.findViewById<EditText>(R.id.edit_usdt_wallet)

		val confirm = root.findViewById<TextView>(R.id.wallet_text_confirm)
		val save = root.findViewById<Button>(R.id.btn_save_wallet)

		txtGlcLink.setOnClickListener {
			val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=de.schildbach.wallet.goldcoin"))
			startActivity(browserIntent)
		}

		profileViewModel.currentProfile.observe(viewLifecycleOwner, Observer { profile ->
			profile.glcWallet?.let {
				glcwallet.setText(it)
			}
			profile.usdtWallet?.let {
				usdtwallet.setText(it)
			}
			save.isEnabled = true
			if (profile.status == "PA" || profile.status == "AV") {
				glcwallet.isEnabled = false
				usdtwallet.isEnabled = false
				save.isEnabled = false
				confirm.visibility = View.INVISIBLE
				save.visibility = View.INVISIBLE
			}
		})

		profileViewModel.profileError.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		profileViewModel.getProfile()

		profileViewModel.profileSaved.observe(viewLifecycleOwner, Observer { success ->
			if (success) {
				findNavController().popBackStack()
			}
		})

		save.setOnClickListener {
			profileViewModel.saveWallet(
				glcwallet = glcwallet.text.toString(),
				usdtwallet = usdtwallet.text.toString()
			)
		}

		return root
	}

}