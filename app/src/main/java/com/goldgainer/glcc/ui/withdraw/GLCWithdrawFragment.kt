package com.goldgainer.glcc.ui.withdraw

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.goldgainer.glcc.R
import com.goldgainer.glcc.data.afterTextChanged
import com.goldgainer.glcc.data.toCurrency
import com.goldgainer.glcc.ui.AuthenticatedFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class GLCWithdrawFragment: AuthenticatedFragment() {

	private lateinit var withdrawViewModel: WithdrawViewModel
	private lateinit var disabledDialog: AlertDialog
	private lateinit var refreshProgress: ProgressBar

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		super.onCreateView(inflater, container, savedInstanceState)
		withdrawViewModel = ViewModelProvider(this).get(WithdrawViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_withdraw_glc, container, false)
		setUserHeader(root.findViewById(R.id.withdraw_user_header))

		val networkDescription = root.findViewById<TextView>(R.id.network_textview)
		val availableReward = root.findViewById<TextView>(R.id.quantity_textview)
		val amountEdit = root.findViewById<EditText>(R.id.amount_edit)
		val minDraw = root.findViewById<TextView>(R.id.min_draw_textview)
		val maxDraw = root.findViewById<TextView>(R.id.max_draw_textview)

		val transferFee = root.findViewById<TextView>(R.id.transferfee_textview)
		val adminFee = root.findViewById<TextView>(R.id.adminfee_textview)
		val received = root.findViewById<TextView>(R.id.received_textview)
		val wallet = root.findViewById<TextView>(R.id.address_textview)

		val pinEdit = root.findViewById<EditText>(R.id.pin_edit)

		val refreshBtn = root.findViewById<Button>(R.id.refresh_btn)
		refreshProgress = root.findViewById(R.id.progress_circular)
		val sendPIN = root.findViewById<Button>(R.id.sendpin_btn)
		val proceedBtn = root.findViewById<Button>(R.id.proceed_btn)
		val cancelBtn = root.findViewById<Button>(R.id.cancel_btn)

		refreshProgress.visibility = View.GONE

		sendPIN.setOnClickListener {
			withdrawViewModel.sendPIN()
		}

		proceedBtn.isEnabled = false
		proceedBtn.setOnClickListener {
			MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
					.setTitle("Request Withdraw")
					.setMessage("Are you sure you want to withdraw?")
					.setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
						val pin = pinEdit.text.toString()
						withdrawViewModel.submitWithdrawal(pin)
					}
					.setNegativeButton(android.R.string.cancel, null)
					.setCancelable(false)
					.show()
		}

		cancelBtn.setOnClickListener {
			withdrawViewModel.cancelRequest()
		}

		disabledDialog = MaterialAlertDialogBuilder(context, R.style.Theme_GLCC_Dialog_Alert)
				.setTitle("Withdraw disabled")
				.setMessage("This feature is temporary disabled. Please check again later.")
				.setPositiveButton(android.R.string.ok) { dialog: DialogInterface, _: Int ->
					findNavController().navigate(R.id.action_global_nav_dashboard)
				}
				.setCancelable(false)
				.create()

		amountEdit.isEnabled = false
		withdrawViewModel.status.observe(viewLifecycleOwner, Observer { status ->
			if (status == "FORBIDDEN") {
				findNavController().navigate(R.id.action_global_nav_wallet_forbidden)
			}

			if (status == "DISABLED") {
				if (!disabledDialog.isShowing) {
					disabledDialog.show()
				}
			}

			if (status == "CANCELED") {
				findNavController().navigate(R.id.action_global_nav_wallet)
			}

			if (status == "SUBMITTED") {
				findNavController().navigate(R.id.action_nav_withdraw_glc_to_nav_withdraw_status)
			}

			if (status == "PENDING") {
				amountEdit.isEnabled = true
				val request = withdrawViewModel.currentRequest?:return@Observer
				networkDescription.text = request.networkCategory
				availableReward.text = request.available.toCurrency("IDR")
				amountEdit.setText(request.amount.toString())
				transferFee.text = request.transferFee.toCurrency("IDR")
				adminFee.text = request.adminFee.toCurrency("IDR")
				wallet.text = request.glcWallet
				withdrawViewModel.updateGLCRate()
			}
		})

		withdrawViewModel.checked.observe(viewLifecycleOwner, Observer { checked ->
			val request = withdrawViewModel.currentRequest?:return@Observer
			if (checked.amountError != null) {
				amountEdit.error = checked.amountError
				received.text = "0.0"
				proceedBtn.isEnabled = false
			}else{
				amountEdit.error = null
				received.text = request.savingGoldcoin.toString()
				proceedBtn.isEnabled = true
			}
			minDraw.text = checked.minWithdraw
			maxDraw.text = checked.maxWithdraw
			if (request.amount <= 0) {
				proceedBtn.isEnabled = false
			}
			networkDescription.text = request.networkCategory
			availableReward.text = request.available.toCurrency("IDR")
			transferFee.text = request.transferFee.toCurrency("IDR")
			adminFee.text = request.adminFee.toCurrency("IDR")
		})

		withdrawViewModel.error.observe(viewLifecycleOwner, Observer {
			Toast.makeText(context, it, Toast.LENGTH_LONG).show()
		})

		amountEdit.afterTextChanged {
			try {
				withdrawViewModel.updateGLCRate()
			}catch (e:Exception) {
				//
			}
		}

		val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
			override fun handleOnBackPressed() {
				withdrawViewModel.cancelRequest()
			}
		}
		requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback);

		refreshBtn.setOnClickListener {
			withdrawViewModel.updateGLCRate()
		}

		withdrawViewModel.glcRate.observe(viewLifecycleOwner, Observer { glcRate ->
			try {
				val withd = amountEdit.text.toString().toDouble()
				val glcidr = glcRate.glcusd * glcRate.usdidr
				val glcest = withd/glcidr
				val glcest8 = String.format("%.8f", glcest).toDouble()
				val glcestfin = glcest8 - (glcest8 * glcRate.slippage)
				val glcestfin8 = String.format("%.8f", glcestfin).toDouble()
				withdrawViewModel.prepareWithdrawal(withd, glcestfin8)
			} catch (e:Exception) {
//				Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
			}
		})

		return root
	}

	override fun onResume() {
		super.onResume()
		withdrawViewModel.checkWithdrawStatus()
	}
}